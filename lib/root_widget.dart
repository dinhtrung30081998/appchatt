import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'application/stores/auth/auth_store.dart';
import 'application/stores/search_friends/search_friends_store.dart';
import 'application/stores/user_info/user_info_store.dart';
import 'infrastructure/core/di/infrastructure_injection.dart';
import 'presentation/core/resources/themes.dart';
import 'presentation/core/routes/routes.dart';

class RootWidget extends StatelessWidget {
  RootWidget({Key? key}) : super(key: key);

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AuthStore>(create: (context) => injector<AuthStore>()),
        ChangeNotifierProvider<UserInfoStore>(create: (context) => injector<UserInfoStore>()),
        ChangeNotifierProvider<SearchFriendsStore>(create: (context) => injector<SearchFriendsStore>()),
        StreamProvider(create: (context) => context.read<AuthStore>().authStateChanges, initialData: null),
      ],
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: 'Chatt',
        theme: AppTheme.light(),
        darkTheme: AppTheme.dark(),
        themeMode: ThemeMode.dark,
        routerDelegate: _appRouter.delegate(),
        routeInformationParser: _appRouter.defaultRouteParser(),
      ),
    );
  }
}
