import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/auth/login/login_store.dart';
import '../../../../domain/value_objects/auth/email_address.dart';
import '../../../../domain/value_objects/auth/password.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/widgets/base_textformfield.dart';

class BodyLoginPage extends StatefulWidget {
  const BodyLoginPage({super.key});

  @override
  State<BodyLoginPage> createState() => _BodyLoginPageState();
}

class _BodyLoginPageState extends State<BodyLoginPage> with ResourceApp {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  final ValueNotifier<bool> hidePassword = ValueNotifier(true);
  final ValueNotifier<bool> hideSuffixIcon = ValueNotifier(false);

  late LoginStore loginStore;

  @override
  void initState() {
    super.initState();
    loginStore = Provider.of(context, listen: false);
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: SingleChildScrollView(
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _globalKey,
          child: ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Chatt',
                          style: TextStyle(fontWeight: FontWeight.w900, fontSize: 40, fontFamily: 'Lobster')),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Xin chào, chào mừng bạn quay trở lại',
                          style: TextStyle(fontWeight: FontWeight.w900, color: appColors.textFaded)),
                    ),
                    const SizedBox(height: 50),
                    _buildTextFieldEmail(),
                    const SizedBox(height: 25),
                    _buildTextFieldPassword(),
                    const SizedBox(height: 15),
                    _loginButton(),
                    _registerButton(),
                    const SizedBox(height: 25),
                    _buildOrText(),
                    const SizedBox(height: 25),
                    _buildLoginWithGoogleOrFacebook(),
                    const SizedBox(height: 180),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        'Quên mật khẩu',
                        style: TextStyle(fontWeight: FontWeight.w900, color: appColors.textLight),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Column _buildLoginWithGoogleOrFacebook() {
    return Column(
      children: [
        _googleButton(),
        const SizedBox(height: 5),
        // _phoneNumberButton(),
      ],
    );
  }

  Widget _googleButton() {
    return Container(
      width: 300,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.white),
          overlayColor: MaterialStateProperty.all(appColors.cardDark.withOpacity(0.5)),
        ),
        onPressed: () => loginStore.signInWithGoogle(context: context),
        child: Wrap(
          alignment: WrapAlignment.center,
          children: [
            Consumer<LoginStore>(
              builder: (_, loginStore, __) {
                if (loginStore.googleAthState == const GoogleAuthState.loading()) {
                  return CupertinoActivityIndicator(color: appColors.cupertinoIndicatorBlack);
                }
                return SvgPicture.asset(
                  'assets/icons/icon_google.svg',
                  width: 22,
                  height: 22,
                );
              },
            ),
            const SizedBox(width: 15),
            Text(
              'Đăng nhập bằng Google',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: appColors.textDark),
            ),
          ],
        ),
      ),
    );
  }

  Row _buildOrText() {
    return Row(
      children: [
        const Expanded(child: Divider(height: 1, thickness: 0.1, indent: 50, endIndent: 12)),
        Text('Hoặc', style: TextStyle(fontWeight: FontWeight.w900, color: appColors.textFaded)),
        const Expanded(child: Divider(height: 1, thickness: 0.1, indent: 12, endIndent: 50)),
      ],
    );
  }

  BaseTextFormField _buildTextFieldEmail() {
    return BaseTextFormField(
      controller: _emailController,
      label: 'Email',
      textInputType: TextInputType.emailAddress,
      prefixIcon: const Icon(CupertinoIcons.mail_solid, size: 18),
      onChange: (value) => loginStore.emailChanged(value),
      validator: (_) {
        return loginStore.email.value.fold<String?>(
          (l) => l.maybeWhen(
            invalidEmail: (_) => 'Email không hợp lệ',
            orElse: () => null,
          ),
          (r) => null,
        );
      },
    );
  }

  ValueListenableBuilder<bool> _buildTextFieldPassword() {
    return ValueListenableBuilder(
      builder: (BuildContext c, bool showPassword, _) {
        return BaseTextFormField(
          controller: _passwordController,
          label: 'Mật khẩu',
          isPassword: showPassword,
          textInputType: TextInputType.visiblePassword,
          textInputAction: TextInputAction.done,
          prefixIcon: const Icon(Icons.key_sharp, size: 18),
          suffixIcon: _suffixIconPassword(showPassword),
          onChange: (passwordStr) {
            if (passwordStr.isEmpty) {
              hideSuffixIcon.value = false;
              return;
            }
            hideSuffixIcon.value = true;
            loginStore.passwordChanged(passwordStr);
          },
          validator: (_) {
            return loginStore.password.value.fold<String?>(
              (l) => l.maybeWhen(
                shortPassword: (_) => 'Mật khẩu phải nhiều hơn hoặc bằng 6 ký tự',
                orElse: () => null,
              ),
              (r) => null,
            );
          },
          onEditingComplete: () {
            executeLogin();
          },
        );
      },
      valueListenable: hidePassword,
    );
  }

  ValueListenableBuilder _suffixIconPassword(bool value) {
    return ValueListenableBuilder(
      valueListenable: hideSuffixIcon,
      builder: (c, showSuffixIcon, _) {
        return showSuffixIcon
            ? IconButton(
                onPressed: () {
                  hidePassword.value = !value;
                },
                icon: value ? const Icon(CupertinoIcons.eye_slash, size: 18) : const Icon(CupertinoIcons.eye, size: 18),
              )
            : const SizedBox();
      },
    );
  }

  Widget _loginButton() {
    return Container(
      width: 300,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
      child: Consumer<LoginStore>(
        builder: (_, loginStore, child) {
          final disableButton = loginStore.loginState == const LoginState.loading();
          return ElevatedButton(
            onPressed: () => disableButton ? null : executeLogin(),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(appColors.secondary),
              overlayColor: MaterialStateProperty.all(appColors.cardDark.withOpacity(0.5)),
            ),
            child: Builder(
              builder: (_) {
                if (loginStore.loginState == const LoginState.loading()) {
                  return CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite);
                }
                return const Text('ĐĂNG NHẬP', style: TextStyle(fontWeight: FontWeight.w900));
              },
            ),
          );
        },
      ),
    );
  }

  Widget _registerButton() {
    return Container(
      width: 300,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
      child: ElevatedButton(
        onPressed: () => context.router.pushNamed('/register-stepper-page'),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(appColors.textFaded),
          overlayColor: MaterialStateProperty.all(appColors.cardDark.withOpacity(0.5)),
        ),
        child: const Text('TẠO TÀI KHOẢN', style: TextStyle(fontWeight: FontWeight.w900)),
      ),
    );
  }

  void executeLogin() {
    loginStore.signInWithEmailAndPassword(
      email: EmailAddress(_emailController.text),
      password: Password(_passwordController.text),
      context: context,
    );
    FocusScope.of(context).unfocus();
  }
}
