import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/auth/login/login_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/mixins/resources.dart';
import '../../core/widgets/base_appbar.dart';
import 'widgets/body_login_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: ChangeNotifierProvider(
        create: (_) => injector<LoginStore>(),
        child: Builder(
          builder: (c) {
            return const BodyLoginPage();
          },
        ),
      ),
    );
  }

  BaseAppBar _appBar() {
    return BaseAppBar();
  }
}
