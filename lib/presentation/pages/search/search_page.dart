import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/search_friends/search_friends_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/mixins/resources.dart';
import '../../core/widgets/base_leading_appbar.dart';
import '../../core/widgets/base_title_appbar.dart';
import '../user_info/user_info_page.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> with ResourceApp, SingleTickerProviderStateMixin {
  final ValueNotifier<bool> _isToggle = ValueNotifier(true);
  final ValueNotifier<String> uidChanged = ValueNotifier('');
  final ValueNotifier<bool> showUserInfoScreen = ValueNotifier(false);

  final TextEditingController _searchController = TextEditingController();

  late SearchFriendsStore searchFriendsStore;
  late Future listPost;
  @override
  void initState() {
    super.initState();
    searchFriendsStore = Provider.of(context, listen: false);
    listPost = injector<FirebaseFirestore>().collection('posts').get();
  }

  @override
  void dispose() {
    super.dispose();
    _isToggle.dispose();
    _searchController.dispose();
    searchFriendsStore.searchs.clear();
    showUserInfoScreen.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: showUserInfoScreen,
      builder: (c, show, _) {
        return GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Scaffold(
            appBar: show ? _appBar() : _search(),
            body: ValueListenableBuilder(
              valueListenable: _isToggle,
              builder: (_, value, __) {
                return show
                    ? UserInfoPage(
                        uid: uidChanged.value,
                        showAppbar: false,
                      )
                    : value
                        ? FutureBuilder(
                            future: listPost,
                            builder: (context, snapshot) {
                              if (snapshot.connectionState == ConnectionState.waiting) {
                                return Center(
                                    child: CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite));
                              } else if (snapshot.connectionState == ConnectionState.done) {
                                if (snapshot.hasData) {
                                  final currentData = snapshot.data!.docs;

                                  return GridView.builder(
                                    itemCount: currentData.length,
                                    gridDelegate: SliverQuiltedGridDelegate(
                                      crossAxisCount: 3,
                                      mainAxisSpacing: 8,
                                      crossAxisSpacing: 8,
                                      repeatPattern: QuiltedGridRepeatPattern.inverted,
                                      pattern: const [
                                        QuiltedGridTile(2, 2),
                                        QuiltedGridTile(1, 1),
                                        QuiltedGridTile(1, 1),
                                      ],
                                    ),
                                    itemBuilder: (BuildContext context, int index) {
                                      final compareUid =
                                          currentData[index]['uid'] == injector<FirebaseAuth>().currentUser!.uid;
                                      bool currentUser = compareUid;
                                      return !currentUser && currentData[index]['hidePost']
                                          ? Container()
                                          : Image.network(
                                              currentData[index]['postUrl'].first,
                                              fit: BoxFit.cover,
                                            );
                                    },
                                  );
                                }
                                return const Text('Không có dữ liệu');
                              } else {
                                return const Text('Mất kết nối internet, vui lòng kiểm tra đường truyền');
                              }
                            },
                          )
                        : _fieldSearch();
              },
            ),
          ),
        );
      },
    );
  }

  AppBar _appBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: _title(),
      leading: _leading(),
    );
  }

  Widget _title() {
    return const Padding(
      padding: EdgeInsets.only(left: 8.0),
      child: BaseTitleAppBar(
        title: 'Trang cá nhân',
      ),
    );
  }

  Widget _leading() {
    return BaseLeadingAppBar(
      onTap: () => showUserInfoScreen.value = false,
    );
  }

  AppBar _search() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: ValueListenableBuilder(
        builder: (c, isToggle, _) {
          return TextFormField(
            controller: _searchController,
            cursorColor: appColors.textLight,
            decoration: InputDecoration(
              hintText: 'Tìm kiếm...',
              floatingLabelStyle: TextStyle(color: appColors.secondary),
              prefixIcon: const Icon(Icons.search),
              prefixIconColor: appColors.secondary,
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: appColors.secondary),
              ),
            ),
            onFieldSubmitted: (searchStr) {
              if (isToggle) {
                _isToggle.value = false;
              }
              if (searchStr.isEmpty) {
                _isToggle.value = true;
              }
              searchFriendsStore.searchFriend(_searchController.text);
            },
          );
        },
        valueListenable: _isToggle,
      ),
    );
  }

  Consumer _fieldSearch() {
    return Consumer<SearchFriendsStore>(
      builder: (_, searchStore, __) {
        if (searchStore.searchFriendsState == const SearchFriendsState.loading()) {
          return const Center(child: CupertinoActivityIndicator());
        } else if (searchStore.searchFriendsState == const SearchFriendsState.searchError()) {
          return Center(child: Text(searchStore.errorMessage));
        }
        return searchStore.searchs.isEmpty
            ? Center(
                child: Text(
                  'Chưa có kết quả trùng khớp',
                  style: TextStyle(
                    color: appColors.textFaded,
                  ),
                ),
              )
            : ListView.builder(
                itemCount: searchStore.searchs.length,
                itemBuilder: (context, index) {
                  var friends = searchStore.searchs[index];

                  return InkWell(
                    onTap: () {
                      uidChanged.value = friends.uid.getValueOrCrash();
                      showUserInfoScreen.value = true;
                    },
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 30.0,
                        backgroundImage: NetworkImage(
                          friends.profilePicture,
                        ),
                        backgroundColor: Colors.transparent,
                      ),
                      title: Text(friends.fullName),
                      subtitle: Text(friends.phoneNumber),
                    ),
                  );
                },
              );
      },
    );
  }
}
