import 'package:app_chat/application/stores/user_info/user_info_store.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/auth/auth_store.dart';
import '../../core/mixins/resources.dart';
import '../login/login_page.dart';
import '../verify_email/verify_email_page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with ResourceApp {
  late Stream<User?> user;
  DateTime? currentBackPressTime;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3)).then((value) {
      FlutterNativeSplash.remove();
    });
    user = context.read<AuthStore>().authStateChanges;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => onWillPop(),
      child: Scaffold(
        body: StreamBuilder<User?>(
          stream: user,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return const VerifyEmailPage();
            }
            return const LoginPage();
          },
        ),
      ),
    );
  }

  Future<bool> onWillPop() async {
    UserInfoStore userInfoStore = Provider.of(context, listen: false);
    DateTime now = DateTime.now();
    if (currentBackPressTime == null || now.difference(currentBackPressTime!) > const Duration(seconds: 1)) {
      currentBackPressTime = now;
      await Fluttertoast.showToast(
        msg: 'Nhấn back lần nữa để thoát',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: appColors.secondary,
        textColor: appColors.textLight,
        fontSize: 12,
      );
      return Future.value(false);
    } else {
      await userInfoStore.setStatusUser('Offline');
      return Future.value(true);
    }
  }
}
