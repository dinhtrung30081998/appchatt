import '../../../../application/stores/post/post_store.dart';
import '../../../../domain/entities/comment/comment_entity.dart';
import '../../../../domain/entities/post/post_entity.dart';
import '../../../core/mixins/resources.dart';
import 'comment_card.dart';
import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/user_info/user_info_store.dart';
import '../../../core/widgets/base_appbar.dart';
import '../../../core/widgets/base_leading_appbar.dart';
import '../../../core/widgets/base_title_appbar.dart';

class BodyCommentsPage extends StatefulWidget {
  final PostEntity postEntity;
  const BodyCommentsPage({super.key, required this.postEntity});

  @override
  State<BodyCommentsPage> createState() => _BodyCommentsPageState();
}

class _BodyCommentsPageState extends State<BodyCommentsPage> with ResourceApp {
  final TextEditingController _commentController = TextEditingController();

  late UserInfoStore userInfoStore;
  late PostStore postStore;
  late Stream<List<CommentEntity>> streamListComments;

  @override
  void initState() {
    super.initState();
    userInfoStore = Provider.of(context, listen: false);
    postStore = Provider.of(context, listen: false);
    streamListComments = postStore.getStreamListCommentByPostId(widget.postEntity.postId);
  }

  @override
  void dispose() {
    _commentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: BaseAppBar(
          leading: _leading(),
          title: _title(),
        ),
        body: StreamBuilder<List<CommentEntity>>(
          stream: streamListComments,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CupertinoActivityIndicator(
                  color: appColors.cupertinoIndicatorBlack,
                ),
              );
            } else if (snapshot.connectionState == ConnectionState.active) {
              if (snapshot.hasData) {
                if (snapshot.data!.isNotEmpty) {
                  final lengthComments = snapshot.data!.length;
                  return ListView.builder(
                    itemCount: lengthComments,
                    itemBuilder: (context, index) {
                      final currentComment = snapshot.data![index];
                      return CommentCard(
                        commentEntity: currentComment,
                        postEntity: widget.postEntity,
                      );
                    },
                  );
                }
                return const Center(
                  child: Text('Không có bình luận nào'),
                );
              } else {
                return const Center(
                  child: Text('Không có dữ liệu'),
                );
              }
            } else {
              return const Center(
                child: Text('Mất kết nối internet, vui lòng kiểm tra đường truyền'),
              );
            }
          },
        ),
        bottomNavigationBar: _bottomNav(),
      ),
    );
  }

  Padding _bottomNav() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
      child: SafeArea(
        child: Container(
          height: 50,
          margin: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          padding: const EdgeInsets.only(left: 16, right: 8),
          child: Row(
            children: [
              CachedNetworkImage(
                width: 45,
                height: 45,
                imageUrl: userInfoStore.getProfilePicture()!,
                placeholder: (context, url) {
                  return CircularProgressIndicator(color: appColors.cupertinoIndicatorBlack);
                },
                imageBuilder: (_, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 8.0),
                  child: TextField(
                    controller: _commentController,
                    decoration: const InputDecoration(
                      hintText: 'Nhập bình luận ...',
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  postStore.commentPost(
                    widget.postEntity.postId,
                    _commentController.text,
                    userInfoStore.getUserId()!,
                    userInfoStore.getFullName()!,
                    userInfoStore.getProfilePicture()!,
                  );
                  FocusScope.of(context).unfocus();
                  _commentController.text = '';
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Text(
                    'Nhập',
                    style: TextStyle(
                      color: appColors.secondary,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _leading() {
    return BaseLeadingAppBar(
      onTap: () => context.router.pop(),
    );
  }

  Widget _title() {
    return const BaseTitleAppBar(title: 'Bình luận');
  }
}
