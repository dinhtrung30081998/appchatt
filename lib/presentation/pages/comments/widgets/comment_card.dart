import 'package:app_chat/application/stores/post/post_store.dart';
import 'package:app_chat/application/stores/user_info/user_info_store.dart';
import 'package:app_chat/domain/entities/post/post_entity.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../domain/entities/comment/comment_entity.dart';
import '../../../core/mixins/resources.dart';
import '../../post/widgets/like_button_animation.dart';

class CommentCard extends StatefulWidget {
  final CommentEntity commentEntity;
  final PostEntity postEntity;
  const CommentCard({super.key, required this.commentEntity, required this.postEntity});

  @override
  State<CommentCard> createState() => _CommentCardState();
}

class _CommentCardState extends State<CommentCard> with ResourceApp {
  final ValueNotifier<bool> isLikeAnAnimating = ValueNotifier(false);
  late PostStore postStore;

  @override
  void initState() {
    super.initState();
    postStore = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CachedNetworkImage(
            width: 45,
            height: 45,
            imageUrl: widget.commentEntity.profilePicture,
            placeholder: (context, url) {
              return CircularProgressIndicator(color: appColors.cupertinoIndicatorBlack);
            },
            imageBuilder: (_, imageProvider) => Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: widget.commentEntity.fullName,
                          style: const TextStyle(
                            fontFamily: 'Mulish',
                            fontSize: 11,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                        const WidgetSpan(
                          alignment: PlaceholderAlignment.baseline,
                          baseline: TextBaseline.alphabetic,
                          child: SizedBox(width: 3),
                        ),
                        TextSpan(
                          text: widget.commentEntity.comment,
                          style: const TextStyle(
                            fontFamily: 'Mulish',
                            fontSize: 11,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4),
                    child: Text(
                      widget.commentEntity.datePublished,
                      style: TextStyle(
                        fontSize: 11,
                        fontWeight: FontWeight.w400,
                        color: appColors.textFaded,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          ValueListenableBuilder(
            builder: (c, animationValue, _) {
              return LikeAnimation(
                isAnimating: widget.commentEntity.likes.contains(context.read<UserInfoStore>().getUserId()),
                smallLike: true,
                child: IconButton(
                  onPressed: () {
                    postStore.likeComment(widget.postEntity.postId, widget.commentEntity.commentId,
                        context.read<UserInfoStore>().getUserId()!, widget.commentEntity.likes);
                  },
                  icon: widget.commentEntity.likes.contains(context.read<UserInfoStore>().getUserId())
                      ? Icon(
                          Icons.favorite,
                          size: 20,
                          color: appColors.heartColor,
                        )
                      : Icon(
                          Icons.favorite_outline,
                          size: 20,
                          color: appColors.iconDark,
                        ),
                ),
              );
            },
            valueListenable: isLikeAnAnimating,
          ),
        ],
      ),
    );
  }
}
