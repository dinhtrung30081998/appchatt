import 'package:app_chat/domain/entities/post/post_entity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/post/post_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/mixins/resources.dart';
import 'widgets/body_comments_page.dart';

class CommentsPage extends StatefulWidget {
  final PostEntity postEntity;
  const CommentsPage({super.key, required this.postEntity});

  @override
  State<CommentsPage> createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => injector<PostStore>(),
      child: Builder(
        builder: (c) {
          return BodyCommentsPage(
            postEntity: widget.postEntity,
          );
        },
      ),
    );
  }
}
