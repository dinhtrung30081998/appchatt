import 'package:flutter/material.dart';

import '../../../../infrastructure/dtos/story/story_dto.dart';
import '../../../core/widgets/avatar.dart';

class StoryCard extends StatelessWidget {
  const StoryCard({
    Key? key,
    required this.storyDTO,
  }) : super(key: key);

  final StoryDTO storyDTO;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        AvatarWidget.medium(url: storyDTO.url),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              storyDTO.name,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 11,
                letterSpacing: 0.3,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
