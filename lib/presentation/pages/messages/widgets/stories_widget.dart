import '../../../core/mixins/helpers.dart';
import '../../../core/mixins/resources.dart';
import 'story_card.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';

import '../../../../infrastructure/dtos/story/story_dto.dart';

class StoriesWidget extends StatelessWidget with ResourceApp, Helpers {
  StoriesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(0),
      elevation: 0,
      color: Colors.transparent,
      child: SizedBox(
        height: 110,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: ListView.builder(
                padding: const EdgeInsets.only(top: 10.0),
                scrollDirection: Axis.horizontal,
                itemCount: 12,
                itemBuilder: (context, index) {
                  final faker = Faker();
                  return Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: SizedBox(
                      width: 60,
                      child: StoryCard(
                        storyDTO: StoryDTO(
                          name: faker.person.firstName(),
                          url: randomPictureUrl(),
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
