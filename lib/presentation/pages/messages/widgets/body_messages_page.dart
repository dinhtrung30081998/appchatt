import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/messages/messages_store.dart';
import '../../../../domain/entities/message/message_entity.dart';
import '../../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../../../infrastructure/dtos/message/message_dto.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/widgets/base_appbar.dart';
import '../../../core/widgets/base_title_appbar.dart';
import 'message_card.dart';

class BodyMessagesPage extends StatefulWidget {
  const BodyMessagesPage({super.key});

  @override
  State<BodyMessagesPage> createState() => _BodyMessagesPageState();
}

class _BodyMessagesPageState extends State<BodyMessagesPage> with ResourceApp, SingleTickerProviderStateMixin {
  final ValueNotifier<bool> _isToggle = ValueNotifier(true);
  final ValueNotifier<String> _searchText = ValueNotifier('');

  late AnimationController _animationController;
  final TextEditingController _searchController = TextEditingController();

  late MessagesStore messagesStore;
  late Stream<DatabaseEvent> streamChats;
  late String currentUser;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 375),
    );
    messagesStore = Provider.of(context, listen: false);
    streamChats = messagesStore.streamListMessages();
    currentUser = injector<FirebaseAuth>().currentUser!.uid;
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
    _searchText.dispose();
    _isToggle.dispose();
    _searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: StreamBuilder(
          stream: streamChats,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite));
            } else if (snapshot.connectionState == ConnectionState.active) {
              if (!snapshot.hasData ||
                  snapshot.data == null ||
                  snapshot.hasError ||
                  (snapshot.data!).snapshot.value == null) {
                return const Center(child: Text('Không có đoạn chat nào'));
              }

              List<MessageEntity> listMessage = [];

              final message = (snapshot.data!).snapshot.value as Map<dynamic, dynamic>;

              final myMessages = Map<dynamic, dynamic>.from(message); //typecasting

              myMessages.forEach((key, value) {
                final currentMessage = Map<String, dynamic>.from(value);

                if (currentMessage['users'].contains(currentUser)) {
                  MessageEntity messageEntity = MessageDTO.fromRTDB(currentMessage).toDomain();
                  listMessage.add(messageEntity);
                }
              });

              if (listMessage.isEmpty) {
                return const Center(
                  child: Text('Không có đoạn chat nào'),
                );
              }
              return CustomScrollView(
                slivers: [
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      childCount: listMessage.length,
                      (context, index) {
                        final messageEntity = listMessage[index];
                        return MessageCard(
                          messagesStore: messagesStore,
                          messageEntity: messageEntity,
                        );
                      },
                    ),
                  )
                ],
              );
            } else {
              return const Center(child: Text('Không có kết nối internet, vui lòng kiểm tra lại đường truyền'));
            }
          },
        ),
      ),
    );
  }

  ValueListenableBuilder<bool> _searchMessage() {
    return ValueListenableBuilder(
      builder: (_, valueToggle, __) {
        return Expanded(
          child: Container(
            margin: const EdgeInsets.only(left: 12),
            height: 40,
            width: valueToggle ? 48 : 340,
            alignment: const Alignment(-1, 0),
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 375),
              height: 50,
              width: valueToggle ? 40 : 340,
              curve: Curves.easeOut,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black26,
                    spreadRadius: -10,
                    blurRadius: 10,
                    offset: Offset(0, 10),
                  )
                ],
              ),
              child: Stack(
                children: [
                  AnimatedPositioned(
                    duration: const Duration(milliseconds: 375),
                    top: 5,
                    right: 4,
                    curve: Curves.easeOut,
                    child: AnimatedOpacity(
                      duration: const Duration(milliseconds: 200),
                      opacity: valueToggle ? 0.0 : 1.0,
                      child: Container(
                        padding: const EdgeInsets.all(3),
                        child: AnimatedBuilder(
                          builder: (context, child) {
                            return Transform.rotate(
                              angle: _animationController.value * 2 * pi,
                              child: child,
                            );
                          },
                          animation: _animationController,
                          child: Icon(
                            CupertinoIcons.bubble_left_bubble_right_fill,
                            color: appColors.cardLight,
                          ),
                        ),
                      ),
                    ),
                  ),
                  AnimatedPositioned(
                    left: valueToggle ? 20 : 40,
                    top: 4,
                    curve: Curves.easeOut,
                    duration: const Duration(milliseconds: 375),
                    child: AnimatedOpacity(
                      duration: const Duration(milliseconds: 200),
                      opacity: valueToggle ? 0 : 1,
                      child: SizedBox(
                        height: 35,
                        width: 300,
                        child: TextFormField(
                          controller: _searchController,
                          cursorWidth: 1.0,
                          showCursor: true,
                          cursorColor: appColors.secondary,
                          decoration: InputDecoration(
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            border: InputBorder.none,
                            labelStyle: TextStyle(
                              color: appColors.textFaded,
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                            ),
                            contentPadding: const EdgeInsets.only(
                              left: 20,
                              right: 40,
                            ),
                            labelText: 'Tìm tin nhắn',
                          ),
                          onChanged: (query) {
                            _searchText.value = query;
                          },
                          onEditingComplete: () => FocusScope.of(context).unfocus(),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 5,
                    left: 5,
                    child: GestureDetector(
                      onTap: () {
                        if (!valueToggle) {
                          _isToggle.value = true;
                          _animationController.forward();
                        } else {
                          _isToggle.value = false;
                          _animationController.reverse();
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.all(3),
                        child: Icon(
                          Icons.search,
                          color: appColors.iconLight,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
      valueListenable: _isToggle,
    );
  }

  BaseAppBar _appBar() {
    return BaseAppBar(
      elevation: 0,
      centerTitle: false,
      title: _title(),
    );
  }

  Widget _title() {
    return const Padding(
      padding: EdgeInsets.only(left: 8.0),
      child: BaseTitleAppBar(
        title: 'Tin nhắn',
      ),
    );
  }
}
