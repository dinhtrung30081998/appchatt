import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/messages/messages_store.dart';
import '../../../../application/stores/user_info/user_info_store.dart';
import '../../../../domain/entities/message/message_entity.dart';
import '../../../../domain/entities/user/user_entity.dart';
import '../../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../../../infrastructure/dtos/message/message_dto.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/routes/routes.dart';
import '../../../core/utils/vi_message.dart';
import '../../../core/widgets/avatar.dart';

class MessageCard extends StatefulWidget {
  final MessageEntity messageEntity;
  final MessagesStore messagesStore;
  const MessageCard({super.key, required this.messageEntity, required this.messagesStore});

  @override
  State<MessageCard> createState() => _MessageCardState();
}

class _MessageCardState extends State<MessageCard> with ResourceApp {
  late UserInfoStore userInfoStore;
  late Stream<UserEntity> streamUser;
  late String partnerUid;
  late String currentId;
  UserEntity? userEntity;

  @override
  void initState() {
    super.initState();
    userInfoStore = Provider.of(context, listen: false);
    currentId = injector<FirebaseAuth>().currentUser!.uid;

    for (var partnerUserId in widget.messageEntity.users) {
      if (currentId != partnerUserId) {
        streamUser = userInfoStore.getUserByUid(partnerUserId);
        partnerUid = partnerUserId;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        context.router.push(
          ChatPageRoute(
            user: userEntity,
          ),
        );
      },
      onLongPress: () {
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          isDismissible: true,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
          ),
          builder: (context) {
            return SizedBox(
              height: 100,
              child: ListView.builder(
                itemCount: 1,
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      widget.messagesStore.deleteMessage(widget.messageEntity.messageId);
                    },
                    leading: const Icon(Icons.delete),
                    title: const Text('Xoá'),
                  );
                },
              ),
            );
          },
        );
      },
      child: StreamBuilder<UserEntity>(
        stream: streamUser,
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            LastMessageEntity? lastMessageEntity;
            List<LastMessageEntity> listLastMessage = [];

            if (widget.messageEntity.users.contains(currentId) && widget.messageEntity.users.contains(partnerUid)) {
              if (listLastMessage.isEmpty) {
                if (widget.messageEntity.listLastMessages != null) {
                  widget.messageEntity.listLastMessages!.forEach((key, value) {
                    final currentLastMessage = Map<String, dynamic>.from(value);
                    lastMessageEntity = LastMessageDTO.fromJson(currentLastMessage).toDomain();
                    listLastMessage.add(lastMessageEntity!);
                  });
                }
              }
            }

            final currentUser = snapshot.data!;
            userEntity = currentUser;

            final startWithsURL = widget.messageEntity.lastMessage.startsWith('https://');
            return Container(
              height: 80,
              margin: const EdgeInsets.symmetric(horizontal: 8),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Stack(
                        children: [
                          AvatarWidget.medium(url: currentUser.profilePicture),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: currentUser.status == 'Online'
                                ? Container(
                                    width: 10,
                                    height: 10,
                                    decoration: BoxDecoration(color: appColors.successColor, shape: BoxShape.circle),
                                  )
                                : Container(),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 6.0),
                            child: Text(
                              currentUser.fullName,
                              style: const TextStyle(
                                letterSpacing: 0.2,
                                wordSpacing: 1.2,
                                fontWeight: FontWeight.w800,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Flexible(
                            child: Text(
                              widget.messageEntity.lastSenderId == currentId
                                  ? startWithsURL
                                      ? "Bạn: Hình ảnh"
                                      : "Bạn: ${widget.messageEntity.lastMessage}"
                                  : startWithsURL
                                      ? "Hình ảnh"
                                      : widget.messageEntity.lastMessage,
                              style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                color: listLastMessage.isNotEmpty && lastMessageEntity!.sendById != currentId
                                    ? appColors.textLight
                                    : appColors.textFaded,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 6.0),
                            child: Text(
                              format(DateTime.parse(widget.messageEntity.lastTimeMessage), locale: 'vi'),
                              style: TextStyle(
                                letterSpacing: -0.2,
                                fontSize: 11,
                                fontWeight: FontWeight.w600,
                                color: appColors.textFaded,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          listLastMessage.isNotEmpty && lastMessageEntity!.sendById != currentId
                              ? Container(
                                  height: 18,
                                  width: 18,
                                  decoration: BoxDecoration(
                                    color: appColors.secondary,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Center(
                                    child: Text(
                                      "${listLastMessage.length}",
                                      style: TextStyle(
                                        color: appColors.textLight,
                                        fontSize: 10,
                                      ),
                                    ),
                                  ),
                                )
                              : Container()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
