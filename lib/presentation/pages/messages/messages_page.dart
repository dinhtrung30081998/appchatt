import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/messages/messages_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import 'widgets/body_messages_page.dart';

class MessagesPage extends StatefulWidget {
  const MessagesPage({super.key});

  @override
  State<MessagesPage> createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => injector<MessagesStore>(),
      builder: (context, child) {
        return Builder(
          builder: (c) {
            return const BodyMessagesPage();
          },
        );
      },
    );
  }
}
