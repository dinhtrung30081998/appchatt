import 'package:app_chat/application/stores/auth/login/verify_phone_number/verify_phone_number_store.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/mixins/resources.dart';
import '../../core/widgets/base_appbar.dart';
import '../../core/widgets/base_leading_appbar.dart';
import 'widgets/body_verify_otp.dart';

class VerifyOtpPage extends StatefulWidget {
  const VerifyOtpPage({super.key});

  @override
  State<VerifyOtpPage> createState() => _VerifyOtpPageState();
}

class _VerifyOtpPageState extends State<VerifyOtpPage> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: ChangeNotifierProvider(
        create: (_) => injector<VerifyPhoneNumberStore>(),
        child: Builder(
          builder: (context) {
            return const BodyVerifyOTPPage();
          },
        ),
      ),
    );
  }

  BaseAppBar _appBar() {
    return BaseAppBar(
      leading: _leading(),
    );
  }

  Widget _leading() {
    return BaseLeadingAppBar(
      onTap: () => context.router.pop(),
    );
  }
}
