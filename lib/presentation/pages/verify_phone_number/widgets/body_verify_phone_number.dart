import 'package:app_chat/domain/value_objects/auth/phone_number.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/auth/login/login_store.dart';
import '../../../../application/stores/auth/login/verify_phone_number/verify_phone_number_store.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/widgets/base_textformfield.dart';

class BodyVerifyPhoneNumberPage extends StatefulWidget {
  const BodyVerifyPhoneNumberPage({super.key});

  @override
  State<BodyVerifyPhoneNumberPage> createState() => _BodyVerifyPhoneNumberPageState();
}

class _BodyVerifyPhoneNumberPageState extends State<BodyVerifyPhoneNumberPage> with ResourceApp {
  final TextEditingController _phoneNumberController = TextEditingController();
  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  late VerifyPhoneNumberStore verifyPhoneNumberStore;

  @override
  void initState() {
    super.initState();
    verifyPhoneNumberStore = Provider.of(context, listen: false);
  }

  @override
  void dispose() {
    super.dispose();
    _phoneNumberController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: SingleChildScrollView(
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _globalKey,
          child: ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Chatt', style: TextStyle(fontWeight: FontWeight.w900, fontSize: 40)),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    const SizedBox(height: 25),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Nhập số điện thoại của bạn để bắt đầu xác thực',
                          style: TextStyle(fontWeight: FontWeight.w900, color: appColors.textFaded)),
                    ),
                    const SizedBox(height: 12),
                    _buildTextFieldPhoneNumber(),
                    const SizedBox(height: 25),
                    _sendCodeOTPButton(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  BaseTextFormField _buildTextFieldPhoneNumber() {
    return BaseTextFormField(
      controller: _phoneNumberController,
      label: 'Số điện thoại',
      textInputType: TextInputType.phone,
      prefixIcon: const Padding(
        padding: EdgeInsets.only(left: 12.0, top: 14.5),
        child: Text(
          '+84',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.lightBlue),
        ),
      ),
      onChange: (value) => verifyPhoneNumberStore.phoneNumberChanged(value),
      validator: (_) {
        return verifyPhoneNumberStore.phoneNumber.value.fold<String?>(
          (l) => l.maybeWhen(
            phoneNumberIsEmpty: (_) => 'Số điện thoại không được để trống',
            orElse: () => null,
          ),
          (r) => null,
        );
      },
    );
  }

  Widget _sendCodeOTPButton() {
    return Container(
      width: 300,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
      child: ElevatedButton(
        onPressed: () => execute(),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(appColors.accent),
          overlayColor: MaterialStateProperty.all(appColors.cardDark.withOpacity(0.5)),
        ),
        child: Consumer<VerifyPhoneNumberStore>(
          builder: (_, verifyStore, child) {
            if (verifyStore.verifyPhoneNumberState == const VerifyPhoneNumberAuthState.loading()) {
              return CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite);
            }
            return const Text('GỬI MÃ OTP', style: TextStyle(fontWeight: FontWeight.w900));
          },
        ),
      ),
    );
  }

  void execute() {
    verifyPhoneNumberStore.verifyPhoneNumber(
      context: context,
      phoneNumber: PhoneNumber(_phoneNumberController.text),
    );
    verifyPhoneNumberStore.setPhoneNumStr = _phoneNumberController.text;
  }
}
