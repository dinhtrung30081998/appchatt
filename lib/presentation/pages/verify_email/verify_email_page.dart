import 'dart:async';

import 'package:app_chat/application/stores/auth/auth_store.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/mixins/resources.dart';
import '../../core/utils/utils.dart';
import '../home/home_page.dart';

class VerifyEmailPage extends StatefulWidget {
  const VerifyEmailPage({super.key});

  @override
  State<VerifyEmailPage> createState() => _VerifyEmailPageState();
}

class _VerifyEmailPageState extends State<VerifyEmailPage> with ResourceApp {
  bool isEmailVerified = false;
  bool timerButton = false;

  Timer? timer;
  @override
  void initState() {
    super.initState();
    isEmailVerified = injector<FirebaseAuth>().currentUser!.emailVerified;

    if (!isEmailVerified) {
      sendVerificationEmail();
    }

    timer = Timer.periodic(
      const Duration(seconds: 2),
      (timer) => checkEmailVerified(),
    );
  }

  Future<void> sendVerificationEmail() async {
    try {
      final user = injector<FirebaseAuth>().currentUser!;
      await user.sendEmailVerification();
      setState(() => timerButton = false);
      await Future.delayed(const Duration(seconds: 5));
      setState(() => timerButton = true);
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future<void> checkEmailVerified() async {
    await injector<FirebaseAuth>().currentUser!.reload();
    setState(() {
      isEmailVerified = injector<FirebaseAuth>().currentUser!.emailVerified;
    });

    if (isEmailVerified) timer?.cancel();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isEmailVerified ? const HomePage() : _body(),
    );
  }

  Widget _body() {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 35.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Align(
                alignment: Alignment.centerLeft,
                child: Text('Chatt', style: TextStyle(fontWeight: FontWeight.w900, fontSize: 40)),
              ),
              const SizedBox(height: 20),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                    'Nhấn nút "'
                    "GỬI LẠI EMAIL"
                    '" để gửi lại yêu cầu xác thực email hoặc "'
                    " HUỶ "
                    '" để huỷ bỏ xác thực',
                    style: TextStyle(fontWeight: FontWeight.w900, color: appColors.textFaded)),
              ),
              const SizedBox(height: 20),
              _buttonResendEmail(),
              const SizedBox(height: 10),
              _buttonCancelResendEmail(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buttonResendEmail() {
    return Container(
      width: 300,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
      child: ElevatedButton(
        onPressed: () => timerButton ? sendVerificationEmail() : null,
        style: ButtonStyle(
          backgroundColor: timerButton
              ? MaterialStateProperty.all(appColors.secondary)
              : MaterialStateProperty.all(appColors.textFaded),
          overlayColor: MaterialStateProperty.all(appColors.cardDark.withOpacity(0.5)),
        ),
        child: const Text('GỬI LẠI EMAIL', style: TextStyle(fontWeight: FontWeight.w900)),
      ),
    );
  }

  Widget _buttonCancelResendEmail() {
    return Container(
      width: 300,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
      child: ElevatedButton(
        onPressed: () => context.read<AuthStore>().signOut('Offline'),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(appColors.textFaded),
          overlayColor: MaterialStateProperty.all(appColors.cardDark.withOpacity(0.5)),
        ),
        child: const Text('HUỶ', style: TextStyle(fontWeight: FontWeight.w900)),
      ),
    );
  }
}
