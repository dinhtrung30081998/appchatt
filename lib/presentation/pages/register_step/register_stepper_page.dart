
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/auth/register/register_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import 'widgets/body_register_stepper.dart';

class RegisterStepperPage extends StatefulWidget {
  const RegisterStepperPage({super.key});

  @override
  State<RegisterStepperPage> createState() => _RegisterStepperPageState();
}

class _RegisterStepperPageState extends State<RegisterStepperPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<RegisterStore>(
      create: (_) => injector<RegisterStore>(),
      child: Builder(
        builder: (c) {
          return const BodyRegisterStepper();
        },
      ),
    );
  }
}
