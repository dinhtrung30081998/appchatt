import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/auth/register/register_store.dart';
import '../../../../domain/value_objects/auth/confirm_password.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/widgets/base_appbar.dart';
import '../../../core/widgets/base_leading_appbar.dart';
import '../../../core/widgets/base_title_appbar.dart';
import 'step_one_page.dart';
import 'step_three_page.dart';
import 'step_two_page.dart';

class BodyRegisterStepper extends StatefulWidget {
  const BodyRegisterStepper({super.key});

  @override
  State<BodyRegisterStepper> createState() => _BodyRegisterStepperState();
}

class _BodyRegisterStepperState extends State<BodyRegisterStepper> with ResourceApp {
  final ValueNotifier<int> currentStep = ValueNotifier(0);
  late RegisterStore registerStore;

  @override
  void initState() {
    super.initState();
    registerStore = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: _appBar(),
        body: ValueListenableBuilder(
          builder: (c, step, _) {
            final isLastStep = step == 2;

            return Consumer<RegisterStore>(
              builder: (_, registerStoreConsumer, child) {
                final disableButton = registerStoreConsumer.registerState == const RegisterState.loading();
                return Stepper(
                  type: StepperType.horizontal,
                  steps: [
                    Step(
                      state: step > 0 ? StepState.complete : StepState.indexed,
                      isActive: step >= 0,
                      title: const Text(
                        'Tạo tài khoản',
                        style: TextStyle(fontSize: 11),
                      ),
                      content: StepOnePage(registerStore: registerStore),
                    ),
                    Step(
                      state: step > 1 ? StepState.complete : StepState.indexed,
                      isActive: step >= 1,
                      title: const Text(
                        'Thông tin',
                        style: TextStyle(fontSize: 11),
                      ),
                      content: StepTwoPage(registerStore: registerStore),
                    ),
                    Step(
                      state: step > 2 ? StepState.complete : StepState.indexed,
                      isActive: step >= 2,
                      title: const Text(
                        'Chụp ảnh',
                        style: TextStyle(fontSize: 11),
                      ),
                      content: StepThreePage(registerStore: registerStore),
                    ),
                  ],
                  currentStep: currentStep.value,
                  onStepTapped: (stepIndex) => currentStep.value = stepIndex,
                  onStepContinue: () {
                    if (isLastStep) {
                      disableButton ? null : executeRegister();
                    } else {
                      currentStep.value += 1;
                    }
                    FocusScope.of(context).unfocus();
                  },
                  onStepCancel: () {
                    if (step == 0) {
                      return;
                    }
                    currentStep.value -= 1;
                    FocusScope.of(context).unfocus();
                  },
                  controlsBuilder: (c, details) {
                    return Container(
                      margin: const EdgeInsets.only(top: 50),
                      child: Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              onPressed: details.onStepContinue,
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(appColors.secondary),
                                overlayColor: MaterialStateProperty.all(appColors.cardDark.withOpacity(0.5)),
                              ),
                              child: Builder(
                                builder: (_) {
                                  if (registerStoreConsumer.registerState == const RegisterState.loading()) {
                                    return CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite);
                                  }
                                  return Text(
                                    isLastStep ? 'ĐĂNG KÝ' : 'TIẾP TỤC',
                                    style: const TextStyle(fontWeight: FontWeight.w900),
                                  );
                                },
                              ),
                            ),
                          ),
                          const SizedBox(width: 12),
                          step != 0
                              ? Expanded(
                                  child: ElevatedButton(
                                    onPressed: details.onStepCancel,
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(appColors.cardDark),
                                      overlayColor: MaterialStateProperty.all(appColors.cardLight.withOpacity(0.5)),
                                    ),
                                    child: const Text(
                                      'QUAY LẠI',
                                      style: TextStyle(fontWeight: FontWeight.w900),
                                    ),
                                  ),
                                )
                              : Container()
                        ],
                      ),
                    );
                  },
                );
              },
            );
          },
          valueListenable: currentStep,
        ),
      ),
    );
  }

  void executeRegister() {
    registerStore.register(
      email: registerStore.email,
      password: registerStore.password,
      confirmPassword: ConfirmPassword(
        registerStore.confirmPassword.getValueOrCrash(),
        registerStore.password.getValueOrCrash(),
      ),
      fullName: registerStore.fullName,
      phoneNumber: registerStore.phoneNumber,
      c: context,
    );
  }

  BaseAppBar _appBar() {
    return BaseAppBar(
      leading: _leading(),
      title: _title(),
    );
  }

  Widget _title() {
    return const BaseTitleAppBar(
      title: 'Tạo tài khoản',
    );
  }

  Widget _leading() {
    return BaseLeadingAppBar(
      onTap: () => context.router.pop(),
    );
  }
}
