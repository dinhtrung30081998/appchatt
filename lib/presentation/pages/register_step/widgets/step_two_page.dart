import 'package:flutter/material.dart';

import '../../../../application/stores/auth/register/register_store.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/widgets/base_textformfield.dart';

class StepTwoPage extends StatefulWidget {
  final RegisterStore registerStore;

  const StepTwoPage({super.key, required this.registerStore});

  @override
  State<StepTwoPage> createState() => _StepTwoPageState();
}

class _StepTwoPageState extends State<StepTwoPage> with ResourceApp {
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();

  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  late FocusNode phoneNumberFocusNode;

  @override
  void initState() {
    super.initState();
    phoneNumberFocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    _fullNameController.dispose();
    _phoneNumberController.dispose();
    phoneNumberFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _globalKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildTextFieldFullName(),
                const SizedBox(height: 25),
                _buildTextFieldPhoneNumber(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  BaseTextFormField _buildTextFieldFullName() {
    return BaseTextFormField(
      controller: _fullNameController,
      label: 'Họ và tên',
      prefixIcon: const Icon(Icons.text_fields_outlined, size: 18),
      onChange: (value) => widget.registerStore.fullNameChanged(value),
      validator: (_) {
        return widget.registerStore.fullName.value.fold<String?>(
          (l) => l.maybeWhen(
            fullNameIsEmpty: (_) => 'Họ và tên không được để trống',
            orElse: () => null,
          ),
          (r) => null,
        );
      },
      onEditingComplete: () => phoneNumberFocusNode.requestFocus(),
    );
  }

  BaseTextFormField _buildTextFieldPhoneNumber() {
    return BaseTextFormField(
      focusNode: phoneNumberFocusNode,
      controller: _phoneNumberController,
      label: 'Số điện thoại',
      textInputType: TextInputType.phone,
      textInputAction: TextInputAction.done,
      prefixIcon: const Icon(Icons.phone_android_sharp, size: 18),
      onChange: (value) => widget.registerStore.phoneNumberChanged(value),
      validator: (_) {
        return widget.registerStore.phoneNumber.value.fold<String?>(
          (l) => l.maybeWhen(
            phoneNumberIsEmpty: (_) => 'Số điện thoại không được để trống',
            orElse: () => null,
          ),
          (r) => null,
        );
      },
      onEditingComplete: () {
        FocusScope.of(context).unfocus();
      },
    );
  }
}
