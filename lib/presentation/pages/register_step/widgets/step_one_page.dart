import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../application/stores/auth/register/register_store.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/widgets/base_textformfield.dart';

class StepOnePage extends StatefulWidget {
  final RegisterStore registerStore;

  const StepOnePage({super.key, required this.registerStore});

  @override
  State<StepOnePage> createState() => _StepOnePageState();
}

class _StepOnePageState extends State<StepOnePage> with ResourceApp {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();

  final GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  final ValueNotifier<bool> hidePassword = ValueNotifier(true);
  final ValueNotifier<bool> hideConfirmPassword = ValueNotifier(true);
  final ValueNotifier<bool> hideSuffixIconPassword = ValueNotifier(false);
  final ValueNotifier<bool> hideSuffixIconConfirmPassword = ValueNotifier(false);

  late FocusNode confirmPasswordFocusNode;

  @override
  void initState() {
    super.initState();
    confirmPasswordFocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();

    hidePassword.dispose();
    hideConfirmPassword.dispose();
    hideSuffixIconPassword.dispose();
    hideSuffixIconConfirmPassword.dispose();
    confirmPasswordFocusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _globalKey,
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildTextFieldEmail(),
                const SizedBox(height: 25),
                _buildTextFieldPassword(),
                const SizedBox(height: 25),
                _buildTextFieldConfirmPassword(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  BaseTextFormField _buildTextFieldEmail() {
    return BaseTextFormField(
      controller: _emailController,
      label: 'Email',
      textInputType: TextInputType.emailAddress,
      prefixIcon: const Icon(CupertinoIcons.mail_solid, size: 18),
      onChange: (value) => widget.registerStore.emailChanged(value),
      validator: (_) {
        return widget.registerStore.email.value.fold<String?>(
          (l) => l.maybeWhen(
            invalidEmail: (_) => 'Email không hợp lệ',
            orElse: () => null,
          ),
          (r) => null,
        );
      },
    );
  }

  ValueListenableBuilder<bool> _buildTextFieldPassword() {
    return ValueListenableBuilder(
      builder: (BuildContext c, bool showPassword, _) {
        return BaseTextFormField(
          controller: _passwordController,
          label: 'Mật khẩu',
          isPassword: showPassword,
          textInputType: TextInputType.visiblePassword,
          textInputAction: TextInputAction.next,
          prefixIcon: const Icon(Icons.key_sharp, size: 18),
          suffixIcon: _suffixIconPassword(showPassword),
          onEditingComplete: () => confirmPasswordFocusNode.requestFocus(),
          onChange: (passwordStr) {
            if (passwordStr.isEmpty) {
              hideSuffixIconPassword.value = false;
              return;
            }
            hideSuffixIconPassword.value = true;
            widget.registerStore.passwordChanged(passwordStr);
          },
          validator: (_) {
            return widget.registerStore.password.value.fold<String?>(
              (l) => l.maybeWhen(
                shortPassword: (_) => 'Mật khẩu phải nhiều hơn hoặc bằng 6 ký tự',
                orElse: () => null,
              ),
              (r) => null,
            );
          },
        );
      },
      valueListenable: hidePassword,
    );
  }

  ValueListenableBuilder _suffixIconPassword(bool value) {
    return ValueListenableBuilder(
      valueListenable: hideSuffixIconPassword,
      builder: (c, showSuffixIcon, _) {
        return showSuffixIcon
            ? IconButton(
                onPressed: () {
                  hidePassword.value = !value;
                },
                icon: value ? const Icon(CupertinoIcons.eye_slash, size: 18) : const Icon(CupertinoIcons.eye, size: 18),
              )
            : const SizedBox();
      },
    );
  }

  ValueListenableBuilder<bool> _buildTextFieldConfirmPassword() {
    return ValueListenableBuilder(
      builder: (BuildContext c, bool showPassword, _) {
        return BaseTextFormField(
          focusNode: confirmPasswordFocusNode,
          controller: _confirmPasswordController,
          label: 'Mật khẩu xác thực',
          isPassword: showPassword,
          textInputType: TextInputType.visiblePassword,
          textInputAction: TextInputAction.done,
          prefixIcon: const Icon(Icons.shield, size: 18),
          suffixIcon: _suffixIconConfirmPassword(showPassword),
          onChange: (passwordStr) {
            if (passwordStr.isEmpty) {
              hideSuffixIconConfirmPassword.value = false;
              return;
            }
            hideSuffixIconConfirmPassword.value = true;
            widget.registerStore.confirmPasswordChanged(passwordStr, _passwordController.text);
          },
          validator: (_) {
            return widget.registerStore.confirmPassword.value.fold<String?>(
              (l) => l.maybeWhen(
                confirmPasswordMustBeSameAsPassword: (_) => 'Mật khẩu xác thực phải giống mật khẩu',
                orElse: () => null,
              ),
              (r) => null,
            );
          },
        );
      },
      valueListenable: hideConfirmPassword,
    );
  }

  ValueListenableBuilder _suffixIconConfirmPassword(bool value) {
    return ValueListenableBuilder(
      valueListenable: hideSuffixIconConfirmPassword,
      builder: (c, showSuffixIcon, _) {
        return showSuffixIcon
            ? IconButton(
                onPressed: () {
                  hideConfirmPassword.value = !value;
                },
                icon: value ? const Icon(CupertinoIcons.eye_slash, size: 18) : const Icon(CupertinoIcons.eye, size: 18),
              )
            : const SizedBox();
      },
    );
  }
}
