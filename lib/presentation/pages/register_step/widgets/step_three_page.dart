import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/auth/register/register_store.dart';
import '../../../core/mixins/resources.dart';

class StepThreePage extends StatefulWidget {
  final RegisterStore registerStore;

  const StepThreePage({super.key, required this.registerStore});

  @override
  State<StepThreePage> createState() => _StepThreePageState();
}

class _StepThreePageState extends State<StepThreePage> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Consumer<RegisterStore>(
          builder: (c, uploadStore, _) {
            if (uploadStore.actionCameraState == const ActionCameraState.actionDone()) {
              return Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Nhấn nút "' "ĐĂNG KÝ" '" để hoàn tất',
                  style: TextStyle(fontWeight: FontWeight.w900, color: appColors.textFaded),
                ),
              );
            }
            return Align(
              alignment: Alignment.center,
              child: Text(
                'Hãy chụp một bức ảnh hoặc chọn ảnh từ thư viện của bạn để làm ảnh đại diện',
                style: TextStyle(fontWeight: FontWeight.w900, color: appColors.textFaded),
              ),
            );
          },
        ),
        const SizedBox(
          height: 12,
        ),
        Consumer<RegisterStore>(
          builder: (c, uploadStore, _) {
            if (uploadStore.actionCameraState == const ActionCameraState.actionDone()) {
              return const SizedBox();
            }
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () => widget.registerStore.actionCamera(),
                  icon: const Icon(Icons.camera_enhance),
                ),
                IconButton(
                  onPressed: () => widget.registerStore.actionGallery(),
                  icon: const Icon(FontAwesomeIcons.image),
                ),
              ],
            );
          },
        ),
        const SizedBox(
          height: 25,
        ),
        Consumer<RegisterStore>(
          builder: (c, uploadStore, _) {
            if (uploadStore.actionCameraState == const ActionCameraState.actionLoading()) {
              return CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite);
            } else if (uploadStore.actionCameraState == const ActionCameraState.actionDone()) {
              return Image.network(uploadStore.imagePath);
            }
            return const SizedBox();
          },
        ),
      ],
    );
  }
}
