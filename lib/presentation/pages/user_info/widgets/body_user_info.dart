import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/post/post_store.dart';
import '../../../../application/stores/user_info/user_info_store.dart';
import '../../../../domain/entities/user/user_entity.dart';
import '../../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/routes/routes.dart';
import '../../../core/widgets/avatar.dart';
import 'follow_button.dart';

class BodyUserInfo extends StatefulWidget {
  final String uid;
  const BodyUserInfo({super.key, required this.uid});

  @override
  State<BodyUserInfo> createState() => _BodyUserInfoState();
}

class _BodyUserInfoState extends State<BodyUserInfo> with ResourceApp {
  late UserInfoStore userInfoStore;
  late PostStore postStore;
  late Stream<UserEntity> streamUser;
  bool isFollowing = false;

  @override
  void initState() {
    super.initState();
    userInfoStore = Provider.of(context, listen: false);
    postStore = Provider.of(context, listen: false);
    streamUser = userInfoStore.getUserByUid(widget.uid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Widget _body() {
    final userId = injector<FirebaseAuth>().currentUser!.uid;
    return StreamBuilder<UserEntity>(
      stream: streamUser,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite));
        }
        final currentUser = snapshot.data!;
        isFollowing = currentUser.followers.contains(userId);
        return snapshot.hasData
            ? SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(child: AvatarWidget.large(url: currentUser.profilePicture)),
                      // Stack(
                      //   children: [
                      //     userId == widget.uid
                      //         ? Positioned(
                      //             bottom: -10,
                      //             right: 100,
                      //             child: IconButton(
                      //               onPressed: () {},
                      //               icon: Icon(FontAwesomeIcons.camera, color: appColors.iconLight),
                      //             ),
                      //           )
                      //         : Container(),
                      //   ],
                      // ),
                      const SizedBox(height: 12),
                      _title(currentUser),
                      const SizedBox(height: 5),
                      _bio(currentUser),
                      const SizedBox(height: 15),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              _buildStatColumn(currentUser.followers.length, 'Người theo dõi'),
                              _buildStatColumn(currentUser.posts.length, 'Bài viết'),
                              _buildStatColumn(currentUser.following.length, 'Đang theo dõi'),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              userId == widget.uid
                                  ? FollowButton(
                                      text: 'Chỉnh sửa trang cá nhân',
                                      onPressed: () => context.router.push(
                                        EditUserInfoPageRoute(userEntity: currentUser),
                                      ),
                                    )
                                  : isFollowing
                                      ? Wrap(
                                          children: [
                                            FollowButton(
                                              text: 'Huỷ theo dõi',
                                              width: 200,
                                              backgroundColor: appColors.cardLight,
                                              overlayColor: appColors.enabledBorderDark,
                                              textColor: appColors.textDark,
                                              borderColor: appColors.enabledBorderDark,
                                              onPressed: () {
                                                userInfoStore.followUser(userId, currentUser.uid.getValueOrCrash());
                                              },
                                            ),
                                            const SizedBox(width: 12),
                                            FollowButton(
                                              text: 'Nhắn tin',
                                              width: 90,
                                              backgroundColor: appColors.cardLight,
                                              overlayColor: appColors.enabledBorderDark,
                                              textColor: appColors.textDark,
                                              borderColor: appColors.enabledBorderDark,
                                              onPressed: () => context.router.push(
                                                ChatPageRoute(user: currentUser),
                                              ),
                                            ),
                                          ],
                                        )
                                      : Wrap(
                                          children: [
                                            FollowButton(
                                              text: 'Theo dõi',
                                              width: 200,
                                              backgroundColor: appColors.secondary,
                                              borderColor: appColors.secondary,
                                              onPressed: () {
                                                userInfoStore.followUser(userId, currentUser.uid.getValueOrCrash());
                                              },
                                            ),
                                            const SizedBox(width: 12),
                                            FollowButton(
                                              text: 'Nhắn tin',
                                              width: 90,
                                              backgroundColor: appColors.cardLight,
                                              overlayColor: appColors.enabledBorderDark,
                                              textColor: appColors.textDark,
                                              borderColor: appColors.enabledBorderDark,
                                              onPressed: () => context.router.push(
                                                ChatPageRoute(user: currentUser),
                                              ),
                                            ),
                                          ],
                                        ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            : const Text('Không có dữ liệu');
      },
    );
  }

  Widget _title(UserEntity currentUser) {
    return Text(
      currentUser.fullName,
      style: const TextStyle(
        fontWeight: FontWeight.w900,
        fontSize: 18,
      ),
    );
  }

  Widget _bio(UserEntity currentUser) {
    return Text(
      currentUser.bio,
      style: TextStyle(
        fontWeight: FontWeight.w900,
        fontSize: 12,
        color: appColors.textFaded,
      ),
    );
  }

  Widget _buildStatColumn(int num, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          num.toString(),
          style: const TextStyle(
            fontWeight: FontWeight.w800,
            fontSize: 16,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 4.0),
          child: Text(
            label,
            style: TextStyle(fontSize: 12, color: appColors.textFaded),
          ),
        ),
      ],
    );
  }
}
