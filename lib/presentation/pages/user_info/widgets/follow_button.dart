import 'package:app_chat/presentation/core/mixins/resources.dart';
import 'package:flutter/material.dart';

class FollowButton extends StatelessWidget with ResourceApp {
  final Function()? onPressed;
  final String text;
  final double? width;
  final Color? backgroundColor;
  final Color? overlayColor;
  final Color? borderColor;
  final Color? textColor;

  FollowButton({
    super.key,
    this.onPressed,
    required this.text,
    this.backgroundColor,
    this.borderColor,
    this.textColor,
    this.overlayColor,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 12),
      width: width ?? 230,
      height: 28,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(backgroundColor ?? Colors.transparent),
          overlayColor: MaterialStateProperty.all(overlayColor ?? appColors.cardLight.withOpacity(0.5)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6),
              side: BorderSide(color: borderColor ?? appColors.cardLight),
            ),
          ),
        ),
        onPressed: onPressed,
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
            color: textColor ?? appColors.textLight,
          ),
        ),
      ),
    );
  }
}
