import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/auth/auth_store.dart';
import '../../../application/stores/post/post_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/mixins/resources.dart';
import '../../core/widgets/base_appbar.dart';
import '../../core/widgets/base_title_appbar.dart';
import 'widgets/body_user_info.dart';

class UserInfoPage extends StatefulWidget {
  final String uid;
  final bool? showAppbar;
  const UserInfoPage({super.key, required this.uid, this.showAppbar});

  @override
  State<UserInfoPage> createState() => _UserInfoPageState();
}

class _UserInfoPageState extends State<UserInfoPage> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.showAppbar! ? _appBar() : null,
      body: ChangeNotifierProvider(
        create: (_) => injector<PostStore>(),
        child: Builder(
          builder: (c) => BodyUserInfo(
            uid: widget.uid,
          ),
        ),
      ),
    );
  }

  BaseAppBar _appBar() {
    final authStore = Provider.of<AuthStore>(context, listen: false);
    return BaseAppBar(
      elevation: 0,
      centerTitle: false,
      title: _title(),
      actions: [
        IconButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  content: const Text('Bạn thật sự muốn đăng xuất ?'),
                  actions: [
                    TextButton(
                      onPressed: () {
                        context.router.pop();
                      },
                      child: Text(
                        'Huỷ',
                        style: TextStyle(color: appColors.secondary),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        context.router.pop();
                        authStore.signOut('Offline');
                      },
                      child: Text(
                        'Đăng xuất',
                        style: TextStyle(color: appColors.secondary),
                      ),
                    ),
                  ],
                );
              },
            );
          },
          icon: const Icon(Icons.door_sliding),
        )
      ],
    );
  }

  Widget _title() {
    return const Padding(
      padding: EdgeInsets.only(left: 8.0),
      child: BaseTitleAppBar(
        title: 'Trang cá nhân',
      ),
    );
  }
}
