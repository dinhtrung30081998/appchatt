import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/user_info/user_info_store.dart';
import '../../../domain/entities/user/user_entity.dart';
import '../../core/mixins/resources.dart';
import '../../core/widgets/avatar.dart';
import '../../core/widgets/base_appbar.dart';
import '../../core/widgets/base_leading_appbar.dart';
import '../../core/widgets/base_title_appbar.dart';

class EditUserInfoPage extends StatefulWidget {
  final UserEntity userEntity;
  const EditUserInfoPage({super.key, required this.userEntity});

  @override
  State<EditUserInfoPage> createState() => _EditUserInfoPageState();
}

class _EditUserInfoPageState extends State<EditUserInfoPage> with ResourceApp {
  final TextEditingController _fullNameController = TextEditingController();
  final TextEditingController _bioController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _fullNameController.text = widget.userEntity.fullName;
    _bioController.text = widget.userEntity.bio;
  }

  @override
  void dispose() {
    super.dispose();
    _fullNameController.dispose();
    _bioController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: _appBar(),
        body: _body(),
      ),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: AvatarWidget.large(url: widget.userEntity.profilePicture),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Text(
                    'Tên người dùng',
                    style: TextStyle(fontSize: 12, color: appColors.textFaded),
                  ),
                ),
                TextField(
                  controller: _fullNameController,
                  decoration: InputDecoration(
                    hintText: widget.userEntity.fullName,
                    hintStyle: TextStyle(
                      color: appColors.textLight,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Text(
                    'Tiểu sử',
                    style: TextStyle(fontSize: 12, color: appColors.textFaded),
                  ),
                ),
                TextField(
                  controller: _bioController,
                  decoration: InputDecoration(
                    hintText: widget.userEntity.bio,
                    hintStyle: TextStyle(
                      color: appColors.textLight,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  BaseAppBar _appBar() {
    UserInfoStore userInfoStore = Provider.of(context, listen: false);

    return BaseAppBar(
      leading: _leading(),
      title: _title(),
      actions: [
        Consumer<UserInfoStore>(
          builder: (c, userStore, _) {
            if (userStore.editUserState == const EditUserState.loading()) {
              return Padding(
                padding: const EdgeInsets.only(right: 16),
                child: CupertinoActivityIndicator(
                  color: appColors.successColor,
                ),
              );
            }
            return IconButton(
              onPressed: () {
                userInfoStore.editUserInfo(
                  context,
                  widget.userEntity.uid.getValueOrCrash(),
                  _fullNameController.text,
                  _bioController.text,
                );
                FocusScope.of(context).unfocus();
              },
              icon: Icon(
                Icons.check,
                color: appColors.successColor,
              ),
            );
          },
        )
      ],
    );
  }

  Widget _title() {
    return const BaseTitleAppBar(
      title: 'Chỉnh sửa trang cá nhân',
    );
  }

  Widget _leading() {
    return BaseLeadingAppBar(
      onTap: () => context.router.pop(),
    );
  }
}
