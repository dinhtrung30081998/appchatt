import 'package:app_chat/domain/entities/user/user_entity.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/messages/messages_store.dart';
import 'action_bar.dart';
import 'app_bar_title.dart';
import 'icon_actions.dart';
import 'message_list.dart';

class BodyChatPage extends StatefulWidget {
  final UserEntity? user;
  const BodyChatPage({super.key, this.user});

  @override
  State<BodyChatPage> createState() => _BodyChatPageState();
}

class _BodyChatPageState extends State<BodyChatPage> {
  late MessagesStore messagesStore;

  @override
  void initState() {
    super.initState();
    messagesStore = Provider.of(context, listen: false);
    messagesStore.readLastMessages(widget.user!.uid.getValueOrCrash());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        messagesStore.outRoomChat(widget.user!.uid.getValueOrCrash());
        return Future(() => true);
      },
      child: Scaffold(
        appBar: AppBar(
          leadingWidth: 54,
          leading: Align(
            alignment: Alignment.centerRight,
            child: IconBackground(
              icon: CupertinoIcons.back,
              onTap: () {
                messagesStore.outRoomChat(widget.user!.uid.getValueOrCrash());
                context.router.pop();
              },
            ),
          ),
          title: AppBarTitle(
            partnerUid: widget.user!.uid.getValueOrCrash(),
          ),
          // actions: [
          //   Padding(
          //     padding: const EdgeInsets.symmetric(horizontal: 8.0),
          //     child: Center(
          //       child: IconBorder(
          //         icon: CupertinoIcons.video_camera_solid,
          //         onTap: () {},
          //       ),
          //     ),
          //   ),
          //   Padding(
          //     padding: const EdgeInsets.only(right: 20),
          //     child: Center(
          //       child: IconBorder(
          //         icon: CupertinoIcons.phone_solid,
          //         onTap: () {},
          //       ),
          //     ),
          //   ),
          // ],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: MessageList(
                messagesStore: messagesStore,
                partnerId: widget.user!.uid.getValueOrCrash(),
              ),
            ),
            ActionBar(
              user: widget.user,
              messagesStore: messagesStore,
            ),
          ],
        ),
      ),
    );
  }
}
