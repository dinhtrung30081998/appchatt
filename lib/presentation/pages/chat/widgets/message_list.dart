import 'package:app_chat/presentation/pages/chat/widgets/show_image.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

import '../../../../application/stores/messages/messages_store.dart';
import '../../../../domain/entities/chat/chat_entity.dart';
import '../../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../../../infrastructure/dtos/chat/chat_dto.dart';
import '../../../core/mixins/resources.dart';
import 'date_label.dart';

class MessageList extends StatefulWidget {
  final MessagesStore messagesStore;
  final String partnerId;
  const MessageList({super.key, required this.messagesStore, required this.partnerId});

  @override
  State<MessageList> createState() => _MessageListState();
}

class _MessageListState extends State<MessageList> with ResourceApp, WidgetsBindingObserver {
  late Stream streamChats;
  late String currentUser;
  final ValueNotifier<bool> isEnd = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    streamChats = widget.messagesStore.streamListChat();
    currentUser = injector<FirebaseAuth>().currentUser!.uid;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: streamChats,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite));
        }

        if (snapshot.connectionState == ConnectionState.active) {
          if (!snapshot.hasData ||
              snapshot.data == null ||
              snapshot.hasError ||
              (snapshot.data!).snapshot.value == null) {
            return const Center(child: Text('Không có đoạn chat nào'));
          }

          List<ChatEntity> listChat = [];

          final message = (snapshot.data!).snapshot.value as Map<dynamic, dynamic>;

          final myMessages = Map<dynamic, dynamic>.from(message); //typecasting

          myMessages.forEach((key, value) {
            final currentMessage = Map<String, dynamic>.from(value);

            if (currentMessage['users'].contains(currentUser) && currentMessage['users'].contains(widget.partnerId)) {
              currentMessage.forEach((key, value) {
                if (key.contains('messages')) {
                  final currentChats = Map<String, dynamic>.from(value);
                  currentChats.forEach((key, value) {
                    final chat = Map<String, dynamic>.from(value);
                    ChatEntity chatDTO = ChatDTO.fromJson(chat).toDomain();

                    listChat.add(chatDTO);
                  });
                }
              });
            }
          });

          if (listChat.isEmpty) {
            return const Center(
              child: Text('Không có đoạn chat nào'),
            );
          }
          return GroupedListView(
            elements: listChat,
            reverse: true,
            padding: const EdgeInsets.only(top: 10),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            groupBy: (element) => injector<DateFormat>().format(element.sendingTime),
            groupSeparatorBuilder: (groupByValue) => DateLabel(dateTime: groupByValue.toString()),
            useStickyGroupSeparators: false,
            floatingHeader: true,
            itemComparator: (element1, element2) {
              return (element1.sendingTime).compareTo(element2.sendingTime);
            },
            order: GroupedListOrder.DESC,
            itemBuilder: (context, element) {
              final currentChat = element;
              return currentChat.sendById == currentUser
                  ? MessageOwnTile(chat: currentChat)
                  : MessageTile(chat: currentChat);
            },
          );
        }
        return const Center(child: Text('Không có kết nối internet, vui lòng kiểm tra đường truyền'));
      },
    );
  }
}

class MessageTile extends StatelessWidget with ResourceApp {
  MessageTile({
    Key? key,
    required this.chat,
  }) : super(key: key);

  final ChatEntity chat;

  static const _borderRadius = 26.0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            chat.type == 'text'
                ? Container(
                    decoration: BoxDecoration(
                      color: appColors.cardDark,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(_borderRadius),
                        topRight: Radius.circular(_borderRadius),
                        bottomRight: Radius.circular(_borderRadius),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: Text(chat.text),
                    ),
                  )
                : InkWell(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => ShowImage(imageUrl: chat.text),
                      ),
                    ),
                    child: SizedBox(
                      width: 200,
                      height: 200,
                      child: chat.text.isNotEmpty
                          ? CachedNetworkImage(
                              imageUrl: chat.text,
                              fit: BoxFit.cover,
                            )
                          : Center(
                              child: CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite),
                            ),
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.only(top: 2),
              child: Text(
                DateFormat.Hm().format(chat.sendingTime),
                style: TextStyle(
                  color: appColors.textFaded,
                  fontSize: 7,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MessageOwnTile extends StatelessWidget with ResourceApp {
  MessageOwnTile({
    Key? key,
    required this.chat,
  }) : super(key: key);

  final ChatEntity chat;

  static const _borderRadius = 26.0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
      child: Align(
        alignment: Alignment.centerRight,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            chat.type == 'text'
                ? Container(
                    decoration: BoxDecoration(
                      color: appColors.secondary,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(_borderRadius),
                        bottomRight: Radius.circular(_borderRadius),
                        bottomLeft: Radius.circular(_borderRadius),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: Text(chat.text),
                    ),
                  )
                : InkWell(
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => ShowImage(imageUrl: chat.text),
                      ),
                    ),
                    child: SizedBox(
                      width: 200,
                      height: 200,
                      child: CachedNetworkImage(
                        imageUrl: chat.text,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.only(top: 2),
              child: Text(
                DateFormat.Hm().format(chat.sendingTime),
                style: TextStyle(
                  color: appColors.textFaded,
                  fontSize: 7,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
