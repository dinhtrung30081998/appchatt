import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../application/stores/messages/messages_store.dart';
import '../../../../domain/entities/user/user_entity.dart';
import '../../../core/mixins/resources.dart';
import '../../home/widgets/glowing_action_button.dart';

class ActionBar extends StatefulWidget {
  final MessagesStore messagesStore;
  final UserEntity? user;
  const ActionBar({Key? key, required this.messagesStore, this.user}) : super(key: key);

  @override
  State<ActionBar> createState() => _ActionBarState();
}

class _ActionBarState extends State<ActionBar> with ResourceApp {
  final TextEditingController textEditingController = TextEditingController();
  final ValueNotifier<String> textChanged = ValueNotifier('');
  final ValueNotifier<bool> showEmoji = ValueNotifier(false);

  @override
  void dispose() {
    super.dispose();
    textChanged.dispose();
    showEmoji.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      top: false,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Card(
                  margin: const EdgeInsets.only(left: 12, bottom: 12),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            right: BorderSide(
                              width: 1,
                              color: Theme.of(context).dividerColor,
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            IconButton(
                              onPressed: () => widget.messagesStore.getImage(
                                widget.user!,
                                'image',
                              ),
                              icon: const Icon(FontAwesomeIcons.image, size: 20),
                            ),
                            // IconButton(
                            //   onPressed: () {},
                            //   icon: const Icon(CupertinoIcons.camera_fill, size: 20),
                            // ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0),
                          child: TextField(
                            onTap: () => showEmoji.value = false,
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: textEditingController,
                            onChanged: (textStr) {
                              textChanged.value = textStr;
                            },
                            style: const TextStyle(fontSize: 14),
                            decoration: const InputDecoration(
                              hintText: 'Nhập tin nhắn...',
                              border: InputBorder.none,
                            ),
                            onEditingComplete: () {},
                            onSubmitted: (_) {},
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          showEmoji.value = !showEmoji.value;
                          FocusScope.of(context).unfocus();
                        },
                        icon: ValueListenableBuilder(
                          builder: (c, showEmojiValue, _) {
                            if (showEmojiValue) {
                              return Icon(
                                Icons.emoji_emotions,
                                size: 20,
                                color: appColors.secondary,
                              );
                            }
                            return const Icon(Icons.emoji_emotions, size: 20);
                          },
                          valueListenable: showEmoji,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 6,
                  right: 12.0,
                  bottom: 12,
                ),
                child: ValueListenableBuilder(
                  builder: (c, textValue, _) {
                    return GlowingActionButton(
                      color: textValue.isEmpty ? appColors.textFaded : appColors.secondary,
                      icon: Icons.send_rounded,
                      size: 40,
                      onPressed: () {
                        if (textValue.isNotEmpty) {
                          widget.messagesStore.sendChat(
                            widget.user!,
                            textEditingController.text.trim(),
                            'text',
                          );
                          textEditingController.clear();
                        }
                        textChanged.value = '';
                      },
                    );
                  },
                  valueListenable: textChanged,
                ),
              ),
            ],
          ),
          ValueListenableBuilder(
            builder: (c, showEmojiValue, _) {
              return Offstage(
                offstage: !showEmojiValue,
                child: SizedBox(
                  height: 250,
                  child: EmojiPicker(
                    textEditingController: textEditingController,
                    onEmojiSelected: (category, emoji) {
                      textChanged.value = emoji.emoji;
                    },
                    config: Config(
                      columns: 7,
                      emojiSizeMax: 32 * (foundation.defaultTargetPlatform == TargetPlatform.iOS ? 1.30 : 1.0),
                      verticalSpacing: 0,
                      horizontalSpacing: 0,
                      gridPadding: EdgeInsets.zero,
                      initCategory: Category.RECENT,
                      bgColor: appColors.cardDark,
                      indicatorColor: appColors.secondary,
                      iconColor: Colors.grey,
                      iconColorSelected: appColors.secondary,
                      backspaceColor: appColors.secondary,
                      skinToneDialogBgColor: Colors.white,
                      skinToneIndicatorColor: Colors.grey,
                      enableSkinTones: true,
                      showRecentsTab: true,
                      recentsLimit: 28,
                      replaceEmojiOnLimitExceed: false,
                      noRecents: const Text(
                        'Không có biểu cảm nào dùng gần đây',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      loadingIndicator: const SizedBox.shrink(),
                      tabIndicatorAnimDuration: kTabScrollDuration,
                      categoryIcons: const CategoryIcons(),
                      buttonMode: ButtonMode.MATERIAL,
                      checkPlatformCompatibility: true,
                    ),
                  ),
                ),
              );
            },
            valueListenable: showEmoji,
          ),
        ],
      ),
    );
  }
}
