import 'package:flutter/material.dart';

import '../../../core/mixins/resources.dart';

class DateLabel extends StatefulWidget {
  const DateLabel({
    Key? key,
    required this.dateTime,
  }) : super(key: key);

  final String dateTime;

  @override
  State<DateLabel> createState() => _DateLabelState();
}

class _DateLabelState extends State<DateLabel> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 12),
            child: Text(
              widget.dateTime,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.bold,
                color: appColors.textFaded,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
