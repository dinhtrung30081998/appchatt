import 'package:app_chat/presentation/core/mixins/resources.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class ShowImage extends StatelessWidget with ResourceApp {
  final String imageUrl;
  ShowImage({super.key, required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PhotoViewGallery.builder(
        scrollPhysics: const BouncingScrollPhysics(),
        builder: _buildItem,
        itemCount: 1,
        loadingBuilder: (context, event) {
          return Center(
            child: CupertinoActivityIndicator(
              color: appColors.cupertinoIndicatorWhite,
            ),
          );
        },
        backgroundDecoration: const BoxDecoration(
          color: Colors.black,
        ),
        scrollDirection: Axis.horizontal,
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    return PhotoViewGalleryPageOptions(
      imageProvider: CachedNetworkImageProvider(imageUrl),
      initialScale: PhotoViewComputedScale.contained,
      minScale: PhotoViewComputedScale.contained * (0.5 + index / 10),
      maxScale: PhotoViewComputedScale.covered * 4.1,
      heroAttributes: PhotoViewHeroAttributes(tag: imageUrl.characters),
    );
  }
}
