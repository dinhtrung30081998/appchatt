import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/user_info/user_info_store.dart';
import '../../../../domain/entities/user/user_entity.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/utils/vi_message.dart';
import '../../../core/widgets/avatar.dart';

class AppBarTitle extends StatefulWidget with ResourceApp {
  final String partnerUid;
  AppBarTitle({
    Key? key,
    required this.partnerUid,
  }) : super(key: key);

  @override
  State<AppBarTitle> createState() => _AppBarTitleState();
}

class _AppBarTitleState extends State<AppBarTitle> with ResourceApp {
  late Stream<UserEntity> streamUser;
  late UserInfoStore userInfoStore;

  @override
  void initState() {
    super.initState();
    userInfoStore = Provider.of(context, listen: false);
    streamUser = userInfoStore.getUserByUid(widget.partnerUid);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserEntity>(
        stream: streamUser,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data != null) {
              final currentUser = snapshot.data!;
              return Row(
                children: [
                  AvatarWidget.small(url: currentUser.profilePicture),
                  const SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          currentUser.fullName,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold, fontFamily: 'Mulish'),
                        ),
                        const SizedBox(height: 5),
                        Row(
                          children: [
                            currentUser.status == 'Online'
                                ? Container(
                                    width: 6,
                                    height: 6,
                                    decoration: BoxDecoration(color: appColors.successColor, shape: BoxShape.circle),
                                  )
                                : Container(),
                            const SizedBox(
                              width: 3,
                            ),
                            Flexible(
                              child: Text(
                                overflow: TextOverflow.ellipsis,
                                currentUser.status == 'Online'
                                    ? currentUser.status
                                    : "Hoạt động ${format(currentUser.lastTimeOnline, locale: 'vi')}",
                                style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Mulish',
                                  color: currentUser.status == 'Online' ? appColors.successColor : appColors.textFaded,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              );
            }
          }
          return Center(
            child: CupertinoActivityIndicator(color: appColors.cupertinoIndicatorWhite),
          );
        });
  }
}
