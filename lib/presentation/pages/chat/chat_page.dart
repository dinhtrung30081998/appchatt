import 'package:app_chat/domain/entities/user/user_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/messages/messages_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import 'widgets/body_chat_page.dart';

class ChatPage extends StatefulWidget {
  final UserEntity? user;
  const ChatPage({super.key, this.user});

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: ChangeNotifierProvider(
        create: (_) => injector<MessagesStore>(),
        child: Builder(
          builder: (c) {
            return BodyChatPage(
              user: widget.user,
            );
          },
        ),
      ),
    );
  }
}
