import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/user_info/user_info_store.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/routes/routes.dart';
import 'glowing_action_button.dart';
import 'nav_item.dart';

class BottomNavWidget extends StatefulWidget {
  final ValueChanged<int> onItemSelected;
  const BottomNavWidget({super.key, required this.onItemSelected});

  @override
  State<BottomNavWidget> createState() => _BottomNavWidgetState();
}

class _BottomNavWidgetState extends State<BottomNavWidget> with ResourceApp {
  var selectedIndex = 0;
  final List<XFile> _selectedFiles = [];
  late UserInfoStore userInfoStore;

  final ImagePicker imagePicker = ImagePicker();

  @override
  void initState() {
    super.initState();
    userInfoStore = Provider.of(context, listen: false);
  }

  takeAPhoto() async {
    context.router.pop();
    if (_selectedFiles.isNotEmpty) {
      _selectedFiles.clear();
    }
    XFile? xFile =
        await imagePicker.pickImage(source: ImageSource.camera, maxHeight: 675, maxWidth: 960, imageQuality: 90);
    if (xFile == null) return '';
    _selectedFiles.add(xFile);
    setState(() {
      if (_selectedFiles.isNotEmpty) {
        context.router.push(AddPostPageRoute(listFiles: _selectedFiles));
      }
    });
  }

  chooseFromGallery() async {
    context.router.pop();
    if (_selectedFiles.isNotEmpty) {
      _selectedFiles.clear();
    }
    try {
      final List<XFile> listFiles = await imagePicker.pickMultiImage(imageQuality: 90);

      if (listFiles.isNotEmpty) {
        _selectedFiles.addAll(listFiles);
      }
      setState(() {
        if (_selectedFiles.isNotEmpty) {
          context.router.push(AddPostPageRoute(listFiles: _selectedFiles));
        }
      });
    } catch (e) {
      print(e.toString());
    }
  }

  selectImage(BuildContext parentContext) {
    return showDialog(
      context: parentContext,
      builder: (context) {
        return SimpleDialog(
          title: Text('Tạo bài viết'.toUpperCase()),
          children: [
            SimpleDialogOption(
              child: InkWell(
                onTap: () => takeAPhoto(),
                child: Container(
                  alignment: Alignment.center,
                  height: 35,
                  child: const Text('Chụp ảnh'),
                ),
              ),
            ),
            SimpleDialogOption(
              child: InkWell(
                onTap: () => chooseFromGallery(),
                child: Container(
                  alignment: Alignment.center,
                  height: 35,
                  child: const Text('Chọn hình ảnh từ thư viện'),
                ),
              ),
            ),
            SimpleDialogOption(
              child: InkWell(
                onTap: () => context.router.pop(),
                child: Container(
                  alignment: Alignment.center,
                  height: 35,
                  child: const Text('Huỷ'),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  void handleItemSelected(int index) {
    setState(() {
      selectedIndex = index;
    });
    widget.onItemSelected(selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(80),
      ),
      elevation: 0,
      color: Colors.black,
      child: SafeArea(
        top: false,
        bottom: true,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              NavItemWidget(
                index: 0,
                icon: CupertinoIcons.house_alt_fill,
                label: 'Chatt',
                onTap: handleItemSelected,
                itemSelected: (selectedIndex == 0),
              ),
              NavItemWidget(
                index: 1,
                icon: CupertinoIcons.search,
                label: 'Tin nhắn',
                onTap: handleItemSelected,
                itemSelected: (selectedIndex == 1),
              ),
              GlowingActionButton(
                color: appColors.secondary,
                icon: Icons.add,
                onPressed: () => selectImage(context),
              ),
              NavItemWidget(
                index: 2,
                icon: CupertinoIcons.bubble_left_bubble_right_fill,
                label: 'Cuộc gọi',
                onTap: handleItemSelected,
                itemSelected: (selectedIndex == 2),
              ),
              NavItemWidget(
                index: 3,
                icon: CupertinoIcons.person_fill,
                label: 'Trang cá nhân',
                onTap: handleItemSelected,
                itemSelected: (selectedIndex == 3),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
