import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import '../../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../../core/mixins/helpers.dart';
import '../../../core/mixins/resources.dart';
import '../../messages/messages_page.dart';
import '../../post/post_page.dart';
import '../../search/search_page.dart';
import '../../user_info/user_info_page.dart';
import 'bottom_nav.dart';

class BodyHomePage extends StatefulWidget {
  final int? defaultIndex;
  const BodyHomePage({super.key, this.defaultIndex = 0});

  @override
  State<BodyHomePage> createState() => _BodyHomePageState();
}

class _BodyHomePageState extends State<BodyHomePage>
    with ResourceApp, Helpers, SingleTickerProviderStateMixin, WidgetsBindingObserver {
  late ValueNotifier<int> pageIndex;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    pageIndex = ValueNotifier(widget.defaultIndex!);
  }

  final _listBody = [
    const PostPage(),
    const SearchPage(),
    const MessagesPage(),
    UserInfoPage(
      uid: injector<FirebaseAuth>().currentUser!.uid,
      showAppbar: true,
    ),
  ];

  void _onNavigationItemSelected(int index) {
    pageIndex.value = index;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
      bottomNavigationBar: _bottomNav(),
    );
  }

  BottomNavWidget _bottomNav() {
    return BottomNavWidget(
      onItemSelected: (indexSelected) => _onNavigationItemSelected(indexSelected),
    );
  }

  ValueListenableBuilder<int> _body() {
    return ValueListenableBuilder(
      builder: (BuildContext c, int value, _) {
        return _listBody[value];
      },
      valueListenable: pageIndex,
    );
  }
}
