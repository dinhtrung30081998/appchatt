import 'package:app_chat/presentation/core/mixins/resources.dart';
import 'package:flutter/material.dart';

class NavItemWidget extends StatelessWidget with ResourceApp {
  final String label;
  final int index;
  final IconData icon;
  final bool itemSelected;
  final ValueChanged<int> onTap;

  NavItemWidget({
    super.key,
    required this.label,
    required this.icon,
    required this.index,
    required this.onTap,
    this.itemSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        onTap(index);
      },
      child: SizedBox(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              icon,
              size: 20,
              color: itemSelected ? appColors.secondary : null,
            ),
            // Text(
            //   label,
            //   style: itemSelected
            //       ? TextStyle(
            //           fontSize: 11,
            //           color: appColors.secondary,
            //         )
            //       : const TextStyle(
            //           fontSize: 11,
            //         ),
            // ),
          ],
        ),
      ),
    );
  }
}
