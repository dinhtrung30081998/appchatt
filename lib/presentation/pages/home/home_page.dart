import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/user_info/user_info_store.dart';
import '../../core/mixins/resources.dart';
import 'widgets/body_home_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver, ResourceApp {
  late UserInfoStore userInfoStore;
  final ValueNotifier<int> setIndex = ValueNotifier(0);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    userInfoStore = Provider.of(context, listen: false);
    userInfoStore.setStatusUser('Online');
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //Online
      if (userInfoStore.isCachedUserId()!) {
        userInfoStore.setStatusUser('Online');
      }
    } else if (state == AppLifecycleState.inactive) {
      //Offline
      if (userInfoStore.isCachedUserId()!) {
        userInfoStore.setStatusUser('Offline');
      }
    } else if (state == AppLifecycleState.paused) {
      // //Offline
      // if (userInfoStore.isCachedUserId()!) {
      //   userInfoStore.setStatusUser('Online');
      // }
    } else if (state == AppLifecycleState.detached) {
      // //Offline
      // if (userInfoStore.isCachedUserId()!) {
      //   userInfoStore.setStatusUser('Online');
      // }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: true,
      child: BodyHomePage(
        defaultIndex: setIndex.value,
      ),
    );
  }
}
