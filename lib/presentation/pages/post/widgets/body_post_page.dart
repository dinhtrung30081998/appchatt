import '../../../../application/stores/user_info/user_info_store.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/post/post_store.dart';
import '../../../../domain/entities/post/post_entity.dart';
import '../../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../../core/mixins/helpers.dart';
import '../../../core/mixins/resources.dart';
import '../../messages/widgets/stories_widget.dart';
import 'post_card.dart';

class BodyPost extends StatefulWidget {
  const BodyPost({super.key});

  @override
  State<BodyPost> createState() => _BodyPostState();
}

class _BodyPostState extends State<BodyPost> with Helpers, ResourceApp {
  late PostStore postStore;
  late UserInfoStore userInfoStore;
  late List<String>? following;
  late Stream<List<PostEntity>> streamListPost;

  @override
  void initState() {
    super.initState();
    postStore = Provider.of(context, listen: false);
    userInfoStore = Provider.of(context, listen: false);
    streamListPost = postStore.getStreamListPost();
    following = userInfoStore.getFollowing();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<PostEntity>>(
      stream: streamListPost,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CupertinoActivityIndicator(
              color: appColors.cupertinoIndicatorBlack,
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.active) {
          if (snapshot.hasData) {
            if (snapshot.data!.isNotEmpty) {
              final posts = snapshot.data!;
              final currentUserId = injector<FirebaseAuth>().currentUser!.uid;

              return CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: StoriesWidget(),
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      childCount: posts.length,
                      (context, index) {
                        final currentPost = posts[index];
                        return PostCard(
                          postEntity: currentPost,
                          userId: currentUserId,
                          postStore: postStore,
                        );
                      },
                    ),
                  ),
                ],
              );
            }
            return CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: StoriesWidget(),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    alignment: Alignment.center,
                    child: Center(
                      child: Text(
                        'Không có bài viết nào, \n hãy cùng tạo bài viết mới để mọi người\n cùng nhau tương tác',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: appColors.textFaded),
                      ),
                    ),
                  ),
                ),
              ],
            );
          } else {
            return const Center(
              child: Text('Không có dữ liệu'),
            );
          }
        } else {
          return const Center(
            child: Text('Mất kết nối internet, vui lòng kiểm tra đường truyền'),
          );
        }
      },
    );
  }
}
