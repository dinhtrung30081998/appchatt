import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../../../../application/stores/post/post_store.dart';
import '../../../../domain/entities/post/post_entity.dart';
import '../../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/routes/routes.dart';
import '../../../core/utils/vi_message.dart';
import '../../../core/widgets/avatar.dart';
import 'gallery_photo.dart';
import 'like_button_animation.dart';

class PostCard extends StatefulWidget {
  final PostEntity postEntity;
  final String userId;
  final PostStore postStore;
  const PostCard({super.key, required this.postEntity, required this.userId, required this.postStore});

  @override
  State<PostCard> createState() => _PostCardState();
}

class _PostCardState extends State<PostCard> with ResourceApp {
  final ValueNotifier<bool> isLikeAnAnimating = ValueNotifier(false);
  final ValueNotifier<bool> isCanHidePost = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    final compareUid = widget.postEntity.uid == injector<FirebaseAuth>().currentUser!.uid;
    isCanHidePost.value = compareUid;
  }

  @override
  void dispose() {
    super.dispose();
    isLikeAnAnimating.dispose();
    isCanHidePost.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.postEntity.hidePost && !isCanHidePost.value
        ? _hidePost()
        : Container(
            margin: const EdgeInsets.only(top: 8),
            color: Colors.transparent,
            child: Column(
              children: [
                _headerPost(),
                _imagePost(),
                _footerPost(),
                _contentPost(),
              ],
            ),
          );
  }

  Widget _hidePost() {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 4,
        horizontal: 16,
      ).copyWith(right: 0),
      child: Row(
        children: [
          const Icon(Icons.hide_image),
          const Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 8),
              child: Text(
                'Bài viết này đã bị ẩn',
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 12,
                ),
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => Dialog(
                  child: ListView(
                    shrinkWrap: true,
                    padding: const EdgeInsets.symmetric(vertical: 6),
                    children: ['Hiện lại bài viết này'].map(
                      (e) {
                        return InkWell(
                          onTap: () {
                            widget.postStore.showPost(widget.postEntity.postId);
                            context.router.pop();
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                            child: Text(e),
                          ),
                        );
                      },
                    ).toList(),
                  ),
                ),
              );
            },
            icon: const Icon(Icons.more_vert),
          ),
        ],
      ),
    );
  }

  Container _contentPost() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "${widget.postEntity.likes.length} lượt thích",
            style: const TextStyle(
              fontSize: 12,
            ),
          ),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.only(top: 8),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: widget.postEntity.fullName,
                    style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w900, fontFamily: 'Mulish'),
                  ),
                  const WidgetSpan(
                    alignment: PlaceholderAlignment.baseline,
                    baseline: TextBaseline.alphabetic,
                    child: SizedBox(width: 3),
                  ),
                  TextSpan(
                    text: widget.postEntity.description,
                    style: const TextStyle(fontSize: 12, fontFamily: 'Mulish'),
                  ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () => context.router.push(CommentsPageRoute(postEntity: widget.postEntity)),
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 4),
              child: ValueListenableBuilder(
                builder: (c, lengthComments, _) {
                  return Text(
                    "Xem ${widget.postEntity.comments.length} bình luận",
                    style: TextStyle(
                      color: appColors.textFaded,
                      fontSize: 11,
                    ),
                  );
                },
                valueListenable: widget.postStore.countLengthCommentsValid,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Row _footerPost() {
    return Row(
      children: [
        ValueListenableBuilder(
          builder: (c, animationValue, _) {
            return LikeAnimation(
              isAnimating: widget.postEntity.likes.contains(widget.userId),
              smallLike: true,
              child: IconButton(
                onPressed: () {
                  widget.postStore.likePost(widget.postEntity.postId, widget.userId, widget.postEntity.likes);
                },
                icon: widget.postEntity.likes.contains(widget.userId)
                    ? Icon(
                        Icons.favorite,
                        size: 20,
                        color: appColors.heartColor,
                      )
                    : Icon(
                        Icons.favorite_outline,
                        size: 20,
                        color: appColors.iconDark,
                      ),
              ),
            );
          },
          valueListenable: isLikeAnAnimating,
        ),
        IconButton(
          onPressed: () => context.router.push(CommentsPageRoute(postEntity: widget.postEntity)),
          icon: const Icon(
            Icons.comment_outlined,
            size: 20,
          ),
        ),
      ],
    );
  }

  Container _headerPost() {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 4,
        horizontal: 16,
      ).copyWith(right: 0),
      child: Row(
        children: [
          AvatarWidget.small(url: widget.postEntity.profileImage),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.postEntity.fullName,
                    style: const TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 12,
                    ),
                  ),
                  Text(
                    format(widget.postEntity.datePublished, locale: 'vi'),
                    style: TextStyle(
                      color: appColors.textFaded,
                      fontSize: 10,
                    ),
                  ),
                ],
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => Dialog(
                  child: ValueListenableBuilder(
                    builder: (c, hidePost, _) {
                      return ListView(
                        shrinkWrap: true,
                        padding: const EdgeInsets.symmetric(vertical: 6),
                        children: [hidePost ? 'Xoá bài viết' : 'Ẩn bài viết này'].map(
                          (e) {
                            return InkWell(
                              onTap: () {
                                if (hidePost) {
                                  widget.postStore.deletePost(widget.postEntity.postId);
                                } else {
                                  widget.postStore.hidePost(widget.postEntity.postId);
                                }
                                context.router.pop();
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                                child: Text(e),
                              ),
                            );
                          },
                        ).toList(),
                      );
                    },
                    valueListenable: isCanHidePost,
                  ),
                ),
              );
            },
            icon: const Icon(Icons.more_vert),
          ),
        ],
      ),
    );
  }

  Widget _imagePost() {
    return GestureDetector(
      onDoubleTap: () {
        widget.postStore.likePost(widget.postEntity.postId, widget.userId, widget.postEntity.likes);
        isLikeAnAnimating.value = true;
      },
      child: Stack(
        alignment: Alignment.center,
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.35,
            width: double.infinity,
            child: PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              itemCount: widget.postEntity.postUrl.length,
              builder: (context, index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider: CachedNetworkImageProvider(
                    widget.postEntity.postUrl[index],
                  ),
                  onTapUp: (context, details, controllerValue) {
                    open(context, index);
                  },
                );
              },
              loadingBuilder: (context, event) {
                return Center(
                  child: CupertinoActivityIndicator(
                    color: appColors.cupertinoIndicatorWhite,
                  ),
                );
              },
            ),
          ),
          ValueListenableBuilder(
            builder: (c, animationValue, _) {
              return AnimatedOpacity(
                duration: const Duration(milliseconds: 200),
                opacity: animationValue ? 1 : 0,
                child: LikeAnimation(
                  isAnimating: isLikeAnAnimating.value,
                  duration: const Duration(milliseconds: 400),
                  onEnd: () {
                    isLikeAnAnimating.value = false;
                  },
                  child: Icon(
                    Icons.favorite,
                    color: appColors.iconDark,
                    size: 100,
                  ),
                ),
              );
            },
            valueListenable: isLikeAnAnimating,
          ),
        ],
      ),
    );
  }

  void open(BuildContext context, final int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GalleryPhotoViewWrapper(
          galleryItems: widget.postEntity.postUrl,
          loadingBuilder: (context, event) {
            return Center(
              child: CupertinoActivityIndicator(
                color: appColors.cupertinoIndicatorWhite,
              ),
            );
          },
          backgroundDecoration: const BoxDecoration(
            color: Colors.black,
          ),
          initialIndex: index,
          scrollDirection: Axis.horizontal,
        ),
      ),
    );
  }
}
