import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/post/post_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/widgets/base_appbar.dart';
import '../../core/widgets/base_title_appbar.dart';
import 'widgets/body_post_page.dart';

class PostPage extends StatefulWidget {
  const PostPage({super.key});

  @override
  State<PostPage> createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: ChangeNotifierProvider(
        create: (_) => injector<PostStore>(),
        child: Builder(
          builder: (c) {
            return const BodyPost();
          },
        ),
      ),
    );
  }

  BaseAppBar _appBar() {
    return BaseAppBar(
      elevation: 0,
      centerTitle: false,
      title: _title(),
    );
  }

  Widget _title() {
    return const Padding(
      padding: EdgeInsets.only(left: 8.0),
      child: BaseTitleAppBar(
        title: 'Chatt',
        fontSize: 22,
        fontFamily: 'Lobster',
      ),
    );
  }
}
