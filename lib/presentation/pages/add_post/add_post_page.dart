import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../../application/stores/post/post_store.dart';
import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../../core/mixins/resources.dart';
import 'widgets/body_add_post.dart';

class AddPostPage extends StatefulWidget {
  final List<XFile>? listFiles;
  const AddPostPage({super.key, this.listFiles});

  @override
  State<AddPostPage> createState() => _AddPostPageState();
}

class _AddPostPageState extends State<AddPostPage> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: ChangeNotifierProvider(
        create: (_) => injector<PostStore>(),
        child: Builder(
          builder: (c) {
            return BodyAddPost(
              listFiles: widget.listFiles,
            );
          },
        ),
      ),
    );
  }
}
