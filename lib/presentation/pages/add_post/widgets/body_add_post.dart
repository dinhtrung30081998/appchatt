import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../../../application/stores/post/post_store.dart';
import '../../../../application/stores/user_info/user_info_store.dart';
import '../../../core/mixins/resources.dart';
import '../../../core/widgets/base_appbar.dart';
import '../../../core/widgets/base_leading_appbar.dart';
import '../../../core/widgets/base_title_appbar.dart';

class BodyAddPost extends StatefulWidget {
  final List<XFile>? listFiles;
  const BodyAddPost({super.key, this.listFiles});

  @override
  State<BodyAddPost> createState() => _BodyAddPostState();
}

class _BodyAddPostState extends State<BodyAddPost> with ResourceApp {
  late PostStore postStore;
  final TextEditingController _postContentController = TextEditingController();

  @override
  void initState() {
    super.initState();
    postStore = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              // Consumer<PostStore>(
              //   builder: (c, store, _) {
              //     if (store.postState == const PostState.postLoading()) {
              //       return LinearProgressIndicator(color: appColors.secondary);
              //     }
              //     return Container();
              //   },
              // ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _avatar(),
                  const SizedBox(width: 8),
                  _postContent(context),
                ],
              ),
              const SizedBox(height: 15),
              GridView.builder(
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                ),
                itemCount: widget.listFiles!.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(2),
                    child: Image.file(
                      File(widget.listFiles![index].path),
                      fit: BoxFit.cover,
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Expanded _postContent(BuildContext context) {
    return Expanded(
      child: SizedBox(
        child: TextFormField(
          maxLines: 5,
          controller: _postContentController,
          cursorWidth: 1.0,
          showCursor: true,
          cursorColor: appColors.secondary,
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(6),
              borderSide: BorderSide(color: appColors.enabledBorderLight, width: 0.5),
              gapPadding: 10,
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(6),
              borderSide: BorderSide(color: appColors.secondary, width: 0.5),
              gapPadding: 10,
            ),
            hintText: 'Viết một caption...',
            hintStyle: TextStyle(
              color: appColors.textFaded,
            ),
          ),
          onChanged: (value) {},
          onEditingComplete: () => FocusScope.of(context).unfocus(),
        ),
      ),
    );
  }

  Column _avatar() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CachedNetworkImage(
          width: 45,
          height: 45,
          imageUrl: context.read<UserInfoStore>().getProfilePicture()!,
          placeholder: (context, url) {
            return CircularProgressIndicator(color: appColors.cupertinoIndicatorBlack);
          },
          imageBuilder: (_, imageProvider) => Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ],
    );
  }

  BaseAppBar _appBar() {
    return BaseAppBar(
      leading: _leading(),
      title: _title(),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Consumer<PostStore>(
            builder: (c, postStoreValue, _) {
              final disableButton = postStoreValue.postState == const PostState.postLoading();
              return TextButton(
                onPressed: () {
                  disableButton ? null : executeCreatePost();
                },
                child: Builder(
                  builder: (_) {
                    if (postStoreValue.postState == const PostState.postLoading()) {
                      return CupertinoActivityIndicator(color: appColors.secondary);
                    }
                    return Text(
                      'ĐĂNG',
                      style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 12,
                        color: appColors.secondary,
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _leading() {
    return BaseLeadingAppBar(
      onTap: () => context.router.pop(),
    );
  }

  Widget _title() {
    return const BaseTitleAppBar(
      title: 'Tạo bài viết',
    );
  }

  void executeCreatePost() {
    if (widget.listFiles!.isNotEmpty) {
      postStore.createPost(context.read<UserInfoStore>().getFullName()!, _postContentController.text.trim().toString(),
          widget.listFiles!, context.read<UserInfoStore>().getProfilePicture()!, context);
    }
    FocusScope.of(context).unfocus();
  }
}
