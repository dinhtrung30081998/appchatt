import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../resources/colors.dart';

mixin ResourceApp {
  final AppColors appColors = injector<AppColors>();
}
