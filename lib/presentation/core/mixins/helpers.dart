import 'dart:math';

mixin Helpers {
  final Random random = Random();
  String randomPictureUrl() {
    final randomInt = random.nextInt(1000);
    return 'https://picsum.photos/seed/$randomInt/300/300';
  }

  DateTime randomDate() {
    final randomDate = Random();
    final currentDate = DateTime.now();
    return currentDate.subtract(Duration(seconds: randomDate.nextInt(20000)));
  }
}
