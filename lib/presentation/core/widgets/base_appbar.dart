import 'package:app_chat/presentation/core/mixins/resources.dart';
import 'package:flutter/material.dart';

class BaseAppBar extends StatefulWidget with ResourceApp implements PreferredSizeWidget {
  final Widget? leading;
  final Widget? title;
  final List<Widget>? actions;
  final bool? centerTitle;
  final Color? backgroundColor;
  final double? elevation;
  final PreferredSizeWidget? bottom;
  final double? leadingWidth;

  BaseAppBar({
    Key? key,
    this.leading,
    this.title,
    this.actions,
    this.centerTitle,
    this.backgroundColor,
    this.elevation,
    this.bottom,
    this.leadingWidth,
  }) : super(key: key);

  @override
  State<BaseAppBar> createState() => _BaseAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(55);
}

class _BaseAppBarState extends State<BaseAppBar> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: widget.elevation ?? 0,
      backgroundColor: widget.backgroundColor ?? Colors.transparent,
      centerTitle: widget.centerTitle ?? false,
      title: widget.title,
      leading: widget.leading,
      leadingWidth: widget.leadingWidth ?? 45,
      actions: widget.actions,
      bottom: widget.bottom,
    );
  }
}
