import 'package:flutter/material.dart';

class BaseTitleAppBar extends StatelessWidget {
  final String title;
  final String? fontFamily;
  final double? fontSize;
  const BaseTitleAppBar({super.key, required this.title, this.fontFamily, this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontWeight: FontWeight.w900,
        fontSize: fontSize ?? 18,
        fontFamily: fontFamily ?? 'Mulish',
      ),
    );
  }
}
