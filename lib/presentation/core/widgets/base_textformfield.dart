import 'package:flutter/material.dart';

import '../mixins/resources.dart';

class BaseTextFormField extends StatelessWidget with ResourceApp {
  final String label;
  final TextEditingController controller;
  final TextInputType? textInputType;
  final TextInputAction? textInputAction;
  final bool? isPassword;
  final bool? autoFocus;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final ValueChanged<String>? onChange;
  final FocusNode? focusNode;
  final int? maxLines;
  final ValueChanged<String>? onFieldSubmitted;
  final VoidCallback? onEditingComplete;

  final FormFieldValidator<String>? validator;

  BaseTextFormField({
    Key? key,
    required this.label,
    required this.controller,
    this.textInputType,
    this.isPassword,
    this.onChange,
    this.validator,
    this.prefixIcon,
    this.textInputAction,
    this.suffixIcon,
    this.autoFocus,
    this.onFieldSubmitted,
    this.onEditingComplete,
    this.focusNode,
    this.maxLines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      autocorrect: false,
      maxLines: maxLines ?? 1,
      autofocus: autoFocus ?? false,
      controller: controller,
      textInputAction: textInputAction ?? TextInputAction.next,
      style: const TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.normal,
      ),
      keyboardType: textInputType ?? TextInputType.text,
      obscureText: isPassword ?? false,
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        labelText: label,
        labelStyle: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
        hintText: "Nhập ${label.toLowerCase()}",
        hintStyle: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.normal,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
          borderSide: BorderSide(color: appColors.enabledBorderLight, width: 0.5),
          gapPadding: 10,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
          borderSide: BorderSide(color: appColors.secondary, width: 0.5),
          gapPadding: 10,
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
          borderSide: BorderSide(color: appColors.errorColor, width: 0.5),
          gapPadding: 10,
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(6),
          borderSide: BorderSide(color: appColors.errorColor, width: 0.5),
          gapPadding: 10,
        ),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
      ),
      onChanged: onChange,
      onFieldSubmitted: onFieldSubmitted,
      onEditingComplete: onEditingComplete,
      validator: validator,
    );
  }
}
