import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../mixins/resources.dart';

class BaseLeadingAppBar extends StatefulWidget {
  final VoidCallback onTap;
  const BaseLeadingAppBar({super.key, required this.onTap});

  @override
  State<BaseLeadingAppBar> createState() => _BaseLeadingAppBarState();
}

class _BaseLeadingAppBarState extends State<BaseLeadingAppBar> with ResourceApp {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Material(
        color: Theme.of(context).cardColor,
        borderRadius: BorderRadius.circular(6),
        child: InkWell(
          borderRadius: BorderRadius.circular(6),
          splashColor: appColors.secondary,
          onTap: widget.onTap,
          child: const Padding(
            padding: EdgeInsets.all(6),
            child: Icon(
              CupertinoIcons.back,
              size: 18,
            ),
          ),
        ),
      ),
    );
  }
}
