
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AvatarWidget extends StatelessWidget {
  final double radius;
  final String url;

  const AvatarWidget({
    super.key,
    required this.radius,
    required this.url,
  });

  const AvatarWidget.small({
    super.key,
    this.radius = 16,
    required this.url,
  });

  const AvatarWidget.medium({
    super.key,
    this.radius = 22,
    required this.url,
  });

  const AvatarWidget.large({
    super.key,
    this.radius = 60,
    required this.url,
  });

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius,
      backgroundImage: CachedNetworkImageProvider(url),
      backgroundColor: Theme.of(context).cardColor,
    );
  }
}
