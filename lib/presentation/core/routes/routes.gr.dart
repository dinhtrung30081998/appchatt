// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

part of 'routes.dart';

class _$AppRouter extends RootStackRouter {
  _$AppRouter([GlobalKey<NavigatorState>? navigatorKey]) : super(navigatorKey);

  @override
  final Map<String, PageFactory> pagesMap = {
    SplashPageRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const SplashPage(),
      );
    },
    LoginPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const LoginPage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    RegisterStepperPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const RegisterStepperPage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    VerifyPhoneNumberPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const VerifyPhoneNumberPage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    VerifyOtpPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const VerifyOtpPage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    VerifyEmailPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const VerifyEmailPage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    HomePageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const HomePage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    SearchPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const SearchPage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    ChatPageRoute.name: (routeData) {
      final args = routeData.argsAs<ChatPageRouteArgs>(
          orElse: () => const ChatPageRouteArgs());
      return CustomPage<dynamic>(
        routeData: routeData,
        child: ChatPage(
          key: args.key,
          user: args.user,
        ),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    UserInfoPageRoute.name: (routeData) {
      final args = routeData.argsAs<UserInfoPageRouteArgs>();
      return CustomPage<dynamic>(
        routeData: routeData,
        child: UserInfoPage(
          key: args.key,
          uid: args.uid,
          showAppbar: args.showAppbar,
        ),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    PostPageRoute.name: (routeData) {
      return CustomPage<dynamic>(
        routeData: routeData,
        child: const PostPage(),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    AddPostPageRoute.name: (routeData) {
      final args = routeData.argsAs<AddPostPageRouteArgs>(
          orElse: () => const AddPostPageRouteArgs());
      return CustomPage<dynamic>(
        routeData: routeData,
        child: AddPostPage(
          key: args.key,
          listFiles: args.listFiles,
        ),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    CommentsPageRoute.name: (routeData) {
      final args = routeData.argsAs<CommentsPageRouteArgs>();
      return CustomPage<dynamic>(
        routeData: routeData,
        child: CommentsPage(
          key: args.key,
          postEntity: args.postEntity,
        ),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
    EditUserInfoPageRoute.name: (routeData) {
      final args = routeData.argsAs<EditUserInfoPageRouteArgs>();
      return CustomPage<dynamic>(
        routeData: routeData,
        child: EditUserInfoPage(
          key: args.key,
          userEntity: args.userEntity,
        ),
        transitionsBuilder: TransitionsBuilders.fadeIn,
        opaque: true,
        barrierDismissible: false,
      );
    },
  };

  @override
  List<RouteConfig> get routes => [
        RouteConfig(
          SplashPageRoute.name,
          path: '/',
        ),
        RouteConfig(
          LoginPageRoute.name,
          path: '/login-page',
        ),
        RouteConfig(
          RegisterStepperPageRoute.name,
          path: '/register-stepper-page',
        ),
        RouteConfig(
          VerifyPhoneNumberPageRoute.name,
          path: '/verify-phone-number-page',
        ),
        RouteConfig(
          VerifyOtpPageRoute.name,
          path: '/verify-otp-page',
        ),
        RouteConfig(
          VerifyEmailPageRoute.name,
          path: '/verify-email-page',
        ),
        RouteConfig(
          HomePageRoute.name,
          path: '/home-page',
        ),
        RouteConfig(
          SearchPageRoute.name,
          path: '/search-page',
        ),
        RouteConfig(
          ChatPageRoute.name,
          path: '/chat-page',
        ),
        RouteConfig(
          UserInfoPageRoute.name,
          path: '/user-info-page',
        ),
        RouteConfig(
          PostPageRoute.name,
          path: '/post-page',
        ),
        RouteConfig(
          AddPostPageRoute.name,
          path: '/add-post-page',
        ),
        RouteConfig(
          CommentsPageRoute.name,
          path: '/comments-page',
        ),
        RouteConfig(
          EditUserInfoPageRoute.name,
          path: '/edit-user-info-page',
        ),
      ];
}

/// generated route for
/// [SplashPage]
class SplashPageRoute extends PageRouteInfo<void> {
  const SplashPageRoute()
      : super(
          SplashPageRoute.name,
          path: '/',
        );

  static const String name = 'SplashPageRoute';
}

/// generated route for
/// [LoginPage]
class LoginPageRoute extends PageRouteInfo<void> {
  const LoginPageRoute()
      : super(
          LoginPageRoute.name,
          path: '/login-page',
        );

  static const String name = 'LoginPageRoute';
}

/// generated route for
/// [RegisterStepperPage]
class RegisterStepperPageRoute extends PageRouteInfo<void> {
  const RegisterStepperPageRoute()
      : super(
          RegisterStepperPageRoute.name,
          path: '/register-stepper-page',
        );

  static const String name = 'RegisterStepperPageRoute';
}

/// generated route for
/// [VerifyPhoneNumberPage]
class VerifyPhoneNumberPageRoute extends PageRouteInfo<void> {
  const VerifyPhoneNumberPageRoute()
      : super(
          VerifyPhoneNumberPageRoute.name,
          path: '/verify-phone-number-page',
        );

  static const String name = 'VerifyPhoneNumberPageRoute';
}

/// generated route for
/// [VerifyOtpPage]
class VerifyOtpPageRoute extends PageRouteInfo<void> {
  const VerifyOtpPageRoute()
      : super(
          VerifyOtpPageRoute.name,
          path: '/verify-otp-page',
        );

  static const String name = 'VerifyOtpPageRoute';
}

/// generated route for
/// [VerifyEmailPage]
class VerifyEmailPageRoute extends PageRouteInfo<void> {
  const VerifyEmailPageRoute()
      : super(
          VerifyEmailPageRoute.name,
          path: '/verify-email-page',
        );

  static const String name = 'VerifyEmailPageRoute';
}

/// generated route for
/// [HomePage]
class HomePageRoute extends PageRouteInfo<void> {
  const HomePageRoute()
      : super(
          HomePageRoute.name,
          path: '/home-page',
        );

  static const String name = 'HomePageRoute';
}

/// generated route for
/// [SearchPage]
class SearchPageRoute extends PageRouteInfo<void> {
  const SearchPageRoute()
      : super(
          SearchPageRoute.name,
          path: '/search-page',
        );

  static const String name = 'SearchPageRoute';
}

/// generated route for
/// [ChatPage]
class ChatPageRoute extends PageRouteInfo<ChatPageRouteArgs> {
  ChatPageRoute({
    Key? key,
    UserEntity? user,
  }) : super(
          ChatPageRoute.name,
          path: '/chat-page',
          args: ChatPageRouteArgs(
            key: key,
            user: user,
          ),
        );

  static const String name = 'ChatPageRoute';
}

class ChatPageRouteArgs {
  const ChatPageRouteArgs({
    this.key,
    this.user,
  });

  final Key? key;

  final UserEntity? user;

  @override
  String toString() {
    return 'ChatPageRouteArgs{key: $key, user: $user}';
  }
}

/// generated route for
/// [UserInfoPage]
class UserInfoPageRoute extends PageRouteInfo<UserInfoPageRouteArgs> {
  UserInfoPageRoute({
    Key? key,
    required String uid,
    bool? showAppbar,
  }) : super(
          UserInfoPageRoute.name,
          path: '/user-info-page',
          args: UserInfoPageRouteArgs(
            key: key,
            uid: uid,
            showAppbar: showAppbar,
          ),
        );

  static const String name = 'UserInfoPageRoute';
}

class UserInfoPageRouteArgs {
  const UserInfoPageRouteArgs({
    this.key,
    required this.uid,
    this.showAppbar,
  });

  final Key? key;

  final String uid;

  final bool? showAppbar;

  @override
  String toString() {
    return 'UserInfoPageRouteArgs{key: $key, uid: $uid, showAppbar: $showAppbar}';
  }
}

/// generated route for
/// [PostPage]
class PostPageRoute extends PageRouteInfo<void> {
  const PostPageRoute()
      : super(
          PostPageRoute.name,
          path: '/post-page',
        );

  static const String name = 'PostPageRoute';
}

/// generated route for
/// [AddPostPage]
class AddPostPageRoute extends PageRouteInfo<AddPostPageRouteArgs> {
  AddPostPageRoute({
    Key? key,
    List<XFile>? listFiles,
  }) : super(
          AddPostPageRoute.name,
          path: '/add-post-page',
          args: AddPostPageRouteArgs(
            key: key,
            listFiles: listFiles,
          ),
        );

  static const String name = 'AddPostPageRoute';
}

class AddPostPageRouteArgs {
  const AddPostPageRouteArgs({
    this.key,
    this.listFiles,
  });

  final Key? key;

  final List<XFile>? listFiles;

  @override
  String toString() {
    return 'AddPostPageRouteArgs{key: $key, listFiles: $listFiles}';
  }
}

/// generated route for
/// [CommentsPage]
class CommentsPageRoute extends PageRouteInfo<CommentsPageRouteArgs> {
  CommentsPageRoute({
    Key? key,
    required PostEntity postEntity,
  }) : super(
          CommentsPageRoute.name,
          path: '/comments-page',
          args: CommentsPageRouteArgs(
            key: key,
            postEntity: postEntity,
          ),
        );

  static const String name = 'CommentsPageRoute';
}

class CommentsPageRouteArgs {
  const CommentsPageRouteArgs({
    this.key,
    required this.postEntity,
  });

  final Key? key;

  final PostEntity postEntity;

  @override
  String toString() {
    return 'CommentsPageRouteArgs{key: $key, postEntity: $postEntity}';
  }
}

/// generated route for
/// [EditUserInfoPage]
class EditUserInfoPageRoute extends PageRouteInfo<EditUserInfoPageRouteArgs> {
  EditUserInfoPageRoute({
    Key? key,
    required UserEntity userEntity,
  }) : super(
          EditUserInfoPageRoute.name,
          path: '/edit-user-info-page',
          args: EditUserInfoPageRouteArgs(
            key: key,
            userEntity: userEntity,
          ),
        );

  static const String name = 'EditUserInfoPageRoute';
}

class EditUserInfoPageRouteArgs {
  const EditUserInfoPageRouteArgs({
    this.key,
    required this.userEntity,
  });

  final Key? key;

  final UserEntity userEntity;

  @override
  String toString() {
    return 'EditUserInfoPageRouteArgs{key: $key, userEntity: $userEntity}';
  }
}
