import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../domain/entities/post/post_entity.dart';
import '../../../domain/entities/user/user_entity.dart';
import '../../pages/add_post/add_post_page.dart';
import '../../pages/chat/chat_page.dart';
import '../../pages/comments/comments_page.dart';
import '../../pages/edit_user_info/edit_user_info_page.dart';
import '../../pages/home/home_page.dart';
import '../../pages/login/login_page.dart';
import '../../pages/post/post_page.dart';
import '../../pages/register_step/register_stepper_page.dart';
import '../../pages/search/search_page.dart';
import '../../pages/splash/splash_page.dart';
import '../../pages/user_info/user_info_page.dart';
import '../../pages/verify_email/verify_email_page.dart';
import '../../pages/verify_otp.dart/verify_otp_page.dart';
import '../../pages/verify_phone_number/verify_phone_number_page.dart';

part 'routes.gr.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page',
  routes: <AutoRoute>[
    AutoRoute(page: SplashPage, initial: true),
    CustomRoute(page: LoginPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: RegisterStepperPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: VerifyPhoneNumberPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: VerifyOtpPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: VerifyEmailPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: HomePage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: SearchPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: ChatPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: UserInfoPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: PostPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: AddPostPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: CommentsPage, transitionsBuilder: TransitionsBuilders.fadeIn),
    CustomRoute(page: EditUserInfoPage, transitionsBuilder: TransitionsBuilders.fadeIn),
  ],
)
class AppRouter extends _$AppRouter {}
