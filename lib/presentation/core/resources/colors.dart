import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

@singleton
class AppColors {
  final secondary = const Color(0xFF3B76F6);
  final accent = const Color(0xFFD6755B);
  final textLight = const Color(0xFFF5F5F5);
  final enabledBorderLight = const Color(0xFFF5F5F5);
  final enabledBorderDark = const Color(0xFF53585A);
  final textDark = const Color(0xFF53585A);
  final textFaded = const Color(0xFF9899A5);
  final iconLight = const Color(0xFFB1B4C0);
  final iconDark = const Color(0xFFB1B3C1);
  final textHighLight = const Color(0xFF3B76F6);
  final cardLight = const Color(0xFFF9FAFE);
  final cardDark = const Color(0xFF303334);
  final successColor = const Color.fromARGB(255, 3, 241, 67);
  final errorColor = const Color.fromARGB(255, 241, 35, 3);
  final heartColor = const Color.fromARGB(255, 241, 35, 3);
  final cupertinoIndicatorWhite = const Color(0xFFF9FAFE);
  final cupertinoIndicatorBlack = const Color(0xFF303334);
}
