import 'package:flutter/material.dart';

class ThemeColors {
  static const secondary = Color(0xFF3B76F6);
  static const accent = Color(0xFFD6755B);
  static const textLight = Color(0xFFF5F5F5);
  static const textDark = Color(0xFF53585A);
  static const enabledBorderLight = Color(0xFFF5F5F5);
  static const enabledBorderDark = Color(0xFF53585A);
  static const dividerLight = Color(0xFFF5F5F5);
  static const dividerDark = Color(0xFF53585A);
  static const errorBorder = Color.fromARGB(255, 234, 20, 5);
  static const textFaded = Color(0xFF9899A5);
  static const iconLight = Color(0xFFB1B4C0);
  static const iconDark = Color(0xFF53585A);
  static const textHighLight = secondary;
  static const cardLight = Color.fromARGB(255, 219, 220, 223);
  static const cardDark = Color(0xFF303334);
}

abstract class LightColors {
  static const background = Colors.white;
  static const card = ThemeColors.cardLight;
}

abstract class DarkColors {
  static const background = Color(0xFF1B1E1F);
  static const card = ThemeColors.cardDark;
}

abstract class AppTheme {
  static const accentColor = ThemeColors.accent;
  static final visualDensity = VisualDensity.adaptivePlatformDensity;

  static ThemeData light() => ThemeData(
        fontFamily: 'Mulish',
        brightness: Brightness.dark,
        visualDensity: visualDensity,
        backgroundColor: LightColors.background,
        scaffoldBackgroundColor: LightColors.background,
        cardColor: LightColors.card,
        primaryTextTheme: const TextTheme(
          titleLarge: TextStyle(
            color: ThemeColors.textDark,
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(6),
            borderSide: const BorderSide(color: ThemeColors.enabledBorderDark, width: 0.5),
            gapPadding: 10,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(6),
            borderSide: const BorderSide(color: ThemeColors.secondary, width: 0.5),
            gapPadding: 10,
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(6),
            borderSide: const BorderSide(color: ThemeColors.errorBorder, width: 0.5),
            gapPadding: 10,
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(6),
            borderSide: const BorderSide(color: ThemeColors.errorBorder, width: 0.5),
            gapPadding: 10,
          ),
        ),
        iconTheme: const IconThemeData(color: ThemeColors.iconDark),
        dividerTheme: const DividerThemeData(color: ThemeColors.dividerDark),
        appBarTheme: AppBarTheme(
          iconTheme: const IconThemeData(color: ThemeColors.iconDark),
          toolbarTextStyle: const TextTheme(
            titleLarge: TextStyle(
              color: ThemeColors.textDark,
            ),
          ).bodyMedium,
          titleTextStyle: const TextTheme(
            titleLarge: TextStyle(
              color: ThemeColors.textDark,
            ),
          ).titleLarge,
        ),
      );

  static ThemeData dark() => ThemeData(
        fontFamily: 'Mulish',
        brightness: Brightness.dark,
        visualDensity: visualDensity,
        backgroundColor: DarkColors.background,
        scaffoldBackgroundColor: DarkColors.background,
        cardColor: DarkColors.card,
        primaryTextTheme: const TextTheme(
          titleLarge: TextStyle(
            color: ThemeColors.textLight,
          ),
        ),
        iconTheme: const IconThemeData(color: ThemeColors.iconLight),
        dividerTheme: const DividerThemeData(color: ThemeColors.dividerLight),
        appBarTheme: AppBarTheme(
          iconTheme: const IconThemeData(color: ThemeColors.iconLight),
          toolbarTextStyle: const TextTheme(
            titleLarge: TextStyle(
              color: ThemeColors.textLight,
            ),
          ).bodyMedium,
          titleTextStyle: const TextTheme(
            titleLarge: TextStyle(
              color: ThemeColors.textLight,
            ),
          ).titleLarge,
        ),
      );
}
