import 'package:flutter/material.dart';

import '../../../infrastructure/core/di/infrastructure_injection.dart';
import '../resources/colors.dart';

void showSnackBar(BuildContext context, String message) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      backgroundColor: injector<AppColors>().secondary,
      content: Text(
        message,
        style: TextStyle(
          fontWeight: FontWeight.w500,
          fontFamily: 'Mulish',
          color: injector<AppColors>().textLight,
        ),
      ),
      duration: const Duration(milliseconds: 1800),
    ),
  );
}
