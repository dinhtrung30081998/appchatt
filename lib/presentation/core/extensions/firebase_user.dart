import 'package:app_chat/domain/value_objects/auth/user_unique_id.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../../domain/entities/user/user_entity.dart';

extension FirebaseUserDomainX on FirebaseAuth {
  UserOnlyUniqueIDEntity toDomain() {
    return UserOnlyUniqueIDEntity(
      uid: UserUniqueID.fromUniqueString(
        currentUser!.uid,
      ),
    );
  }
}

extension FirebaseFireStoreX on FirebaseFirestore {
  Future<DocumentReference> userCollection() async {
    return FirebaseFirestore.instance.collection('user').doc(FirebaseAuth.instance.toDomain().uid.getValueOrCrash());
  }
}
