import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/user_info/user_info_failure.dart';
import '../../../domain/entities/comment/comment_entity.dart';
import '../../../domain/entities/post/post_entity.dart';
import '../../../infrastructure/repositories/auth/action_camera.dart';
import '../../../infrastructure/repositories/post/comment_post_impl.dart';
import '../../../infrastructure/repositories/post/create_post_impl.dart';
import '../../../infrastructure/repositories/post/delete_post_impl.dart';
import '../../../infrastructure/repositories/post/hide_post_impl.dart';
import '../../../infrastructure/repositories/post/like_comment_impl.dart';
import '../../../infrastructure/repositories/post/like_post_impl.dart';
import '../../../infrastructure/repositories/post/show_post_impl.dart';
import '../../../infrastructure/repositories/post/stream_list_comment_by_post_id_impl.dart';
import '../../../infrastructure/repositories/post/stream_list_post_impl.dart';
import '../../../infrastructure/repositories/user_infor/upload_post_image_to_storage_impl.dart';
import '../../../presentation/core/utils/utils.dart';

part 'post_store.freezed.dart';

@freezed
class ActionState with _$ActionState {
  const factory ActionState.initial() = CamInitial;
  const factory ActionState.actionLoading() = ActionLoading;
  const factory ActionState.actionDone() = ActionDone;
  const factory ActionState.actionError() = ActionError;
}

@freezed
class PostState with _$PostState {
  const factory PostState.postInitial() = PostInitial;
  const factory PostState.postLoading() = PostLoading;
  const factory PostState.postDone() = PostDone;
  const factory PostState.postError() = PostError;
}

@injectable
class PostStore extends ChangeNotifier {
  final ActionCameraImpl actionCameraImpl;
  final CreatePostImpl createPostImpl;
  final ICommentPostImpl commentPostImpl;
  final GetListPostImpl getListPostImpl;
  final ILikePostImpl likePostImpl;
  final ILikeCommentImpl likeCommentImpl;
  final IDeletePostImpl deletePostImpl;
  final IHidePostImpl hidePostImpl;
  final IShowPostImpl showPostImpl;
  final IGetListCommentByPostIdImpl listCommentByPostIdImpl;
  final IUploadPostImageToStorageImpl uploadPostImageToStorageImpl;

  PostStore(
    this.actionCameraImpl,
    this.createPostImpl,
    this.getListPostImpl,
    this.likePostImpl,
    this.commentPostImpl,
    this.listCommentByPostIdImpl,
    this.likeCommentImpl,
    this.deletePostImpl,
    this.uploadPostImageToStorageImpl,
    this.hidePostImpl,
    this.showPostImpl,
  );

  ActionState _actionCameraState = const ActionState.initial();
  ActionState get actionCameraState => _actionCameraState;

  String _imagePath = '';
  String get imagePath => _imagePath;

  void _setStateCamera(ActionState state) {
    _actionCameraState = state;
    notifyListeners();
  }

  PostState _postState = const PostState.postInitial();
  PostState get postState => _postState;

  void _setStatePost(PostState state) {
    _postState = state;
    notifyListeners();
  }

  Future<Unit> createPost(
    String fullName,
    String description,
    List<XFile> listPostImageUrl,
    String profileImage,
    BuildContext context,
  ) async {
    if (description.isNotEmpty) {
      _setStatePost(const PostState.postLoading());
      List<String> listPostImageUrlToString = [];

      String imageUrl = '';
      for (var image in listPostImageUrl) {
        imageUrl = await uploadPostImageToStorage('postImages', image, true);
        listPostImageUrlToString.add(imageUrl.toString());
      }

      if (listPostImageUrlToString.isNotEmpty) {
        final post = await createPostImpl.call(fullName, description, listPostImageUrlToString, profileImage);

        post.fold(
          (l) {
            _setStatePost(const PostState.postError());
            showSnackBar(context, _mapFailureToMessage(l));
          },
          (r) {
            _setStatePost(const PostState.postDone());
            context.router.pop();
          },
        );
      }
    }

    return Future.value(unit);
  }

  Future<String> uploadPostImageToStorage(String childName, XFile file, bool isPost) async {
    String imageUrl = await uploadPostImageToStorageImpl.call(childName, file, isPost);
    return imageUrl;
  }

  //Length Comments By Post Id
  final ValueNotifier<int> _countLengthCommentsValid = ValueNotifier(0);
  ValueNotifier<int> get countLengthCommentsValid => _countLengthCommentsValid;

  void countCommentsChanged(int value) {
    _countLengthCommentsValid.value = value;
  }

  Stream<List<PostEntity>> getStreamListPost() => getListPostImpl.call();

  Stream<List<CommentEntity>> getStreamListCommentByPostId(String postId) => listCommentByPostIdImpl.call(postId);

  Future<void> actionGallery() async {
    _setStateCamera(const ActionState.actionLoading());

    final action = await actionCameraImpl.call(ImageSource.gallery, 'posts');

    action.fold(
      (l) {
        _setStateCamera(const ActionState.actionError());
      },
      (r) {
        _setStateCamera(const ActionState.actionDone());
        _imagePath = r;
      },
    );
  }

  Future<void> actionCamera() async {
    _setStateCamera(const ActionState.actionLoading());

    final action = await actionCameraImpl.call(ImageSource.camera, 'posts');

    action.fold(
      (l) {
        _setStateCamera(const ActionState.actionError());
      },
      (r) {
        _setStateCamera(const ActionState.actionDone());
        _imagePath = r;
      },
    );
  }

  Future<Unit> deletePost(String postId) async {
    return await deletePostImpl.call(postId);
  }

  Future<Unit> hidePost(String postId) async {
    return await hidePostImpl.call(postId);
  }

  Future<Unit> showPost(String postId) async {
    return await showPostImpl.call(postId);
  }

  Future<Unit> likePost(String postId, String userId, List likes) async {
    return await likePostImpl.call(postId, userId, likes);
  }

  Future<Unit> likeComment(String postId, String commentId, String userId, List likes) async {
    return await likeCommentImpl.call(postId, commentId, userId, likes);
  }

  Future<Unit> commentPost(
    String postId,
    String comment,
    String userId,
    String userName,
    String profilePicture,
  ) async {
    return await commentPostImpl.call(postId, comment, userId, userName, profilePicture);
  }

  String _mapFailureToMessage(CreatePostFailure createPostFailure) {
    return createPostFailure.map(
      createPostOffline: (_) => 'Mất kết nối internet, vui lòng kiểm tra đường truyền',
      createPostFail: (_) => 'Không tạo được bài viết, hãy thử lại',
    );
  }
}
