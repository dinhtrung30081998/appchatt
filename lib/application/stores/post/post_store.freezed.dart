// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'post_store.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ActionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActionStateCopyWith<$Res> {
  factory $ActionStateCopyWith(
          ActionState value, $Res Function(ActionState) then) =
      _$ActionStateCopyWithImpl<$Res, ActionState>;
}

/// @nodoc
class _$ActionStateCopyWithImpl<$Res, $Val extends ActionState>
    implements $ActionStateCopyWith<$Res> {
  _$ActionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CamInitialCopyWith<$Res> {
  factory _$$CamInitialCopyWith(
          _$CamInitial value, $Res Function(_$CamInitial) then) =
      __$$CamInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CamInitialCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$CamInitial>
    implements _$$CamInitialCopyWith<$Res> {
  __$$CamInitialCopyWithImpl(
      _$CamInitial _value, $Res Function(_$CamInitial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CamInitial implements CamInitial {
  const _$CamInitial();

  @override
  String toString() {
    return 'ActionState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CamInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class CamInitial implements ActionState {
  const factory CamInitial() = _$CamInitial;
}

/// @nodoc
abstract class _$$ActionLoadingCopyWith<$Res> {
  factory _$$ActionLoadingCopyWith(
          _$ActionLoading value, $Res Function(_$ActionLoading) then) =
      __$$ActionLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionLoadingCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$ActionLoading>
    implements _$$ActionLoadingCopyWith<$Res> {
  __$$ActionLoadingCopyWithImpl(
      _$ActionLoading _value, $Res Function(_$ActionLoading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionLoading implements ActionLoading {
  const _$ActionLoading();

  @override
  String toString() {
    return 'ActionState.actionLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionLoading != null) {
      return actionLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionLoading != null) {
      return actionLoading(this);
    }
    return orElse();
  }
}

abstract class ActionLoading implements ActionState {
  const factory ActionLoading() = _$ActionLoading;
}

/// @nodoc
abstract class _$$ActionDoneCopyWith<$Res> {
  factory _$$ActionDoneCopyWith(
          _$ActionDone value, $Res Function(_$ActionDone) then) =
      __$$ActionDoneCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionDoneCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$ActionDone>
    implements _$$ActionDoneCopyWith<$Res> {
  __$$ActionDoneCopyWithImpl(
      _$ActionDone _value, $Res Function(_$ActionDone) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionDone implements ActionDone {
  const _$ActionDone();

  @override
  String toString() {
    return 'ActionState.actionDone()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionDone);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionDone();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionDone?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionDone != null) {
      return actionDone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionDone != null) {
      return actionDone(this);
    }
    return orElse();
  }
}

abstract class ActionDone implements ActionState {
  const factory ActionDone() = _$ActionDone;
}

/// @nodoc
abstract class _$$ActionErrorCopyWith<$Res> {
  factory _$$ActionErrorCopyWith(
          _$ActionError value, $Res Function(_$ActionError) then) =
      __$$ActionErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionErrorCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$ActionError>
    implements _$$ActionErrorCopyWith<$Res> {
  __$$ActionErrorCopyWithImpl(
      _$ActionError _value, $Res Function(_$ActionError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionError implements ActionError {
  const _$ActionError();

  @override
  String toString() {
    return 'ActionState.actionError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionError != null) {
      return actionError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionError != null) {
      return actionError(this);
    }
    return orElse();
  }
}

abstract class ActionError implements ActionState {
  const factory ActionError() = _$ActionError;
}

/// @nodoc
mixin _$PostState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() postInitial,
    required TResult Function() postLoading,
    required TResult Function() postDone,
    required TResult Function() postError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? postInitial,
    TResult? Function()? postLoading,
    TResult? Function()? postDone,
    TResult? Function()? postError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? postInitial,
    TResult Function()? postLoading,
    TResult Function()? postDone,
    TResult Function()? postError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostInitial value) postInitial,
    required TResult Function(PostLoading value) postLoading,
    required TResult Function(PostDone value) postDone,
    required TResult Function(PostError value) postError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PostInitial value)? postInitial,
    TResult? Function(PostLoading value)? postLoading,
    TResult? Function(PostDone value)? postDone,
    TResult? Function(PostError value)? postError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostInitial value)? postInitial,
    TResult Function(PostLoading value)? postLoading,
    TResult Function(PostDone value)? postDone,
    TResult Function(PostError value)? postError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostStateCopyWith<$Res> {
  factory $PostStateCopyWith(PostState value, $Res Function(PostState) then) =
      _$PostStateCopyWithImpl<$Res, PostState>;
}

/// @nodoc
class _$PostStateCopyWithImpl<$Res, $Val extends PostState>
    implements $PostStateCopyWith<$Res> {
  _$PostStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$PostInitialCopyWith<$Res> {
  factory _$$PostInitialCopyWith(
          _$PostInitial value, $Res Function(_$PostInitial) then) =
      __$$PostInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PostInitialCopyWithImpl<$Res>
    extends _$PostStateCopyWithImpl<$Res, _$PostInitial>
    implements _$$PostInitialCopyWith<$Res> {
  __$$PostInitialCopyWithImpl(
      _$PostInitial _value, $Res Function(_$PostInitial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PostInitial implements PostInitial {
  const _$PostInitial();

  @override
  String toString() {
    return 'PostState.postInitial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PostInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() postInitial,
    required TResult Function() postLoading,
    required TResult Function() postDone,
    required TResult Function() postError,
  }) {
    return postInitial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? postInitial,
    TResult? Function()? postLoading,
    TResult? Function()? postDone,
    TResult? Function()? postError,
  }) {
    return postInitial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? postInitial,
    TResult Function()? postLoading,
    TResult Function()? postDone,
    TResult Function()? postError,
    required TResult orElse(),
  }) {
    if (postInitial != null) {
      return postInitial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostInitial value) postInitial,
    required TResult Function(PostLoading value) postLoading,
    required TResult Function(PostDone value) postDone,
    required TResult Function(PostError value) postError,
  }) {
    return postInitial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PostInitial value)? postInitial,
    TResult? Function(PostLoading value)? postLoading,
    TResult? Function(PostDone value)? postDone,
    TResult? Function(PostError value)? postError,
  }) {
    return postInitial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostInitial value)? postInitial,
    TResult Function(PostLoading value)? postLoading,
    TResult Function(PostDone value)? postDone,
    TResult Function(PostError value)? postError,
    required TResult orElse(),
  }) {
    if (postInitial != null) {
      return postInitial(this);
    }
    return orElse();
  }
}

abstract class PostInitial implements PostState {
  const factory PostInitial() = _$PostInitial;
}

/// @nodoc
abstract class _$$PostLoadingCopyWith<$Res> {
  factory _$$PostLoadingCopyWith(
          _$PostLoading value, $Res Function(_$PostLoading) then) =
      __$$PostLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PostLoadingCopyWithImpl<$Res>
    extends _$PostStateCopyWithImpl<$Res, _$PostLoading>
    implements _$$PostLoadingCopyWith<$Res> {
  __$$PostLoadingCopyWithImpl(
      _$PostLoading _value, $Res Function(_$PostLoading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PostLoading implements PostLoading {
  const _$PostLoading();

  @override
  String toString() {
    return 'PostState.postLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PostLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() postInitial,
    required TResult Function() postLoading,
    required TResult Function() postDone,
    required TResult Function() postError,
  }) {
    return postLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? postInitial,
    TResult? Function()? postLoading,
    TResult? Function()? postDone,
    TResult? Function()? postError,
  }) {
    return postLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? postInitial,
    TResult Function()? postLoading,
    TResult Function()? postDone,
    TResult Function()? postError,
    required TResult orElse(),
  }) {
    if (postLoading != null) {
      return postLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostInitial value) postInitial,
    required TResult Function(PostLoading value) postLoading,
    required TResult Function(PostDone value) postDone,
    required TResult Function(PostError value) postError,
  }) {
    return postLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PostInitial value)? postInitial,
    TResult? Function(PostLoading value)? postLoading,
    TResult? Function(PostDone value)? postDone,
    TResult? Function(PostError value)? postError,
  }) {
    return postLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostInitial value)? postInitial,
    TResult Function(PostLoading value)? postLoading,
    TResult Function(PostDone value)? postDone,
    TResult Function(PostError value)? postError,
    required TResult orElse(),
  }) {
    if (postLoading != null) {
      return postLoading(this);
    }
    return orElse();
  }
}

abstract class PostLoading implements PostState {
  const factory PostLoading() = _$PostLoading;
}

/// @nodoc
abstract class _$$PostDoneCopyWith<$Res> {
  factory _$$PostDoneCopyWith(
          _$PostDone value, $Res Function(_$PostDone) then) =
      __$$PostDoneCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PostDoneCopyWithImpl<$Res>
    extends _$PostStateCopyWithImpl<$Res, _$PostDone>
    implements _$$PostDoneCopyWith<$Res> {
  __$$PostDoneCopyWithImpl(_$PostDone _value, $Res Function(_$PostDone) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PostDone implements PostDone {
  const _$PostDone();

  @override
  String toString() {
    return 'PostState.postDone()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PostDone);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() postInitial,
    required TResult Function() postLoading,
    required TResult Function() postDone,
    required TResult Function() postError,
  }) {
    return postDone();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? postInitial,
    TResult? Function()? postLoading,
    TResult? Function()? postDone,
    TResult? Function()? postError,
  }) {
    return postDone?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? postInitial,
    TResult Function()? postLoading,
    TResult Function()? postDone,
    TResult Function()? postError,
    required TResult orElse(),
  }) {
    if (postDone != null) {
      return postDone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostInitial value) postInitial,
    required TResult Function(PostLoading value) postLoading,
    required TResult Function(PostDone value) postDone,
    required TResult Function(PostError value) postError,
  }) {
    return postDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PostInitial value)? postInitial,
    TResult? Function(PostLoading value)? postLoading,
    TResult? Function(PostDone value)? postDone,
    TResult? Function(PostError value)? postError,
  }) {
    return postDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostInitial value)? postInitial,
    TResult Function(PostLoading value)? postLoading,
    TResult Function(PostDone value)? postDone,
    TResult Function(PostError value)? postError,
    required TResult orElse(),
  }) {
    if (postDone != null) {
      return postDone(this);
    }
    return orElse();
  }
}

abstract class PostDone implements PostState {
  const factory PostDone() = _$PostDone;
}

/// @nodoc
abstract class _$$PostErrorCopyWith<$Res> {
  factory _$$PostErrorCopyWith(
          _$PostError value, $Res Function(_$PostError) then) =
      __$$PostErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PostErrorCopyWithImpl<$Res>
    extends _$PostStateCopyWithImpl<$Res, _$PostError>
    implements _$$PostErrorCopyWith<$Res> {
  __$$PostErrorCopyWithImpl(
      _$PostError _value, $Res Function(_$PostError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PostError implements PostError {
  const _$PostError();

  @override
  String toString() {
    return 'PostState.postError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PostError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() postInitial,
    required TResult Function() postLoading,
    required TResult Function() postDone,
    required TResult Function() postError,
  }) {
    return postError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? postInitial,
    TResult? Function()? postLoading,
    TResult? Function()? postDone,
    TResult? Function()? postError,
  }) {
    return postError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? postInitial,
    TResult Function()? postLoading,
    TResult Function()? postDone,
    TResult Function()? postError,
    required TResult orElse(),
  }) {
    if (postError != null) {
      return postError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostInitial value) postInitial,
    required TResult Function(PostLoading value) postLoading,
    required TResult Function(PostDone value) postDone,
    required TResult Function(PostError value) postError,
  }) {
    return postError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PostInitial value)? postInitial,
    TResult? Function(PostLoading value)? postLoading,
    TResult? Function(PostDone value)? postDone,
    TResult? Function(PostError value)? postError,
  }) {
    return postError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostInitial value)? postInitial,
    TResult Function(PostLoading value)? postLoading,
    TResult Function(PostDone value)? postDone,
    TResult Function(PostError value)? postError,
    required TResult orElse(),
  }) {
    if (postError != null) {
      return postError(this);
    }
    return orElse();
  }
}

abstract class PostError implements PostState {
  const factory PostError() = _$PostError;
}
