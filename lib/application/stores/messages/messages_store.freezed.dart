// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'messages_store.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ActionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActionStateCopyWith<$Res> {
  factory $ActionStateCopyWith(
          ActionState value, $Res Function(ActionState) then) =
      _$ActionStateCopyWithImpl<$Res, ActionState>;
}

/// @nodoc
class _$ActionStateCopyWithImpl<$Res, $Val extends ActionState>
    implements $ActionStateCopyWith<$Res> {
  _$ActionStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CamInitialCopyWith<$Res> {
  factory _$$CamInitialCopyWith(
          _$CamInitial value, $Res Function(_$CamInitial) then) =
      __$$CamInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CamInitialCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$CamInitial>
    implements _$$CamInitialCopyWith<$Res> {
  __$$CamInitialCopyWithImpl(
      _$CamInitial _value, $Res Function(_$CamInitial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CamInitial implements CamInitial {
  const _$CamInitial();

  @override
  String toString() {
    return 'ActionState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CamInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class CamInitial implements ActionState {
  const factory CamInitial() = _$CamInitial;
}

/// @nodoc
abstract class _$$ActionLoadingCopyWith<$Res> {
  factory _$$ActionLoadingCopyWith(
          _$ActionLoading value, $Res Function(_$ActionLoading) then) =
      __$$ActionLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionLoadingCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$ActionLoading>
    implements _$$ActionLoadingCopyWith<$Res> {
  __$$ActionLoadingCopyWithImpl(
      _$ActionLoading _value, $Res Function(_$ActionLoading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionLoading implements ActionLoading {
  const _$ActionLoading();

  @override
  String toString() {
    return 'ActionState.actionLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionLoading != null) {
      return actionLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionLoading != null) {
      return actionLoading(this);
    }
    return orElse();
  }
}

abstract class ActionLoading implements ActionState {
  const factory ActionLoading() = _$ActionLoading;
}

/// @nodoc
abstract class _$$ActionDoneCopyWith<$Res> {
  factory _$$ActionDoneCopyWith(
          _$ActionDone value, $Res Function(_$ActionDone) then) =
      __$$ActionDoneCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionDoneCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$ActionDone>
    implements _$$ActionDoneCopyWith<$Res> {
  __$$ActionDoneCopyWithImpl(
      _$ActionDone _value, $Res Function(_$ActionDone) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionDone implements ActionDone {
  const _$ActionDone();

  @override
  String toString() {
    return 'ActionState.actionDone()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionDone);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionDone();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionDone?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionDone != null) {
      return actionDone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionDone != null) {
      return actionDone(this);
    }
    return orElse();
  }
}

abstract class ActionDone implements ActionState {
  const factory ActionDone() = _$ActionDone;
}

/// @nodoc
abstract class _$$ActionErrorCopyWith<$Res> {
  factory _$$ActionErrorCopyWith(
          _$ActionError value, $Res Function(_$ActionError) then) =
      __$$ActionErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionErrorCopyWithImpl<$Res>
    extends _$ActionStateCopyWithImpl<$Res, _$ActionError>
    implements _$$ActionErrorCopyWith<$Res> {
  __$$ActionErrorCopyWithImpl(
      _$ActionError _value, $Res Function(_$ActionError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionError implements ActionError {
  const _$ActionError();

  @override
  String toString() {
    return 'ActionState.actionError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionError != null) {
      return actionError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CamInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CamInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CamInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionError != null) {
      return actionError(this);
    }
    return orElse();
  }
}

abstract class ActionError implements ActionState {
  const factory ActionError() = _$ActionError;
}
