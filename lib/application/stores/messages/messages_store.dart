import 'package:app_chat/domain/entities/user/user_entity.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';

import '../../../infrastructure/repositories/chat/out_room_chat_impl.dart';
import '../../../infrastructure/repositories/chat/send_chat_impl.dart';
import '../../../infrastructure/repositories/chat/stream_list_chat_impl.dart';
import '../../../infrastructure/repositories/messages/delete_messages_impl.dart';
import '../../../infrastructure/repositories/messages/read_last_messages_impl.dart';
import '../../../infrastructure/repositories/messages/stream_list_messages_impl.dart';
import '../../../infrastructure/repositories/messages/upload_image_impl.dart';

part 'messages_store.freezed.dart';

@freezed
class ActionState with _$ActionState {
  const factory ActionState.initial() = CamInitial;
  const factory ActionState.actionLoading() = ActionLoading;
  const factory ActionState.actionDone() = ActionDone;
  const factory ActionState.actionError() = ActionError;
}

@injectable
class MessagesStore extends ChangeNotifier {
  final IStreamListMessagesImpl streamListMessagesImpl;
  final IStreamListChatImpl streamListChatImpl;
  final IReadLastMessagesImpl readLastMessagesImpl;
  final ISendChatImpl sendChatImpl;
  final IDeleteMessageByIdImpl deleteMessageByIdImpl;
  final IOutRoomChatImpl outRoomChatImpl;
  final IUploadImageImpl uploadImageImpl;

  MessagesStore(
    this.readLastMessagesImpl,
    this.streamListChatImpl,
    this.sendChatImpl,
    this.deleteMessageByIdImpl,
    this.outRoomChatImpl,
    this.uploadImageImpl, {
    required this.streamListMessagesImpl,
  });

  Stream<DatabaseEvent> streamListMessages() => streamListMessagesImpl.call();

  Stream<DatabaseEvent> streamListChat() => streamListChatImpl.call();

  Future<Unit> readLastMessages(String partnerId) async {
    return await readLastMessagesImpl.call(partnerId);
  }

  Future<Unit> sendChat(
    UserEntity userPartner,
    String message,
    String type,
  ) async {
    return await sendChatImpl.call(userPartner, message, type);
  }

  Future<Unit> deleteMessage(
    String chatId,
  ) async {
    return await deleteMessageByIdImpl.call(chatId);
  }

  Future<Unit> outRoomChat(String partnerId) async {
    return await outRoomChatImpl.call(partnerId);
  }

  XFile? xFile;
  Future<Unit> getImage(
    UserEntity userPartner,
    String type,
  ) async {
    ImagePicker imagePicker = ImagePicker();

    await imagePicker.pickImage(source: ImageSource.gallery).then((xFile) {
      if (xFile != null) {
        this.xFile = XFile(xFile.path);
        uploadImageImpl.call(xFile).then((imageUrl) => sendChat(userPartner, imageUrl, type));
      }
    });
    return Future.value(unit);
  }
}
