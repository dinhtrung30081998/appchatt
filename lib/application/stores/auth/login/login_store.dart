import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/core/failures/auth/auth_failure.dart';
import '../../../../domain/value_objects/auth/email_address.dart';
import '../../../../domain/value_objects/auth/password.dart';
import '../../../../domain/value_objects/auth/phone_number.dart';
import '../../../../infrastructure/repositories/auth/get_firebase_messaging_token_impl.dart';
import '../../../../infrastructure/repositories/auth/sign_in_with_email.dart';
import '../../../../infrastructure/repositories/auth/sign_in_with_facebook.dart';
import '../../../../infrastructure/repositories/auth/sign_in_with_google.dart';
import '../../../../infrastructure/repositories/user_infor/cache_user_info_impl.dart';
import '../../../../presentation/core/utils/utils.dart';

part 'login_store.freezed.dart';

@freezed
class LoginState with _$LoginState {
  const factory LoginState.initial() = Initial;
  const factory LoginState.loading() = Loading;
  const factory LoginState.done() = Done;
  const factory LoginState.error() = Error;
}

@freezed
class GoogleAuthState with _$GoogleAuthState {
  const factory GoogleAuthState.initial() = GGInitial;
  const factory GoogleAuthState.loading() = GGLoading;
  const factory GoogleAuthState.done() = GGDone;
  const factory GoogleAuthState.error() = GGError;
}

@injectable
class LoginStore extends ChangeNotifier {
  final SignInWithEmailAndPasswordImpl signInWithEmailAndPasswordImpl;
  final SignInWithGoogleImpl signInWithGoogleImpl;
  final SignInWithFacebookImpl signInWithFacebookImpl;
  final ICacheUserInfoImpl cacheUserInfoImpl;
  final IGetFirebaseMessagingTokenImpl getFirebaseMessagingTokenImpl;

  LoginStore(
    this.signInWithEmailAndPasswordImpl,
    this.signInWithGoogleImpl,
    this.signInWithFacebookImpl,
    this.cacheUserInfoImpl,
    this.getFirebaseMessagingTokenImpl,
  );

  LoginState _loginState = const LoginState.initial();
  LoginState get loginState => _loginState;

  GoogleAuthState _googleAuthState = const GoogleAuthState.initial();
  GoogleAuthState get googleAthState => _googleAuthState;

  String _errorMessage = '';
  String get errorMessage => _errorMessage;

  void _setState(LoginState state) {
    _loginState = state;
    notifyListeners();
  }

  void _setStateSignInGoogle(GoogleAuthState state) {
    _googleAuthState = state;
    notifyListeners();
  }

  //Email Changed
  final ValueNotifier<bool> _emailValid = ValueNotifier(false);
  ValueNotifier<bool> get emailValid => _emailValid;

  EmailAddress _email = EmailAddress('');
  EmailAddress get email => _email;

  void emailChanged(String str) {
    _email = EmailAddress(str);
    _emailValid.value = _email.isRight();
  }

  //Password Changed
  final ValueNotifier<bool> _passwordValid = ValueNotifier(false);
  ValueNotifier<bool> get passwordValid => _passwordValid;

  Password _password = Password('');
  Password get password => _password;

  void passwordChanged(String str) {
    _password = Password(str);
    _passwordValid.value = _password.isRight();
  }

  //Password Changed
  final ValueNotifier<bool> _phoneNumberValid = ValueNotifier(false);
  ValueNotifier<bool> get phoneNumberValid => _phoneNumberValid;

  PhoneNumber _phoneNumber = PhoneNumber('');
  PhoneNumber get phoneNumber => _phoneNumber;

  void phoneNumberChanged(String str) {
    _phoneNumber = PhoneNumber(str);
    _phoneNumberValid.value = _phoneNumber.isRight();
  }

  void signInWithEmailAndPassword({
    required EmailAddress email,
    required Password password,
    required BuildContext context,
  }) async {
    final isEmailValid = email.isRight();
    final isPasswordValid = password.isRight();

    if (isEmailValid && isPasswordValid) {
      _setState(const LoginState.loading());

      final signIn = await signInWithEmailAndPasswordImpl.call(email: email, password: password);
      final token = await getToken();

      signIn.fold(
        (failure) {
          _setState(const LoginState.error());
          showSnackBar(context, _mapFailureToMessage(failure));
        },
        (r) {
          getUserDetail(email, token);
        },
      );
    }
  }

  Future<Unit> getUserDetail(EmailAddress emailAddress, String token) async {
    return await cacheUserInfoImpl.call(emailAddress, token);
  }

  Future<String> getToken() async {
    return await getFirebaseMessagingTokenImpl.call();
  }

  void signInWithGoogle({required BuildContext context}) async {
    _setStateSignInGoogle(const GoogleAuthState.loading());
    final token = await getToken();

    final signInWGoogle = await signInWithGoogleImpl.call(token);

    signInWGoogle.fold(
      (failure) {
        _setStateSignInGoogle(const GoogleAuthState.error());
        showSnackBar(context, _mapFailureToMessageGoogle(failure));
      },
      (r) {},
    );
  }

  void signInWithFacebook({required BuildContext context}) async {
    await signInWithFacebookImpl.call().then(
          (value) => value.fold(
            (failure) {
              _setState(const LoginState.error());
              showSnackBar(context, _mapFailureToMessageFacebook(failure));
            },
            (r) {
              _setState(const LoginState.done());
            },
          ),
        );
  }

  String _mapFailureToMessageFacebook(FacebookAuthFailure facebookAuthFailure) {
    return _errorMessage = facebookAuthFailure.map(
      cancelled: (_) => 'Người dùng đã huỷ',
      serverError: (_) => 'Lỗi máy chủ, vui lòng thử lại',
      failed: (_) => 'Không thành công, vui lòng thử lại',
      operationInProgress: (_) => 'Đang hoạt động',
      offline: (_) => 'Mất kết nối internet, vui lòng kiểm tra đường truyền',
    );
  }

  String _mapFailureToMessageGoogle(GoogleAuthFailure googleAuthFailure) {
    return _errorMessage = googleAuthFailure.map(
      cancelledByUser: (_) => 'Người dùng đã huỷ',
      serverError: (_) => 'Lỗi máy chủ, vui lòng thử lại',
      emailAlreadyInUse: (_) => 'Email đã được sử dụng',
      invalidEmailAndPassword: (_) => 'Email và mật khẩu không hợp lệ',
      offline: (_) => 'Mất kết nối internet, vui lòng kiểm tra đường truyền',
      accountExistsWithDifferentCredential: (_) => 'Tài khoản tồn tại với thông tin xác thực khác',
    );
  }

  String _mapFailureToMessage(AuthFailure authFailure) {
    return _errorMessage = authFailure.map(
      cancelledByUser: (_) => 'Người dùng đã huỷ',
      serverError: (_) => 'Lỗi máy chủ, vui lòng thử lại',
      emailAlreadyInUse: (_) => 'Email đã được sử dụng',
      invalidEmailAndPassword: (_) => 'Email và mật khẩu không hợp lệ',
      offline: (_) => 'Mất kết nối internet, vui lòng kiểm tra đường truyền',
    );
  }
}
