import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../../../domain/core/failures/auth/auth_failure.dart';
import '../../../../../domain/value_objects/auth/phone_number.dart';
import '../../../../../infrastructure/repositories/auth/verify_phone_number.dart';
import '../../../../../presentation/core/utils/utils.dart';

part 'verify_phone_number_store.freezed.dart';

@freezed
class VerifyPhoneNumberAuthState with _$VerifyPhoneNumberAuthState {
  const factory VerifyPhoneNumberAuthState.initial() = Initial;
  const factory VerifyPhoneNumberAuthState.loading() = Loading;
  const factory VerifyPhoneNumberAuthState.done() = Done;
  const factory VerifyPhoneNumberAuthState.error() = Error;
}

@injectable
class VerifyPhoneNumberStore extends ChangeNotifier {
  final VerifyPhoneNumberImpl _verifyPhoneNumberImpl;

  VerifyPhoneNumberStore(this._verifyPhoneNumberImpl);

  VerifyPhoneNumberAuthState _verifyPhoneNumberState = const VerifyPhoneNumberAuthState.initial();
  VerifyPhoneNumberAuthState get verifyPhoneNumberState => _verifyPhoneNumberState;

  String _errorMessage = '';
  String get errorMessage => _errorMessage;

  String _phoneNumberStr = '';
  String get phoneNumberStr => _phoneNumberStr;

  set setPhoneNumStr(String phoneNumber) {
    _phoneNumberStr = phoneNumber;
    return;
  }

  void _setState(VerifyPhoneNumberAuthState state) {
    _verifyPhoneNumberState = state;
    notifyListeners();
  }

  //PhoneNumber Changed
  final ValueNotifier<bool> _phoneNumberValid = ValueNotifier(false);
  ValueNotifier<bool> get phoneNumberValid => _phoneNumberValid;

  PhoneNumber _phoneNumber = PhoneNumber('');
  PhoneNumber get phoneNumber => _phoneNumber;

  void phoneNumberChanged(String str) {
    _phoneNumber = PhoneNumber(str);
    _phoneNumberValid.value = _phoneNumber.isRight();
  }

  void verifyPhoneNumber({
    required PhoneNumber phoneNumber,
    required BuildContext context,
  }) async {
    final isPhoneNumberValid = phoneNumber.isRight();

    if (isPhoneNumberValid) {
      _setState(const VerifyPhoneNumberAuthState.loading());

      final verify = await _verifyPhoneNumberImpl.call(phoneNumber: phoneNumber, context: context);

      verify.fold(
        (failure) {
          _setState(const VerifyPhoneNumberAuthState.error());
          showSnackBar(context, _mapFailureToMessage(failure));
        },
        (r) {
          _setState(const VerifyPhoneNumberAuthState.done());
        },
      );
    }
  }

  String _mapFailureToMessage(VerifyPhoneNumberAuthFailure verifyFailure) {
    return _errorMessage = verifyFailure.map(
      cancelledByUser: (_) => 'Người dùng đã huỷ',
      serverError: (_) => 'Lỗi máy chủ, vui lòng thử lại',
      invalidPhoneNumber: (_) => 'Số điện thoại đã cung cấp không hợp lệ',
      offline: (_) => 'Mất kết nối internet, vui lòng kiểm tra đường truyền',
    );
  }

  @override
  void dispose() {
    super.dispose();
    _phoneNumberValid.dispose();
  }
}
