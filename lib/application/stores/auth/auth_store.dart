import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import '../../../infrastructure/repositories/auth/sign_out.dart';

@injectable
class AuthStore extends ChangeNotifier {
  final SignOutImpl _signOutImpl;

  AuthStore(this._signOutImpl);

  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  Stream<User?> get authStateChanges => firebaseAuth.authStateChanges();

  Future<Unit> signOut(String status) async {
    Future.wait([
      _signOutImpl.call(status),
    ]);
    return unit;
  }
}
