import 'package:app_chat/infrastructure/repositories/auth/get_firebase_messaging_token_impl.dart';
import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';

import '../../../../domain/core/failures/auth/auth_failure.dart';
import '../../../../domain/value_objects/auth/confirm_password.dart';
import '../../../../domain/value_objects/auth/email_address.dart';
import '../../../../domain/value_objects/auth/full_name.dart';
import '../../../../domain/value_objects/auth/password.dart';
import '../../../../domain/value_objects/auth/phone_number.dart';
import '../../../../infrastructure/repositories/auth/action_camera.dart';
import '../../../../infrastructure/repositories/auth/sign_up_with_email.dart';
import '../../../../infrastructure/repositories/user_infor/cache_user_info_impl.dart';
import '../../../../presentation/core/utils/utils.dart';

part 'register_store.freezed.dart';

@freezed
class RegisterState with _$RegisterState {
  const factory RegisterState.initial() = Initial;
  const factory RegisterState.loading() = Loading;
  const factory RegisterState.done() = Done;
  const factory RegisterState.error() = Error;
  const factory RegisterState.mustHaveEnoughInformation() = MustHaveEnoughInformation;
}

@freezed
class ActionCameraState with _$ActionCameraState {
  const factory ActionCameraState.initial() = ActionInitial;
  const factory ActionCameraState.actionLoading() = ActionLoading;
  const factory ActionCameraState.actionDone() = ActionDone;
  const factory ActionCameraState.actionError() = ActionError;
}

@injectable
class RegisterStore extends ChangeNotifier {
  final SignUpWithEmailAndPasswordImpl registerWithEmailAndPasswordImpl;
  final ActionCameraImpl actionCameraImpl;
  final ICacheUserInfoImpl cacheUserInfoImpl;
  final IGetFirebaseMessagingTokenImpl getFirebaseMessagingTokenImpl;

  RegisterStore(this.registerWithEmailAndPasswordImpl, this.actionCameraImpl, this.cacheUserInfoImpl,
      this.getFirebaseMessagingTokenImpl);

  RegisterState _registerState = const RegisterState.initial();
  RegisterState get registerState => _registerState;

  String _errorMessage = '';
  String get errorMessage => _errorMessage;

  void _setState(RegisterState state) {
    _registerState = state;
    notifyListeners();
  }

  ActionCameraState _actionCameraState = const ActionCameraState.initial();
  ActionCameraState get actionCameraState => _actionCameraState;

  String _imagePath = '';
  String get imagePath => _imagePath;

  void _setStateCamera(ActionCameraState state) {
    _actionCameraState = state;
    notifyListeners();
  }

  //Email Changed
  final ValueNotifier<bool> _emailValid = ValueNotifier(false);
  ValueNotifier<bool> get emailValid => _emailValid;

  EmailAddress _email = EmailAddress('');
  EmailAddress get email => _email;

  void emailChanged(String str) {
    _email = EmailAddress(str);
    _emailValid.value = _email.isRight();
  }

  //Password Changed
  final ValueNotifier<bool> _passwordValid = ValueNotifier(false);
  ValueNotifier<bool> get passwordValid => _passwordValid;

  Password _password = Password('');
  Password get password => _password;

  void passwordChanged(String str) {
    _password = Password(str);
    _passwordValid.value = _password.isRight();
  }

  //Confirm-Password Changed
  final ValueNotifier<bool> _confirmPasswordValid = ValueNotifier(false);
  ValueNotifier<bool> get confirmPasswordValid => _confirmPasswordValid;

  ConfirmPassword _confirmPassword = ConfirmPassword('', '');
  ConfirmPassword get confirmPassword => _confirmPassword;

  void confirmPasswordChanged(String confirmPassword, String password) {
    _confirmPassword = ConfirmPassword(confirmPassword, password);
    _confirmPasswordValid.value = _confirmPassword.isRight();
  }

  //Full name Changed
  final ValueNotifier<bool> _fullNameValid = ValueNotifier(false);
  ValueNotifier<bool> get fullNameValid => _fullNameValid;

  FullName _fullName = FullName('');
  FullName get fullName => _fullName;

  void fullNameChanged(String str) {
    _fullName = FullName(str);
    _fullNameValid.value = _fullName.isRight();
  }

  //Phone number Changed
  final ValueNotifier<bool> _phoneNumberValid = ValueNotifier(false);
  ValueNotifier<bool> get phoneNumberValid => _phoneNumberValid;

  PhoneNumber _phoneNumber = PhoneNumber('');
  PhoneNumber get phoneNumber => _phoneNumber;

  void phoneNumberChanged(String str) {
    _phoneNumber = PhoneNumber(str);
    _phoneNumberValid.value = _phoneNumber.isRight();
  }

  void register({
    required EmailAddress email,
    required Password password,
    required ConfirmPassword confirmPassword,
    required FullName fullName,
    required PhoneNumber phoneNumber,
    required BuildContext c,
  }) async {
    final isEmailValid = email.isRight();
    final isPasswordValid = password.isRight();
    final isConfirmPasswordValid = confirmPassword.isRight();
    final isFullNameValid = fullName.isRight();
    final isPhoneNumberValid = phoneNumber.isRight();

    if (isEmailValid &&
        isPasswordValid &&
        isConfirmPasswordValid &&
        isFullNameValid &&
        isPhoneNumberValid &&
        imagePath.isNotEmpty) {
      _setState(const RegisterState.loading());
      final token = await getToken();

      final register = await registerWithEmailAndPasswordImpl.call(
        email: email,
        password: password,
        fullName: fullName,
        phoneNumber: phoneNumber,
        profilePicture: _imagePath,
        token: token,
      );

      register.fold(
        (failure) {
          _setState(const RegisterState.error());
          showSnackBar(c, _mapFailureToMessage(failure));
        },
        (r) {
          _setState(const RegisterState.done());
          getUserDetail(email, token);
          c.router.pop();
        },
      );
    } else {
      _setState(const RegisterState.mustHaveEnoughInformation());
      showSnackBar(c, 'Bạn phải nhập đầy đủ thông tin và chụp 1 bức ảnh để hoàn tất thủ tục đăng ký');
    }
  }

  Future<Unit> getUserDetail(EmailAddress emailAddress, String token) async {
    return await cacheUserInfoImpl.call(emailAddress, token);
  }

  Future<String> getToken() async {
    return await getFirebaseMessagingTokenImpl.call();
  }

  Future<void> actionCamera() async {
    _setStateCamera(const ActionCameraState.actionLoading());
    final action = await actionCameraImpl.call(ImageSource.camera, 'avatars');

    action.fold(
      (l) {
        _setStateCamera(const ActionCameraState.actionError());
      },
      (r) {
        _setStateCamera(const ActionCameraState.actionDone());
        _imagePath = r;
      },
    );
  }

  Future<void> actionGallery() async {
    _setStateCamera(const ActionCameraState.actionLoading());

    final action = await actionCameraImpl.call(ImageSource.gallery, 'avatars');

    action.fold(
      (l) {
        _setStateCamera(const ActionCameraState.actionError());
      },
      (r) {
        _setStateCamera(const ActionCameraState.actionDone());
        _imagePath = r;
      },
    );
  }

  String _mapFailureToMessage(AuthFailure authFailure) {
    return _errorMessage = authFailure.map(
      cancelledByUser: (_) => 'Người dùng đã huỷ',
      serverError: (_) => 'Lỗi máy chủ, vui lòng thử lại',
      emailAlreadyInUse: (_) => 'Email đã được sử dụng',
      invalidEmailAndPassword: (_) => 'Email và mật khẩu không hợp lệ',
      offline: (_) => 'Mất kết nối internet, vui lòng kiểm tra đường truyền',
    );
  }

  @override
  void dispose() {
    super.dispose();
    _emailValid.dispose();
    _passwordValid.dispose();
    _fullNameValid.dispose();
    _phoneNumberValid.dispose();
  }
}
