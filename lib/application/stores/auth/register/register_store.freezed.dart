// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'register_store.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RegisterState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() done,
    required TResult Function() error,
    required TResult Function() mustHaveEnoughInformation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? done,
    TResult? Function()? error,
    TResult? Function()? mustHaveEnoughInformation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? done,
    TResult Function()? error,
    TResult Function()? mustHaveEnoughInformation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Done value) done,
    required TResult Function(Error value) error,
    required TResult Function(MustHaveEnoughInformation value)
        mustHaveEnoughInformation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initial value)? initial,
    TResult? Function(Loading value)? loading,
    TResult? Function(Done value)? done,
    TResult? Function(Error value)? error,
    TResult? Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Done value)? done,
    TResult Function(Error value)? error,
    TResult Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterStateCopyWith<$Res> {
  factory $RegisterStateCopyWith(
          RegisterState value, $Res Function(RegisterState) then) =
      _$RegisterStateCopyWithImpl<$Res, RegisterState>;
}

/// @nodoc
class _$RegisterStateCopyWithImpl<$Res, $Val extends RegisterState>
    implements $RegisterStateCopyWith<$Res> {
  _$RegisterStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialCopyWith<$Res> {
  factory _$$InitialCopyWith(_$Initial value, $Res Function(_$Initial) then) =
      __$$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res, _$Initial>
    implements _$$InitialCopyWith<$Res> {
  __$$InitialCopyWithImpl(_$Initial _value, $Res Function(_$Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Initial implements Initial {
  const _$Initial();

  @override
  String toString() {
    return 'RegisterState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() done,
    required TResult Function() error,
    required TResult Function() mustHaveEnoughInformation,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? done,
    TResult? Function()? error,
    TResult? Function()? mustHaveEnoughInformation,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? done,
    TResult Function()? error,
    TResult Function()? mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Done value) done,
    required TResult Function(Error value) error,
    required TResult Function(MustHaveEnoughInformation value)
        mustHaveEnoughInformation,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initial value)? initial,
    TResult? Function(Loading value)? loading,
    TResult? Function(Done value)? done,
    TResult? Function(Error value)? error,
    TResult? Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Done value)? done,
    TResult Function(Error value)? error,
    TResult Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class Initial implements RegisterState {
  const factory Initial() = _$Initial;
}

/// @nodoc
abstract class _$$LoadingCopyWith<$Res> {
  factory _$$LoadingCopyWith(_$Loading value, $Res Function(_$Loading) then) =
      __$$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res, _$Loading>
    implements _$$LoadingCopyWith<$Res> {
  __$$LoadingCopyWithImpl(_$Loading _value, $Res Function(_$Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Loading implements Loading {
  const _$Loading();

  @override
  String toString() {
    return 'RegisterState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() done,
    required TResult Function() error,
    required TResult Function() mustHaveEnoughInformation,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? done,
    TResult? Function()? error,
    TResult? Function()? mustHaveEnoughInformation,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? done,
    TResult Function()? error,
    TResult Function()? mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Done value) done,
    required TResult Function(Error value) error,
    required TResult Function(MustHaveEnoughInformation value)
        mustHaveEnoughInformation,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initial value)? initial,
    TResult? Function(Loading value)? loading,
    TResult? Function(Done value)? done,
    TResult? Function(Error value)? error,
    TResult? Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Done value)? done,
    TResult Function(Error value)? error,
    TResult Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading implements RegisterState {
  const factory Loading() = _$Loading;
}

/// @nodoc
abstract class _$$DoneCopyWith<$Res> {
  factory _$$DoneCopyWith(_$Done value, $Res Function(_$Done) then) =
      __$$DoneCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DoneCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res, _$Done>
    implements _$$DoneCopyWith<$Res> {
  __$$DoneCopyWithImpl(_$Done _value, $Res Function(_$Done) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Done implements Done {
  const _$Done();

  @override
  String toString() {
    return 'RegisterState.done()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Done);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() done,
    required TResult Function() error,
    required TResult Function() mustHaveEnoughInformation,
  }) {
    return done();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? done,
    TResult? Function()? error,
    TResult? Function()? mustHaveEnoughInformation,
  }) {
    return done?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? done,
    TResult Function()? error,
    TResult Function()? mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (done != null) {
      return done();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Done value) done,
    required TResult Function(Error value) error,
    required TResult Function(MustHaveEnoughInformation value)
        mustHaveEnoughInformation,
  }) {
    return done(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initial value)? initial,
    TResult? Function(Loading value)? loading,
    TResult? Function(Done value)? done,
    TResult? Function(Error value)? error,
    TResult? Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
  }) {
    return done?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Done value)? done,
    TResult Function(Error value)? error,
    TResult Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (done != null) {
      return done(this);
    }
    return orElse();
  }
}

abstract class Done implements RegisterState {
  const factory Done() = _$Done;
}

/// @nodoc
abstract class _$$ErrorCopyWith<$Res> {
  factory _$$ErrorCopyWith(_$Error value, $Res Function(_$Error) then) =
      __$$ErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ErrorCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res, _$Error>
    implements _$$ErrorCopyWith<$Res> {
  __$$ErrorCopyWithImpl(_$Error _value, $Res Function(_$Error) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Error implements Error {
  const _$Error();

  @override
  String toString() {
    return 'RegisterState.error()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Error);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() done,
    required TResult Function() error,
    required TResult Function() mustHaveEnoughInformation,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? done,
    TResult? Function()? error,
    TResult? Function()? mustHaveEnoughInformation,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? done,
    TResult Function()? error,
    TResult Function()? mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Done value) done,
    required TResult Function(Error value) error,
    required TResult Function(MustHaveEnoughInformation value)
        mustHaveEnoughInformation,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initial value)? initial,
    TResult? Function(Loading value)? loading,
    TResult? Function(Done value)? done,
    TResult? Function(Error value)? error,
    TResult? Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Done value)? done,
    TResult Function(Error value)? error,
    TResult Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class Error implements RegisterState {
  const factory Error() = _$Error;
}

/// @nodoc
abstract class _$$MustHaveEnoughInformationCopyWith<$Res> {
  factory _$$MustHaveEnoughInformationCopyWith(
          _$MustHaveEnoughInformation value,
          $Res Function(_$MustHaveEnoughInformation) then) =
      __$$MustHaveEnoughInformationCopyWithImpl<$Res>;
}

/// @nodoc
class __$$MustHaveEnoughInformationCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res, _$MustHaveEnoughInformation>
    implements _$$MustHaveEnoughInformationCopyWith<$Res> {
  __$$MustHaveEnoughInformationCopyWithImpl(_$MustHaveEnoughInformation _value,
      $Res Function(_$MustHaveEnoughInformation) _then)
      : super(_value, _then);
}

/// @nodoc

class _$MustHaveEnoughInformation implements MustHaveEnoughInformation {
  const _$MustHaveEnoughInformation();

  @override
  String toString() {
    return 'RegisterState.mustHaveEnoughInformation()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MustHaveEnoughInformation);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() done,
    required TResult Function() error,
    required TResult Function() mustHaveEnoughInformation,
  }) {
    return mustHaveEnoughInformation();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function()? done,
    TResult? Function()? error,
    TResult? Function()? mustHaveEnoughInformation,
  }) {
    return mustHaveEnoughInformation?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? done,
    TResult Function()? error,
    TResult Function()? mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (mustHaveEnoughInformation != null) {
      return mustHaveEnoughInformation();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Done value) done,
    required TResult Function(Error value) error,
    required TResult Function(MustHaveEnoughInformation value)
        mustHaveEnoughInformation,
  }) {
    return mustHaveEnoughInformation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initial value)? initial,
    TResult? Function(Loading value)? loading,
    TResult? Function(Done value)? done,
    TResult? Function(Error value)? error,
    TResult? Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
  }) {
    return mustHaveEnoughInformation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Done value)? done,
    TResult Function(Error value)? error,
    TResult Function(MustHaveEnoughInformation value)?
        mustHaveEnoughInformation,
    required TResult orElse(),
  }) {
    if (mustHaveEnoughInformation != null) {
      return mustHaveEnoughInformation(this);
    }
    return orElse();
  }
}

abstract class MustHaveEnoughInformation implements RegisterState {
  const factory MustHaveEnoughInformation() = _$MustHaveEnoughInformation;
}

/// @nodoc
mixin _$ActionCameraState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ActionInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ActionInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ActionInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ActionCameraStateCopyWith<$Res> {
  factory $ActionCameraStateCopyWith(
          ActionCameraState value, $Res Function(ActionCameraState) then) =
      _$ActionCameraStateCopyWithImpl<$Res, ActionCameraState>;
}

/// @nodoc
class _$ActionCameraStateCopyWithImpl<$Res, $Val extends ActionCameraState>
    implements $ActionCameraStateCopyWith<$Res> {
  _$ActionCameraStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ActionInitialCopyWith<$Res> {
  factory _$$ActionInitialCopyWith(
          _$ActionInitial value, $Res Function(_$ActionInitial) then) =
      __$$ActionInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionInitialCopyWithImpl<$Res>
    extends _$ActionCameraStateCopyWithImpl<$Res, _$ActionInitial>
    implements _$$ActionInitialCopyWith<$Res> {
  __$$ActionInitialCopyWithImpl(
      _$ActionInitial _value, $Res Function(_$ActionInitial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionInitial implements ActionInitial {
  const _$ActionInitial();

  @override
  String toString() {
    return 'ActionCameraState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ActionInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ActionInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ActionInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class ActionInitial implements ActionCameraState {
  const factory ActionInitial() = _$ActionInitial;
}

/// @nodoc
abstract class _$$ActionLoadingCopyWith<$Res> {
  factory _$$ActionLoadingCopyWith(
          _$ActionLoading value, $Res Function(_$ActionLoading) then) =
      __$$ActionLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionLoadingCopyWithImpl<$Res>
    extends _$ActionCameraStateCopyWithImpl<$Res, _$ActionLoading>
    implements _$$ActionLoadingCopyWith<$Res> {
  __$$ActionLoadingCopyWithImpl(
      _$ActionLoading _value, $Res Function(_$ActionLoading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionLoading implements ActionLoading {
  const _$ActionLoading();

  @override
  String toString() {
    return 'ActionCameraState.actionLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionLoading != null) {
      return actionLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ActionInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ActionInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ActionInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionLoading != null) {
      return actionLoading(this);
    }
    return orElse();
  }
}

abstract class ActionLoading implements ActionCameraState {
  const factory ActionLoading() = _$ActionLoading;
}

/// @nodoc
abstract class _$$ActionDoneCopyWith<$Res> {
  factory _$$ActionDoneCopyWith(
          _$ActionDone value, $Res Function(_$ActionDone) then) =
      __$$ActionDoneCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionDoneCopyWithImpl<$Res>
    extends _$ActionCameraStateCopyWithImpl<$Res, _$ActionDone>
    implements _$$ActionDoneCopyWith<$Res> {
  __$$ActionDoneCopyWithImpl(
      _$ActionDone _value, $Res Function(_$ActionDone) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionDone implements ActionDone {
  const _$ActionDone();

  @override
  String toString() {
    return 'ActionCameraState.actionDone()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionDone);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionDone();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionDone?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionDone != null) {
      return actionDone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ActionInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ActionInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ActionInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionDone != null) {
      return actionDone(this);
    }
    return orElse();
  }
}

abstract class ActionDone implements ActionCameraState {
  const factory ActionDone() = _$ActionDone;
}

/// @nodoc
abstract class _$$ActionErrorCopyWith<$Res> {
  factory _$$ActionErrorCopyWith(
          _$ActionError value, $Res Function(_$ActionError) then) =
      __$$ActionErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActionErrorCopyWithImpl<$Res>
    extends _$ActionCameraStateCopyWithImpl<$Res, _$ActionError>
    implements _$$ActionErrorCopyWith<$Res> {
  __$$ActionErrorCopyWithImpl(
      _$ActionError _value, $Res Function(_$ActionError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ActionError implements ActionError {
  const _$ActionError();

  @override
  String toString() {
    return 'ActionCameraState.actionError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActionError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() actionLoading,
    required TResult Function() actionDone,
    required TResult Function() actionError,
  }) {
    return actionError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? actionLoading,
    TResult? Function()? actionDone,
    TResult? Function()? actionError,
  }) {
    return actionError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? actionLoading,
    TResult Function()? actionDone,
    TResult Function()? actionError,
    required TResult orElse(),
  }) {
    if (actionError != null) {
      return actionError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ActionInitial value) initial,
    required TResult Function(ActionLoading value) actionLoading,
    required TResult Function(ActionDone value) actionDone,
    required TResult Function(ActionError value) actionError,
  }) {
    return actionError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ActionInitial value)? initial,
    TResult? Function(ActionLoading value)? actionLoading,
    TResult? Function(ActionDone value)? actionDone,
    TResult? Function(ActionError value)? actionError,
  }) {
    return actionError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ActionInitial value)? initial,
    TResult Function(ActionLoading value)? actionLoading,
    TResult Function(ActionDone value)? actionDone,
    TResult Function(ActionError value)? actionError,
    required TResult orElse(),
  }) {
    if (actionError != null) {
      return actionError(this);
    }
    return orElse();
  }
}

abstract class ActionError implements ActionCameraState {
  const factory ActionError() = _$ActionError;
}
