import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/search_friends/search_friends_failure.dart';
import '../../../domain/entities/user/user_entity.dart';
import '../../../infrastructure/repositories/search_friends/search_friends_impl.dart';

part 'search_friends_store.freezed.dart';

@freezed
abstract class SearchFriendsState with _$SearchFriendsState {
  const factory SearchFriendsState.initial() = Initial;
  const factory SearchFriendsState.loading() = Loading;
  const factory SearchFriendsState.searchDone() = SearchDone;
  const factory SearchFriendsState.searchError() = SearchError;
}

@injectable
class SearchFriendsStore extends ChangeNotifier {
  final SearchFriendsImpl _searchFriendsImpl;

  SearchFriendsStore(this._searchFriendsImpl);

  SearchFriendsState _searchFriendsState = const SearchFriendsState.initial();
  SearchFriendsState get searchFriendsState => _searchFriendsState;

  String _errorMessage = '';
  String get errorMessage => _errorMessage;

  void setState(SearchFriendsState state) {
    _searchFriendsState = state;
    notifyListeners();
  }

  List<UserEntity> _searchs = [];
  List<UserEntity> get searchs => _searchs;

  Future<List<UserEntity>> searchFriend(String query) async {
    setState(const SearchFriendsState.loading());

    final search = await _searchFriendsImpl.call(query);

    search.fold(
      (failure) {
        setState(const SearchFriendsState.searchError());
        _mapFailureToMessage(failure);
      },
      (r) {
        setState(const SearchFriendsState.searchDone());
        _searchs = r;
      },
    );

    return _searchs;
  }

  String _mapFailureToMessage(SearchFriendsFailure failure) {
    return _errorMessage = failure.map(
      serverError: (_) => 'Lỗi máy chủ, vui lòng thử lại',
      noResultIsFound: (_) => 'Không tìm thấy kết quả nào',
      offline: (_) => 'Mất kết nối internet, vui lòng kiểm tra đường truyền',
      emptyCacheFriends: (_) => 'Không có dữ liệu',
    );
  }
}
