import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/user_info/user_info_failure.dart';
import '../../../domain/entities/user/user_entity.dart';
import '../../../infrastructure/repositories/auth/get_firebase_messaging_token_impl.dart';
import '../../../infrastructure/repositories/user_infor/edit_user_info_impl.dart';
import '../../../infrastructure/repositories/user_infor/follow_user_impl.dart';
import '../../../infrastructure/repositories/user_infor/get_user_info_local_impl.dart';
import '../../../infrastructure/repositories/user_infor/set_status_user_impl.dart';
import '../../../infrastructure/repositories/user_infor/stream_get_user_info_by_uid_impl.dart';
import '../../../presentation/core/utils/utils.dart';

part 'user_info_store.freezed.dart';

@freezed
class EditUserState with _$EditUserState {
  const factory EditUserState.initial() = Initial;
  const factory EditUserState.loading() = Loading;
  const factory EditUserState.done() = Done;
  const factory EditUserState.error() = Error;
}

@injectable
class UserInfoStore extends ChangeNotifier {
  final IGetUserInfoLocalImpl getUserInfoImpl;
  final IStreamGetUserInfoByUidImpl streamGetUserInfoByUidImpl;
  final IEditUserInfoByUidImpl editUserInfoByUidImpl;
  final IFollowUserImpl followUserImpl;
  final ISetStatusUserImpl setStatusUserImpl;
  final IGetFirebaseMessagingTokenImpl getFirebaseMessagingTokenImpl;

  UserInfoStore(
    this.streamGetUserInfoByUidImpl,
    this.followUserImpl,
    this.editUserInfoByUidImpl,
    this.setStatusUserImpl,
    this.getFirebaseMessagingTokenImpl, {
    required this.getUserInfoImpl,
  });

  Future<Unit> followUser(String uid, String followId) async {
    return await followUserImpl.call(uid, followId);
  }

  Future<Unit> setStatusUser(String status) async {
    final token = await getTokenFromFirebase();
    return await setStatusUserImpl.call(status, token);
  }

  Stream<UserEntity> getUserByUid(String uid) => streamGetUserInfoByUidImpl.call(uid);

  EditUserState _editUserState = const EditUserState.initial();
  EditUserState get editUserState => _editUserState;

  void _setEditState(EditUserState state) {
    _editUserState = state;
    notifyListeners();
  }

  void editUserInfo(BuildContext context, String uid, String fullName, String bio) async {
    if (fullName.isNotEmpty || bio.isNotEmpty) {
      _setEditState(const EditUserState.loading());
    }

    final edit = await editUserInfoByUidImpl.call(uid, fullName, bio);
    edit.fold(
      (l) {
        _setEditState(const EditUserState.error());
        showSnackBar(context, _mapFailureToMessage(l));
      },
      (r) {
        _setEditState(const EditUserState.done());
        context.router.pop();
      },
    );
  }

  Future<String> getTokenFromFirebase() async {
    return await getFirebaseMessagingTokenImpl.call();
  }

  String _mapFailureToMessage(EditUserInfoFailure failure) {
    return failure.map(
      editOffline: (_) => 'Không có kết nối internet, vui lòng kiểm tra lại đường truyền',
      editFail: (_) => 'Thay đổi thất bại, vui lòng thử lại',
    );
  }

  bool? isCachedUserId() {
    return getUserInfoImpl.isCachedUserId();
  }

  String? getToken() {
    return getUserInfoImpl.getToken();
  }

  String? getUserId() {
    return getUserInfoImpl.getUserId();
  }

  String? getEmail() {
    return getUserInfoImpl.getEmail();
  }

  String? getFullName() {
    return getUserInfoImpl.getFullName();
  }

  String? getPhoneNumber() {
    return getUserInfoImpl.getPhoneNumber();
  }

  String? getProfilePicture() {
    return getUserInfoImpl.getProfilePicture();
  }

  List<String>? getFollowing() {
    return getUserInfoImpl.getFollowing() ?? [];
  }
}
