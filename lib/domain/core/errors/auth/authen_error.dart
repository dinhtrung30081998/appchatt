import '../../../value_objects/failures/auth/value_auth_failure.dart';

class AuthError extends Error {
  final ValueAuthFailure authFailure;

  AuthError(this.authFailure);

  @override
  String toString() {
    return Error.safeToString('Failure was: $authFailure');
  }
}
