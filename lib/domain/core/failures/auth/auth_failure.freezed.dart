// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CancelledByUser value) cancelledByUser,
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(InvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(OfflineError value) offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CancelledByUser value)? cancelledByUser,
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(OfflineError value)? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CancelledByUser value)? cancelledByUser,
    TResult Function(ServerError value)? serverError,
    TResult Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthFailureCopyWith<$Res> {
  factory $AuthFailureCopyWith(
          AuthFailure value, $Res Function(AuthFailure) then) =
      _$AuthFailureCopyWithImpl<$Res, AuthFailure>;
}

/// @nodoc
class _$AuthFailureCopyWithImpl<$Res, $Val extends AuthFailure>
    implements $AuthFailureCopyWith<$Res> {
  _$AuthFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CancelledByUserCopyWith<$Res> {
  factory _$$CancelledByUserCopyWith(
          _$CancelledByUser value, $Res Function(_$CancelledByUser) then) =
      __$$CancelledByUserCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CancelledByUserCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res, _$CancelledByUser>
    implements _$$CancelledByUserCopyWith<$Res> {
  __$$CancelledByUserCopyWithImpl(
      _$CancelledByUser _value, $Res Function(_$CancelledByUser) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CancelledByUser
    with DiagnosticableTreeMixin
    implements CancelledByUser {
  const _$CancelledByUser();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AuthFailure.cancelledByUser()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'AuthFailure.cancelledByUser'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CancelledByUser);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return cancelledByUser();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return cancelledByUser?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (cancelledByUser != null) {
      return cancelledByUser();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CancelledByUser value) cancelledByUser,
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(InvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(OfflineError value) offline,
  }) {
    return cancelledByUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CancelledByUser value)? cancelledByUser,
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(OfflineError value)? offline,
  }) {
    return cancelledByUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CancelledByUser value)? cancelledByUser,
    TResult Function(ServerError value)? serverError,
    TResult Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (cancelledByUser != null) {
      return cancelledByUser(this);
    }
    return orElse();
  }
}

abstract class CancelledByUser implements AuthFailure {
  const factory CancelledByUser() = _$CancelledByUser;
}

/// @nodoc
abstract class _$$ServerErrorCopyWith<$Res> {
  factory _$$ServerErrorCopyWith(
          _$ServerError value, $Res Function(_$ServerError) then) =
      __$$ServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ServerErrorCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res, _$ServerError>
    implements _$$ServerErrorCopyWith<$Res> {
  __$$ServerErrorCopyWithImpl(
      _$ServerError _value, $Res Function(_$ServerError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ServerError with DiagnosticableTreeMixin implements ServerError {
  const _$ServerError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AuthFailure.serverError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'AuthFailure.serverError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CancelledByUser value) cancelledByUser,
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(InvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(OfflineError value) offline,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CancelledByUser value)? cancelledByUser,
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(OfflineError value)? offline,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CancelledByUser value)? cancelledByUser,
    TResult Function(ServerError value)? serverError,
    TResult Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class ServerError implements AuthFailure {
  const factory ServerError() = _$ServerError;
}

/// @nodoc
abstract class _$$EmailAlreadyInUseCopyWith<$Res> {
  factory _$$EmailAlreadyInUseCopyWith(
          _$EmailAlreadyInUse value, $Res Function(_$EmailAlreadyInUse) then) =
      __$$EmailAlreadyInUseCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EmailAlreadyInUseCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res, _$EmailAlreadyInUse>
    implements _$$EmailAlreadyInUseCopyWith<$Res> {
  __$$EmailAlreadyInUseCopyWithImpl(
      _$EmailAlreadyInUse _value, $Res Function(_$EmailAlreadyInUse) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EmailAlreadyInUse
    with DiagnosticableTreeMixin
    implements EmailAlreadyInUse {
  const _$EmailAlreadyInUse();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AuthFailure.emailAlreadyInUse()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'AuthFailure.emailAlreadyInUse'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EmailAlreadyInUse);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return emailAlreadyInUse();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return emailAlreadyInUse?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (emailAlreadyInUse != null) {
      return emailAlreadyInUse();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CancelledByUser value) cancelledByUser,
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(InvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(OfflineError value) offline,
  }) {
    return emailAlreadyInUse(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CancelledByUser value)? cancelledByUser,
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(OfflineError value)? offline,
  }) {
    return emailAlreadyInUse?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CancelledByUser value)? cancelledByUser,
    TResult Function(ServerError value)? serverError,
    TResult Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (emailAlreadyInUse != null) {
      return emailAlreadyInUse(this);
    }
    return orElse();
  }
}

abstract class EmailAlreadyInUse implements AuthFailure {
  const factory EmailAlreadyInUse() = _$EmailAlreadyInUse;
}

/// @nodoc
abstract class _$$InvalidEmailAndPasswordCopyWith<$Res> {
  factory _$$InvalidEmailAndPasswordCopyWith(_$InvalidEmailAndPassword value,
          $Res Function(_$InvalidEmailAndPassword) then) =
      __$$InvalidEmailAndPasswordCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvalidEmailAndPasswordCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res, _$InvalidEmailAndPassword>
    implements _$$InvalidEmailAndPasswordCopyWith<$Res> {
  __$$InvalidEmailAndPasswordCopyWithImpl(_$InvalidEmailAndPassword _value,
      $Res Function(_$InvalidEmailAndPassword) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InvalidEmailAndPassword
    with DiagnosticableTreeMixin
    implements InvalidEmailAndPassword {
  const _$InvalidEmailAndPassword();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AuthFailure.invalidEmailAndPassword()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty('type', 'AuthFailure.invalidEmailAndPassword'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidEmailAndPassword);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return invalidEmailAndPassword();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return invalidEmailAndPassword?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (invalidEmailAndPassword != null) {
      return invalidEmailAndPassword();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CancelledByUser value) cancelledByUser,
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(InvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(OfflineError value) offline,
  }) {
    return invalidEmailAndPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CancelledByUser value)? cancelledByUser,
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(OfflineError value)? offline,
  }) {
    return invalidEmailAndPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CancelledByUser value)? cancelledByUser,
    TResult Function(ServerError value)? serverError,
    TResult Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (invalidEmailAndPassword != null) {
      return invalidEmailAndPassword(this);
    }
    return orElse();
  }
}

abstract class InvalidEmailAndPassword implements AuthFailure {
  const factory InvalidEmailAndPassword() = _$InvalidEmailAndPassword;
}

/// @nodoc
abstract class _$$OfflineErrorCopyWith<$Res> {
  factory _$$OfflineErrorCopyWith(
          _$OfflineError value, $Res Function(_$OfflineError) then) =
      __$$OfflineErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$OfflineErrorCopyWithImpl<$Res>
    extends _$AuthFailureCopyWithImpl<$Res, _$OfflineError>
    implements _$$OfflineErrorCopyWith<$Res> {
  __$$OfflineErrorCopyWithImpl(
      _$OfflineError _value, $Res Function(_$OfflineError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$OfflineError with DiagnosticableTreeMixin implements OfflineError {
  const _$OfflineError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AuthFailure.offline()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'AuthFailure.offline'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$OfflineError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return offline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return offline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CancelledByUser value) cancelledByUser,
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(InvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(OfflineError value) offline,
  }) {
    return offline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CancelledByUser value)? cancelledByUser,
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(OfflineError value)? offline,
  }) {
    return offline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CancelledByUser value)? cancelledByUser,
    TResult Function(ServerError value)? serverError,
    TResult Function(EmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(InvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline(this);
    }
    return orElse();
  }
}

abstract class OfflineError implements AuthFailure {
  const factory OfflineError() = _$OfflineError;
}

/// @nodoc
mixin _$GoogleAuthFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() accountExistsWithDifferentCredential,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? accountExistsWithDifferentCredential,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? accountExistsWithDifferentCredential,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GGCancelledByUser value) cancelledByUser,
    required TResult Function(GGServerError value) serverError,
    required TResult Function(AccountExistsWithDifferentCredential value)
        accountExistsWithDifferentCredential,
    required TResult Function(GGEmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(GGInvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(GGOfflineError value) offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GGCancelledByUser value)? cancelledByUser,
    TResult? Function(GGServerError value)? serverError,
    TResult? Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult? Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(GGOfflineError value)? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GGCancelledByUser value)? cancelledByUser,
    TResult Function(GGServerError value)? serverError,
    TResult Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(GGOfflineError value)? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GoogleAuthFailureCopyWith<$Res> {
  factory $GoogleAuthFailureCopyWith(
          GoogleAuthFailure value, $Res Function(GoogleAuthFailure) then) =
      _$GoogleAuthFailureCopyWithImpl<$Res, GoogleAuthFailure>;
}

/// @nodoc
class _$GoogleAuthFailureCopyWithImpl<$Res, $Val extends GoogleAuthFailure>
    implements $GoogleAuthFailureCopyWith<$Res> {
  _$GoogleAuthFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GGCancelledByUserCopyWith<$Res> {
  factory _$$GGCancelledByUserCopyWith(
          _$GGCancelledByUser value, $Res Function(_$GGCancelledByUser) then) =
      __$$GGCancelledByUserCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GGCancelledByUserCopyWithImpl<$Res>
    extends _$GoogleAuthFailureCopyWithImpl<$Res, _$GGCancelledByUser>
    implements _$$GGCancelledByUserCopyWith<$Res> {
  __$$GGCancelledByUserCopyWithImpl(
      _$GGCancelledByUser _value, $Res Function(_$GGCancelledByUser) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GGCancelledByUser
    with DiagnosticableTreeMixin
    implements GGCancelledByUser {
  const _$GGCancelledByUser();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GoogleAuthFailure.cancelledByUser()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'GoogleAuthFailure.cancelledByUser'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GGCancelledByUser);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() accountExistsWithDifferentCredential,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return cancelledByUser();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? accountExistsWithDifferentCredential,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return cancelledByUser?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? accountExistsWithDifferentCredential,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (cancelledByUser != null) {
      return cancelledByUser();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GGCancelledByUser value) cancelledByUser,
    required TResult Function(GGServerError value) serverError,
    required TResult Function(AccountExistsWithDifferentCredential value)
        accountExistsWithDifferentCredential,
    required TResult Function(GGEmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(GGInvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(GGOfflineError value) offline,
  }) {
    return cancelledByUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GGCancelledByUser value)? cancelledByUser,
    TResult? Function(GGServerError value)? serverError,
    TResult? Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult? Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(GGOfflineError value)? offline,
  }) {
    return cancelledByUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GGCancelledByUser value)? cancelledByUser,
    TResult Function(GGServerError value)? serverError,
    TResult Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(GGOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (cancelledByUser != null) {
      return cancelledByUser(this);
    }
    return orElse();
  }
}

abstract class GGCancelledByUser implements GoogleAuthFailure {
  const factory GGCancelledByUser() = _$GGCancelledByUser;
}

/// @nodoc
abstract class _$$GGServerErrorCopyWith<$Res> {
  factory _$$GGServerErrorCopyWith(
          _$GGServerError value, $Res Function(_$GGServerError) then) =
      __$$GGServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GGServerErrorCopyWithImpl<$Res>
    extends _$GoogleAuthFailureCopyWithImpl<$Res, _$GGServerError>
    implements _$$GGServerErrorCopyWith<$Res> {
  __$$GGServerErrorCopyWithImpl(
      _$GGServerError _value, $Res Function(_$GGServerError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GGServerError with DiagnosticableTreeMixin implements GGServerError {
  const _$GGServerError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GoogleAuthFailure.serverError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'GoogleAuthFailure.serverError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GGServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() accountExistsWithDifferentCredential,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? accountExistsWithDifferentCredential,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? accountExistsWithDifferentCredential,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GGCancelledByUser value) cancelledByUser,
    required TResult Function(GGServerError value) serverError,
    required TResult Function(AccountExistsWithDifferentCredential value)
        accountExistsWithDifferentCredential,
    required TResult Function(GGEmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(GGInvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(GGOfflineError value) offline,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GGCancelledByUser value)? cancelledByUser,
    TResult? Function(GGServerError value)? serverError,
    TResult? Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult? Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(GGOfflineError value)? offline,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GGCancelledByUser value)? cancelledByUser,
    TResult Function(GGServerError value)? serverError,
    TResult Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(GGOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class GGServerError implements GoogleAuthFailure {
  const factory GGServerError() = _$GGServerError;
}

/// @nodoc
abstract class _$$AccountExistsWithDifferentCredentialCopyWith<$Res> {
  factory _$$AccountExistsWithDifferentCredentialCopyWith(
          _$AccountExistsWithDifferentCredential value,
          $Res Function(_$AccountExistsWithDifferentCredential) then) =
      __$$AccountExistsWithDifferentCredentialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AccountExistsWithDifferentCredentialCopyWithImpl<$Res>
    extends _$GoogleAuthFailureCopyWithImpl<$Res,
        _$AccountExistsWithDifferentCredential>
    implements _$$AccountExistsWithDifferentCredentialCopyWith<$Res> {
  __$$AccountExistsWithDifferentCredentialCopyWithImpl(
      _$AccountExistsWithDifferentCredential _value,
      $Res Function(_$AccountExistsWithDifferentCredential) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AccountExistsWithDifferentCredential
    with DiagnosticableTreeMixin
    implements AccountExistsWithDifferentCredential {
  const _$AccountExistsWithDifferentCredential();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GoogleAuthFailure.accountExistsWithDifferentCredential()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty(
        'type', 'GoogleAuthFailure.accountExistsWithDifferentCredential'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AccountExistsWithDifferentCredential);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() accountExistsWithDifferentCredential,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return accountExistsWithDifferentCredential();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? accountExistsWithDifferentCredential,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return accountExistsWithDifferentCredential?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? accountExistsWithDifferentCredential,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (accountExistsWithDifferentCredential != null) {
      return accountExistsWithDifferentCredential();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GGCancelledByUser value) cancelledByUser,
    required TResult Function(GGServerError value) serverError,
    required TResult Function(AccountExistsWithDifferentCredential value)
        accountExistsWithDifferentCredential,
    required TResult Function(GGEmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(GGInvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(GGOfflineError value) offline,
  }) {
    return accountExistsWithDifferentCredential(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GGCancelledByUser value)? cancelledByUser,
    TResult? Function(GGServerError value)? serverError,
    TResult? Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult? Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(GGOfflineError value)? offline,
  }) {
    return accountExistsWithDifferentCredential?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GGCancelledByUser value)? cancelledByUser,
    TResult Function(GGServerError value)? serverError,
    TResult Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(GGOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (accountExistsWithDifferentCredential != null) {
      return accountExistsWithDifferentCredential(this);
    }
    return orElse();
  }
}

abstract class AccountExistsWithDifferentCredential
    implements GoogleAuthFailure {
  const factory AccountExistsWithDifferentCredential() =
      _$AccountExistsWithDifferentCredential;
}

/// @nodoc
abstract class _$$GGEmailAlreadyInUseCopyWith<$Res> {
  factory _$$GGEmailAlreadyInUseCopyWith(_$GGEmailAlreadyInUse value,
          $Res Function(_$GGEmailAlreadyInUse) then) =
      __$$GGEmailAlreadyInUseCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GGEmailAlreadyInUseCopyWithImpl<$Res>
    extends _$GoogleAuthFailureCopyWithImpl<$Res, _$GGEmailAlreadyInUse>
    implements _$$GGEmailAlreadyInUseCopyWith<$Res> {
  __$$GGEmailAlreadyInUseCopyWithImpl(
      _$GGEmailAlreadyInUse _value, $Res Function(_$GGEmailAlreadyInUse) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GGEmailAlreadyInUse
    with DiagnosticableTreeMixin
    implements GGEmailAlreadyInUse {
  const _$GGEmailAlreadyInUse();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GoogleAuthFailure.emailAlreadyInUse()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty('type', 'GoogleAuthFailure.emailAlreadyInUse'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GGEmailAlreadyInUse);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() accountExistsWithDifferentCredential,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return emailAlreadyInUse();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? accountExistsWithDifferentCredential,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return emailAlreadyInUse?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? accountExistsWithDifferentCredential,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (emailAlreadyInUse != null) {
      return emailAlreadyInUse();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GGCancelledByUser value) cancelledByUser,
    required TResult Function(GGServerError value) serverError,
    required TResult Function(AccountExistsWithDifferentCredential value)
        accountExistsWithDifferentCredential,
    required TResult Function(GGEmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(GGInvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(GGOfflineError value) offline,
  }) {
    return emailAlreadyInUse(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GGCancelledByUser value)? cancelledByUser,
    TResult? Function(GGServerError value)? serverError,
    TResult? Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult? Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(GGOfflineError value)? offline,
  }) {
    return emailAlreadyInUse?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GGCancelledByUser value)? cancelledByUser,
    TResult Function(GGServerError value)? serverError,
    TResult Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(GGOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (emailAlreadyInUse != null) {
      return emailAlreadyInUse(this);
    }
    return orElse();
  }
}

abstract class GGEmailAlreadyInUse implements GoogleAuthFailure {
  const factory GGEmailAlreadyInUse() = _$GGEmailAlreadyInUse;
}

/// @nodoc
abstract class _$$GGInvalidEmailAndPasswordCopyWith<$Res> {
  factory _$$GGInvalidEmailAndPasswordCopyWith(
          _$GGInvalidEmailAndPassword value,
          $Res Function(_$GGInvalidEmailAndPassword) then) =
      __$$GGInvalidEmailAndPasswordCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GGInvalidEmailAndPasswordCopyWithImpl<$Res>
    extends _$GoogleAuthFailureCopyWithImpl<$Res, _$GGInvalidEmailAndPassword>
    implements _$$GGInvalidEmailAndPasswordCopyWith<$Res> {
  __$$GGInvalidEmailAndPasswordCopyWithImpl(_$GGInvalidEmailAndPassword _value,
      $Res Function(_$GGInvalidEmailAndPassword) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GGInvalidEmailAndPassword
    with DiagnosticableTreeMixin
    implements GGInvalidEmailAndPassword {
  const _$GGInvalidEmailAndPassword();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GoogleAuthFailure.invalidEmailAndPassword()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty(
        'type', 'GoogleAuthFailure.invalidEmailAndPassword'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GGInvalidEmailAndPassword);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() accountExistsWithDifferentCredential,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return invalidEmailAndPassword();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? accountExistsWithDifferentCredential,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return invalidEmailAndPassword?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? accountExistsWithDifferentCredential,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (invalidEmailAndPassword != null) {
      return invalidEmailAndPassword();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GGCancelledByUser value) cancelledByUser,
    required TResult Function(GGServerError value) serverError,
    required TResult Function(AccountExistsWithDifferentCredential value)
        accountExistsWithDifferentCredential,
    required TResult Function(GGEmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(GGInvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(GGOfflineError value) offline,
  }) {
    return invalidEmailAndPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GGCancelledByUser value)? cancelledByUser,
    TResult? Function(GGServerError value)? serverError,
    TResult? Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult? Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(GGOfflineError value)? offline,
  }) {
    return invalidEmailAndPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GGCancelledByUser value)? cancelledByUser,
    TResult Function(GGServerError value)? serverError,
    TResult Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(GGOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (invalidEmailAndPassword != null) {
      return invalidEmailAndPassword(this);
    }
    return orElse();
  }
}

abstract class GGInvalidEmailAndPassword implements GoogleAuthFailure {
  const factory GGInvalidEmailAndPassword() = _$GGInvalidEmailAndPassword;
}

/// @nodoc
abstract class _$$GGOfflineErrorCopyWith<$Res> {
  factory _$$GGOfflineErrorCopyWith(
          _$GGOfflineError value, $Res Function(_$GGOfflineError) then) =
      __$$GGOfflineErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GGOfflineErrorCopyWithImpl<$Res>
    extends _$GoogleAuthFailureCopyWithImpl<$Res, _$GGOfflineError>
    implements _$$GGOfflineErrorCopyWith<$Res> {
  __$$GGOfflineErrorCopyWithImpl(
      _$GGOfflineError _value, $Res Function(_$GGOfflineError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GGOfflineError with DiagnosticableTreeMixin implements GGOfflineError {
  const _$GGOfflineError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GoogleAuthFailure.offline()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'GoogleAuthFailure.offline'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GGOfflineError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() accountExistsWithDifferentCredential,
    required TResult Function() emailAlreadyInUse,
    required TResult Function() invalidEmailAndPassword,
    required TResult Function() offline,
  }) {
    return offline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? accountExistsWithDifferentCredential,
    TResult? Function()? emailAlreadyInUse,
    TResult? Function()? invalidEmailAndPassword,
    TResult? Function()? offline,
  }) {
    return offline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? accountExistsWithDifferentCredential,
    TResult Function()? emailAlreadyInUse,
    TResult Function()? invalidEmailAndPassword,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GGCancelledByUser value) cancelledByUser,
    required TResult Function(GGServerError value) serverError,
    required TResult Function(AccountExistsWithDifferentCredential value)
        accountExistsWithDifferentCredential,
    required TResult Function(GGEmailAlreadyInUse value) emailAlreadyInUse,
    required TResult Function(GGInvalidEmailAndPassword value)
        invalidEmailAndPassword,
    required TResult Function(GGOfflineError value) offline,
  }) {
    return offline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GGCancelledByUser value)? cancelledByUser,
    TResult? Function(GGServerError value)? serverError,
    TResult? Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult? Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult? Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult? Function(GGOfflineError value)? offline,
  }) {
    return offline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GGCancelledByUser value)? cancelledByUser,
    TResult Function(GGServerError value)? serverError,
    TResult Function(AccountExistsWithDifferentCredential value)?
        accountExistsWithDifferentCredential,
    TResult Function(GGEmailAlreadyInUse value)? emailAlreadyInUse,
    TResult Function(GGInvalidEmailAndPassword value)? invalidEmailAndPassword,
    TResult Function(GGOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline(this);
    }
    return orElse();
  }
}

abstract class GGOfflineError implements GoogleAuthFailure {
  const factory GGOfflineError() = _$GGOfflineError;
}

/// @nodoc
mixin _$FacebookAuthFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() failed,
    required TResult Function() operationInProgress,
    required TResult Function() cancelled,
    required TResult Function() serverError,
    required TResult Function() offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? failed,
    TResult? Function()? operationInProgress,
    TResult? Function()? cancelled,
    TResult? Function()? serverError,
    TResult? Function()? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? failed,
    TResult Function()? operationInProgress,
    TResult Function()? cancelled,
    TResult Function()? serverError,
    TResult Function()? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Failed value) failed,
    required TResult Function(OperationInProgress value) operationInProgress,
    required TResult Function(Cancelled value) cancelled,
    required TResult Function(FbServerError value) serverError,
    required TResult Function(FbOfflineError value) offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Failed value)? failed,
    TResult? Function(OperationInProgress value)? operationInProgress,
    TResult? Function(Cancelled value)? cancelled,
    TResult? Function(FbServerError value)? serverError,
    TResult? Function(FbOfflineError value)? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Failed value)? failed,
    TResult Function(OperationInProgress value)? operationInProgress,
    TResult Function(Cancelled value)? cancelled,
    TResult Function(FbServerError value)? serverError,
    TResult Function(FbOfflineError value)? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FacebookAuthFailureCopyWith<$Res> {
  factory $FacebookAuthFailureCopyWith(
          FacebookAuthFailure value, $Res Function(FacebookAuthFailure) then) =
      _$FacebookAuthFailureCopyWithImpl<$Res, FacebookAuthFailure>;
}

/// @nodoc
class _$FacebookAuthFailureCopyWithImpl<$Res, $Val extends FacebookAuthFailure>
    implements $FacebookAuthFailureCopyWith<$Res> {
  _$FacebookAuthFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$FailedCopyWith<$Res> {
  factory _$$FailedCopyWith(_$Failed value, $Res Function(_$Failed) then) =
      __$$FailedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FailedCopyWithImpl<$Res>
    extends _$FacebookAuthFailureCopyWithImpl<$Res, _$Failed>
    implements _$$FailedCopyWith<$Res> {
  __$$FailedCopyWithImpl(_$Failed _value, $Res Function(_$Failed) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Failed with DiagnosticableTreeMixin implements Failed {
  const _$Failed();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FacebookAuthFailure.failed()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'FacebookAuthFailure.failed'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Failed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() failed,
    required TResult Function() operationInProgress,
    required TResult Function() cancelled,
    required TResult Function() serverError,
    required TResult Function() offline,
  }) {
    return failed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? failed,
    TResult? Function()? operationInProgress,
    TResult? Function()? cancelled,
    TResult? Function()? serverError,
    TResult? Function()? offline,
  }) {
    return failed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? failed,
    TResult Function()? operationInProgress,
    TResult Function()? cancelled,
    TResult Function()? serverError,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Failed value) failed,
    required TResult Function(OperationInProgress value) operationInProgress,
    required TResult Function(Cancelled value) cancelled,
    required TResult Function(FbServerError value) serverError,
    required TResult Function(FbOfflineError value) offline,
  }) {
    return failed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Failed value)? failed,
    TResult? Function(OperationInProgress value)? operationInProgress,
    TResult? Function(Cancelled value)? cancelled,
    TResult? Function(FbServerError value)? serverError,
    TResult? Function(FbOfflineError value)? offline,
  }) {
    return failed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Failed value)? failed,
    TResult Function(OperationInProgress value)? operationInProgress,
    TResult Function(Cancelled value)? cancelled,
    TResult Function(FbServerError value)? serverError,
    TResult Function(FbOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(this);
    }
    return orElse();
  }
}

abstract class Failed implements FacebookAuthFailure {
  const factory Failed() = _$Failed;
}

/// @nodoc
abstract class _$$OperationInProgressCopyWith<$Res> {
  factory _$$OperationInProgressCopyWith(_$OperationInProgress value,
          $Res Function(_$OperationInProgress) then) =
      __$$OperationInProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$$OperationInProgressCopyWithImpl<$Res>
    extends _$FacebookAuthFailureCopyWithImpl<$Res, _$OperationInProgress>
    implements _$$OperationInProgressCopyWith<$Res> {
  __$$OperationInProgressCopyWithImpl(
      _$OperationInProgress _value, $Res Function(_$OperationInProgress) _then)
      : super(_value, _then);
}

/// @nodoc

class _$OperationInProgress
    with DiagnosticableTreeMixin
    implements OperationInProgress {
  const _$OperationInProgress();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FacebookAuthFailure.operationInProgress()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty('type', 'FacebookAuthFailure.operationInProgress'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$OperationInProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() failed,
    required TResult Function() operationInProgress,
    required TResult Function() cancelled,
    required TResult Function() serverError,
    required TResult Function() offline,
  }) {
    return operationInProgress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? failed,
    TResult? Function()? operationInProgress,
    TResult? Function()? cancelled,
    TResult? Function()? serverError,
    TResult? Function()? offline,
  }) {
    return operationInProgress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? failed,
    TResult Function()? operationInProgress,
    TResult Function()? cancelled,
    TResult Function()? serverError,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (operationInProgress != null) {
      return operationInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Failed value) failed,
    required TResult Function(OperationInProgress value) operationInProgress,
    required TResult Function(Cancelled value) cancelled,
    required TResult Function(FbServerError value) serverError,
    required TResult Function(FbOfflineError value) offline,
  }) {
    return operationInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Failed value)? failed,
    TResult? Function(OperationInProgress value)? operationInProgress,
    TResult? Function(Cancelled value)? cancelled,
    TResult? Function(FbServerError value)? serverError,
    TResult? Function(FbOfflineError value)? offline,
  }) {
    return operationInProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Failed value)? failed,
    TResult Function(OperationInProgress value)? operationInProgress,
    TResult Function(Cancelled value)? cancelled,
    TResult Function(FbServerError value)? serverError,
    TResult Function(FbOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (operationInProgress != null) {
      return operationInProgress(this);
    }
    return orElse();
  }
}

abstract class OperationInProgress implements FacebookAuthFailure {
  const factory OperationInProgress() = _$OperationInProgress;
}

/// @nodoc
abstract class _$$CancelledCopyWith<$Res> {
  factory _$$CancelledCopyWith(
          _$Cancelled value, $Res Function(_$Cancelled) then) =
      __$$CancelledCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CancelledCopyWithImpl<$Res>
    extends _$FacebookAuthFailureCopyWithImpl<$Res, _$Cancelled>
    implements _$$CancelledCopyWith<$Res> {
  __$$CancelledCopyWithImpl(
      _$Cancelled _value, $Res Function(_$Cancelled) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Cancelled with DiagnosticableTreeMixin implements Cancelled {
  const _$Cancelled();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FacebookAuthFailure.cancelled()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'FacebookAuthFailure.cancelled'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Cancelled);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() failed,
    required TResult Function() operationInProgress,
    required TResult Function() cancelled,
    required TResult Function() serverError,
    required TResult Function() offline,
  }) {
    return cancelled();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? failed,
    TResult? Function()? operationInProgress,
    TResult? Function()? cancelled,
    TResult? Function()? serverError,
    TResult? Function()? offline,
  }) {
    return cancelled?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? failed,
    TResult Function()? operationInProgress,
    TResult Function()? cancelled,
    TResult Function()? serverError,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (cancelled != null) {
      return cancelled();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Failed value) failed,
    required TResult Function(OperationInProgress value) operationInProgress,
    required TResult Function(Cancelled value) cancelled,
    required TResult Function(FbServerError value) serverError,
    required TResult Function(FbOfflineError value) offline,
  }) {
    return cancelled(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Failed value)? failed,
    TResult? Function(OperationInProgress value)? operationInProgress,
    TResult? Function(Cancelled value)? cancelled,
    TResult? Function(FbServerError value)? serverError,
    TResult? Function(FbOfflineError value)? offline,
  }) {
    return cancelled?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Failed value)? failed,
    TResult Function(OperationInProgress value)? operationInProgress,
    TResult Function(Cancelled value)? cancelled,
    TResult Function(FbServerError value)? serverError,
    TResult Function(FbOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (cancelled != null) {
      return cancelled(this);
    }
    return orElse();
  }
}

abstract class Cancelled implements FacebookAuthFailure {
  const factory Cancelled() = _$Cancelled;
}

/// @nodoc
abstract class _$$FbServerErrorCopyWith<$Res> {
  factory _$$FbServerErrorCopyWith(
          _$FbServerError value, $Res Function(_$FbServerError) then) =
      __$$FbServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FbServerErrorCopyWithImpl<$Res>
    extends _$FacebookAuthFailureCopyWithImpl<$Res, _$FbServerError>
    implements _$$FbServerErrorCopyWith<$Res> {
  __$$FbServerErrorCopyWithImpl(
      _$FbServerError _value, $Res Function(_$FbServerError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FbServerError with DiagnosticableTreeMixin implements FbServerError {
  const _$FbServerError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FacebookAuthFailure.serverError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'FacebookAuthFailure.serverError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FbServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() failed,
    required TResult Function() operationInProgress,
    required TResult Function() cancelled,
    required TResult Function() serverError,
    required TResult Function() offline,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? failed,
    TResult? Function()? operationInProgress,
    TResult? Function()? cancelled,
    TResult? Function()? serverError,
    TResult? Function()? offline,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? failed,
    TResult Function()? operationInProgress,
    TResult Function()? cancelled,
    TResult Function()? serverError,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Failed value) failed,
    required TResult Function(OperationInProgress value) operationInProgress,
    required TResult Function(Cancelled value) cancelled,
    required TResult Function(FbServerError value) serverError,
    required TResult Function(FbOfflineError value) offline,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Failed value)? failed,
    TResult? Function(OperationInProgress value)? operationInProgress,
    TResult? Function(Cancelled value)? cancelled,
    TResult? Function(FbServerError value)? serverError,
    TResult? Function(FbOfflineError value)? offline,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Failed value)? failed,
    TResult Function(OperationInProgress value)? operationInProgress,
    TResult Function(Cancelled value)? cancelled,
    TResult Function(FbServerError value)? serverError,
    TResult Function(FbOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class FbServerError implements FacebookAuthFailure {
  const factory FbServerError() = _$FbServerError;
}

/// @nodoc
abstract class _$$FbOfflineErrorCopyWith<$Res> {
  factory _$$FbOfflineErrorCopyWith(
          _$FbOfflineError value, $Res Function(_$FbOfflineError) then) =
      __$$FbOfflineErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FbOfflineErrorCopyWithImpl<$Res>
    extends _$FacebookAuthFailureCopyWithImpl<$Res, _$FbOfflineError>
    implements _$$FbOfflineErrorCopyWith<$Res> {
  __$$FbOfflineErrorCopyWithImpl(
      _$FbOfflineError _value, $Res Function(_$FbOfflineError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FbOfflineError with DiagnosticableTreeMixin implements FbOfflineError {
  const _$FbOfflineError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FacebookAuthFailure.offline()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'FacebookAuthFailure.offline'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FbOfflineError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() failed,
    required TResult Function() operationInProgress,
    required TResult Function() cancelled,
    required TResult Function() serverError,
    required TResult Function() offline,
  }) {
    return offline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? failed,
    TResult? Function()? operationInProgress,
    TResult? Function()? cancelled,
    TResult? Function()? serverError,
    TResult? Function()? offline,
  }) {
    return offline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? failed,
    TResult Function()? operationInProgress,
    TResult Function()? cancelled,
    TResult Function()? serverError,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Failed value) failed,
    required TResult Function(OperationInProgress value) operationInProgress,
    required TResult Function(Cancelled value) cancelled,
    required TResult Function(FbServerError value) serverError,
    required TResult Function(FbOfflineError value) offline,
  }) {
    return offline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Failed value)? failed,
    TResult? Function(OperationInProgress value)? operationInProgress,
    TResult? Function(Cancelled value)? cancelled,
    TResult? Function(FbServerError value)? serverError,
    TResult? Function(FbOfflineError value)? offline,
  }) {
    return offline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Failed value)? failed,
    TResult Function(OperationInProgress value)? operationInProgress,
    TResult Function(Cancelled value)? cancelled,
    TResult Function(FbServerError value)? serverError,
    TResult Function(FbOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline(this);
    }
    return orElse();
  }
}

abstract class FbOfflineError implements FacebookAuthFailure {
  const factory FbOfflineError() = _$FbOfflineError;
}

/// @nodoc
mixin _$VerifyPhoneNumberAuthFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() invalidPhoneNumber,
    required TResult Function() offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? invalidPhoneNumber,
    TResult? Function()? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? invalidPhoneNumber,
    TResult Function()? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PNCancelledByUser value) cancelledByUser,
    required TResult Function(PNServerError value) serverError,
    required TResult Function(InvalidPhoneNumber value) invalidPhoneNumber,
    required TResult Function(PNOfflineError value) offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PNCancelledByUser value)? cancelledByUser,
    TResult? Function(PNServerError value)? serverError,
    TResult? Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult? Function(PNOfflineError value)? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PNCancelledByUser value)? cancelledByUser,
    TResult Function(PNServerError value)? serverError,
    TResult Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult Function(PNOfflineError value)? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VerifyPhoneNumberAuthFailureCopyWith<$Res> {
  factory $VerifyPhoneNumberAuthFailureCopyWith(
          VerifyPhoneNumberAuthFailure value,
          $Res Function(VerifyPhoneNumberAuthFailure) then) =
      _$VerifyPhoneNumberAuthFailureCopyWithImpl<$Res,
          VerifyPhoneNumberAuthFailure>;
}

/// @nodoc
class _$VerifyPhoneNumberAuthFailureCopyWithImpl<$Res,
        $Val extends VerifyPhoneNumberAuthFailure>
    implements $VerifyPhoneNumberAuthFailureCopyWith<$Res> {
  _$VerifyPhoneNumberAuthFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$PNCancelledByUserCopyWith<$Res> {
  factory _$$PNCancelledByUserCopyWith(
          _$PNCancelledByUser value, $Res Function(_$PNCancelledByUser) then) =
      __$$PNCancelledByUserCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PNCancelledByUserCopyWithImpl<$Res>
    extends _$VerifyPhoneNumberAuthFailureCopyWithImpl<$Res,
        _$PNCancelledByUser> implements _$$PNCancelledByUserCopyWith<$Res> {
  __$$PNCancelledByUserCopyWithImpl(
      _$PNCancelledByUser _value, $Res Function(_$PNCancelledByUser) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PNCancelledByUser
    with DiagnosticableTreeMixin
    implements PNCancelledByUser {
  const _$PNCancelledByUser();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'VerifyPhoneNumberAuthFailure.cancelledByUser()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty(
        'type', 'VerifyPhoneNumberAuthFailure.cancelledByUser'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PNCancelledByUser);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() invalidPhoneNumber,
    required TResult Function() offline,
  }) {
    return cancelledByUser();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? invalidPhoneNumber,
    TResult? Function()? offline,
  }) {
    return cancelledByUser?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? invalidPhoneNumber,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (cancelledByUser != null) {
      return cancelledByUser();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PNCancelledByUser value) cancelledByUser,
    required TResult Function(PNServerError value) serverError,
    required TResult Function(InvalidPhoneNumber value) invalidPhoneNumber,
    required TResult Function(PNOfflineError value) offline,
  }) {
    return cancelledByUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PNCancelledByUser value)? cancelledByUser,
    TResult? Function(PNServerError value)? serverError,
    TResult? Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult? Function(PNOfflineError value)? offline,
  }) {
    return cancelledByUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PNCancelledByUser value)? cancelledByUser,
    TResult Function(PNServerError value)? serverError,
    TResult Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult Function(PNOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (cancelledByUser != null) {
      return cancelledByUser(this);
    }
    return orElse();
  }
}

abstract class PNCancelledByUser implements VerifyPhoneNumberAuthFailure {
  const factory PNCancelledByUser() = _$PNCancelledByUser;
}

/// @nodoc
abstract class _$$PNServerErrorCopyWith<$Res> {
  factory _$$PNServerErrorCopyWith(
          _$PNServerError value, $Res Function(_$PNServerError) then) =
      __$$PNServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PNServerErrorCopyWithImpl<$Res>
    extends _$VerifyPhoneNumberAuthFailureCopyWithImpl<$Res, _$PNServerError>
    implements _$$PNServerErrorCopyWith<$Res> {
  __$$PNServerErrorCopyWithImpl(
      _$PNServerError _value, $Res Function(_$PNServerError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PNServerError with DiagnosticableTreeMixin implements PNServerError {
  const _$PNServerError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'VerifyPhoneNumberAuthFailure.serverError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty(
        'type', 'VerifyPhoneNumberAuthFailure.serverError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PNServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() invalidPhoneNumber,
    required TResult Function() offline,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? invalidPhoneNumber,
    TResult? Function()? offline,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? invalidPhoneNumber,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PNCancelledByUser value) cancelledByUser,
    required TResult Function(PNServerError value) serverError,
    required TResult Function(InvalidPhoneNumber value) invalidPhoneNumber,
    required TResult Function(PNOfflineError value) offline,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PNCancelledByUser value)? cancelledByUser,
    TResult? Function(PNServerError value)? serverError,
    TResult? Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult? Function(PNOfflineError value)? offline,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PNCancelledByUser value)? cancelledByUser,
    TResult Function(PNServerError value)? serverError,
    TResult Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult Function(PNOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class PNServerError implements VerifyPhoneNumberAuthFailure {
  const factory PNServerError() = _$PNServerError;
}

/// @nodoc
abstract class _$$InvalidPhoneNumberCopyWith<$Res> {
  factory _$$InvalidPhoneNumberCopyWith(_$InvalidPhoneNumber value,
          $Res Function(_$InvalidPhoneNumber) then) =
      __$$InvalidPhoneNumberCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvalidPhoneNumberCopyWithImpl<$Res>
    extends _$VerifyPhoneNumberAuthFailureCopyWithImpl<$Res,
        _$InvalidPhoneNumber> implements _$$InvalidPhoneNumberCopyWith<$Res> {
  __$$InvalidPhoneNumberCopyWithImpl(
      _$InvalidPhoneNumber _value, $Res Function(_$InvalidPhoneNumber) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InvalidPhoneNumber
    with DiagnosticableTreeMixin
    implements InvalidPhoneNumber {
  const _$InvalidPhoneNumber();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'VerifyPhoneNumberAuthFailure.invalidPhoneNumber()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty(
        'type', 'VerifyPhoneNumberAuthFailure.invalidPhoneNumber'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvalidPhoneNumber);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() invalidPhoneNumber,
    required TResult Function() offline,
  }) {
    return invalidPhoneNumber();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? invalidPhoneNumber,
    TResult? Function()? offline,
  }) {
    return invalidPhoneNumber?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? invalidPhoneNumber,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (invalidPhoneNumber != null) {
      return invalidPhoneNumber();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PNCancelledByUser value) cancelledByUser,
    required TResult Function(PNServerError value) serverError,
    required TResult Function(InvalidPhoneNumber value) invalidPhoneNumber,
    required TResult Function(PNOfflineError value) offline,
  }) {
    return invalidPhoneNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PNCancelledByUser value)? cancelledByUser,
    TResult? Function(PNServerError value)? serverError,
    TResult? Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult? Function(PNOfflineError value)? offline,
  }) {
    return invalidPhoneNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PNCancelledByUser value)? cancelledByUser,
    TResult Function(PNServerError value)? serverError,
    TResult Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult Function(PNOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (invalidPhoneNumber != null) {
      return invalidPhoneNumber(this);
    }
    return orElse();
  }
}

abstract class InvalidPhoneNumber implements VerifyPhoneNumberAuthFailure {
  const factory InvalidPhoneNumber() = _$InvalidPhoneNumber;
}

/// @nodoc
abstract class _$$PNOfflineErrorCopyWith<$Res> {
  factory _$$PNOfflineErrorCopyWith(
          _$PNOfflineError value, $Res Function(_$PNOfflineError) then) =
      __$$PNOfflineErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PNOfflineErrorCopyWithImpl<$Res>
    extends _$VerifyPhoneNumberAuthFailureCopyWithImpl<$Res, _$PNOfflineError>
    implements _$$PNOfflineErrorCopyWith<$Res> {
  __$$PNOfflineErrorCopyWithImpl(
      _$PNOfflineError _value, $Res Function(_$PNOfflineError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PNOfflineError with DiagnosticableTreeMixin implements PNOfflineError {
  const _$PNOfflineError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'VerifyPhoneNumberAuthFailure.offline()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty('type', 'VerifyPhoneNumberAuthFailure.offline'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PNOfflineError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() cancelledByUser,
    required TResult Function() serverError,
    required TResult Function() invalidPhoneNumber,
    required TResult Function() offline,
  }) {
    return offline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? cancelledByUser,
    TResult? Function()? serverError,
    TResult? Function()? invalidPhoneNumber,
    TResult? Function()? offline,
  }) {
    return offline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? cancelledByUser,
    TResult Function()? serverError,
    TResult Function()? invalidPhoneNumber,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PNCancelledByUser value) cancelledByUser,
    required TResult Function(PNServerError value) serverError,
    required TResult Function(InvalidPhoneNumber value) invalidPhoneNumber,
    required TResult Function(PNOfflineError value) offline,
  }) {
    return offline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PNCancelledByUser value)? cancelledByUser,
    TResult? Function(PNServerError value)? serverError,
    TResult? Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult? Function(PNOfflineError value)? offline,
  }) {
    return offline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PNCancelledByUser value)? cancelledByUser,
    TResult Function(PNServerError value)? serverError,
    TResult Function(InvalidPhoneNumber value)? invalidPhoneNumber,
    TResult Function(PNOfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline(this);
    }
    return orElse();
  }
}

abstract class PNOfflineError implements VerifyPhoneNumberAuthFailure {
  const factory PNOfflineError() = _$PNOfflineError;
}
