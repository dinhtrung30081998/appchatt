import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_failure.freezed.dart';

@freezed
abstract class AuthFailure with _$AuthFailure {
  const factory AuthFailure.cancelledByUser() = CancelledByUser;
  const factory AuthFailure.serverError() = ServerError;
  const factory AuthFailure.emailAlreadyInUse() = EmailAlreadyInUse;
  const factory AuthFailure.invalidEmailAndPassword() = InvalidEmailAndPassword;
  const factory AuthFailure.offline() = OfflineError;
}

@freezed
abstract class GoogleAuthFailure with _$GoogleAuthFailure {
  const factory GoogleAuthFailure.cancelledByUser() = GGCancelledByUser;
  const factory GoogleAuthFailure.serverError() = GGServerError;
  const factory GoogleAuthFailure.accountExistsWithDifferentCredential() = AccountExistsWithDifferentCredential;
  const factory GoogleAuthFailure.emailAlreadyInUse() = GGEmailAlreadyInUse;
  const factory GoogleAuthFailure.invalidEmailAndPassword() = GGInvalidEmailAndPassword;
  const factory GoogleAuthFailure.offline() = GGOfflineError;
}

@freezed
abstract class FacebookAuthFailure with _$FacebookAuthFailure {
  const factory FacebookAuthFailure.failed() = Failed;
  const factory FacebookAuthFailure.operationInProgress() = OperationInProgress;
  const factory FacebookAuthFailure.cancelled() = Cancelled;
  const factory FacebookAuthFailure.serverError() = FbServerError;
  const factory FacebookAuthFailure.offline() = FbOfflineError;
}

@freezed
abstract class VerifyPhoneNumberAuthFailure with _$VerifyPhoneNumberAuthFailure {
  const factory VerifyPhoneNumberAuthFailure.cancelledByUser() = PNCancelledByUser;
  const factory VerifyPhoneNumberAuthFailure.serverError() = PNServerError;
  const factory VerifyPhoneNumberAuthFailure.invalidPhoneNumber() = InvalidPhoneNumber;
  const factory VerifyPhoneNumberAuthFailure.offline() = PNOfflineError;
}
