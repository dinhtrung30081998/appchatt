import 'package:freezed_annotation/freezed_annotation.dart';

part 'search_friends_failure.freezed.dart';

@freezed
abstract class SearchFriendsFailure with _$SearchFriendsFailure {
  const factory SearchFriendsFailure.serverError() = ServerError;
  const factory SearchFriendsFailure.noResultIsFound() = NoResultIsFound;
  const factory SearchFriendsFailure.offline() = OfflineError;
  const factory SearchFriendsFailure.emptyCacheFriends() = EmptyCacheFriends;
}
