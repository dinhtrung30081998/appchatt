// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'search_friends_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SearchFriendsFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() noResultIsFound,
    required TResult Function() offline,
    required TResult Function() emptyCacheFriends,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? noResultIsFound,
    TResult? Function()? offline,
    TResult? Function()? emptyCacheFriends,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? noResultIsFound,
    TResult Function()? offline,
    TResult Function()? emptyCacheFriends,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(NoResultIsFound value) noResultIsFound,
    required TResult Function(OfflineError value) offline,
    required TResult Function(EmptyCacheFriends value) emptyCacheFriends,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(NoResultIsFound value)? noResultIsFound,
    TResult? Function(OfflineError value)? offline,
    TResult? Function(EmptyCacheFriends value)? emptyCacheFriends,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(NoResultIsFound value)? noResultIsFound,
    TResult Function(OfflineError value)? offline,
    TResult Function(EmptyCacheFriends value)? emptyCacheFriends,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchFriendsFailureCopyWith<$Res> {
  factory $SearchFriendsFailureCopyWith(SearchFriendsFailure value,
          $Res Function(SearchFriendsFailure) then) =
      _$SearchFriendsFailureCopyWithImpl<$Res, SearchFriendsFailure>;
}

/// @nodoc
class _$SearchFriendsFailureCopyWithImpl<$Res,
        $Val extends SearchFriendsFailure>
    implements $SearchFriendsFailureCopyWith<$Res> {
  _$SearchFriendsFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ServerErrorCopyWith<$Res> {
  factory _$$ServerErrorCopyWith(
          _$ServerError value, $Res Function(_$ServerError) then) =
      __$$ServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ServerErrorCopyWithImpl<$Res>
    extends _$SearchFriendsFailureCopyWithImpl<$Res, _$ServerError>
    implements _$$ServerErrorCopyWith<$Res> {
  __$$ServerErrorCopyWithImpl(
      _$ServerError _value, $Res Function(_$ServerError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ServerError implements ServerError {
  const _$ServerError();

  @override
  String toString() {
    return 'SearchFriendsFailure.serverError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() noResultIsFound,
    required TResult Function() offline,
    required TResult Function() emptyCacheFriends,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? noResultIsFound,
    TResult? Function()? offline,
    TResult? Function()? emptyCacheFriends,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? noResultIsFound,
    TResult Function()? offline,
    TResult Function()? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(NoResultIsFound value) noResultIsFound,
    required TResult Function(OfflineError value) offline,
    required TResult Function(EmptyCacheFriends value) emptyCacheFriends,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(NoResultIsFound value)? noResultIsFound,
    TResult? Function(OfflineError value)? offline,
    TResult? Function(EmptyCacheFriends value)? emptyCacheFriends,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(NoResultIsFound value)? noResultIsFound,
    TResult Function(OfflineError value)? offline,
    TResult Function(EmptyCacheFriends value)? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class ServerError implements SearchFriendsFailure {
  const factory ServerError() = _$ServerError;
}

/// @nodoc
abstract class _$$NoResultIsFoundCopyWith<$Res> {
  factory _$$NoResultIsFoundCopyWith(
          _$NoResultIsFound value, $Res Function(_$NoResultIsFound) then) =
      __$$NoResultIsFoundCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NoResultIsFoundCopyWithImpl<$Res>
    extends _$SearchFriendsFailureCopyWithImpl<$Res, _$NoResultIsFound>
    implements _$$NoResultIsFoundCopyWith<$Res> {
  __$$NoResultIsFoundCopyWithImpl(
      _$NoResultIsFound _value, $Res Function(_$NoResultIsFound) _then)
      : super(_value, _then);
}

/// @nodoc

class _$NoResultIsFound implements NoResultIsFound {
  const _$NoResultIsFound();

  @override
  String toString() {
    return 'SearchFriendsFailure.noResultIsFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NoResultIsFound);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() noResultIsFound,
    required TResult Function() offline,
    required TResult Function() emptyCacheFriends,
  }) {
    return noResultIsFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? noResultIsFound,
    TResult? Function()? offline,
    TResult? Function()? emptyCacheFriends,
  }) {
    return noResultIsFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? noResultIsFound,
    TResult Function()? offline,
    TResult Function()? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (noResultIsFound != null) {
      return noResultIsFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(NoResultIsFound value) noResultIsFound,
    required TResult Function(OfflineError value) offline,
    required TResult Function(EmptyCacheFriends value) emptyCacheFriends,
  }) {
    return noResultIsFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(NoResultIsFound value)? noResultIsFound,
    TResult? Function(OfflineError value)? offline,
    TResult? Function(EmptyCacheFriends value)? emptyCacheFriends,
  }) {
    return noResultIsFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(NoResultIsFound value)? noResultIsFound,
    TResult Function(OfflineError value)? offline,
    TResult Function(EmptyCacheFriends value)? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (noResultIsFound != null) {
      return noResultIsFound(this);
    }
    return orElse();
  }
}

abstract class NoResultIsFound implements SearchFriendsFailure {
  const factory NoResultIsFound() = _$NoResultIsFound;
}

/// @nodoc
abstract class _$$OfflineErrorCopyWith<$Res> {
  factory _$$OfflineErrorCopyWith(
          _$OfflineError value, $Res Function(_$OfflineError) then) =
      __$$OfflineErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$OfflineErrorCopyWithImpl<$Res>
    extends _$SearchFriendsFailureCopyWithImpl<$Res, _$OfflineError>
    implements _$$OfflineErrorCopyWith<$Res> {
  __$$OfflineErrorCopyWithImpl(
      _$OfflineError _value, $Res Function(_$OfflineError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$OfflineError implements OfflineError {
  const _$OfflineError();

  @override
  String toString() {
    return 'SearchFriendsFailure.offline()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$OfflineError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() noResultIsFound,
    required TResult Function() offline,
    required TResult Function() emptyCacheFriends,
  }) {
    return offline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? noResultIsFound,
    TResult? Function()? offline,
    TResult? Function()? emptyCacheFriends,
  }) {
    return offline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? noResultIsFound,
    TResult Function()? offline,
    TResult Function()? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(NoResultIsFound value) noResultIsFound,
    required TResult Function(OfflineError value) offline,
    required TResult Function(EmptyCacheFriends value) emptyCacheFriends,
  }) {
    return offline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(NoResultIsFound value)? noResultIsFound,
    TResult? Function(OfflineError value)? offline,
    TResult? Function(EmptyCacheFriends value)? emptyCacheFriends,
  }) {
    return offline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(NoResultIsFound value)? noResultIsFound,
    TResult Function(OfflineError value)? offline,
    TResult Function(EmptyCacheFriends value)? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline(this);
    }
    return orElse();
  }
}

abstract class OfflineError implements SearchFriendsFailure {
  const factory OfflineError() = _$OfflineError;
}

/// @nodoc
abstract class _$$EmptyCacheFriendsCopyWith<$Res> {
  factory _$$EmptyCacheFriendsCopyWith(
          _$EmptyCacheFriends value, $Res Function(_$EmptyCacheFriends) then) =
      __$$EmptyCacheFriendsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EmptyCacheFriendsCopyWithImpl<$Res>
    extends _$SearchFriendsFailureCopyWithImpl<$Res, _$EmptyCacheFriends>
    implements _$$EmptyCacheFriendsCopyWith<$Res> {
  __$$EmptyCacheFriendsCopyWithImpl(
      _$EmptyCacheFriends _value, $Res Function(_$EmptyCacheFriends) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EmptyCacheFriends implements EmptyCacheFriends {
  const _$EmptyCacheFriends();

  @override
  String toString() {
    return 'SearchFriendsFailure.emptyCacheFriends()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EmptyCacheFriends);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() noResultIsFound,
    required TResult Function() offline,
    required TResult Function() emptyCacheFriends,
  }) {
    return emptyCacheFriends();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? noResultIsFound,
    TResult? Function()? offline,
    TResult? Function()? emptyCacheFriends,
  }) {
    return emptyCacheFriends?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? noResultIsFound,
    TResult Function()? offline,
    TResult Function()? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (emptyCacheFriends != null) {
      return emptyCacheFriends();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(NoResultIsFound value) noResultIsFound,
    required TResult Function(OfflineError value) offline,
    required TResult Function(EmptyCacheFriends value) emptyCacheFriends,
  }) {
    return emptyCacheFriends(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(NoResultIsFound value)? noResultIsFound,
    TResult? Function(OfflineError value)? offline,
    TResult? Function(EmptyCacheFriends value)? emptyCacheFriends,
  }) {
    return emptyCacheFriends?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(NoResultIsFound value)? noResultIsFound,
    TResult Function(OfflineError value)? offline,
    TResult Function(EmptyCacheFriends value)? emptyCacheFriends,
    required TResult orElse(),
  }) {
    if (emptyCacheFriends != null) {
      return emptyCacheFriends(this);
    }
    return orElse();
  }
}

abstract class EmptyCacheFriends implements SearchFriendsFailure {
  const factory EmptyCacheFriends() = _$EmptyCacheFriends;
}
