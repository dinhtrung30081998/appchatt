import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'user_info_failure.freezed.dart';

@freezed
abstract class UserInfoFailure with _$UserInfoFailure {
  const factory UserInfoFailure.serverError() = ServerError;
  const factory UserInfoFailure.emptyCacheFailure() = EmptyCacheFailure;
  const factory UserInfoFailure.offline() = OfflineError;
}

@freezed
abstract class CreatePostFailure with _$CreatePostFailure {
  const factory CreatePostFailure.createPostOffline() = CreatePostOffline;
  const factory CreatePostFailure.createPostFail() = CreatePostFail;
}

@freezed
abstract class EditUserInfoFailure with _$EditUserInfoFailure {
  const factory EditUserInfoFailure.editOffline() = EditOffline;
  const factory EditUserInfoFailure.editFail() = EditFail;
}
