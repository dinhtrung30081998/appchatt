// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_info_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UserInfoFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() emptyCacheFailure,
    required TResult Function() offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? emptyCacheFailure,
    TResult? Function()? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? emptyCacheFailure,
    TResult Function()? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmptyCacheFailure value) emptyCacheFailure,
    required TResult Function(OfflineError value) offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult? Function(OfflineError value)? offline,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfoFailureCopyWith<$Res> {
  factory $UserInfoFailureCopyWith(
          UserInfoFailure value, $Res Function(UserInfoFailure) then) =
      _$UserInfoFailureCopyWithImpl<$Res, UserInfoFailure>;
}

/// @nodoc
class _$UserInfoFailureCopyWithImpl<$Res, $Val extends UserInfoFailure>
    implements $UserInfoFailureCopyWith<$Res> {
  _$UserInfoFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ServerErrorCopyWith<$Res> {
  factory _$$ServerErrorCopyWith(
          _$ServerError value, $Res Function(_$ServerError) then) =
      __$$ServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ServerErrorCopyWithImpl<$Res>
    extends _$UserInfoFailureCopyWithImpl<$Res, _$ServerError>
    implements _$$ServerErrorCopyWith<$Res> {
  __$$ServerErrorCopyWithImpl(
      _$ServerError _value, $Res Function(_$ServerError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ServerError with DiagnosticableTreeMixin implements ServerError {
  const _$ServerError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserInfoFailure.serverError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'UserInfoFailure.serverError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() emptyCacheFailure,
    required TResult Function() offline,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? emptyCacheFailure,
    TResult? Function()? offline,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? emptyCacheFailure,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmptyCacheFailure value) emptyCacheFailure,
    required TResult Function(OfflineError value) offline,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult? Function(OfflineError value)? offline,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class ServerError implements UserInfoFailure {
  const factory ServerError() = _$ServerError;
}

/// @nodoc
abstract class _$$EmptyCacheFailureCopyWith<$Res> {
  factory _$$EmptyCacheFailureCopyWith(
          _$EmptyCacheFailure value, $Res Function(_$EmptyCacheFailure) then) =
      __$$EmptyCacheFailureCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EmptyCacheFailureCopyWithImpl<$Res>
    extends _$UserInfoFailureCopyWithImpl<$Res, _$EmptyCacheFailure>
    implements _$$EmptyCacheFailureCopyWith<$Res> {
  __$$EmptyCacheFailureCopyWithImpl(
      _$EmptyCacheFailure _value, $Res Function(_$EmptyCacheFailure) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EmptyCacheFailure
    with DiagnosticableTreeMixin
    implements EmptyCacheFailure {
  const _$EmptyCacheFailure();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserInfoFailure.emptyCacheFailure()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'UserInfoFailure.emptyCacheFailure'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EmptyCacheFailure);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() emptyCacheFailure,
    required TResult Function() offline,
  }) {
    return emptyCacheFailure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? emptyCacheFailure,
    TResult? Function()? offline,
  }) {
    return emptyCacheFailure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? emptyCacheFailure,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (emptyCacheFailure != null) {
      return emptyCacheFailure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmptyCacheFailure value) emptyCacheFailure,
    required TResult Function(OfflineError value) offline,
  }) {
    return emptyCacheFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult? Function(OfflineError value)? offline,
  }) {
    return emptyCacheFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (emptyCacheFailure != null) {
      return emptyCacheFailure(this);
    }
    return orElse();
  }
}

abstract class EmptyCacheFailure implements UserInfoFailure {
  const factory EmptyCacheFailure() = _$EmptyCacheFailure;
}

/// @nodoc
abstract class _$$OfflineErrorCopyWith<$Res> {
  factory _$$OfflineErrorCopyWith(
          _$OfflineError value, $Res Function(_$OfflineError) then) =
      __$$OfflineErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$OfflineErrorCopyWithImpl<$Res>
    extends _$UserInfoFailureCopyWithImpl<$Res, _$OfflineError>
    implements _$$OfflineErrorCopyWith<$Res> {
  __$$OfflineErrorCopyWithImpl(
      _$OfflineError _value, $Res Function(_$OfflineError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$OfflineError with DiagnosticableTreeMixin implements OfflineError {
  const _$OfflineError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserInfoFailure.offline()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'UserInfoFailure.offline'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$OfflineError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() serverError,
    required TResult Function() emptyCacheFailure,
    required TResult Function() offline,
  }) {
    return offline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? serverError,
    TResult? Function()? emptyCacheFailure,
    TResult? Function()? offline,
  }) {
    return offline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? serverError,
    TResult Function()? emptyCacheFailure,
    TResult Function()? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ServerError value) serverError,
    required TResult Function(EmptyCacheFailure value) emptyCacheFailure,
    required TResult Function(OfflineError value) offline,
  }) {
    return offline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ServerError value)? serverError,
    TResult? Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult? Function(OfflineError value)? offline,
  }) {
    return offline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ServerError value)? serverError,
    TResult Function(EmptyCacheFailure value)? emptyCacheFailure,
    TResult Function(OfflineError value)? offline,
    required TResult orElse(),
  }) {
    if (offline != null) {
      return offline(this);
    }
    return orElse();
  }
}

abstract class OfflineError implements UserInfoFailure {
  const factory OfflineError() = _$OfflineError;
}

/// @nodoc
mixin _$CreatePostFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() createPostOffline,
    required TResult Function() createPostFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? createPostOffline,
    TResult? Function()? createPostFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? createPostOffline,
    TResult Function()? createPostFail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreatePostOffline value) createPostOffline,
    required TResult Function(CreatePostFail value) createPostFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreatePostOffline value)? createPostOffline,
    TResult? Function(CreatePostFail value)? createPostFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreatePostOffline value)? createPostOffline,
    TResult Function(CreatePostFail value)? createPostFail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreatePostFailureCopyWith<$Res> {
  factory $CreatePostFailureCopyWith(
          CreatePostFailure value, $Res Function(CreatePostFailure) then) =
      _$CreatePostFailureCopyWithImpl<$Res, CreatePostFailure>;
}

/// @nodoc
class _$CreatePostFailureCopyWithImpl<$Res, $Val extends CreatePostFailure>
    implements $CreatePostFailureCopyWith<$Res> {
  _$CreatePostFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CreatePostOfflineCopyWith<$Res> {
  factory _$$CreatePostOfflineCopyWith(
          _$CreatePostOffline value, $Res Function(_$CreatePostOffline) then) =
      __$$CreatePostOfflineCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreatePostOfflineCopyWithImpl<$Res>
    extends _$CreatePostFailureCopyWithImpl<$Res, _$CreatePostOffline>
    implements _$$CreatePostOfflineCopyWith<$Res> {
  __$$CreatePostOfflineCopyWithImpl(
      _$CreatePostOffline _value, $Res Function(_$CreatePostOffline) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CreatePostOffline
    with DiagnosticableTreeMixin
    implements CreatePostOffline {
  const _$CreatePostOffline();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CreatePostFailure.createPostOffline()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty('type', 'CreatePostFailure.createPostOffline'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CreatePostOffline);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() createPostOffline,
    required TResult Function() createPostFail,
  }) {
    return createPostOffline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? createPostOffline,
    TResult? Function()? createPostFail,
  }) {
    return createPostOffline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? createPostOffline,
    TResult Function()? createPostFail,
    required TResult orElse(),
  }) {
    if (createPostOffline != null) {
      return createPostOffline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreatePostOffline value) createPostOffline,
    required TResult Function(CreatePostFail value) createPostFail,
  }) {
    return createPostOffline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreatePostOffline value)? createPostOffline,
    TResult? Function(CreatePostFail value)? createPostFail,
  }) {
    return createPostOffline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreatePostOffline value)? createPostOffline,
    TResult Function(CreatePostFail value)? createPostFail,
    required TResult orElse(),
  }) {
    if (createPostOffline != null) {
      return createPostOffline(this);
    }
    return orElse();
  }
}

abstract class CreatePostOffline implements CreatePostFailure {
  const factory CreatePostOffline() = _$CreatePostOffline;
}

/// @nodoc
abstract class _$$CreatePostFailCopyWith<$Res> {
  factory _$$CreatePostFailCopyWith(
          _$CreatePostFail value, $Res Function(_$CreatePostFail) then) =
      __$$CreatePostFailCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreatePostFailCopyWithImpl<$Res>
    extends _$CreatePostFailureCopyWithImpl<$Res, _$CreatePostFail>
    implements _$$CreatePostFailCopyWith<$Res> {
  __$$CreatePostFailCopyWithImpl(
      _$CreatePostFail _value, $Res Function(_$CreatePostFail) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CreatePostFail with DiagnosticableTreeMixin implements CreatePostFail {
  const _$CreatePostFail();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'CreatePostFailure.createPostFail()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'CreatePostFailure.createPostFail'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CreatePostFail);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() createPostOffline,
    required TResult Function() createPostFail,
  }) {
    return createPostFail();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? createPostOffline,
    TResult? Function()? createPostFail,
  }) {
    return createPostFail?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? createPostOffline,
    TResult Function()? createPostFail,
    required TResult orElse(),
  }) {
    if (createPostFail != null) {
      return createPostFail();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreatePostOffline value) createPostOffline,
    required TResult Function(CreatePostFail value) createPostFail,
  }) {
    return createPostFail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreatePostOffline value)? createPostOffline,
    TResult? Function(CreatePostFail value)? createPostFail,
  }) {
    return createPostFail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreatePostOffline value)? createPostOffline,
    TResult Function(CreatePostFail value)? createPostFail,
    required TResult orElse(),
  }) {
    if (createPostFail != null) {
      return createPostFail(this);
    }
    return orElse();
  }
}

abstract class CreatePostFail implements CreatePostFailure {
  const factory CreatePostFail() = _$CreatePostFail;
}

/// @nodoc
mixin _$EditUserInfoFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() editOffline,
    required TResult Function() editFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? editOffline,
    TResult? Function()? editFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? editOffline,
    TResult Function()? editFail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EditOffline value) editOffline,
    required TResult Function(EditFail value) editFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EditOffline value)? editOffline,
    TResult? Function(EditFail value)? editFail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EditOffline value)? editOffline,
    TResult Function(EditFail value)? editFail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EditUserInfoFailureCopyWith<$Res> {
  factory $EditUserInfoFailureCopyWith(
          EditUserInfoFailure value, $Res Function(EditUserInfoFailure) then) =
      _$EditUserInfoFailureCopyWithImpl<$Res, EditUserInfoFailure>;
}

/// @nodoc
class _$EditUserInfoFailureCopyWithImpl<$Res, $Val extends EditUserInfoFailure>
    implements $EditUserInfoFailureCopyWith<$Res> {
  _$EditUserInfoFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$EditOfflineCopyWith<$Res> {
  factory _$$EditOfflineCopyWith(
          _$EditOffline value, $Res Function(_$EditOffline) then) =
      __$$EditOfflineCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EditOfflineCopyWithImpl<$Res>
    extends _$EditUserInfoFailureCopyWithImpl<$Res, _$EditOffline>
    implements _$$EditOfflineCopyWith<$Res> {
  __$$EditOfflineCopyWithImpl(
      _$EditOffline _value, $Res Function(_$EditOffline) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EditOffline with DiagnosticableTreeMixin implements EditOffline {
  const _$EditOffline();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EditUserInfoFailure.editOffline()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'EditUserInfoFailure.editOffline'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EditOffline);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() editOffline,
    required TResult Function() editFail,
  }) {
    return editOffline();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? editOffline,
    TResult? Function()? editFail,
  }) {
    return editOffline?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? editOffline,
    TResult Function()? editFail,
    required TResult orElse(),
  }) {
    if (editOffline != null) {
      return editOffline();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EditOffline value) editOffline,
    required TResult Function(EditFail value) editFail,
  }) {
    return editOffline(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EditOffline value)? editOffline,
    TResult? Function(EditFail value)? editFail,
  }) {
    return editOffline?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EditOffline value)? editOffline,
    TResult Function(EditFail value)? editFail,
    required TResult orElse(),
  }) {
    if (editOffline != null) {
      return editOffline(this);
    }
    return orElse();
  }
}

abstract class EditOffline implements EditUserInfoFailure {
  const factory EditOffline() = _$EditOffline;
}

/// @nodoc
abstract class _$$EditFailCopyWith<$Res> {
  factory _$$EditFailCopyWith(
          _$EditFail value, $Res Function(_$EditFail) then) =
      __$$EditFailCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EditFailCopyWithImpl<$Res>
    extends _$EditUserInfoFailureCopyWithImpl<$Res, _$EditFail>
    implements _$$EditFailCopyWith<$Res> {
  __$$EditFailCopyWithImpl(_$EditFail _value, $Res Function(_$EditFail) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EditFail with DiagnosticableTreeMixin implements EditFail {
  const _$EditFail();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EditUserInfoFailure.editFail()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'EditUserInfoFailure.editFail'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EditFail);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() editOffline,
    required TResult Function() editFail,
  }) {
    return editFail();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? editOffline,
    TResult? Function()? editFail,
  }) {
    return editFail?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? editOffline,
    TResult Function()? editFail,
    required TResult orElse(),
  }) {
    if (editFail != null) {
      return editFail();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EditOffline value) editOffline,
    required TResult Function(EditFail value) editFail,
  }) {
    return editFail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EditOffline value)? editOffline,
    TResult? Function(EditFail value)? editFail,
  }) {
    return editFail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EditOffline value)? editOffline,
    TResult Function(EditFail value)? editFail,
    required TResult orElse(),
  }) {
    if (editFail != null) {
      return editFail(this);
    }
    return orElse();
  }
}

abstract class EditFail implements EditUserInfoFailure {
  const factory EditFail() = _$EditFail;
}
