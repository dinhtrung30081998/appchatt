import 'package:dartz/dartz.dart';

import '../../value_objects/failures/auth/value_auth_failure.dart';

const _email =
    r"""^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$""";

Either<ValueAuthFailure<String>, String> validateEmailAddress(String input) {
  if (input.isEmpty) return left(ValueAuthFailure.invalidEmail(failedValue: input));
  if (RegExp(_email).hasMatch(input)) {
    return right(input);
  } else {
    return left(ValueAuthFailure.invalidEmail(failedValue: input));
  }
}

Either<ValueAuthFailure<String>, String> validatePassword(String input) {
  if (input.isEmpty) return left(ValueAuthFailure.shortPassword(failedValue: input));
  if (input.length >= 6) {
    return right(input);
  } else {
    return left(ValueAuthFailure.shortPassword(failedValue: input));
  }
}

Either<ValueAuthFailure<String>, String> validateFullName(String input) {
  if (input.isNotEmpty) {
    return right(input);
  } else {
    return left(ValueAuthFailure.fullNameIsEmpty(failedValue: input));
  }
}

Either<ValueAuthFailure<String>, String> validatePhoneNumber(String input) {
  if (input.isNotEmpty) {
    return right(input);
  } else {
    return left(ValueAuthFailure.phoneNumberIsEmpty(failedValue: input));
  }
}

Either<ValueAuthFailure<String>, String> validateConfirmPassword(String confirmPassword, String password) {
  if (confirmPassword.isEmpty) {
    return left(ValueAuthFailure.confirmPasswordMustBeSameAsPassword(failedValue: confirmPassword));
  }
  if (confirmPassword == password) {
    return right(confirmPassword);
  } else {
    return left(ValueAuthFailure.confirmPasswordMustBeSameAsPassword(failedValue: confirmPassword));
  }
}
