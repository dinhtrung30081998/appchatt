import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';

import '../../core/failures/auth/auth_failure.dart';
import '../../value_objects/auth/email_address.dart';
import '../../value_objects/auth/full_name.dart';
import '../../value_objects/auth/password.dart';
import '../../value_objects/auth/phone_number.dart';

abstract class ISignUpWithEmailAndPassword {
  Future<Either<AuthFailure, Unit>> call({
    required EmailAddress email,
    required Password password,
    required FullName fullName,
    required PhoneNumber phoneNumber,
    required String profilePicture,
    required String token,
  });
}

abstract class ISignInWithEmailAndPassword {
  Future<Either<AuthFailure, Unit>> call({
    required EmailAddress email,
    required Password password,
  });
}

abstract class IVerifyPhoneNumber {
  Future<Either<VerifyPhoneNumberAuthFailure, Unit>> call(
      {required PhoneNumber phoneNumber, required BuildContext context});
}

abstract class ISignInWithGoogle {
  Future<Either<GoogleAuthFailure, Unit>> call(String token);
}

abstract class ISignInWithFacebook {
  Future<Either<FacebookAuthFailure, Unit>> call();
}

abstract class IUploadImageToStorage {
  Future<Either<AuthFailure, Unit>> call(String imageUrl);
}

abstract class IActionCamera {
  Future<Either<AuthFailure, String>> call(ImageSource imageSource, String dirImages);
}

abstract class IGetFirebaseMessagingToken {
  Future<String> call();
}

abstract class ISignOut {
  Future<Unit> call(String status);
}
