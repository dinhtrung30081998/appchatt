import 'package:dartz/dartz.dart';

import '../../core/failures/user_info/user_info_failure.dart';
import '../../entities/post/post_entity.dart';
import '../../entities/user/user_entity.dart';
import '../../value_objects/auth/email_address.dart';

abstract class ICacheUserInfo {
  Future<Unit> call(EmailAddress emailAddress, String token);
}

abstract class ICreateConversationChat {
  Future<Unit> call(String partnerUserId);
}

abstract class IDeleteConversationChat {
  Future<Unit> call(String partnerUserId);
}

abstract class IGetUserInfoLocal {
  bool? isCachedUserId();
  String? getUserId();
  String? getEmail();
  String? getFullName();
  String? getPhoneNumber();
  String? getProfilePicture();
  List<String>? getFollowing();
  String? getToken();
}

abstract class IStreamGetUserInfoByUid {
  Stream<UserEntity> call(String uid);
}

abstract class IEditUserInfoByUid {
  Future<Either<EditUserInfoFailure, Unit>> call(String uid, String fullName, String bio);
}

abstract class IFollowUser {
  Future<Unit> call(String uid, String followId);
}

abstract class ISetStatusUser {
  Future<Unit> call(String status, String token);
}

abstract class IGetListPost {
  Stream<List<PostEntity>> call();
}
