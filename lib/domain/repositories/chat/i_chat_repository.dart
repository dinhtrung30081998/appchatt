import 'package:app_chat/domain/entities/user/user_entity.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_database/firebase_database.dart';

abstract class IStreamListChat {
  Stream<DatabaseEvent> call();
}

abstract class ISendChat {
  Future<Unit> call(UserEntity userPartner, String message, String type);
}

abstract class ISendNotification {
  Future<Unit> call(UserEntity userPartner, String message);
}

abstract class IOutRoomChat {
  Future<Unit> call(String partnerId);
}
