import 'package:dartz/dartz.dart';
import 'package:image_picker/image_picker.dart';

import '../../core/failures/user_info/user_info_failure.dart';
import '../../entities/comment/comment_entity.dart';

abstract class ILikePost {
  Future<Unit> call(String postId, String userId, List likes);
}

abstract class ILikeComment {
  Future<Unit> call(String postId, String commentId, String userId, List likes);
}

abstract class ICommentPost {
  Future<Unit> call(
    String postId,
    String comment,
    String userId,
    String userName,
    String profilePicture,
  );
}

abstract class ICreatePost {
  Future<Either<CreatePostFailure, Unit>> call(
    String fullName,
    String description,
    List<String> postImageUrl,
    String profileImage,
  );
}

abstract class IDeletePost {
  Future<Unit> call(String postId);
}

abstract class IHidePost {
  Future<Unit> call(String postId);
}

abstract class IShowPost {
  Future<Unit> call(String postId);
}

abstract class IGetListCommentByPostId {
  Stream<List<CommentEntity>> call(String postId);
}

abstract class IUploadPostImageToStorage {
  Future<String> call(String childName, XFile file, bool isPost);
}
