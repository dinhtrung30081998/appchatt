
import 'package:dartz/dartz.dart';

import '../../core/failures/search_friends/search_friends_failure.dart';
import '../../entities/user/user_entity.dart';

abstract class ISearchFriends {
  Future<Either<SearchFriendsFailure, List<UserEntity>>> call(String query);
}
