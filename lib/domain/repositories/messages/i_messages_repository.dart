import 'package:dartz/dartz.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:image_picker/image_picker.dart';

abstract class IStreamListMessages {
  Stream<DatabaseEvent> call();
}

abstract class IReadLastMessages {
  Future<Unit> call(String partnerId);
}

abstract class IDeleteMessageById {
  Future<Unit> call(String chatId);
}

abstract class IUploadImage {
  Future<String> call(XFile xFile);
}
