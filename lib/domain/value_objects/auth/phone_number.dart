import '../failures/auth/value_auth_failure.dart';
import 'package:dartz/dartz.dart';

import '../../core/validators/value_validators.dart';
import '../value_objects.dart';

class PhoneNumber extends ValueObject<String> {
  final Either<ValueAuthFailure<String>, String> _value;

  factory PhoneNumber(String input) {
    return PhoneNumber._(
      validatePhoneNumber(input),
    );
  }

  const PhoneNumber._(this._value);

  @override
  Either<ValueAuthFailure<String>, String> get value => _value;
}
