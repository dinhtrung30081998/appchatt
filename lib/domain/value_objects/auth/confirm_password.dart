import 'package:dartz/dartz.dart';

import '../../core/validators/value_validators.dart';
import '../failures/auth/value_auth_failure.dart';
import '../value_objects.dart';

class ConfirmPassword extends ValueObject<String> {
  final Either<ValueAuthFailure<String>, String> _value;

  factory ConfirmPassword(String confirmPassword, String password) {
    return ConfirmPassword._(
      validateConfirmPassword(confirmPassword, password),
    );
  }

  const ConfirmPassword._(this._value);

  @override
  Either<ValueAuthFailure<String>, String> get value => _value;
}
