import 'package:dartz/dartz.dart';

import '../../core/validators/value_validators.dart';
import '../failures/auth/value_auth_failure.dart';
import '../value_objects.dart';

class FullName extends ValueObject<String> {
  final Either<ValueAuthFailure<String>, String> _value;

  factory FullName(String input) {
    return FullName._(
      validateFullName(input),
    );
  }

  const FullName._(this._value);

  @override
  Either<ValueAuthFailure<String>, String> get value => _value;
}
