import 'package:dartz/dartz.dart';
import 'package:uuid/uuid.dart';

import '../failures/auth/value_auth_failure.dart';
import '../value_objects.dart';

class UserUniqueID extends ValueObject<String> {
  final Either<ValueAuthFailure<String>, String> _value;

  factory UserUniqueID() {
    return UserUniqueID._(
      right(const Uuid().v1()),
    );
  }

  factory UserUniqueID.fromUniqueString(String uniqueID) {
    return UserUniqueID._(right(uniqueID));
  }
  const UserUniqueID._(this._value);

  @override
  Either<ValueAuthFailure<String>, String> get value => _value;
}
