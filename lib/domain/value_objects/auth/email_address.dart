import 'package:dartz/dartz.dart';

import '../../core/validators/value_validators.dart';
import '../failures/auth/value_auth_failure.dart';
import '../value_objects.dart';

class EmailAddress extends ValueObject<String> {
  final Either<ValueAuthFailure<String>, String> _value;

  factory EmailAddress(String input) {
    return EmailAddress._(
      validateEmailAddress(input),
    );
  }

  const EmailAddress._(this._value);

  @override
  Either<ValueAuthFailure<String>, String> get value => _value;
}
