import 'package:freezed_annotation/freezed_annotation.dart';

part 'value_auth_failure.freezed.dart';

@freezed
abstract class ValueAuthFailure<T> with _$ValueAuthFailure<T> {
  const factory ValueAuthFailure.invalidEmail({
    required String failedValue,
  }) = InvalidEmail<T>;
  const factory ValueAuthFailure.shortPassword({
    required String failedValue,
  }) = ShortPassword<T>;
  const factory ValueAuthFailure.fullNameIsEmpty({
    required String failedValue,
  }) = FullNameIsEmpty<T>;
  const factory ValueAuthFailure.phoneNumberIsEmpty({
    required String failedValue,
  }) = PhoneNumberIsEmpty<T>;
  const factory ValueAuthFailure.confirmPasswordMustBeSameAsPassword({
    required String failedValue,
  }) = ConfirmPasswordMustBeSameAsPassword<T>;
}
