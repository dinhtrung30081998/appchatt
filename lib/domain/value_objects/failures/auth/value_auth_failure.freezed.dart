// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'value_auth_failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ValueAuthFailure<T> {
  String get failedValue => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) shortPassword,
    required TResult Function(String failedValue) fullNameIsEmpty,
    required TResult Function(String failedValue) phoneNumberIsEmpty,
    required TResult Function(String failedValue)
        confirmPasswordMustBeSameAsPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? shortPassword,
    TResult? Function(String failedValue)? fullNameIsEmpty,
    TResult? Function(String failedValue)? phoneNumberIsEmpty,
    TResult? Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? shortPassword,
    TResult Function(String failedValue)? fullNameIsEmpty,
    TResult Function(String failedValue)? phoneNumberIsEmpty,
    TResult Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(ShortPassword<T> value) shortPassword,
    required TResult Function(FullNameIsEmpty<T> value) fullNameIsEmpty,
    required TResult Function(PhoneNumberIsEmpty<T> value) phoneNumberIsEmpty,
    required TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)
        confirmPasswordMustBeSameAsPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidEmail<T> value)? invalidEmail,
    TResult? Function(ShortPassword<T> value)? shortPassword,
    TResult? Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult? Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult? Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(ShortPassword<T> value)? shortPassword,
    TResult Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValueAuthFailureCopyWith<T, ValueAuthFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueAuthFailureCopyWith<T, $Res> {
  factory $ValueAuthFailureCopyWith(
          ValueAuthFailure<T> value, $Res Function(ValueAuthFailure<T>) then) =
      _$ValueAuthFailureCopyWithImpl<T, $Res, ValueAuthFailure<T>>;
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class _$ValueAuthFailureCopyWithImpl<T, $Res, $Val extends ValueAuthFailure<T>>
    implements $ValueAuthFailureCopyWith<T, $Res> {
  _$ValueAuthFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_value.copyWith(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$InvalidEmailCopyWith<T, $Res>
    implements $ValueAuthFailureCopyWith<T, $Res> {
  factory _$$InvalidEmailCopyWith(
          _$InvalidEmail<T> value, $Res Function(_$InvalidEmail<T>) then) =
      __$$InvalidEmailCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$InvalidEmailCopyWithImpl<T, $Res>
    extends _$ValueAuthFailureCopyWithImpl<T, $Res, _$InvalidEmail<T>>
    implements _$$InvalidEmailCopyWith<T, $Res> {
  __$$InvalidEmailCopyWithImpl(
      _$InvalidEmail<T> _value, $Res Function(_$InvalidEmail<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$InvalidEmail<T>(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InvalidEmail<T> implements InvalidEmail<T> {
  const _$InvalidEmail({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueAuthFailure<$T>.invalidEmail(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InvalidEmail<T> &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InvalidEmailCopyWith<T, _$InvalidEmail<T>> get copyWith =>
      __$$InvalidEmailCopyWithImpl<T, _$InvalidEmail<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) shortPassword,
    required TResult Function(String failedValue) fullNameIsEmpty,
    required TResult Function(String failedValue) phoneNumberIsEmpty,
    required TResult Function(String failedValue)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return invalidEmail(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? shortPassword,
    TResult? Function(String failedValue)? fullNameIsEmpty,
    TResult? Function(String failedValue)? phoneNumberIsEmpty,
    TResult? Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
  }) {
    return invalidEmail?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? shortPassword,
    TResult Function(String failedValue)? fullNameIsEmpty,
    TResult Function(String failedValue)? phoneNumberIsEmpty,
    TResult Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(ShortPassword<T> value) shortPassword,
    required TResult Function(FullNameIsEmpty<T> value) fullNameIsEmpty,
    required TResult Function(PhoneNumberIsEmpty<T> value) phoneNumberIsEmpty,
    required TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return invalidEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidEmail<T> value)? invalidEmail,
    TResult? Function(ShortPassword<T> value)? shortPassword,
    TResult? Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult? Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult? Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
  }) {
    return invalidEmail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(ShortPassword<T> value)? shortPassword,
    TResult Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(this);
    }
    return orElse();
  }
}

abstract class InvalidEmail<T> implements ValueAuthFailure<T> {
  const factory InvalidEmail({required final String failedValue}) =
      _$InvalidEmail<T>;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$InvalidEmailCopyWith<T, _$InvalidEmail<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ShortPasswordCopyWith<T, $Res>
    implements $ValueAuthFailureCopyWith<T, $Res> {
  factory _$$ShortPasswordCopyWith(
          _$ShortPassword<T> value, $Res Function(_$ShortPassword<T>) then) =
      __$$ShortPasswordCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$ShortPasswordCopyWithImpl<T, $Res>
    extends _$ValueAuthFailureCopyWithImpl<T, $Res, _$ShortPassword<T>>
    implements _$$ShortPasswordCopyWith<T, $Res> {
  __$$ShortPasswordCopyWithImpl(
      _$ShortPassword<T> _value, $Res Function(_$ShortPassword<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$ShortPassword<T>(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ShortPassword<T> implements ShortPassword<T> {
  const _$ShortPassword({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueAuthFailure<$T>.shortPassword(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShortPassword<T> &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ShortPasswordCopyWith<T, _$ShortPassword<T>> get copyWith =>
      __$$ShortPasswordCopyWithImpl<T, _$ShortPassword<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) shortPassword,
    required TResult Function(String failedValue) fullNameIsEmpty,
    required TResult Function(String failedValue) phoneNumberIsEmpty,
    required TResult Function(String failedValue)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return shortPassword(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? shortPassword,
    TResult? Function(String failedValue)? fullNameIsEmpty,
    TResult? Function(String failedValue)? phoneNumberIsEmpty,
    TResult? Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
  }) {
    return shortPassword?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? shortPassword,
    TResult Function(String failedValue)? fullNameIsEmpty,
    TResult Function(String failedValue)? phoneNumberIsEmpty,
    TResult Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (shortPassword != null) {
      return shortPassword(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(ShortPassword<T> value) shortPassword,
    required TResult Function(FullNameIsEmpty<T> value) fullNameIsEmpty,
    required TResult Function(PhoneNumberIsEmpty<T> value) phoneNumberIsEmpty,
    required TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return shortPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidEmail<T> value)? invalidEmail,
    TResult? Function(ShortPassword<T> value)? shortPassword,
    TResult? Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult? Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult? Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
  }) {
    return shortPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(ShortPassword<T> value)? shortPassword,
    TResult Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (shortPassword != null) {
      return shortPassword(this);
    }
    return orElse();
  }
}

abstract class ShortPassword<T> implements ValueAuthFailure<T> {
  const factory ShortPassword({required final String failedValue}) =
      _$ShortPassword<T>;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$ShortPasswordCopyWith<T, _$ShortPassword<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FullNameIsEmptyCopyWith<T, $Res>
    implements $ValueAuthFailureCopyWith<T, $Res> {
  factory _$$FullNameIsEmptyCopyWith(_$FullNameIsEmpty<T> value,
          $Res Function(_$FullNameIsEmpty<T>) then) =
      __$$FullNameIsEmptyCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$FullNameIsEmptyCopyWithImpl<T, $Res>
    extends _$ValueAuthFailureCopyWithImpl<T, $Res, _$FullNameIsEmpty<T>>
    implements _$$FullNameIsEmptyCopyWith<T, $Res> {
  __$$FullNameIsEmptyCopyWithImpl(
      _$FullNameIsEmpty<T> _value, $Res Function(_$FullNameIsEmpty<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$FullNameIsEmpty<T>(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FullNameIsEmpty<T> implements FullNameIsEmpty<T> {
  const _$FullNameIsEmpty({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueAuthFailure<$T>.fullNameIsEmpty(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FullNameIsEmpty<T> &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FullNameIsEmptyCopyWith<T, _$FullNameIsEmpty<T>> get copyWith =>
      __$$FullNameIsEmptyCopyWithImpl<T, _$FullNameIsEmpty<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) shortPassword,
    required TResult Function(String failedValue) fullNameIsEmpty,
    required TResult Function(String failedValue) phoneNumberIsEmpty,
    required TResult Function(String failedValue)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return fullNameIsEmpty(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? shortPassword,
    TResult? Function(String failedValue)? fullNameIsEmpty,
    TResult? Function(String failedValue)? phoneNumberIsEmpty,
    TResult? Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
  }) {
    return fullNameIsEmpty?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? shortPassword,
    TResult Function(String failedValue)? fullNameIsEmpty,
    TResult Function(String failedValue)? phoneNumberIsEmpty,
    TResult Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (fullNameIsEmpty != null) {
      return fullNameIsEmpty(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(ShortPassword<T> value) shortPassword,
    required TResult Function(FullNameIsEmpty<T> value) fullNameIsEmpty,
    required TResult Function(PhoneNumberIsEmpty<T> value) phoneNumberIsEmpty,
    required TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return fullNameIsEmpty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidEmail<T> value)? invalidEmail,
    TResult? Function(ShortPassword<T> value)? shortPassword,
    TResult? Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult? Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult? Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
  }) {
    return fullNameIsEmpty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(ShortPassword<T> value)? shortPassword,
    TResult Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (fullNameIsEmpty != null) {
      return fullNameIsEmpty(this);
    }
    return orElse();
  }
}

abstract class FullNameIsEmpty<T> implements ValueAuthFailure<T> {
  const factory FullNameIsEmpty({required final String failedValue}) =
      _$FullNameIsEmpty<T>;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$FullNameIsEmptyCopyWith<T, _$FullNameIsEmpty<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$PhoneNumberIsEmptyCopyWith<T, $Res>
    implements $ValueAuthFailureCopyWith<T, $Res> {
  factory _$$PhoneNumberIsEmptyCopyWith(_$PhoneNumberIsEmpty<T> value,
          $Res Function(_$PhoneNumberIsEmpty<T>) then) =
      __$$PhoneNumberIsEmptyCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$PhoneNumberIsEmptyCopyWithImpl<T, $Res>
    extends _$ValueAuthFailureCopyWithImpl<T, $Res, _$PhoneNumberIsEmpty<T>>
    implements _$$PhoneNumberIsEmptyCopyWith<T, $Res> {
  __$$PhoneNumberIsEmptyCopyWithImpl(_$PhoneNumberIsEmpty<T> _value,
      $Res Function(_$PhoneNumberIsEmpty<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$PhoneNumberIsEmpty<T>(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PhoneNumberIsEmpty<T> implements PhoneNumberIsEmpty<T> {
  const _$PhoneNumberIsEmpty({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueAuthFailure<$T>.phoneNumberIsEmpty(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PhoneNumberIsEmpty<T> &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PhoneNumberIsEmptyCopyWith<T, _$PhoneNumberIsEmpty<T>> get copyWith =>
      __$$PhoneNumberIsEmptyCopyWithImpl<T, _$PhoneNumberIsEmpty<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) shortPassword,
    required TResult Function(String failedValue) fullNameIsEmpty,
    required TResult Function(String failedValue) phoneNumberIsEmpty,
    required TResult Function(String failedValue)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return phoneNumberIsEmpty(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? shortPassword,
    TResult? Function(String failedValue)? fullNameIsEmpty,
    TResult? Function(String failedValue)? phoneNumberIsEmpty,
    TResult? Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
  }) {
    return phoneNumberIsEmpty?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? shortPassword,
    TResult Function(String failedValue)? fullNameIsEmpty,
    TResult Function(String failedValue)? phoneNumberIsEmpty,
    TResult Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (phoneNumberIsEmpty != null) {
      return phoneNumberIsEmpty(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(ShortPassword<T> value) shortPassword,
    required TResult Function(FullNameIsEmpty<T> value) fullNameIsEmpty,
    required TResult Function(PhoneNumberIsEmpty<T> value) phoneNumberIsEmpty,
    required TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return phoneNumberIsEmpty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidEmail<T> value)? invalidEmail,
    TResult? Function(ShortPassword<T> value)? shortPassword,
    TResult? Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult? Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult? Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
  }) {
    return phoneNumberIsEmpty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(ShortPassword<T> value)? shortPassword,
    TResult Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (phoneNumberIsEmpty != null) {
      return phoneNumberIsEmpty(this);
    }
    return orElse();
  }
}

abstract class PhoneNumberIsEmpty<T> implements ValueAuthFailure<T> {
  const factory PhoneNumberIsEmpty({required final String failedValue}) =
      _$PhoneNumberIsEmpty<T>;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$PhoneNumberIsEmptyCopyWith<T, _$PhoneNumberIsEmpty<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ConfirmPasswordMustBeSameAsPasswordCopyWith<T, $Res>
    implements $ValueAuthFailureCopyWith<T, $Res> {
  factory _$$ConfirmPasswordMustBeSameAsPasswordCopyWith(
          _$ConfirmPasswordMustBeSameAsPassword<T> value,
          $Res Function(_$ConfirmPasswordMustBeSameAsPassword<T>) then) =
      __$$ConfirmPasswordMustBeSameAsPasswordCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({String failedValue});
}

/// @nodoc
class __$$ConfirmPasswordMustBeSameAsPasswordCopyWithImpl<T, $Res>
    extends _$ValueAuthFailureCopyWithImpl<T, $Res,
        _$ConfirmPasswordMustBeSameAsPassword<T>>
    implements _$$ConfirmPasswordMustBeSameAsPasswordCopyWith<T, $Res> {
  __$$ConfirmPasswordMustBeSameAsPasswordCopyWithImpl(
      _$ConfirmPasswordMustBeSameAsPassword<T> _value,
      $Res Function(_$ConfirmPasswordMustBeSameAsPassword<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = null,
  }) {
    return _then(_$ConfirmPasswordMustBeSameAsPassword<T>(
      failedValue: null == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ConfirmPasswordMustBeSameAsPassword<T>
    implements ConfirmPasswordMustBeSameAsPassword<T> {
  const _$ConfirmPasswordMustBeSameAsPassword({required this.failedValue});

  @override
  final String failedValue;

  @override
  String toString() {
    return 'ValueAuthFailure<$T>.confirmPasswordMustBeSameAsPassword(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ConfirmPasswordMustBeSameAsPassword<T> &&
            (identical(other.failedValue, failedValue) ||
                other.failedValue == failedValue));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failedValue);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ConfirmPasswordMustBeSameAsPasswordCopyWith<T,
          _$ConfirmPasswordMustBeSameAsPassword<T>>
      get copyWith => __$$ConfirmPasswordMustBeSameAsPasswordCopyWithImpl<T,
          _$ConfirmPasswordMustBeSameAsPassword<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failedValue) invalidEmail,
    required TResult Function(String failedValue) shortPassword,
    required TResult Function(String failedValue) fullNameIsEmpty,
    required TResult Function(String failedValue) phoneNumberIsEmpty,
    required TResult Function(String failedValue)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return confirmPasswordMustBeSameAsPassword(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String failedValue)? invalidEmail,
    TResult? Function(String failedValue)? shortPassword,
    TResult? Function(String failedValue)? fullNameIsEmpty,
    TResult? Function(String failedValue)? phoneNumberIsEmpty,
    TResult? Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
  }) {
    return confirmPasswordMustBeSameAsPassword?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failedValue)? invalidEmail,
    TResult Function(String failedValue)? shortPassword,
    TResult Function(String failedValue)? fullNameIsEmpty,
    TResult Function(String failedValue)? phoneNumberIsEmpty,
    TResult Function(String failedValue)? confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (confirmPasswordMustBeSameAsPassword != null) {
      return confirmPasswordMustBeSameAsPassword(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidEmail<T> value) invalidEmail,
    required TResult Function(ShortPassword<T> value) shortPassword,
    required TResult Function(FullNameIsEmpty<T> value) fullNameIsEmpty,
    required TResult Function(PhoneNumberIsEmpty<T> value) phoneNumberIsEmpty,
    required TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)
        confirmPasswordMustBeSameAsPassword,
  }) {
    return confirmPasswordMustBeSameAsPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidEmail<T> value)? invalidEmail,
    TResult? Function(ShortPassword<T> value)? shortPassword,
    TResult? Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult? Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult? Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
  }) {
    return confirmPasswordMustBeSameAsPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidEmail<T> value)? invalidEmail,
    TResult Function(ShortPassword<T> value)? shortPassword,
    TResult Function(FullNameIsEmpty<T> value)? fullNameIsEmpty,
    TResult Function(PhoneNumberIsEmpty<T> value)? phoneNumberIsEmpty,
    TResult Function(ConfirmPasswordMustBeSameAsPassword<T> value)?
        confirmPasswordMustBeSameAsPassword,
    required TResult orElse(),
  }) {
    if (confirmPasswordMustBeSameAsPassword != null) {
      return confirmPasswordMustBeSameAsPassword(this);
    }
    return orElse();
  }
}

abstract class ConfirmPasswordMustBeSameAsPassword<T>
    implements ValueAuthFailure<T> {
  const factory ConfirmPasswordMustBeSameAsPassword(
          {required final String failedValue}) =
      _$ConfirmPasswordMustBeSameAsPassword<T>;

  @override
  String get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$ConfirmPasswordMustBeSameAsPasswordCopyWith<T,
          _$ConfirmPasswordMustBeSameAsPassword<T>>
      get copyWith => throw _privateConstructorUsedError;
}
