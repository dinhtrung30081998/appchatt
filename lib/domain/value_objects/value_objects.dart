import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

import '../core/errors/auth/authen_error.dart';
import 'failures/auth/value_auth_failure.dart';

@immutable
abstract class ValueObject<T> {
  const ValueObject();
  Either<ValueAuthFailure<T>, T> get value;

  //Throws [AuthError] containing the [ValueFailure]
  //id = identity - same as writing (right) => right
  T getValueOrCrash() => value.fold((failure) => throw AuthError(failure), id);

  bool isRight() => value.isRight();
  bool isLeft() => value.isLeft();

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ValueObject<T> && other.value == value;
  }

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => 'Value(value: $value)';
}
