import 'package:freezed_annotation/freezed_annotation.dart';

part 'chat_entity.freezed.dart';

@freezed
class ChatEntity with _$ChatEntity {
  const factory ChatEntity({
    required String sendById,
    required String type,
    required DateTime sendingTime,
    required String text,
  }) = _ChatEntity;
}
