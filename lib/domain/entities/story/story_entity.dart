import 'package:freezed_annotation/freezed_annotation.dart';

part 'story_entity.freezed.dart';

@freezed
class StoryEntity with _$StoryEntity {
  const factory StoryEntity({
    required String name,
    required String url,
  }) = _StoryEntity;

  factory StoryEntity.empty() => const _StoryEntity(
        name: '',
        url: '',
      );
}
