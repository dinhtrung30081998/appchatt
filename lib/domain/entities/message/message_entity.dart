import 'package:freezed_annotation/freezed_annotation.dart';

part 'message_entity.freezed.dart';

@freezed
class MessageEntity with _$MessageEntity {
  const factory MessageEntity({
    required String messageId,
    required String lastSenderId,
    required List users,
    required Map messages,
    required Map peopleInChat,
    required Map? listLastMessages,
    required String lastMessage,
    required String lastTimeMessage,
  }) = _MessageEntity;
}

@freezed
class LastMessageEntity with _$LastMessageEntity {
  const factory LastMessageEntity({
    required String sendById,
    required String message,
  }) = _LastMessageEntity;
}
