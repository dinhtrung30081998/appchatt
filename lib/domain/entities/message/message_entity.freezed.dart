// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'message_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MessageEntity {
  String get messageId => throw _privateConstructorUsedError;
  String get lastSenderId => throw _privateConstructorUsedError;
  List<dynamic> get users => throw _privateConstructorUsedError;
  Map<dynamic, dynamic> get messages => throw _privateConstructorUsedError;
  Map<dynamic, dynamic> get peopleInChat => throw _privateConstructorUsedError;
  Map<dynamic, dynamic>? get listLastMessages =>
      throw _privateConstructorUsedError;
  String get lastMessage => throw _privateConstructorUsedError;
  String get lastTimeMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MessageEntityCopyWith<MessageEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageEntityCopyWith<$Res> {
  factory $MessageEntityCopyWith(
          MessageEntity value, $Res Function(MessageEntity) then) =
      _$MessageEntityCopyWithImpl<$Res, MessageEntity>;
  @useResult
  $Res call(
      {String messageId,
      String lastSenderId,
      List<dynamic> users,
      Map<dynamic, dynamic> messages,
      Map<dynamic, dynamic> peopleInChat,
      Map<dynamic, dynamic>? listLastMessages,
      String lastMessage,
      String lastTimeMessage});
}

/// @nodoc
class _$MessageEntityCopyWithImpl<$Res, $Val extends MessageEntity>
    implements $MessageEntityCopyWith<$Res> {
  _$MessageEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? messageId = null,
    Object? lastSenderId = null,
    Object? users = null,
    Object? messages = null,
    Object? peopleInChat = null,
    Object? listLastMessages = freezed,
    Object? lastMessage = null,
    Object? lastTimeMessage = null,
  }) {
    return _then(_value.copyWith(
      messageId: null == messageId
          ? _value.messageId
          : messageId // ignore: cast_nullable_to_non_nullable
              as String,
      lastSenderId: null == lastSenderId
          ? _value.lastSenderId
          : lastSenderId // ignore: cast_nullable_to_non_nullable
              as String,
      users: null == users
          ? _value.users
          : users // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      messages: null == messages
          ? _value.messages
          : messages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      peopleInChat: null == peopleInChat
          ? _value.peopleInChat
          : peopleInChat // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      listLastMessages: freezed == listLastMessages
          ? _value.listLastMessages
          : listLastMessages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      lastMessage: null == lastMessage
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String,
      lastTimeMessage: null == lastTimeMessage
          ? _value.lastTimeMessage
          : lastTimeMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MessageEntityCopyWith<$Res>
    implements $MessageEntityCopyWith<$Res> {
  factory _$$_MessageEntityCopyWith(
          _$_MessageEntity value, $Res Function(_$_MessageEntity) then) =
      __$$_MessageEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String messageId,
      String lastSenderId,
      List<dynamic> users,
      Map<dynamic, dynamic> messages,
      Map<dynamic, dynamic> peopleInChat,
      Map<dynamic, dynamic>? listLastMessages,
      String lastMessage,
      String lastTimeMessage});
}

/// @nodoc
class __$$_MessageEntityCopyWithImpl<$Res>
    extends _$MessageEntityCopyWithImpl<$Res, _$_MessageEntity>
    implements _$$_MessageEntityCopyWith<$Res> {
  __$$_MessageEntityCopyWithImpl(
      _$_MessageEntity _value, $Res Function(_$_MessageEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? messageId = null,
    Object? lastSenderId = null,
    Object? users = null,
    Object? messages = null,
    Object? peopleInChat = null,
    Object? listLastMessages = freezed,
    Object? lastMessage = null,
    Object? lastTimeMessage = null,
  }) {
    return _then(_$_MessageEntity(
      messageId: null == messageId
          ? _value.messageId
          : messageId // ignore: cast_nullable_to_non_nullable
              as String,
      lastSenderId: null == lastSenderId
          ? _value.lastSenderId
          : lastSenderId // ignore: cast_nullable_to_non_nullable
              as String,
      users: null == users
          ? _value._users
          : users // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      messages: null == messages
          ? _value._messages
          : messages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      peopleInChat: null == peopleInChat
          ? _value._peopleInChat
          : peopleInChat // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      listLastMessages: freezed == listLastMessages
          ? _value._listLastMessages
          : listLastMessages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      lastMessage: null == lastMessage
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String,
      lastTimeMessage: null == lastTimeMessage
          ? _value.lastTimeMessage
          : lastTimeMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_MessageEntity implements _MessageEntity {
  const _$_MessageEntity(
      {required this.messageId,
      required this.lastSenderId,
      required final List<dynamic> users,
      required final Map<dynamic, dynamic> messages,
      required final Map<dynamic, dynamic> peopleInChat,
      required final Map<dynamic, dynamic>? listLastMessages,
      required this.lastMessage,
      required this.lastTimeMessage})
      : _users = users,
        _messages = messages,
        _peopleInChat = peopleInChat,
        _listLastMessages = listLastMessages;

  @override
  final String messageId;
  @override
  final String lastSenderId;
  final List<dynamic> _users;
  @override
  List<dynamic> get users {
    if (_users is EqualUnmodifiableListView) return _users;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_users);
  }

  final Map<dynamic, dynamic> _messages;
  @override
  Map<dynamic, dynamic> get messages {
    if (_messages is EqualUnmodifiableMapView) return _messages;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_messages);
  }

  final Map<dynamic, dynamic> _peopleInChat;
  @override
  Map<dynamic, dynamic> get peopleInChat {
    if (_peopleInChat is EqualUnmodifiableMapView) return _peopleInChat;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_peopleInChat);
  }

  final Map<dynamic, dynamic>? _listLastMessages;
  @override
  Map<dynamic, dynamic>? get listLastMessages {
    final value = _listLastMessages;
    if (value == null) return null;
    if (_listLastMessages is EqualUnmodifiableMapView) return _listLastMessages;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  final String lastMessage;
  @override
  final String lastTimeMessage;

  @override
  String toString() {
    return 'MessageEntity(messageId: $messageId, lastSenderId: $lastSenderId, users: $users, messages: $messages, peopleInChat: $peopleInChat, listLastMessages: $listLastMessages, lastMessage: $lastMessage, lastTimeMessage: $lastTimeMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MessageEntity &&
            (identical(other.messageId, messageId) ||
                other.messageId == messageId) &&
            (identical(other.lastSenderId, lastSenderId) ||
                other.lastSenderId == lastSenderId) &&
            const DeepCollectionEquality().equals(other._users, _users) &&
            const DeepCollectionEquality().equals(other._messages, _messages) &&
            const DeepCollectionEquality()
                .equals(other._peopleInChat, _peopleInChat) &&
            const DeepCollectionEquality()
                .equals(other._listLastMessages, _listLastMessages) &&
            (identical(other.lastMessage, lastMessage) ||
                other.lastMessage == lastMessage) &&
            (identical(other.lastTimeMessage, lastTimeMessage) ||
                other.lastTimeMessage == lastTimeMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      messageId,
      lastSenderId,
      const DeepCollectionEquality().hash(_users),
      const DeepCollectionEquality().hash(_messages),
      const DeepCollectionEquality().hash(_peopleInChat),
      const DeepCollectionEquality().hash(_listLastMessages),
      lastMessage,
      lastTimeMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MessageEntityCopyWith<_$_MessageEntity> get copyWith =>
      __$$_MessageEntityCopyWithImpl<_$_MessageEntity>(this, _$identity);
}

abstract class _MessageEntity implements MessageEntity {
  const factory _MessageEntity(
      {required final String messageId,
      required final String lastSenderId,
      required final List<dynamic> users,
      required final Map<dynamic, dynamic> messages,
      required final Map<dynamic, dynamic> peopleInChat,
      required final Map<dynamic, dynamic>? listLastMessages,
      required final String lastMessage,
      required final String lastTimeMessage}) = _$_MessageEntity;

  @override
  String get messageId;
  @override
  String get lastSenderId;
  @override
  List<dynamic> get users;
  @override
  Map<dynamic, dynamic> get messages;
  @override
  Map<dynamic, dynamic> get peopleInChat;
  @override
  Map<dynamic, dynamic>? get listLastMessages;
  @override
  String get lastMessage;
  @override
  String get lastTimeMessage;
  @override
  @JsonKey(ignore: true)
  _$$_MessageEntityCopyWith<_$_MessageEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$LastMessageEntity {
  String get sendById => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LastMessageEntityCopyWith<LastMessageEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LastMessageEntityCopyWith<$Res> {
  factory $LastMessageEntityCopyWith(
          LastMessageEntity value, $Res Function(LastMessageEntity) then) =
      _$LastMessageEntityCopyWithImpl<$Res, LastMessageEntity>;
  @useResult
  $Res call({String sendById, String message});
}

/// @nodoc
class _$LastMessageEntityCopyWithImpl<$Res, $Val extends LastMessageEntity>
    implements $LastMessageEntityCopyWith<$Res> {
  _$LastMessageEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sendById = null,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      sendById: null == sendById
          ? _value.sendById
          : sendById // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LastMessageEntityCopyWith<$Res>
    implements $LastMessageEntityCopyWith<$Res> {
  factory _$$_LastMessageEntityCopyWith(_$_LastMessageEntity value,
          $Res Function(_$_LastMessageEntity) then) =
      __$$_LastMessageEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String sendById, String message});
}

/// @nodoc
class __$$_LastMessageEntityCopyWithImpl<$Res>
    extends _$LastMessageEntityCopyWithImpl<$Res, _$_LastMessageEntity>
    implements _$$_LastMessageEntityCopyWith<$Res> {
  __$$_LastMessageEntityCopyWithImpl(
      _$_LastMessageEntity _value, $Res Function(_$_LastMessageEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sendById = null,
    Object? message = null,
  }) {
    return _then(_$_LastMessageEntity(
      sendById: null == sendById
          ? _value.sendById
          : sendById // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_LastMessageEntity implements _LastMessageEntity {
  const _$_LastMessageEntity({required this.sendById, required this.message});

  @override
  final String sendById;
  @override
  final String message;

  @override
  String toString() {
    return 'LastMessageEntity(sendById: $sendById, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LastMessageEntity &&
            (identical(other.sendById, sendById) ||
                other.sendById == sendById) &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, sendById, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LastMessageEntityCopyWith<_$_LastMessageEntity> get copyWith =>
      __$$_LastMessageEntityCopyWithImpl<_$_LastMessageEntity>(
          this, _$identity);
}

abstract class _LastMessageEntity implements LastMessageEntity {
  const factory _LastMessageEntity(
      {required final String sendById,
      required final String message}) = _$_LastMessageEntity;

  @override
  String get sendById;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$_LastMessageEntityCopyWith<_$_LastMessageEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
