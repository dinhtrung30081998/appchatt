import 'package:freezed_annotation/freezed_annotation.dart';

import '../../value_objects/auth/user_unique_id.dart';

part 'user_entity.freezed.dart';

@freezed
class UserOnlyUniqueIDEntity with _$UserOnlyUniqueIDEntity {
  const factory UserOnlyUniqueIDEntity({
    required UserUniqueID uid,
  }) = _UserOnlyUniqueIDEntity;
}

@freezed
class UserEntity with _$UserEntity {
  const factory UserEntity({
    required UserUniqueID uid,
    required String email,
    required String token,
    required String fullName,
    required String phoneNumber,
    required String bio,
    required List<String> followers,
    required List<String> following,
    required List<String> posts,
    required String profilePicture,
    required String status,
    required DateTime lastTimeOnline,
  }) = _UserEntity;

  factory UserEntity.empty() => UserEntity(
        uid: UserUniqueID.fromUniqueString(''),
        email: '',
        token: '',
        fullName: '',
        phoneNumber: '',
        bio: '',
        followers: [],
        following: [],
        posts: [],
        profilePicture: '',
        status: '',
        lastTimeOnline: DateTime.now(),
      );
}
