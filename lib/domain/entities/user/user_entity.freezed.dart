// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UserOnlyUniqueIDEntity {
  UserUniqueID get uid => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserOnlyUniqueIDEntityCopyWith<UserOnlyUniqueIDEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserOnlyUniqueIDEntityCopyWith<$Res> {
  factory $UserOnlyUniqueIDEntityCopyWith(UserOnlyUniqueIDEntity value,
          $Res Function(UserOnlyUniqueIDEntity) then) =
      _$UserOnlyUniqueIDEntityCopyWithImpl<$Res, UserOnlyUniqueIDEntity>;
  @useResult
  $Res call({UserUniqueID uid});
}

/// @nodoc
class _$UserOnlyUniqueIDEntityCopyWithImpl<$Res,
        $Val extends UserOnlyUniqueIDEntity>
    implements $UserOnlyUniqueIDEntityCopyWith<$Res> {
  _$UserOnlyUniqueIDEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
  }) {
    return _then(_value.copyWith(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as UserUniqueID,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserOnlyUniqueIDEntityCopyWith<$Res>
    implements $UserOnlyUniqueIDEntityCopyWith<$Res> {
  factory _$$_UserOnlyUniqueIDEntityCopyWith(_$_UserOnlyUniqueIDEntity value,
          $Res Function(_$_UserOnlyUniqueIDEntity) then) =
      __$$_UserOnlyUniqueIDEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({UserUniqueID uid});
}

/// @nodoc
class __$$_UserOnlyUniqueIDEntityCopyWithImpl<$Res>
    extends _$UserOnlyUniqueIDEntityCopyWithImpl<$Res,
        _$_UserOnlyUniqueIDEntity>
    implements _$$_UserOnlyUniqueIDEntityCopyWith<$Res> {
  __$$_UserOnlyUniqueIDEntityCopyWithImpl(_$_UserOnlyUniqueIDEntity _value,
      $Res Function(_$_UserOnlyUniqueIDEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
  }) {
    return _then(_$_UserOnlyUniqueIDEntity(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as UserUniqueID,
    ));
  }
}

/// @nodoc

class _$_UserOnlyUniqueIDEntity implements _UserOnlyUniqueIDEntity {
  const _$_UserOnlyUniqueIDEntity({required this.uid});

  @override
  final UserUniqueID uid;

  @override
  String toString() {
    return 'UserOnlyUniqueIDEntity(uid: $uid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserOnlyUniqueIDEntity &&
            (identical(other.uid, uid) || other.uid == uid));
  }

  @override
  int get hashCode => Object.hash(runtimeType, uid);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserOnlyUniqueIDEntityCopyWith<_$_UserOnlyUniqueIDEntity> get copyWith =>
      __$$_UserOnlyUniqueIDEntityCopyWithImpl<_$_UserOnlyUniqueIDEntity>(
          this, _$identity);
}

abstract class _UserOnlyUniqueIDEntity implements UserOnlyUniqueIDEntity {
  const factory _UserOnlyUniqueIDEntity({required final UserUniqueID uid}) =
      _$_UserOnlyUniqueIDEntity;

  @override
  UserUniqueID get uid;
  @override
  @JsonKey(ignore: true)
  _$$_UserOnlyUniqueIDEntityCopyWith<_$_UserOnlyUniqueIDEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$UserEntity {
  UserUniqueID get uid => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get token => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  String get bio => throw _privateConstructorUsedError;
  List<String> get followers => throw _privateConstructorUsedError;
  List<String> get following => throw _privateConstructorUsedError;
  List<String> get posts => throw _privateConstructorUsedError;
  String get profilePicture => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;
  DateTime get lastTimeOnline => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserEntityCopyWith<UserEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserEntityCopyWith<$Res> {
  factory $UserEntityCopyWith(
          UserEntity value, $Res Function(UserEntity) then) =
      _$UserEntityCopyWithImpl<$Res, UserEntity>;
  @useResult
  $Res call(
      {UserUniqueID uid,
      String email,
      String token,
      String fullName,
      String phoneNumber,
      String bio,
      List<String> followers,
      List<String> following,
      List<String> posts,
      String profilePicture,
      String status,
      DateTime lastTimeOnline});
}

/// @nodoc
class _$UserEntityCopyWithImpl<$Res, $Val extends UserEntity>
    implements $UserEntityCopyWith<$Res> {
  _$UserEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? email = null,
    Object? token = null,
    Object? fullName = null,
    Object? phoneNumber = null,
    Object? bio = null,
    Object? followers = null,
    Object? following = null,
    Object? posts = null,
    Object? profilePicture = null,
    Object? status = null,
    Object? lastTimeOnline = null,
  }) {
    return _then(_value.copyWith(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as UserUniqueID,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      bio: null == bio
          ? _value.bio
          : bio // ignore: cast_nullable_to_non_nullable
              as String,
      followers: null == followers
          ? _value.followers
          : followers // ignore: cast_nullable_to_non_nullable
              as List<String>,
      following: null == following
          ? _value.following
          : following // ignore: cast_nullable_to_non_nullable
              as List<String>,
      posts: null == posts
          ? _value.posts
          : posts // ignore: cast_nullable_to_non_nullable
              as List<String>,
      profilePicture: null == profilePicture
          ? _value.profilePicture
          : profilePicture // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      lastTimeOnline: null == lastTimeOnline
          ? _value.lastTimeOnline
          : lastTimeOnline // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserEntityCopyWith<$Res>
    implements $UserEntityCopyWith<$Res> {
  factory _$$_UserEntityCopyWith(
          _$_UserEntity value, $Res Function(_$_UserEntity) then) =
      __$$_UserEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {UserUniqueID uid,
      String email,
      String token,
      String fullName,
      String phoneNumber,
      String bio,
      List<String> followers,
      List<String> following,
      List<String> posts,
      String profilePicture,
      String status,
      DateTime lastTimeOnline});
}

/// @nodoc
class __$$_UserEntityCopyWithImpl<$Res>
    extends _$UserEntityCopyWithImpl<$Res, _$_UserEntity>
    implements _$$_UserEntityCopyWith<$Res> {
  __$$_UserEntityCopyWithImpl(
      _$_UserEntity _value, $Res Function(_$_UserEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? email = null,
    Object? token = null,
    Object? fullName = null,
    Object? phoneNumber = null,
    Object? bio = null,
    Object? followers = null,
    Object? following = null,
    Object? posts = null,
    Object? profilePicture = null,
    Object? status = null,
    Object? lastTimeOnline = null,
  }) {
    return _then(_$_UserEntity(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as UserUniqueID,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      bio: null == bio
          ? _value.bio
          : bio // ignore: cast_nullable_to_non_nullable
              as String,
      followers: null == followers
          ? _value._followers
          : followers // ignore: cast_nullable_to_non_nullable
              as List<String>,
      following: null == following
          ? _value._following
          : following // ignore: cast_nullable_to_non_nullable
              as List<String>,
      posts: null == posts
          ? _value._posts
          : posts // ignore: cast_nullable_to_non_nullable
              as List<String>,
      profilePicture: null == profilePicture
          ? _value.profilePicture
          : profilePicture // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      lastTimeOnline: null == lastTimeOnline
          ? _value.lastTimeOnline
          : lastTimeOnline // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_UserEntity implements _UserEntity {
  const _$_UserEntity(
      {required this.uid,
      required this.email,
      required this.token,
      required this.fullName,
      required this.phoneNumber,
      required this.bio,
      required final List<String> followers,
      required final List<String> following,
      required final List<String> posts,
      required this.profilePicture,
      required this.status,
      required this.lastTimeOnline})
      : _followers = followers,
        _following = following,
        _posts = posts;

  @override
  final UserUniqueID uid;
  @override
  final String email;
  @override
  final String token;
  @override
  final String fullName;
  @override
  final String phoneNumber;
  @override
  final String bio;
  final List<String> _followers;
  @override
  List<String> get followers {
    if (_followers is EqualUnmodifiableListView) return _followers;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_followers);
  }

  final List<String> _following;
  @override
  List<String> get following {
    if (_following is EqualUnmodifiableListView) return _following;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_following);
  }

  final List<String> _posts;
  @override
  List<String> get posts {
    if (_posts is EqualUnmodifiableListView) return _posts;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_posts);
  }

  @override
  final String profilePicture;
  @override
  final String status;
  @override
  final DateTime lastTimeOnline;

  @override
  String toString() {
    return 'UserEntity(uid: $uid, email: $email, token: $token, fullName: $fullName, phoneNumber: $phoneNumber, bio: $bio, followers: $followers, following: $following, posts: $posts, profilePicture: $profilePicture, status: $status, lastTimeOnline: $lastTimeOnline)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserEntity &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.bio, bio) || other.bio == bio) &&
            const DeepCollectionEquality()
                .equals(other._followers, _followers) &&
            const DeepCollectionEquality()
                .equals(other._following, _following) &&
            const DeepCollectionEquality().equals(other._posts, _posts) &&
            (identical(other.profilePicture, profilePicture) ||
                other.profilePicture == profilePicture) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.lastTimeOnline, lastTimeOnline) ||
                other.lastTimeOnline == lastTimeOnline));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      uid,
      email,
      token,
      fullName,
      phoneNumber,
      bio,
      const DeepCollectionEquality().hash(_followers),
      const DeepCollectionEquality().hash(_following),
      const DeepCollectionEquality().hash(_posts),
      profilePicture,
      status,
      lastTimeOnline);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserEntityCopyWith<_$_UserEntity> get copyWith =>
      __$$_UserEntityCopyWithImpl<_$_UserEntity>(this, _$identity);
}

abstract class _UserEntity implements UserEntity {
  const factory _UserEntity(
      {required final UserUniqueID uid,
      required final String email,
      required final String token,
      required final String fullName,
      required final String phoneNumber,
      required final String bio,
      required final List<String> followers,
      required final List<String> following,
      required final List<String> posts,
      required final String profilePicture,
      required final String status,
      required final DateTime lastTimeOnline}) = _$_UserEntity;

  @override
  UserUniqueID get uid;
  @override
  String get email;
  @override
  String get token;
  @override
  String get fullName;
  @override
  String get phoneNumber;
  @override
  String get bio;
  @override
  List<String> get followers;
  @override
  List<String> get following;
  @override
  List<String> get posts;
  @override
  String get profilePicture;
  @override
  String get status;
  @override
  DateTime get lastTimeOnline;
  @override
  @JsonKey(ignore: true)
  _$$_UserEntityCopyWith<_$_UserEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
