import 'package:freezed_annotation/freezed_annotation.dart';

part 'comment_entity.freezed.dart';

@freezed
class CommentEntity with _$CommentEntity {
  const factory CommentEntity({
    required String profilePicture,
    required String uid,
    required String fullName,
    required String comment,
    required String commentId,
    required String datePublished,
    required List likes,
  }) = _CommentEntity;

  factory CommentEntity.empty() => const _CommentEntity(
        profilePicture: '',
        uid: '',
        fullName: '',
        comment: '',
        commentId: '',
        datePublished: '',
        likes: [],
      );
}
