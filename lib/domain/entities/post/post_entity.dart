import 'package:freezed_annotation/freezed_annotation.dart';

part 'post_entity.freezed.dart';

@freezed
class PostEntity with _$PostEntity {
  const factory PostEntity({
    required String postId,
    required String uid,
    required String fullName,
    required String description,
    required DateTime datePublished,
    required List<String> postUrl,
    required String profileImage,
    required bool hidePost,
    required List likes,
    required List comments,
  }) = _PostEntity;

  factory PostEntity.empty() => _PostEntity(
        postId: '',
        uid: '',
        fullName: '',
        description: '',
        datePublished: DateTime.now(),
        postUrl: [],
        profileImage: '',
        hidePost: false,
        likes: [],
        comments: [],
      );
}
