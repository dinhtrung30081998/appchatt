// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:app_chat/application/stores/auth/auth_store.dart' as _i54;
import 'package:app_chat/application/stores/auth/login/login_store.dart'
    as _i56;
import 'package:app_chat/application/stores/auth/login/verify_phone_number/verify_phone_number_store.dart'
    as _i32;
import 'package:app_chat/application/stores/auth/register/register_store.dart'
    as _i49;
import 'package:app_chat/application/stores/messages/messages_store.dart'
    as _i57;
import 'package:app_chat/application/stores/post/post_store.dart' as _i48;
import 'package:app_chat/application/stores/search_friends/search_friends_store.dart'
    as _i59;
import 'package:app_chat/application/stores/user_info/user_info_store.dart'
    as _i53;
import 'package:app_chat/infrastructure/core/module/injectable_module.dart'
    as _i60;
import 'package:app_chat/infrastructure/core/network/network_info.dart' as _i23;
import 'package:app_chat/infrastructure/data_sources/search_friends/search_friends_local.dart'
    as _i50;
import 'package:app_chat/infrastructure/data_sources/search_friends/search_friends_remote.dart'
    as _i24;
import 'package:app_chat/infrastructure/data_sources/user_infor/user_infor_local.dart'
    as _i29;
import 'package:app_chat/infrastructure/data_sources/user_infor/user_infor_remote.dart'
    as _i30;
import 'package:app_chat/infrastructure/repositories/auth/action_camera.dart'
    as _i33;
import 'package:app_chat/infrastructure/repositories/auth/get_firebase_messaging_token_impl.dart'
    as _i40;
import 'package:app_chat/infrastructure/repositories/auth/sign_in_with_email.dart'
    as _i26;
import 'package:app_chat/infrastructure/repositories/auth/sign_in_with_facebook.dart'
    as _i27;
import 'package:app_chat/infrastructure/repositories/auth/sign_in_with_google.dart'
    as _i51;
import 'package:app_chat/infrastructure/repositories/auth/sign_out.dart'
    as _i52;
import 'package:app_chat/infrastructure/repositories/auth/sign_up_with_email.dart'
    as _i28;
import 'package:app_chat/infrastructure/repositories/auth/verify_phone_number.dart'
    as _i31;
import 'package:app_chat/infrastructure/repositories/chat/out_room_chat_impl.dart'
    as _i15;
import 'package:app_chat/infrastructure/repositories/chat/send_chat_impl.dart'
    as _i55;
import 'package:app_chat/infrastructure/repositories/chat/send_notification_impl.dart'
    as _i45;
import 'package:app_chat/infrastructure/repositories/chat/stream_list_chat_impl.dart'
    as _i19;
import 'package:app_chat/infrastructure/repositories/messages/delete_messages_impl.dart'
    as _i13;
import 'package:app_chat/infrastructure/repositories/messages/read_last_messages_impl.dart'
    as _i16;
import 'package:app_chat/infrastructure/repositories/messages/stream_list_messages_impl.dart'
    as _i20;
import 'package:app_chat/infrastructure/repositories/messages/upload_image_impl.dart'
    as _i21;
import 'package:app_chat/infrastructure/repositories/post/comment_post_impl.dart'
    as _i36;
import 'package:app_chat/infrastructure/repositories/post/create_post_impl.dart'
    as _i34;
import 'package:app_chat/infrastructure/repositories/post/delete_post_impl.dart'
    as _i37;
import 'package:app_chat/infrastructure/repositories/post/hide_post_impl.dart'
    as _i42;
import 'package:app_chat/infrastructure/repositories/post/like_comment_impl.dart'
    as _i43;
import 'package:app_chat/infrastructure/repositories/post/like_post_impl.dart'
    as _i44;
import 'package:app_chat/infrastructure/repositories/post/show_post_impl.dart'
    as _i46;
import 'package:app_chat/infrastructure/repositories/post/stream_list_comment_by_post_id_impl.dart'
    as _i14;
import 'package:app_chat/infrastructure/repositories/post/stream_list_post_impl.dart'
    as _i11;
import 'package:app_chat/infrastructure/repositories/search_friends/search_friends_impl.dart'
    as _i58;
import 'package:app_chat/infrastructure/repositories/user_infor/cache_user_info_impl.dart'
    as _i35;
import 'package:app_chat/infrastructure/repositories/user_infor/edit_user_info_impl.dart'
    as _i38;
import 'package:app_chat/infrastructure/repositories/user_infor/follow_user_impl.dart'
    as _i39;
import 'package:app_chat/infrastructure/repositories/user_infor/get_user_info_local_impl.dart'
    as _i41;
import 'package:app_chat/infrastructure/repositories/user_infor/set_status_user_impl.dart'
    as _i17;
import 'package:app_chat/infrastructure/repositories/user_infor/stream_get_user_info_by_uid_impl.dart'
    as _i18;
import 'package:app_chat/infrastructure/repositories/user_infor/upload_post_image_to_storage_impl.dart'
    as _i47;
import 'package:app_chat/presentation/core/resources/colors.dart' as _i3;
import 'package:cloud_firestore/cloud_firestore.dart' as _i8;
import 'package:firebase_auth/firebase_auth.dart' as _i6;
import 'package:firebase_database/firebase_database.dart' as _i7;
import 'package:firebase_messaging/firebase_messaging.dart' as _i9;
import 'package:firebase_storage/firebase_storage.dart' as _i10;
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart' as _i5;
import 'package:get_it/get_it.dart' as _i1;
import 'package:google_sign_in/google_sign_in.dart' as _i12;
import 'package:injectable/injectable.dart' as _i2;
import 'package:internet_connection_checker/internet_connection_checker.dart'
    as _i22;
import 'package:intl/intl.dart' as _i4;
import 'package:shared_preferences/shared_preferences.dart'
    as _i25; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
extension GetItInjectableX on _i1.GetIt {
  // initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final injectableModule = _$InjectableModule();
    gh.singleton<_i3.AppColors>(_i3.AppColors());
    gh.factory<_i4.DateFormat>(() => injectableModule.dateFormat);
    gh.factory<DateTime>(() => injectableModule.dateTime);
    gh.lazySingleton<_i5.FacebookAuth>(() => injectableModule.facebookAuth);
    gh.lazySingleton<_i6.FirebaseAuth>(() => injectableModule.firebaseAuth);
    gh.lazySingleton<_i7.FirebaseDatabase>(
        () => injectableModule.firebaseDatabase);
    gh.lazySingleton<_i8.FirebaseFirestore>(
        () => injectableModule.firebaseFirestore);
    gh.lazySingleton<_i9.FirebaseMessaging>(
        () => injectableModule.firebaseMessaging);
    gh.lazySingleton<_i10.FirebaseStorage>(
        () => injectableModule.firebaseStorage);
    gh.lazySingleton<_i11.GetListPostImpl>(() =>
        _i11.GetListPostImpl(firebaseFirestore: gh<_i8.FirebaseFirestore>()));
    gh.lazySingleton<_i12.GoogleSignIn>(() => injectableModule.googleSignIn);
    gh.lazySingleton<_i13.IDeleteMessageByIdImpl>(
        () => _i13.IDeleteMessageByIdImpl(
              gh<_i7.FirebaseDatabase>(),
              gh<_i6.FirebaseAuth>(),
            ));
    gh.lazySingleton<_i14.IGetListCommentByPostIdImpl>(() =>
        _i14.IGetListCommentByPostIdImpl(
            firebaseFirestore: gh<_i8.FirebaseFirestore>()));
    gh.lazySingleton<_i15.IOutRoomChatImpl>(() => _i15.IOutRoomChatImpl(
          gh<_i7.FirebaseDatabase>(),
          gh<_i6.FirebaseAuth>(),
        ));
    gh.lazySingleton<_i16.IReadLastMessagesImpl>(
        () => _i16.IReadLastMessagesImpl(
              gh<_i7.FirebaseDatabase>(),
              gh<_i6.FirebaseAuth>(),
            ));
    gh.lazySingleton<_i17.ISetStatusUserImpl>(() => _i17.ISetStatusUserImpl(
          gh<_i6.FirebaseAuth>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i18.IStreamGetUserInfoByUidImpl>(() =>
        _i18.IStreamGetUserInfoByUidImpl(
            firebaseFirestore: gh<_i8.FirebaseFirestore>()));
    gh.lazySingleton<_i19.IStreamListChatImpl>(() => _i19.IStreamListChatImpl(
          gh<_i7.FirebaseDatabase>(),
          gh<_i6.FirebaseAuth>(),
        ));
    gh.lazySingleton<_i20.IStreamListMessagesImpl>(
        () => _i20.IStreamListMessagesImpl(
              gh<_i7.FirebaseDatabase>(),
              gh<_i6.FirebaseAuth>(),
            ));
    gh.lazySingleton<_i21.IUploadImageImpl>(
        () => _i21.IUploadImageImpl(gh<_i10.FirebaseStorage>()));
    gh.lazySingleton<_i22.InternetConnectionChecker>(
        () => injectableModule.internetConnectionChecker);
    gh.lazySingleton<_i23.NetworkInfoImpl>(() => _i23.NetworkInfoImpl(
        internetConnectionChecker: gh<_i22.InternetConnectionChecker>()));
    gh.lazySingleton<_i4.NumberFormat>(() => injectableModule.currencyFormat);
    gh.lazySingleton<_i24.SearchFriendsRemoteImpl>(
        () => _i24.SearchFriendsRemoteImpl(gh<_i8.FirebaseFirestore>()));
    await gh.factoryAsync<_i25.SharedPreferences>(
      () => injectableModule.sharedPreferencesshared,
      preResolve: true,
    );
    gh.lazySingleton<_i26.SignInWithEmailAndPasswordImpl>(
        () => _i26.SignInWithEmailAndPasswordImpl(
              networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
              firebaseAuth: gh<_i6.FirebaseAuth>(),
            ));
    gh.lazySingleton<_i27.SignInWithFacebookImpl>(
        () => _i27.SignInWithFacebookImpl(
              gh<_i5.FacebookAuth>(),
              gh<_i8.FirebaseFirestore>(),
              gh<_i23.NetworkInfoImpl>(),
            ));
    gh.lazySingleton<_i28.SignUpWithEmailAndPasswordImpl>(
        () => _i28.SignUpWithEmailAndPasswordImpl(
              gh<_i8.FirebaseFirestore>(),
              firebaseAuth: gh<_i6.FirebaseAuth>(),
              networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
            ));
    gh.lazySingleton<_i29.UserInforLocalImpl>(() => _i29.UserInforLocalImpl(
        sharedPreferences: gh<_i25.SharedPreferences>()));
    gh.lazySingleton<_i30.UserInforRemoteImpl>(() => _i30.UserInforRemoteImpl(
          gh<_i6.FirebaseAuth>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i31.VerifyPhoneNumberImpl>(
        () => _i31.VerifyPhoneNumberImpl(
              firebaseAuth: gh<_i6.FirebaseAuth>(),
              networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
            ));
    gh.factory<_i32.VerifyPhoneNumberStore>(
        () => _i32.VerifyPhoneNumberStore(gh<_i31.VerifyPhoneNumberImpl>()));
    gh.lazySingleton<_i33.ActionCameraImpl>(() => _i33.ActionCameraImpl(
          gh<_i10.FirebaseStorage>(),
          gh<_i8.FirebaseFirestore>(),
          networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
        ));
    gh.lazySingleton<_i34.CreatePostImpl>(() => _i34.CreatePostImpl(
          gh<_i6.FirebaseAuth>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
          networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
        ));
    gh.lazySingleton<_i35.ICacheUserInfoImpl>(() => _i35.ICacheUserInfoImpl(
          gh<_i30.UserInforRemoteImpl>(),
          gh<_i29.UserInforLocalImpl>(),
        ));
    gh.lazySingleton<_i36.ICommentPostImpl>(() => _i36.ICommentPostImpl(
          gh<_i23.NetworkInfoImpl>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i37.IDeletePostImpl>(() => _i37.IDeletePostImpl(
          gh<_i23.NetworkInfoImpl>(),
          gh<_i6.FirebaseAuth>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i38.IEditUserInfoByUidImpl>(
        () => _i38.IEditUserInfoByUidImpl(
              gh<_i29.UserInforLocalImpl>(),
              firebaseFirestore: gh<_i8.FirebaseFirestore>(),
              networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
            ));
    gh.lazySingleton<_i39.IFollowUserImpl>(() => _i39.IFollowUserImpl(
          gh<_i29.UserInforLocalImpl>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i40.IGetFirebaseMessagingTokenImpl>(
        () => _i40.IGetFirebaseMessagingTokenImpl(
              gh<_i9.FirebaseMessaging>(),
              networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
            ));
    gh.lazySingleton<_i41.IGetUserInfoLocalImpl>(
        () => _i41.IGetUserInfoLocalImpl(gh<_i29.UserInforLocalImpl>()));
    gh.lazySingleton<_i42.IHidePostImpl>(() => _i42.IHidePostImpl(
          gh<_i23.NetworkInfoImpl>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i43.ILikeCommentImpl>(() => _i43.ILikeCommentImpl(
          gh<_i23.NetworkInfoImpl>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i44.ILikePostImpl>(() => _i44.ILikePostImpl(
          gh<_i23.NetworkInfoImpl>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i45.ISendNotificationImpl>(
        () => _i45.ISendNotificationImpl(
              gh<_i6.FirebaseAuth>(),
              gh<_i41.IGetUserInfoLocalImpl>(),
            ));
    gh.lazySingleton<_i46.IShowPostImpl>(() => _i46.IShowPostImpl(
          gh<_i23.NetworkInfoImpl>(),
          firebaseFirestore: gh<_i8.FirebaseFirestore>(),
        ));
    gh.lazySingleton<_i47.IUploadPostImageToStorageImpl>(
        () => _i47.IUploadPostImageToStorageImpl(
              gh<_i6.FirebaseAuth>(),
              firebaseStorage: gh<_i10.FirebaseStorage>(),
              networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
            ));
    gh.factory<_i48.PostStore>(() => _i48.PostStore(
          gh<_i33.ActionCameraImpl>(),
          gh<_i34.CreatePostImpl>(),
          gh<_i11.GetListPostImpl>(),
          gh<_i44.ILikePostImpl>(),
          gh<_i36.ICommentPostImpl>(),
          gh<_i14.IGetListCommentByPostIdImpl>(),
          gh<_i43.ILikeCommentImpl>(),
          gh<_i37.IDeletePostImpl>(),
          gh<_i47.IUploadPostImageToStorageImpl>(),
          gh<_i42.IHidePostImpl>(),
          gh<_i46.IShowPostImpl>(),
        ));
    gh.factory<_i49.RegisterStore>(() => _i49.RegisterStore(
          gh<_i28.SignUpWithEmailAndPasswordImpl>(),
          gh<_i33.ActionCameraImpl>(),
          gh<_i35.ICacheUserInfoImpl>(),
          gh<_i40.IGetFirebaseMessagingTokenImpl>(),
        ));
    gh.lazySingleton<_i50.SearchFriendsLocalImpl>(
        () => _i50.SearchFriendsLocalImpl(gh<_i25.SharedPreferences>()));
    gh.lazySingleton<_i51.SignInWithGoogleImpl>(() => _i51.SignInWithGoogleImpl(
          gh<_i8.FirebaseFirestore>(),
          gh<_i35.ICacheUserInfoImpl>(),
          googleSignIn: gh<_i12.GoogleSignIn>(),
          firebaseAuth: gh<_i6.FirebaseAuth>(),
          networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
        ));
    gh.lazySingleton<_i52.SignOutImpl>(() => _i52.SignOutImpl(
          gh<_i12.GoogleSignIn>(),
          gh<_i29.UserInforLocalImpl>(),
          gh<_i8.FirebaseFirestore>(),
          firebaseAuth: gh<_i6.FirebaseAuth>(),
        ));
    gh.factory<_i53.UserInfoStore>(() => _i53.UserInfoStore(
          gh<_i18.IStreamGetUserInfoByUidImpl>(),
          gh<_i39.IFollowUserImpl>(),
          gh<_i38.IEditUserInfoByUidImpl>(),
          gh<_i17.ISetStatusUserImpl>(),
          gh<_i40.IGetFirebaseMessagingTokenImpl>(),
          getUserInfoImpl: gh<_i41.IGetUserInfoLocalImpl>(),
        ));
    gh.factory<_i54.AuthStore>(() => _i54.AuthStore(gh<_i52.SignOutImpl>()));
    gh.lazySingleton<_i55.ISendChatImpl>(() => _i55.ISendChatImpl(
          gh<_i6.FirebaseAuth>(),
          gh<_i7.FirebaseDatabase>(),
          gh<_i45.ISendNotificationImpl>(),
        ));
    gh.factory<_i56.LoginStore>(() => _i56.LoginStore(
          gh<_i26.SignInWithEmailAndPasswordImpl>(),
          gh<_i51.SignInWithGoogleImpl>(),
          gh<_i27.SignInWithFacebookImpl>(),
          gh<_i35.ICacheUserInfoImpl>(),
          gh<_i40.IGetFirebaseMessagingTokenImpl>(),
        ));
    gh.factory<_i57.MessagesStore>(() => _i57.MessagesStore(
          gh<_i16.IReadLastMessagesImpl>(),
          gh<_i19.IStreamListChatImpl>(),
          gh<_i55.ISendChatImpl>(),
          gh<_i13.IDeleteMessageByIdImpl>(),
          gh<_i15.IOutRoomChatImpl>(),
          gh<_i21.IUploadImageImpl>(),
          streamListMessagesImpl: gh<_i20.IStreamListMessagesImpl>(),
        ));
    gh.lazySingleton<_i58.SearchFriendsImpl>(() => _i58.SearchFriendsImpl(
          gh<_i50.SearchFriendsLocalImpl>(),
          gh<_i24.SearchFriendsRemoteImpl>(),
          networkInfoImpl: gh<_i23.NetworkInfoImpl>(),
        ));
    gh.factory<_i59.SearchFriendsStore>(
        () => _i59.SearchFriendsStore(gh<_i58.SearchFriendsImpl>()));
    return this;
  }
}

class _$InjectableModule extends _i60.InjectableModule {}
