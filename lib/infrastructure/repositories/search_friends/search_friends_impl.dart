import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/search_friends/search_friends_failure.dart';
import '../../../domain/entities/user/user_entity.dart';
import '../../../domain/repositories/search_friends/i_search_friends_repository.dart';
import '../../core/network/network_info.dart';
import '../../data_sources/search_friends/search_friends_local.dart';
import '../../data_sources/search_friends/search_friends_remote.dart';

@lazySingleton
@Injectable(as: ISearchFriends)
class SearchFriendsImpl implements ISearchFriends {
  final SearchFriendsLocalImpl _searchFriendsLocalImpl;
  final SearchFriendsRemoteImpl _searchFriendsRemoteImpl;
  final NetworkInfoImpl networkInfoImpl;

  SearchFriendsImpl(this._searchFriendsLocalImpl, this._searchFriendsRemoteImpl, {required this.networkInfoImpl});

  @override
  Future<Either<SearchFriendsFailure, List<UserEntity>>> call(String query) async {
    if (await networkInfoImpl.isConnected) {
      try {
        final remoteSearch = await _searchFriendsRemoteImpl.searchFriendsRemote(query);
        _searchFriendsLocalImpl.cachedSearchFriends(remoteSearch);
        return right(remoteSearch);
      } on FirebaseException catch (_) {
        return left(const SearchFriendsFailure.serverError());
      }
    } else {
      try {
        final localSearch = await _searchFriendsLocalImpl.getCachedSearchFriends();
        return right(localSearch);
      } on EmptyCacheFriends catch (_) {
        return left(const SearchFriendsFailure.emptyCacheFriends());
      }
    }
  }
}
