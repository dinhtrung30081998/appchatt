import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';
import 'package:uuid/uuid.dart';

import '../../../domain/repositories/messages/i_messages_repository.dart';

@lazySingleton
@Injectable(as: IUploadImage)
class IUploadImageImpl implements IUploadImage {
  final FirebaseStorage firebaseDatabase;

  IUploadImageImpl(
    this.firebaseDatabase,
  );

  @override
  Future<String> call(XFile xFile) async {
    String fileName = const Uuid().v1();

    var ref = FirebaseStorage.instance.ref().child('chats').child('$fileName.jpg');

    var uploadTask = await ref.putFile(File(xFile.path));

    String imageUrl = await uploadTask.ref.getDownloadURL();

    return Future.value(imageUrl);
  }
}
