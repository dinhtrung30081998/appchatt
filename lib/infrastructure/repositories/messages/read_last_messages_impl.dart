import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/messages/i_messages_repository.dart';

@lazySingleton
@Injectable(as: IReadLastMessages)
class IReadLastMessagesImpl implements IReadLastMessages {
  final FirebaseDatabase firebaseDatabase;
  final FirebaseAuth firebaseAuth;

  IReadLastMessagesImpl(
    this.firebaseDatabase,
    this.firebaseAuth,
  );

  @override
  Future<Unit> call(String partnerId) async {
    final currentUserId = firebaseAuth.currentUser!.uid;
    String chatId = '';
    bool seenNotCurrentUser = false;
    await firebaseDatabase.ref('chats').get().then((value) async {
      for (var item in value.children) {
        List users = item.child('users').value as List;
        if (users.isNotEmpty) {
          if (users.contains(currentUserId) && users.contains(partnerId)) {
            dynamic messageId = item.child('messageId').value as String;
            dynamic listLastMessages = item.child('listLastMessages').value as Map<Object?, Object?>?;
            if (listLastMessages != null) {
              listLastMessages.forEach((key, value) {
                final senderById = Map<String, dynamic>.from(value);
                seenNotCurrentUser = senderById['sendById'] != currentUserId;
              });
            }
            chatId = messageId;
          }
        }
      }

      if (chatId.isNotEmpty) {
        if (seenNotCurrentUser) {
          firebaseDatabase.ref('chats').child("$chatId/listLastMessages").remove();
        }
        firebaseDatabase.ref('chats').child("$chatId/peopleInChat/$currentUserId").update({
          'is_read': true,
        });
      }
    });

    return Future.value(unit);
  }
}
