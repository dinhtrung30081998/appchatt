import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/messages/i_messages_repository.dart';

@lazySingleton
@Injectable(as: IDeleteMessageById)
class IDeleteMessageByIdImpl implements IDeleteMessageById {
  final FirebaseDatabase firebaseDatabase;
  final FirebaseAuth firebaseAuth;

  IDeleteMessageByIdImpl(
    this.firebaseDatabase,
    this.firebaseAuth,
  );

  @override
  Future<Unit> call(String chatId) async {
    await firebaseDatabase.ref('chats').child(chatId).remove();
    return Future.value(unit);
  }
}
