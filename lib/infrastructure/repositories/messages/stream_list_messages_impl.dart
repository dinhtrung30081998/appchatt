import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/messages/i_messages_repository.dart';

@lazySingleton
@Injectable(as: IStreamListMessages)
class IStreamListMessagesImpl implements IStreamListMessages {
  final FirebaseDatabase firebaseDatabase;
  final FirebaseAuth firebaseAuth;

  IStreamListMessagesImpl(
    this.firebaseDatabase,
    this.firebaseAuth,
  );

  @override
  Stream<DatabaseEvent> call() async* {
    yield* firebaseDatabase.ref('chats').onValue;
  }
}
