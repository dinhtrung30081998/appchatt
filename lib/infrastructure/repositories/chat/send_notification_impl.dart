import 'dart:convert';
import 'dart:io';

import 'package:app_chat/infrastructure/repositories/user_infor/get_user_info_local_impl.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

import '../../../domain/entities/user/user_entity.dart';
import '../../../domain/repositories/chat/i_chat_repository.dart';

@lazySingleton
@Injectable(as: ISendNotification)
class ISendNotificationImpl implements ISendNotification {
  final FirebaseAuth firebaseAuth;
  final IGetUserInfoLocalImpl userInfoLocalImpl;
  ISendNotificationImpl(this.firebaseAuth, this.userInfoLocalImpl);

  @override
  Future<Unit> call(UserEntity userPartner, String message) async {
    try {
      final body = {
        "to": userPartner.token,
        "notification": {
          "title": "Tin nhắn",
          "body": "${userInfoLocalImpl.getFullName()} vừa gửi tin nhắn cho bạn",
        }
      };

      var response = await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader:
              'key=AAAACGmaoWw:APA91bEMY4B9FMZEeQ9fBRveaD_f_c4IyYttPn09pGi0FRZaYnkX1siDvAXcrIKDQM-ZikHEblhKFFtyPR_NyknOLlGN5wL-pp6F8WMuNhs1yQaoxyXBfyMDMVXF75E5rcvokYNzG_oo',
        },
        body: jsonEncode(body),
      );

      print("Status code ${response.statusCode}");
      print("Body code ${response.body}");
    } catch (e) {
      print("Error code ${e.toString()}");
    }
    return Future.value(unit);
  }
}
