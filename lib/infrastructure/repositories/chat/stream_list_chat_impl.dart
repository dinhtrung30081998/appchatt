import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/chat/i_chat_repository.dart';

@lazySingleton
@Injectable(as: IStreamListChat)
class IStreamListChatImpl implements IStreamListChat {
  final FirebaseDatabase firebaseDatabase;
  final FirebaseAuth firebaseAuth;

  IStreamListChatImpl(
    this.firebaseDatabase,
    this.firebaseAuth,
  );

  @override
  Stream<DatabaseEvent> call() async* {
    yield* firebaseDatabase.ref('chats').onValue;
  }
}
