import 'package:app_chat/infrastructure/repositories/chat/send_notification_impl.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:injectable/injectable.dart';
import 'package:uuid/uuid.dart';

import '../../../domain/entities/user/user_entity.dart';
import '../../../domain/repositories/chat/i_chat_repository.dart';
import '../../core/di/infrastructure_injection.dart';

@lazySingleton
@Injectable(as: ISendChat)
class ISendChatImpl implements ISendChat {
  final FirebaseDatabase firebaseDatabase;
  final FirebaseAuth firebaseAuth;
  final ISendNotificationImpl sendNotificationImpl;

  ISendChatImpl(
    this.firebaseAuth,
    this.firebaseDatabase,
    this.sendNotificationImpl,
  );

  @override
  Future<Unit> call(UserEntity userPartner, String message, String type) async {
    final currentId = firebaseAuth.currentUser!.uid;

    final idV1 = const Uuid().v1();
    final idV4 = const Uuid().v4();
    final idV3 = const Uuid().v1();
    String chatID = '';
    String sendingTime = injector<DateTime>().toString();

    bool currentUserIsRead = false;
    bool partnerUserIsRead = false;
    await firebaseDatabase.ref('chats').get().then(
      (value) async {
        for (var item in value.children) {
          List users = item.child('users').value as List;
          if (users.isNotEmpty) {
            if (users.contains(currentId) && users.contains(userPartner.uid.getValueOrCrash())) {
              dynamic messageId = item.child('messageId').value as String;
              dynamic inRoom = item.child('peopleInChat').value as Map<Object?, Object?>;
              if (inRoom.isNotEmpty) {
                inRoom.forEach((key, value) {
                  if (key.contains(currentId)) {
                    final currentUserInChat = Map<String, dynamic>.from(value);
                    currentUserIsRead = currentUserInChat['is_read'];
                  }

                  if (key.contains(userPartner.uid.getValueOrCrash())) {
                    final partnerUserInChat = Map<String, dynamic>.from(value);
                    partnerUserIsRead = partnerUserInChat['is_read'];
                  }
                });
              }
              chatID = messageId;
            }
          }
        }

        if (chatID.isEmpty) {
          firebaseDatabase.ref().child('chats/$idV1').set({
            'messages': {
              idV4: {
                'sendingTime': sendingTime,
                'sendById': currentId,
                'type': type,
                'text': message,
              }
            },
            'listLastMessages': {
              idV3: {
                'sendById': currentId,
                'message': message,
              }
            },
            'peopleInChat': {
              currentId: {
                'is_read': true,
              },
              userPartner.uid.getValueOrCrash(): {
                'is_read': false,
              }
            },
            'users': [
              currentId,
              userPartner.uid.getValueOrCrash(),
            ],
            'messageId': idV1,
            'lastMessage': message,
            'lastTimeMessage': sendingTime,
            'lastSenderId': currentId,
          });
        } else {
          firebaseDatabase.ref().child('chats/$chatID').update({
            'lastMessage': message,
            'lastTimeMessage': sendingTime,
            'lastSenderId': currentId,
          });
          firebaseDatabase.ref().child('chats/$chatID').child('messages/$idV1').set({
            'sendingTime': sendingTime,
            'sendById': currentId,
            'type': type,
            'text': message,
          });
          if (!(currentUserIsRead && partnerUserIsRead)) {
            sendNotificationImpl.call(userPartner, message);
            firebaseDatabase.ref().child('chats/$chatID').child('listLastMessages/$idV3').set({
              'sendById': currentId,
              'message': message,
            });
          }
        }
      },
    );

    return Future.value(unit);
  }
}
