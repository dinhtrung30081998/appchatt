import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/chat/i_chat_repository.dart';

@lazySingleton
@Injectable(as: IOutRoomChat)
class IOutRoomChatImpl implements IOutRoomChat {
  final FirebaseDatabase firebaseDatabase;
  final FirebaseAuth firebaseAuth;

  IOutRoomChatImpl(
    this.firebaseDatabase,
    this.firebaseAuth,
  );

  @override
  Future<Unit> call(String partnerId) async {
    final currentUserId = firebaseAuth.currentUser!.uid;
    final currentId = firebaseAuth.currentUser!.uid;

    String chatId = '';

    await firebaseDatabase.ref('chats').get().then((value) async {
      for (var item in value.children) {
        List users = item.child('users').value as List;
        if (users.isNotEmpty) {
          if (users.contains(currentId) && users.contains(partnerId)) {
            dynamic messageId = item.child('messageId').value as String;
            chatId = messageId;
          }
        }
      }

      if (chatId.isNotEmpty) {
        await firebaseDatabase.ref('chats').child("$chatId/peopleInChat/$currentUserId").update({
          'is_read': false,
        });
      }
    });

    return Future.value(unit);
  }
}
