import 'package:app_chat/domain/value_objects/auth/email_address.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/auth/auth_failure.dart';
import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../core/di/infrastructure_injection.dart';
import '../../core/network/network_info.dart';
import '../../dtos/user/user_dto.dart';
import '../user_infor/cache_user_info_impl.dart';

@lazySingleton
@Injectable(as: ISignInWithGoogle)
class SignInWithGoogleImpl implements ISignInWithGoogle {
  final GoogleSignIn googleSignIn;
  final FirebaseAuth firebaseAuth;
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;
  final ICacheUserInfoImpl cacheUserInfoImpl;

  SignInWithGoogleImpl(
    this.firebaseFirestore,
    this.cacheUserInfoImpl, {
    required this.googleSignIn,
    required this.firebaseAuth,
    required this.networkInfoImpl,
  });

  @override
  Future<Either<GoogleAuthFailure, Unit>> call(String token) async {
    if (await networkInfoImpl.isConnected) {
      try {
        final googleUser = await googleSignIn.signIn();
        if (googleUser == null) {
          return left(const GoogleAuthFailure.cancelledByUser());
        }

        final googleAuthentication = await googleUser.authentication;

        final googleAuthCredential = GoogleAuthProvider.credential(
          idToken: googleAuthentication.idToken,
          accessToken: googleAuthentication.accessToken,
        );

        final User user = (await firebaseAuth.signInWithCredential(googleAuthCredential)).user!;
        final userDTO = UserDTO(
          email: user.email!,
          phoneNumber: user.phoneNumber ?? '',
          fullName: user.displayName!,
          followers: [],
          following: [],
          posts: [],
          profilePicture: user.photoURL!,
          uid: user.uid,
          bio: '',
          status: 'Online',
          lastTimeOnline: injector<DateTime>(),
          token: token,
        );

        await firebaseFirestore
            .collection('users')
            .doc(user.uid)
            .update(
              userDTO.toJson(),
            )
            .then((value) async => await cacheUserInfoImpl.call(EmailAddress(user.email!), token));

        return right(unit);
      } on FirebaseAuthException catch (e) {
        if (e.code == 'sign_in_canceled') {
          return left(const GoogleAuthFailure.cancelledByUser());
        } else if (e.code == 'account-exists-with-different-credential') {
          return left(const GoogleAuthFailure.accountExistsWithDifferentCredential());
        } else {
          return left(const GoogleAuthFailure.serverError());
        }
      }
    } else {
      return left(const GoogleAuthFailure.offline());
    }
  }
}
