import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/auth/auth_failure.dart';
import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../../domain/value_objects/auth/email_address.dart';
import '../../../domain/value_objects/auth/password.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: ISignInWithEmailAndPassword)
class SignInWithEmailAndPasswordImpl implements ISignInWithEmailAndPassword {
  final FirebaseAuth firebaseAuth;
  final NetworkInfoImpl networkInfoImpl;

  SignInWithEmailAndPasswordImpl({required this.networkInfoImpl, required this.firebaseAuth});

  @override
  Future<Either<AuthFailure, Unit>> call({required EmailAddress email, required Password password}) async {
    if (await networkInfoImpl.isConnected) {
      try {
        final emailStr = email.getValueOrCrash();
        final passwordStr = password.getValueOrCrash();

        await firebaseAuth.signInWithEmailAndPassword(email: emailStr, password: passwordStr);
        return right(unit);
      } on FirebaseAuthException catch (e) {
        if (e.code == 'wrong-password' || e.code == 'user-not-found' || e.code == 'invalid-email') {
          return left(const AuthFailure.invalidEmailAndPassword());
        } else {
          return left(const AuthFailure.serverError());
        }
      }
    } else {
      return left(const AuthFailure.offline());
    }
  }
}
