import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/auth/auth_failure.dart';
import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: ISignInWithFacebook)
class SignInWithFacebookImpl implements ISignInWithFacebook {
  final FacebookAuth facebookAuth;
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;

  SignInWithFacebookImpl(
    this.facebookAuth,
    this.firebaseFirestore,
    this.networkInfoImpl,
  );

  @override
  Future<Either<FacebookAuthFailure, Unit>> call() async {
    if (await networkInfoImpl.isConnected) {
      try {
        await facebookAuth.login();
        return right(unit);
      } on PlatformException catch (e) {
        if (e.code == 'CANCELLED') {
          return left(const FacebookAuthFailure.cancelled());
        } else if (e.code == 'OPERATION_IN_PROGRESS') {
          return left(const FacebookAuthFailure.operationInProgress());
        } else {
          return left(const FacebookAuthFailure.serverError());
        }
      }
    } else {
      return left(const FacebookAuthFailure.offline());
    }
  }
}
