import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/auth/auth_failure.dart';
import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../../domain/value_objects/auth/email_address.dart';
import '../../../domain/value_objects/auth/full_name.dart';
import '../../../domain/value_objects/auth/password.dart';
import '../../../domain/value_objects/auth/phone_number.dart';
import '../../core/di/infrastructure_injection.dart';
import '../../core/network/network_info.dart';
import '../../dtos/user/user_dto.dart';

@lazySingleton
@Injectable(as: ISignUpWithEmailAndPassword)
class SignUpWithEmailAndPasswordImpl implements ISignUpWithEmailAndPassword {
  final FirebaseAuth firebaseAuth;
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;

  SignUpWithEmailAndPasswordImpl(this.firebaseFirestore, {required this.firebaseAuth, required this.networkInfoImpl});

  @override
  Future<Either<AuthFailure, Unit>> call({
    required EmailAddress email,
    required Password password,
    required FullName fullName,
    required PhoneNumber phoneNumber,
    required String profilePicture,
    required String token,
  }) async {
    if (await networkInfoImpl.isConnected) {
      try {
        final emailStr = email.getValueOrCrash();
        final passwordStr = password.getValueOrCrash();
        final fullNameStr = fullName.getValueOrCrash();
        final phoneNumberStr = phoneNumber.getValueOrCrash();

        UserCredential userCredential =
            await firebaseAuth.createUserWithEmailAndPassword(email: emailStr, password: passwordStr);

        final userDTO = UserDTO(
          email: emailStr,
          phoneNumber: phoneNumberStr,
          fullName: fullNameStr,
          followers: [],
          following: [],
          posts: [],
          profilePicture: profilePicture,
          uid: userCredential.user!.uid,
          bio: '',
          status: 'Online',
          lastTimeOnline: injector<DateTime>(),
          token: token,
        );

        await firebaseFirestore.collection('users').doc(userCredential.user!.uid).set(
              userDTO.toJson(),
            );

        return right(unit);
      } on FirebaseAuthException catch (e) {
        if (e.code == 'email-already-in-use') {
          return left(const AuthFailure.emailAlreadyInUse());
        } else {
          return left(const AuthFailure.serverError());
        }
      }
    } else {
      return left(const AuthFailure.offline());
    }
  }
}
