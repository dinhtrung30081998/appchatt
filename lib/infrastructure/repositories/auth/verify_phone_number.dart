import 'package:auto_route/auto_route.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/auth/auth_failure.dart';
import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../../domain/value_objects/auth/phone_number.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: IVerifyPhoneNumber)
class VerifyPhoneNumberImpl implements IVerifyPhoneNumber {
  final FirebaseAuth firebaseAuth;
  final NetworkInfoImpl networkInfoImpl;

  VerifyPhoneNumberImpl({required this.firebaseAuth, required this.networkInfoImpl});

  @override
  Future<Either<VerifyPhoneNumberAuthFailure, Unit>> call(
      {required PhoneNumber phoneNumber, required BuildContext context}) async {
    if (await networkInfoImpl.isConnected) {
      try {
        final phoneNumberStr = phoneNumber.getValueOrCrash();
        await firebaseAuth.verifyPhoneNumber(
          phoneNumber: '+84 $phoneNumberStr',
          codeAutoRetrievalTimeout: (String verificationId) {},
          codeSent: (String verificationId, int? forceResendingToken) {
            context.router.pushNamed('/verify-otp-page');
          },
          verificationCompleted: (PhoneAuthCredential phoneAuthCredential) async {
            await firebaseAuth.signInWithCredential(phoneAuthCredential);
          },
          verificationFailed: (FirebaseAuthException e) {},
        );
        return right(unit);
      } on FirebaseAuthException catch (e) {
        if (e.code == 'invalid-phone-number') {
          return left(const VerifyPhoneNumberAuthFailure.invalidPhoneNumber());
        } else {
          return left(const VerifyPhoneNumberAuthFailure.serverError());
        }
      }
    } else {
      return left(const VerifyPhoneNumberAuthFailure.offline());
    }
  }
}
