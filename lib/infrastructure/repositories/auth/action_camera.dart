import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/auth/auth_failure.dart';
import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: IActionCamera)
class ActionCameraImpl implements IActionCamera {
  final NetworkInfoImpl networkInfoImpl;
  final FirebaseStorage firebaseStorage;
  final FirebaseFirestore firebaseFirestore;

  ActionCameraImpl(this.firebaseStorage, this.firebaseFirestore, {required this.networkInfoImpl});

  @override
  Future<Either<AuthFailure, String>> call(ImageSource imageSource, String dirImages) async {
    if (await networkInfoImpl.isConnected) {
      String imageUrl = '';
      String uniqueFileNameImage = DateTime.now().millisecondsSinceEpoch.toString();
      //Step 1: Install image_picker packages
      //import the corresponding library
      ImagePicker imagePicker = ImagePicker();
      XFile? xFile = await imagePicker.pickImage(source: imageSource, imageQuality: 100);
      if (xFile == null) return left(const AuthFailure.serverError());

      //Step 2: Upload image to firebase storage
      Reference referenceRoot = FirebaseStorage.instance.ref();
      Reference referenceDirImages = referenceRoot.child(dirImages);

      //Step 3: Create a reference for the image to be stored
      Reference referenceImageToUpload = referenceDirImages.child(uniqueFileNameImage);

      //Step 4: Handling error/success
      try {
        //Step 5: Store the image
        await referenceImageToUpload.putFile(File(xFile.path));
        imageUrl = await referenceImageToUpload.getDownloadURL();
      } on PlatformException catch (_) {
        return left(const AuthFailure.serverError());
      }

      return right(imageUrl);
    } else {
      return left(const AuthFailure.offline());
    }
  }
}
