import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: IGetFirebaseMessagingToken)
class IGetFirebaseMessagingTokenImpl implements IGetFirebaseMessagingToken {
  final NetworkInfoImpl networkInfoImpl;
  final FirebaseMessaging firebaseMessaging;

  IGetFirebaseMessagingTokenImpl(this.firebaseMessaging, {required this.networkInfoImpl});

  @override
  Future<String> call() async {
    String token = '';
    if (await networkInfoImpl.isConnected) {
      await firebaseMessaging.requestPermission();

      await firebaseMessaging.getToken().then((value) {
        if (value != null) {
          token = value;
        }
      });
      return token;
    }
    return '';
  }
}
