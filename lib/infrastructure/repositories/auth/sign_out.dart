import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/auth/i_auth_repository.dart';
import '../../core/di/infrastructure_injection.dart';
import '../../data_sources/user_infor/user_infor_local.dart';

@lazySingleton
@Injectable(as: ISignOut)
class SignOutImpl implements ISignOut {
  final FirebaseAuth firebaseAuth;
  final FirebaseFirestore firebaseFirestore;
  final GoogleSignIn googleSignIn;
  final UserInforLocalImpl userInforLocalImpl;

  SignOutImpl(this.googleSignIn, this.userInforLocalImpl, this.firebaseFirestore, {required this.firebaseAuth});

  @override
  Future<Unit> call(String status) {
    final currentUser = firebaseAuth.currentUser!.uid;
    Future.wait([
      firebaseFirestore.collection('users').doc(currentUser).update({
        'status': status,
        'lastTimeOnline': injector<DateTime>().toString(),
      }),
      firebaseAuth.signOut(),
      googleSignIn.signOut(),
      userInforLocalImpl.cachedUserId(''),
      userInforLocalImpl.cachedEmail(''),
      userInforLocalImpl.cachedFullName(''),
      userInforLocalImpl.cachedPhoneNumber(''),
      userInforLocalImpl.cachedProfilePicture(''),
      userInforLocalImpl.cachedToken(''),
    ]);

    return Future.value(unit);
  }
}
