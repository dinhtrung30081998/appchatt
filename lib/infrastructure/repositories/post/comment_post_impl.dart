import 'package:app_chat/infrastructure/core/network/network_info.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

import '../../../domain/repositories/post/i_post_repository.dart';
import '../../core/di/infrastructure_injection.dart';
import '../../dtos/comment/comment_dto.dart';

@lazySingleton
@Injectable(as: ICommentPost)
class ICommentPostImpl implements ICommentPost {
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;

  ICommentPostImpl(this.networkInfoImpl, {required this.firebaseFirestore});

  @override
  Future<Unit> call(
    String postId,
    String comment,
    String userId,
    String userName,
    String profilePicture,
  ) async {
    if (await networkInfoImpl.isConnected) {
      try {
        if (comment.isNotEmpty) {
          String commentId = const Uuid().v1();
          String datePublished = injector<DateFormat>().format(injector<DateTime>());

          await firebaseFirestore.collection('posts').doc(postId).collection('comments').doc(commentId).set(CommentDTO(
                profilePicture: profilePicture,
                uid: userId,
                fullName: userName,
                comment: comment,
                commentId: commentId,
                datePublished: datePublished,
                likes: [],
              ).toJson());

          await firebaseFirestore.collection('posts').doc(postId).update({
            'comments': FieldValue.arrayUnion([commentId]),
          });
        }
      } catch (_) {}
    } else {}
    return unit;
  }
}
