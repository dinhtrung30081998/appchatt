import 'package:app_chat/infrastructure/core/di/infrastructure_injection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/post/i_post_repository.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: IDeletePost)
class IDeletePostImpl implements IDeletePost {
  final FirebaseFirestore firebaseFirestore;
  final FirebaseAuth firebaseAuth;

  final NetworkInfoImpl networkInfoImpl;

  IDeletePostImpl(this.networkInfoImpl, this.firebaseAuth, {required this.firebaseFirestore});

  @override
  Future<Unit> call(String postId) async {
    if (await networkInfoImpl.isConnected) {
      try {
        final uid = firebaseAuth.currentUser!.uid;

        await firebaseFirestore.collection('posts').doc(postId).collection('comments').get().then((value) {
          if (value.docs.isNotEmpty) {
            for (var element in value.docs) {
              firebaseFirestore.collection('posts').doc(postId).collection('comments').doc(element.id).delete();
            }
          }
        }).then(
          (_) async {
            await firebaseFirestore.collection('users').doc(uid).update({
              'posts': FieldValue.arrayRemove([postId]),
            });
            await firebaseFirestore.collection('posts').doc(postId).delete();
          },
        );
      } catch (_) {}
    } else {}
    return unit;
  }
}
