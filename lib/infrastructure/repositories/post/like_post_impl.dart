import 'package:app_chat/infrastructure/core/network/network_info.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/post/i_post_repository.dart';

@lazySingleton
@Injectable(as: ILikePost)
class ILikePostImpl implements ILikePost {
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;
  ILikePostImpl(this.networkInfoImpl, {required this.firebaseFirestore});

  @override
  Future<Unit> call(String postId, String userId, List likes) async {
    if (await networkInfoImpl.isConnected) {
      try {
        if (likes.contains(userId)) {
          await firebaseFirestore.collection('posts').doc(postId).update({
            'likes': FieldValue.arrayRemove([userId]),
          });
        } else {
          await firebaseFirestore.collection('posts').doc(postId).update({
            'likes': FieldValue.arrayUnion([userId]),
          });
        }
      } catch (_) {}
    } else {}
    return unit;
  }
}
