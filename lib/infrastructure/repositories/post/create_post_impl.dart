import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';
import 'package:uuid/uuid.dart';

import '../../../domain/core/failures/user_info/user_info_failure.dart';
import '../../../domain/repositories/post/i_post_repository.dart';
import '../../core/di/infrastructure_injection.dart';
import '../../core/network/network_info.dart';
import '../../dtos/post/post_dto.dart';

@lazySingleton
@Injectable(as: ICreatePost)
class CreatePostImpl implements ICreatePost {
  final FirebaseFirestore firebaseFirestore;
  final FirebaseAuth firebaseAuth;
  final NetworkInfoImpl networkInfoImpl;

  CreatePostImpl(this.firebaseAuth, {required this.firebaseFirestore, required this.networkInfoImpl});

  @override
  Future<Either<CreatePostFailure, Unit>> call(
    String fullName,
    String description,
    List<String> listPostImageUrl,
    String profileImage,
  ) async {
    if (await networkInfoImpl.isConnected) {
      try {
        String postId = const Uuid().v1();
        String uid = firebaseAuth.currentUser!.uid;

        PostDTO postDTO = PostDTO(
          postId: postId,
          uid: uid,
          fullName: fullName,
          description: description,
          datePublished: injector<DateTime>(),
          postUrl: listPostImageUrl,
          profileImage: profileImage,
          hidePost: false,
          likes: [],
          comments: [],
        );

        await firebaseFirestore.collection('posts').doc(postId).set(postDTO.toJson()).then(
              (_) => firebaseFirestore.collection('users').doc(uid).update(
                {
                  'posts': FieldValue.arrayUnion([postId]),
                },
              ),
            );

        return right(unit);
      } on FirebaseException catch (_) {
        return left(const CreatePostFailure.createPostFail());
      }
    } else {
      return left(const CreatePostFailure.createPostOffline());
    }
  }
}
