
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/entities/post/post_entity.dart';
import '../../../domain/repositories/user_infor/i_user_infor_repository.dart';
import '../../dtos/post/post_dto.dart';

@lazySingleton
@Injectable(as: IGetListPost)
class GetListPostImpl implements IGetListPost {
  final FirebaseFirestore firebaseFirestore;

  GetListPostImpl({required this.firebaseFirestore});

  @override
  Stream<List<PostEntity>> call() async* {
    yield* firebaseFirestore.collection('posts').orderBy('datePublished', descending: true).snapshots().map(
          (querySnapshot) =>
              querySnapshot.docs.map((document) => PostDTO.fromJson(document.data()).toDomain()).toList(),
        );
  }
}
