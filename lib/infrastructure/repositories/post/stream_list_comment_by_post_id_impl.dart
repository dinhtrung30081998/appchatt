import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/entities/comment/comment_entity.dart';
import '../../../domain/repositories/post/i_post_repository.dart';
import '../../dtos/comment/comment_dto.dart';

@lazySingleton
@Injectable(as: IGetListCommentByPostId)
class IGetListCommentByPostIdImpl implements IGetListCommentByPostId {
  final FirebaseFirestore firebaseFirestore;

  IGetListCommentByPostIdImpl({required this.firebaseFirestore});

  @override
  Stream<List<CommentEntity>> call(String postId) async* {
    yield* firebaseFirestore
        .collection('posts')
        .doc(postId)
        .collection('comments')
        .orderBy('datePublished', descending: true)
        .snapshots()
        .map(
          (querySnapshot) =>
              querySnapshot.docs.map((document) => CommentDTO.fromJson(document.data()).toDomain()).toList(),
        );
  }
}
