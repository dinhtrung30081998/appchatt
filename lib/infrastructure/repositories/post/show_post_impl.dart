import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/post/i_post_repository.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: IShowPost)
class IShowPostImpl implements IShowPost {
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;

  IShowPostImpl(this.networkInfoImpl, {required this.firebaseFirestore});

  @override
  Future<Unit> call(String postId) async {
    if (await networkInfoImpl.isConnected) {
      try {
        await firebaseFirestore.collection('posts').doc(postId).update({
          'hidePost': false,
        });
      } catch (_) {}
    } else {}
    return unit;
  }
}
