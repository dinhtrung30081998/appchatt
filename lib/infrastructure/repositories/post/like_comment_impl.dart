import 'package:app_chat/infrastructure/core/network/network_info.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/post/i_post_repository.dart';

@lazySingleton
@Injectable(as: ILikeComment)
class ILikeCommentImpl implements ILikeComment {
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;

  ILikeCommentImpl(this.networkInfoImpl, {required this.firebaseFirestore});

  @override
  Future<Unit> call(String postId, String commentId, String userId, List likes) async {
    if (await networkInfoImpl.isConnected) {
      try {
        if (likes.contains(userId)) {
          await firebaseFirestore.collection('posts').doc(postId).collection('comments').doc(commentId).update({
            'likes': FieldValue.arrayRemove([userId]),
          });
        } else {
          await firebaseFirestore.collection('posts').doc(postId).collection('comments').doc(commentId).update({
            'likes': FieldValue.arrayUnion([userId]),
          });
        }
      } catch (_) {}
    } else {}
    return unit;
  }
}
