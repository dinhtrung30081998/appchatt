import 'package:app_chat/infrastructure/dtos/user/user_dto.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/entities/user/user_entity.dart';
import '../../../domain/repositories/user_infor/i_user_infor_repository.dart';

@lazySingleton
@Injectable(as: IStreamGetUserInfoByUid)
class IStreamGetUserInfoByUidImpl implements IStreamGetUserInfoByUid {
  final FirebaseFirestore firebaseFirestore;

  IStreamGetUserInfoByUidImpl({required this.firebaseFirestore});

  @override
  Stream<UserEntity> call(String uid) async* {
    yield* firebaseFirestore.collection('users').doc(uid).snapshots().map(
          (querySnapshot) => UserDTO.fromJson(querySnapshot.data()!).toDomain(),
        );
  }

  // @override
  // Future<UserEntity> call(String uid) async {
  //   final snapshot = await firebaseFirestore.collection('users').doc(uid).get();
  //   if (snapshot.exists) {
  //     Map<String, dynamic>? data = snapshot.data();
  //     return UserDTO.fromJson(data!).toDomain();
  //   }
  //   return UserEntity.empty();
  // }
}
