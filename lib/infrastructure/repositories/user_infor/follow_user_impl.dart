import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/user_infor/i_user_infor_repository.dart';
import '../../data_sources/user_infor/user_infor_local.dart';

@lazySingleton
@Injectable(as: IFollowUser)
class IFollowUserImpl implements IFollowUser {
  final FirebaseFirestore firebaseFirestore;
  final UserInforLocalImpl userInforLocalImpl;

  IFollowUserImpl(this.userInforLocalImpl, {required this.firebaseFirestore});

  @override
  Future<Unit> call(String uid, String followId) async {
    final snapshot = await firebaseFirestore.collection('users').doc(uid).get();
    List following = snapshot.data()!['following'];

    if (following.contains(followId)) {
      await firebaseFirestore.collection('users').doc(followId).update({
        'followers': FieldValue.arrayRemove([uid]),
      });

      await firebaseFirestore.collection('users').doc(uid).update({
        'following': FieldValue.arrayRemove([followId]),
      }).then((_) {
        final following = userInforLocalImpl.getFollowing() ?? [];
        following.removeWhere((id) => id == followId);
        userInforLocalImpl.cachedFollowing(following);
        print("Đang theo dõi (remove): ${userInforLocalImpl.getFollowing().toString()}");
      });
    } else {
      await firebaseFirestore.collection('users').doc(followId).update({
        'followers': FieldValue.arrayUnion([uid]),
      });

      await firebaseFirestore.collection('users').doc(uid).update({
        'following': FieldValue.arrayUnion([followId]),
      }).then((_) {
        final following = userInforLocalImpl.getFollowing() ?? [];
        following.add(followId);
        userInforLocalImpl.cachedFollowing(following);
        print("Đang theo dõi (add): ${userInforLocalImpl.getFollowing().toString()}");
      });
    }
    return Future.value(unit);
  }
}
