import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';
import 'package:uuid/uuid.dart';

import '../../../domain/repositories/post/i_post_repository.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: IUploadPostImageToStorage)
class IUploadPostImageToStorageImpl implements IUploadPostImageToStorage {
  final FirebaseStorage firebaseStorage;
  final FirebaseAuth firebaseAuth;
  final NetworkInfoImpl networkInfoImpl;

  IUploadPostImageToStorageImpl(this.firebaseAuth, {required this.firebaseStorage, required this.networkInfoImpl});

  @override
  Future<String> call(String childName, XFile file, bool isPost) async {
    if (await networkInfoImpl.isConnected) {
      Reference ref = firebaseStorage.ref().child(childName).child(firebaseAuth.currentUser!.uid);

      if (isPost) {
        String id = const Uuid().v1();
        ref = ref.child(id);
      }
      UploadTask uploadTask = ref.putFile(File(file.path));

      TaskSnapshot taskSnapshot = await uploadTask;
      String imageUrl = await taskSnapshot.ref.getDownloadURL();
      return imageUrl;
    } else {
      return '';
    }
  }
}
