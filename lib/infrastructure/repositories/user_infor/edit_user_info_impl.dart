import 'package:app_chat/infrastructure/data_sources/user_infor/user_infor_local.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/core/failures/user_info/user_info_failure.dart';
import '../../../domain/repositories/user_infor/i_user_infor_repository.dart';
import '../../core/network/network_info.dart';

@lazySingleton
@Injectable(as: IEditUserInfoByUid)
class IEditUserInfoByUidImpl implements IEditUserInfoByUid {
  final FirebaseFirestore firebaseFirestore;
  final NetworkInfoImpl networkInfoImpl;
  final UserInforLocalImpl userInforLocalImpl;

  IEditUserInfoByUidImpl(this.userInforLocalImpl, {required this.firebaseFirestore, required this.networkInfoImpl});

  @override
  Future<Either<EditUserInfoFailure, Unit>> call(
    String uid,
    String fullName,
    String bio,
  ) async {
    if (await networkInfoImpl.isConnected) {
      try {
        if (fullName.isNotEmpty && bio.isNotEmpty) {
          await firebaseFirestore.collection('users').doc(uid).update({
            'fullName': fullName,
            'bio': bio,
          });
          if (userInforLocalImpl.isCachedFullName() && userInforLocalImpl.isCachedBio()) {
            await userInforLocalImpl.cachedFullName('');
            await userInforLocalImpl.cachedBio('');
            await userInforLocalImpl.cachedFullName(fullName);
            await userInforLocalImpl.cachedBio(bio);
          }
        }

        return right(unit);
      } on FirebaseException catch (_) {
        return left(const EditUserInfoFailure.editFail());
      }
    } else {
      return left(const EditUserInfoFailure.editOffline());
    }
  }
}
