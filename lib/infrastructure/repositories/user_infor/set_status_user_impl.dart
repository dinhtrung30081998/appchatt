import 'package:app_chat/infrastructure/core/di/infrastructure_injection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/user_infor/i_user_infor_repository.dart';

@lazySingleton
@Injectable(as: ISetStatusUser)
class ISetStatusUserImpl implements ISetStatusUser {
  final FirebaseFirestore firebaseFirestore;
  final FirebaseAuth firebaseAuth;

  ISetStatusUserImpl(this.firebaseAuth, {required this.firebaseFirestore});

  @override
  Future<Unit> call(String status, String token) async {
    final currentUserId = firebaseAuth.currentUser!.uid;

    await firebaseFirestore.collection('users').doc(currentUserId).update({
      'status': status,
      'token': token,
      'lastTimeOnline': injector<DateTime>().toString(),
    });
    return unit;
  }
}
