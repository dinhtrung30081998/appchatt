import 'package:injectable/injectable.dart';

import '../../../domain/repositories/user_infor/i_user_infor_repository.dart';
import '../../data_sources/user_infor/user_infor_local.dart';

@lazySingleton
@Injectable(as: IGetUserInfoLocal)
class IGetUserInfoLocalImpl implements IGetUserInfoLocal {
  final UserInforLocalImpl userInforLocalImpl;

  IGetUserInfoLocalImpl(this.userInforLocalImpl);

  @override
  String? getUserId() {
    return userInforLocalImpl.getCacheUid();
  }

  @override
  String? getEmail() {
    return userInforLocalImpl.getCacheEmail();
  }

  @override
  String? getFullName() {
    return userInforLocalImpl.getCacheFullName();
  }

  @override
  String? getPhoneNumber() {
    return userInforLocalImpl.getCachePhoneNumber();
  }

  @override
  String? getProfilePicture() {
    return userInforLocalImpl.getCacheProfilePicture();
  }

  @override
  String? getToken() {
    return userInforLocalImpl.getCacheToken();
  }

  @override
  bool? isCachedUserId() {
    return userInforLocalImpl.isCachedUserId();
  }

  @override
  List<String>? getFollowing() {
    return userInforLocalImpl.getFollowing();
  }
}
