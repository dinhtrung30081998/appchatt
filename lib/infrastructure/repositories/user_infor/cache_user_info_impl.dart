import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories/user_infor/i_user_infor_repository.dart';
import '../../../domain/value_objects/auth/email_address.dart';
import '../../data_sources/user_infor/user_infor_local.dart';
import '../../data_sources/user_infor/user_infor_remote.dart';

@lazySingleton
@Injectable(as: ICacheUserInfo)
class ICacheUserInfoImpl implements ICacheUserInfo {
  final UserInforRemoteImpl userInforRemoteImpl;
  final UserInforLocalImpl userInforLocalImpl;

  ICacheUserInfoImpl(this.userInforRemoteImpl, this.userInforLocalImpl);

  @override
  Future<Unit> call(EmailAddress emailAddress, String token) async {
    final userDetailRemote = await userInforRemoteImpl.getUserDetails(emailAddress);
    userInforLocalImpl.cachedUserId(userDetailRemote.uid.getValueOrCrash());
    userInforLocalImpl.cachedEmail(userDetailRemote.email);
    userInforLocalImpl.cachedBio(userDetailRemote.bio);
    userInforLocalImpl.cachedFullName(userDetailRemote.fullName);
    userInforLocalImpl.cachedPhoneNumber(userDetailRemote.phoneNumber);
    userInforLocalImpl.cachedProfilePicture(userDetailRemote.profilePicture);
    userInforLocalImpl.cachedToken(token);
    return unit;
  }
}
