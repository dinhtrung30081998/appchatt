import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/post/post_entity.dart';

part 'post_dto.freezed.dart';
part 'post_dto.g.dart';

@freezed
abstract class PostDTO implements _$PostDTO {
  const PostDTO._();

  const factory PostDTO({
    required String postId,
    required String uid,
    required String fullName,
    required String description,
    required DateTime datePublished,
    required List<String> postUrl,
    required String profileImage,
    required bool hidePost,
    required List likes,
    required List comments,
  }) = _PostDTO;

  factory PostDTO.fromDomain(PostEntity postEntity) {
    return PostDTO(
      postId: postEntity.postId,
      uid: postEntity.uid,
      fullName: postEntity.fullName,
      description: postEntity.description,
      datePublished: postEntity.datePublished,
      postUrl: postEntity.postUrl,
      profileImage: postEntity.profileImage,
      hidePost: postEntity.hidePost,
      likes: postEntity.likes,
      comments: postEntity.comments,
    );
  }

  PostEntity toDomain() {
    return PostEntity(
      postId: postId,
      uid: uid,
      fullName: fullName,
      description: description,
      datePublished: datePublished,
      postUrl: postUrl,
      profileImage: profileImage,
      hidePost: hidePost,
      likes: likes,
      comments: comments,
    );
  }

  factory PostDTO.fromJson(Map<String, dynamic> json) => _$PostDTOFromJson(json);
}
