// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PostDTO _$$_PostDTOFromJson(Map<String, dynamic> json) => _$_PostDTO(
      postId: json['postId'] as String,
      uid: json['uid'] as String,
      fullName: json['fullName'] as String,
      description: json['description'] as String,
      datePublished: DateTime.parse(json['datePublished'] as String),
      postUrl:
          (json['postUrl'] as List<dynamic>).map((e) => e as String).toList(),
      profileImage: json['profileImage'] as String,
      hidePost: json['hidePost'] as bool,
      likes: json['likes'] as List<dynamic>,
      comments: json['comments'] as List<dynamic>,
    );

Map<String, dynamic> _$$_PostDTOToJson(_$_PostDTO instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'uid': instance.uid,
      'fullName': instance.fullName,
      'description': instance.description,
      'datePublished': instance.datePublished.toIso8601String(),
      'postUrl': instance.postUrl,
      'profileImage': instance.profileImage,
      'hidePost': instance.hidePost,
      'likes': instance.likes,
      'comments': instance.comments,
    };
