// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'post_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PostDTO _$PostDTOFromJson(Map<String, dynamic> json) {
  return _PostDTO.fromJson(json);
}

/// @nodoc
mixin _$PostDTO {
  String get postId => throw _privateConstructorUsedError;
  String get uid => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  DateTime get datePublished => throw _privateConstructorUsedError;
  List<String> get postUrl => throw _privateConstructorUsedError;
  String get profileImage => throw _privateConstructorUsedError;
  bool get hidePost => throw _privateConstructorUsedError;
  List<dynamic> get likes => throw _privateConstructorUsedError;
  List<dynamic> get comments => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PostDTOCopyWith<PostDTO> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostDTOCopyWith<$Res> {
  factory $PostDTOCopyWith(PostDTO value, $Res Function(PostDTO) then) =
      _$PostDTOCopyWithImpl<$Res, PostDTO>;
  @useResult
  $Res call(
      {String postId,
      String uid,
      String fullName,
      String description,
      DateTime datePublished,
      List<String> postUrl,
      String profileImage,
      bool hidePost,
      List<dynamic> likes,
      List<dynamic> comments});
}

/// @nodoc
class _$PostDTOCopyWithImpl<$Res, $Val extends PostDTO>
    implements $PostDTOCopyWith<$Res> {
  _$PostDTOCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? postId = null,
    Object? uid = null,
    Object? fullName = null,
    Object? description = null,
    Object? datePublished = null,
    Object? postUrl = null,
    Object? profileImage = null,
    Object? hidePost = null,
    Object? likes = null,
    Object? comments = null,
  }) {
    return _then(_value.copyWith(
      postId: null == postId
          ? _value.postId
          : postId // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      datePublished: null == datePublished
          ? _value.datePublished
          : datePublished // ignore: cast_nullable_to_non_nullable
              as DateTime,
      postUrl: null == postUrl
          ? _value.postUrl
          : postUrl // ignore: cast_nullable_to_non_nullable
              as List<String>,
      profileImage: null == profileImage
          ? _value.profileImage
          : profileImage // ignore: cast_nullable_to_non_nullable
              as String,
      hidePost: null == hidePost
          ? _value.hidePost
          : hidePost // ignore: cast_nullable_to_non_nullable
              as bool,
      likes: null == likes
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      comments: null == comments
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PostDTOCopyWith<$Res> implements $PostDTOCopyWith<$Res> {
  factory _$$_PostDTOCopyWith(
          _$_PostDTO value, $Res Function(_$_PostDTO) then) =
      __$$_PostDTOCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String postId,
      String uid,
      String fullName,
      String description,
      DateTime datePublished,
      List<String> postUrl,
      String profileImage,
      bool hidePost,
      List<dynamic> likes,
      List<dynamic> comments});
}

/// @nodoc
class __$$_PostDTOCopyWithImpl<$Res>
    extends _$PostDTOCopyWithImpl<$Res, _$_PostDTO>
    implements _$$_PostDTOCopyWith<$Res> {
  __$$_PostDTOCopyWithImpl(_$_PostDTO _value, $Res Function(_$_PostDTO) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? postId = null,
    Object? uid = null,
    Object? fullName = null,
    Object? description = null,
    Object? datePublished = null,
    Object? postUrl = null,
    Object? profileImage = null,
    Object? hidePost = null,
    Object? likes = null,
    Object? comments = null,
  }) {
    return _then(_$_PostDTO(
      postId: null == postId
          ? _value.postId
          : postId // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      datePublished: null == datePublished
          ? _value.datePublished
          : datePublished // ignore: cast_nullable_to_non_nullable
              as DateTime,
      postUrl: null == postUrl
          ? _value._postUrl
          : postUrl // ignore: cast_nullable_to_non_nullable
              as List<String>,
      profileImage: null == profileImage
          ? _value.profileImage
          : profileImage // ignore: cast_nullable_to_non_nullable
              as String,
      hidePost: null == hidePost
          ? _value.hidePost
          : hidePost // ignore: cast_nullable_to_non_nullable
              as bool,
      likes: null == likes
          ? _value._likes
          : likes // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      comments: null == comments
          ? _value._comments
          : comments // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PostDTO extends _PostDTO {
  const _$_PostDTO(
      {required this.postId,
      required this.uid,
      required this.fullName,
      required this.description,
      required this.datePublished,
      required final List<String> postUrl,
      required this.profileImage,
      required this.hidePost,
      required final List<dynamic> likes,
      required final List<dynamic> comments})
      : _postUrl = postUrl,
        _likes = likes,
        _comments = comments,
        super._();

  factory _$_PostDTO.fromJson(Map<String, dynamic> json) =>
      _$$_PostDTOFromJson(json);

  @override
  final String postId;
  @override
  final String uid;
  @override
  final String fullName;
  @override
  final String description;
  @override
  final DateTime datePublished;
  final List<String> _postUrl;
  @override
  List<String> get postUrl {
    if (_postUrl is EqualUnmodifiableListView) return _postUrl;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_postUrl);
  }

  @override
  final String profileImage;
  @override
  final bool hidePost;
  final List<dynamic> _likes;
  @override
  List<dynamic> get likes {
    if (_likes is EqualUnmodifiableListView) return _likes;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_likes);
  }

  final List<dynamic> _comments;
  @override
  List<dynamic> get comments {
    if (_comments is EqualUnmodifiableListView) return _comments;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_comments);
  }

  @override
  String toString() {
    return 'PostDTO(postId: $postId, uid: $uid, fullName: $fullName, description: $description, datePublished: $datePublished, postUrl: $postUrl, profileImage: $profileImage, hidePost: $hidePost, likes: $likes, comments: $comments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PostDTO &&
            (identical(other.postId, postId) || other.postId == postId) &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.datePublished, datePublished) ||
                other.datePublished == datePublished) &&
            const DeepCollectionEquality().equals(other._postUrl, _postUrl) &&
            (identical(other.profileImage, profileImage) ||
                other.profileImage == profileImage) &&
            (identical(other.hidePost, hidePost) ||
                other.hidePost == hidePost) &&
            const DeepCollectionEquality().equals(other._likes, _likes) &&
            const DeepCollectionEquality().equals(other._comments, _comments));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      postId,
      uid,
      fullName,
      description,
      datePublished,
      const DeepCollectionEquality().hash(_postUrl),
      profileImage,
      hidePost,
      const DeepCollectionEquality().hash(_likes),
      const DeepCollectionEquality().hash(_comments));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PostDTOCopyWith<_$_PostDTO> get copyWith =>
      __$$_PostDTOCopyWithImpl<_$_PostDTO>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PostDTOToJson(
      this,
    );
  }
}

abstract class _PostDTO extends PostDTO {
  const factory _PostDTO(
      {required final String postId,
      required final String uid,
      required final String fullName,
      required final String description,
      required final DateTime datePublished,
      required final List<String> postUrl,
      required final String profileImage,
      required final bool hidePost,
      required final List<dynamic> likes,
      required final List<dynamic> comments}) = _$_PostDTO;
  const _PostDTO._() : super._();

  factory _PostDTO.fromJson(Map<String, dynamic> json) = _$_PostDTO.fromJson;

  @override
  String get postId;
  @override
  String get uid;
  @override
  String get fullName;
  @override
  String get description;
  @override
  DateTime get datePublished;
  @override
  List<String> get postUrl;
  @override
  String get profileImage;
  @override
  bool get hidePost;
  @override
  List<dynamic> get likes;
  @override
  List<dynamic> get comments;
  @override
  @JsonKey(ignore: true)
  _$$_PostDTOCopyWith<_$_PostDTO> get copyWith =>
      throw _privateConstructorUsedError;
}
