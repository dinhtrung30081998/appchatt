// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'story_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StoryDTO _$$_StoryDTOFromJson(Map<String, dynamic> json) => _$_StoryDTO(
      name: json['name'] as String,
      url: json['url'] as String,
    );

Map<String, dynamic> _$$_StoryDTOToJson(_$_StoryDTO instance) =>
    <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };
