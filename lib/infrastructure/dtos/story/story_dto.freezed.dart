// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'story_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StoryDTO _$StoryDTOFromJson(Map<String, dynamic> json) {
  return _StoryDTO.fromJson(json);
}

/// @nodoc
mixin _$StoryDTO {
  String get name => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StoryDTOCopyWith<StoryDTO> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StoryDTOCopyWith<$Res> {
  factory $StoryDTOCopyWith(StoryDTO value, $Res Function(StoryDTO) then) =
      _$StoryDTOCopyWithImpl<$Res, StoryDTO>;
  @useResult
  $Res call({String name, String url});
}

/// @nodoc
class _$StoryDTOCopyWithImpl<$Res, $Val extends StoryDTO>
    implements $StoryDTOCopyWith<$Res> {
  _$StoryDTOCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? url = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_StoryDTOCopyWith<$Res> implements $StoryDTOCopyWith<$Res> {
  factory _$$_StoryDTOCopyWith(
          _$_StoryDTO value, $Res Function(_$_StoryDTO) then) =
      __$$_StoryDTOCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String name, String url});
}

/// @nodoc
class __$$_StoryDTOCopyWithImpl<$Res>
    extends _$StoryDTOCopyWithImpl<$Res, _$_StoryDTO>
    implements _$$_StoryDTOCopyWith<$Res> {
  __$$_StoryDTOCopyWithImpl(
      _$_StoryDTO _value, $Res Function(_$_StoryDTO) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? url = null,
  }) {
    return _then(_$_StoryDTO(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_StoryDTO extends _StoryDTO {
  const _$_StoryDTO({required this.name, required this.url}) : super._();

  factory _$_StoryDTO.fromJson(Map<String, dynamic> json) =>
      _$$_StoryDTOFromJson(json);

  @override
  final String name;
  @override
  final String url;

  @override
  String toString() {
    return 'StoryDTO(name: $name, url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StoryDTO &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.url, url) || other.url == url));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, name, url);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_StoryDTOCopyWith<_$_StoryDTO> get copyWith =>
      __$$_StoryDTOCopyWithImpl<_$_StoryDTO>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StoryDTOToJson(
      this,
    );
  }
}

abstract class _StoryDTO extends StoryDTO {
  const factory _StoryDTO(
      {required final String name, required final String url}) = _$_StoryDTO;
  const _StoryDTO._() : super._();

  factory _StoryDTO.fromJson(Map<String, dynamic> json) = _$_StoryDTO.fromJson;

  @override
  String get name;
  @override
  String get url;
  @override
  @JsonKey(ignore: true)
  _$$_StoryDTOCopyWith<_$_StoryDTO> get copyWith =>
      throw _privateConstructorUsedError;
}
