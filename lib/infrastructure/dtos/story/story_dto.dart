import 'package:app_chat/domain/entities/story/story_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'story_dto.freezed.dart';
part 'story_dto.g.dart';

@freezed
abstract class StoryDTO implements _$StoryDTO {
  const StoryDTO._();

  const factory StoryDTO({
    required String name,
    required String url,
  }) = _StoryDTO;

  factory StoryDTO.fromDomain(StoryEntity storyEntity) {
    return StoryDTO(
      name: storyEntity.name,
      url: storyEntity.url,
    );
  }

  StoryEntity toDomain() {
    return StoryEntity(
      name: name,
      url: url,
    );
  }

  factory StoryDTO.fromJson(Map<String, dynamic> json) => _$StoryDTOFromJson(json);
}
