// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserDTO _$$_UserDTOFromJson(Map<String, dynamic> json) => _$_UserDTO(
      uid: json['uid'] as String,
      email: json['email'] as String,
      token: json['token'] as String,
      fullName: json['fullName'] as String,
      phoneNumber: json['phoneNumber'] as String,
      bio: json['bio'] as String,
      followers:
          (json['followers'] as List<dynamic>).map((e) => e as String).toList(),
      following:
          (json['following'] as List<dynamic>).map((e) => e as String).toList(),
      posts: (json['posts'] as List<dynamic>).map((e) => e as String).toList(),
      profilePicture: json['profilePicture'] as String,
      status: json['status'] as String,
      lastTimeOnline: DateTime.parse(json['lastTimeOnline'] as String),
    );

Map<String, dynamic> _$$_UserDTOToJson(_$_UserDTO instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'email': instance.email,
      'token': instance.token,
      'fullName': instance.fullName,
      'phoneNumber': instance.phoneNumber,
      'bio': instance.bio,
      'followers': instance.followers,
      'following': instance.following,
      'posts': instance.posts,
      'profilePicture': instance.profilePicture,
      'status': instance.status,
      'lastTimeOnline': instance.lastTimeOnline.toIso8601String(),
    };
