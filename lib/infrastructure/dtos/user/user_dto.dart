import '../../../domain/value_objects/auth/user_unique_id.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/user/user_entity.dart';

part 'user_dto.freezed.dart';
part 'user_dto.g.dart';

@freezed
abstract class UserDTO implements _$UserDTO {
  const UserDTO._();

  const factory UserDTO({
    required String uid,
    required String email,
    required String token,
    required String fullName,
    required String phoneNumber,
    required String bio,
    required List<String> followers,
    required List<String> following,
    required List<String> posts,
    required String profilePicture,
    required String status,
    required DateTime lastTimeOnline,
  }) = _UserDTO;

  factory UserDTO.fromDomain(UserEntity userEntity) {
    return UserDTO(
      uid: userEntity.uid.getValueOrCrash(),
      email: userEntity.email,
      token: userEntity.token,
      phoneNumber: userEntity.phoneNumber,
      fullName: userEntity.fullName,
      bio: userEntity.bio,
      followers: userEntity.followers,
      following: userEntity.following,
      posts: userEntity.posts,
      profilePicture: userEntity.profilePicture,
      lastTimeOnline: userEntity.lastTimeOnline,
      status: userEntity.status,
    );
  }

  UserEntity toDomain() {
    return UserEntity(
      uid: UserUniqueID.fromUniqueString(uid),
      email: email,
      token: token,
      phoneNumber: phoneNumber,
      fullName: fullName,
      bio: bio,
      followers: followers,
      following: following,
      posts: posts,
      profilePicture: profilePicture,
      status: status,
      lastTimeOnline: lastTimeOnline,
    );
  }

  factory UserDTO.fromJson(Map<String, dynamic> json) => _$UserDTOFromJson(json);
}
