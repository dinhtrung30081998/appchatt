// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MessageDTO _$$_MessageDTOFromJson(Map<String, dynamic> json) =>
    _$_MessageDTO(
      messageId: json['messageId'] as String,
      lastSenderId: json['lastSenderId'] as String,
      users: json['users'] as List<dynamic>,
      messages: json['messages'] as Map<String, dynamic>,
      peopleInChat: json['peopleInChat'] as Map<String, dynamic>,
      listLastMessages: json['listLastMessages'] as Map<String, dynamic>?,
      lastMessage: json['lastMessage'] as String,
      lastTimeMessage: json['lastTimeMessage'] as String,
    );

Map<String, dynamic> _$$_MessageDTOToJson(_$_MessageDTO instance) =>
    <String, dynamic>{
      'messageId': instance.messageId,
      'lastSenderId': instance.lastSenderId,
      'users': instance.users,
      'messages': instance.messages,
      'peopleInChat': instance.peopleInChat,
      'listLastMessages': instance.listLastMessages,
      'lastMessage': instance.lastMessage,
      'lastTimeMessage': instance.lastTimeMessage,
    };

_$_LastMessageDTO _$$_LastMessageDTOFromJson(Map<String, dynamic> json) =>
    _$_LastMessageDTO(
      sendById: json['sendById'] as String,
      message: json['message'] as String,
    );

Map<String, dynamic> _$$_LastMessageDTOToJson(_$_LastMessageDTO instance) =>
    <String, dynamic>{
      'sendById': instance.sendById,
      'message': instance.message,
    };
