import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/message/message_entity.dart';

part 'message_dto.freezed.dart';
part 'message_dto.g.dart';

@freezed
abstract class MessageDTO implements _$MessageDTO {
  const MessageDTO._();

  const factory MessageDTO({
    required String messageId,
    required String lastSenderId,
    required List users,
    required Map messages,
    required Map peopleInChat,
    required Map? listLastMessages,
    required String lastMessage,
    required String lastTimeMessage,
  }) = _MessageDTO;

  factory MessageDTO.fromDomain(MessageEntity messageEntity) {
    return MessageDTO(
      messageId: messageEntity.messageId,
      lastSenderId: messageEntity.lastSenderId,
      users: messageEntity.users,
      messages: messageEntity.messages,
      peopleInChat: messageEntity.peopleInChat,
      listLastMessages: messageEntity.listLastMessages,
      lastMessage: messageEntity.lastMessage,
      lastTimeMessage: messageEntity.lastTimeMessage,
    );
  }

  factory MessageDTO.fromRTDB(Map<String, dynamic> json) {
    return MessageDTO(
      messageId: json['messageId'] as String,
      lastSenderId: json['lastSenderId'] as String,
      users: json['users'] as List<dynamic>,
      messages: json['messages'] as Map<Object?, Object?>,
      peopleInChat: json['peopleInChat'] as Map<Object?, Object?>,
      listLastMessages: json['listLastMessages'] as Map<Object?, Object?>?,
      lastMessage: json['lastMessage'] as String,
      lastTimeMessage: json['lastTimeMessage'] as String,
    );
  }

  MessageEntity toDomain() {
    return MessageEntity(
      messageId: messageId,
      lastSenderId: lastSenderId,
      users: users,
      messages: messages,
      peopleInChat: peopleInChat,
      listLastMessages: listLastMessages,
      lastMessage: lastMessage,
      lastTimeMessage: lastTimeMessage,
    );
  }

  factory MessageDTO.fromJson(Map<String, dynamic> json) => _$MessageDTOFromJson(json);
}

@freezed
abstract class LastMessageDTO implements _$LastMessageDTO {
  const LastMessageDTO._();

  const factory LastMessageDTO({
    required String sendById,
    required String message,
  }) = _LastMessageDTO;

  factory LastMessageDTO.fromDomain(LastMessageEntity lastMessageEntity) {
    return LastMessageDTO(sendById: lastMessageEntity.sendById, message: lastMessageEntity.message);
  }

  LastMessageEntity toDomain() {
    return LastMessageEntity(sendById: sendById, message: message);
  }

  factory LastMessageDTO.fromJson(Map<String, dynamic> json) => _$LastMessageDTOFromJson(json);
}
