// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'message_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MessageDTO _$MessageDTOFromJson(Map<String, dynamic> json) {
  return _MessageDTO.fromJson(json);
}

/// @nodoc
mixin _$MessageDTO {
  String get messageId => throw _privateConstructorUsedError;
  String get lastSenderId => throw _privateConstructorUsedError;
  List<dynamic> get users => throw _privateConstructorUsedError;
  Map<dynamic, dynamic> get messages => throw _privateConstructorUsedError;
  Map<dynamic, dynamic> get peopleInChat => throw _privateConstructorUsedError;
  Map<dynamic, dynamic>? get listLastMessages =>
      throw _privateConstructorUsedError;
  String get lastMessage => throw _privateConstructorUsedError;
  String get lastTimeMessage => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MessageDTOCopyWith<MessageDTO> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageDTOCopyWith<$Res> {
  factory $MessageDTOCopyWith(
          MessageDTO value, $Res Function(MessageDTO) then) =
      _$MessageDTOCopyWithImpl<$Res, MessageDTO>;
  @useResult
  $Res call(
      {String messageId,
      String lastSenderId,
      List<dynamic> users,
      Map<dynamic, dynamic> messages,
      Map<dynamic, dynamic> peopleInChat,
      Map<dynamic, dynamic>? listLastMessages,
      String lastMessage,
      String lastTimeMessage});
}

/// @nodoc
class _$MessageDTOCopyWithImpl<$Res, $Val extends MessageDTO>
    implements $MessageDTOCopyWith<$Res> {
  _$MessageDTOCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? messageId = null,
    Object? lastSenderId = null,
    Object? users = null,
    Object? messages = null,
    Object? peopleInChat = null,
    Object? listLastMessages = freezed,
    Object? lastMessage = null,
    Object? lastTimeMessage = null,
  }) {
    return _then(_value.copyWith(
      messageId: null == messageId
          ? _value.messageId
          : messageId // ignore: cast_nullable_to_non_nullable
              as String,
      lastSenderId: null == lastSenderId
          ? _value.lastSenderId
          : lastSenderId // ignore: cast_nullable_to_non_nullable
              as String,
      users: null == users
          ? _value.users
          : users // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      messages: null == messages
          ? _value.messages
          : messages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      peopleInChat: null == peopleInChat
          ? _value.peopleInChat
          : peopleInChat // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      listLastMessages: freezed == listLastMessages
          ? _value.listLastMessages
          : listLastMessages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      lastMessage: null == lastMessage
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String,
      lastTimeMessage: null == lastTimeMessage
          ? _value.lastTimeMessage
          : lastTimeMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MessageDTOCopyWith<$Res>
    implements $MessageDTOCopyWith<$Res> {
  factory _$$_MessageDTOCopyWith(
          _$_MessageDTO value, $Res Function(_$_MessageDTO) then) =
      __$$_MessageDTOCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String messageId,
      String lastSenderId,
      List<dynamic> users,
      Map<dynamic, dynamic> messages,
      Map<dynamic, dynamic> peopleInChat,
      Map<dynamic, dynamic>? listLastMessages,
      String lastMessage,
      String lastTimeMessage});
}

/// @nodoc
class __$$_MessageDTOCopyWithImpl<$Res>
    extends _$MessageDTOCopyWithImpl<$Res, _$_MessageDTO>
    implements _$$_MessageDTOCopyWith<$Res> {
  __$$_MessageDTOCopyWithImpl(
      _$_MessageDTO _value, $Res Function(_$_MessageDTO) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? messageId = null,
    Object? lastSenderId = null,
    Object? users = null,
    Object? messages = null,
    Object? peopleInChat = null,
    Object? listLastMessages = freezed,
    Object? lastMessage = null,
    Object? lastTimeMessage = null,
  }) {
    return _then(_$_MessageDTO(
      messageId: null == messageId
          ? _value.messageId
          : messageId // ignore: cast_nullable_to_non_nullable
              as String,
      lastSenderId: null == lastSenderId
          ? _value.lastSenderId
          : lastSenderId // ignore: cast_nullable_to_non_nullable
              as String,
      users: null == users
          ? _value._users
          : users // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      messages: null == messages
          ? _value._messages
          : messages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      peopleInChat: null == peopleInChat
          ? _value._peopleInChat
          : peopleInChat // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>,
      listLastMessages: freezed == listLastMessages
          ? _value._listLastMessages
          : listLastMessages // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, dynamic>?,
      lastMessage: null == lastMessage
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String,
      lastTimeMessage: null == lastTimeMessage
          ? _value.lastTimeMessage
          : lastTimeMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MessageDTO extends _MessageDTO {
  const _$_MessageDTO(
      {required this.messageId,
      required this.lastSenderId,
      required final List<dynamic> users,
      required final Map<dynamic, dynamic> messages,
      required final Map<dynamic, dynamic> peopleInChat,
      required final Map<dynamic, dynamic>? listLastMessages,
      required this.lastMessage,
      required this.lastTimeMessage})
      : _users = users,
        _messages = messages,
        _peopleInChat = peopleInChat,
        _listLastMessages = listLastMessages,
        super._();

  factory _$_MessageDTO.fromJson(Map<String, dynamic> json) =>
      _$$_MessageDTOFromJson(json);

  @override
  final String messageId;
  @override
  final String lastSenderId;
  final List<dynamic> _users;
  @override
  List<dynamic> get users {
    if (_users is EqualUnmodifiableListView) return _users;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_users);
  }

  final Map<dynamic, dynamic> _messages;
  @override
  Map<dynamic, dynamic> get messages {
    if (_messages is EqualUnmodifiableMapView) return _messages;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_messages);
  }

  final Map<dynamic, dynamic> _peopleInChat;
  @override
  Map<dynamic, dynamic> get peopleInChat {
    if (_peopleInChat is EqualUnmodifiableMapView) return _peopleInChat;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_peopleInChat);
  }

  final Map<dynamic, dynamic>? _listLastMessages;
  @override
  Map<dynamic, dynamic>? get listLastMessages {
    final value = _listLastMessages;
    if (value == null) return null;
    if (_listLastMessages is EqualUnmodifiableMapView) return _listLastMessages;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  final String lastMessage;
  @override
  final String lastTimeMessage;

  @override
  String toString() {
    return 'MessageDTO(messageId: $messageId, lastSenderId: $lastSenderId, users: $users, messages: $messages, peopleInChat: $peopleInChat, listLastMessages: $listLastMessages, lastMessage: $lastMessage, lastTimeMessage: $lastTimeMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MessageDTO &&
            (identical(other.messageId, messageId) ||
                other.messageId == messageId) &&
            (identical(other.lastSenderId, lastSenderId) ||
                other.lastSenderId == lastSenderId) &&
            const DeepCollectionEquality().equals(other._users, _users) &&
            const DeepCollectionEquality().equals(other._messages, _messages) &&
            const DeepCollectionEquality()
                .equals(other._peopleInChat, _peopleInChat) &&
            const DeepCollectionEquality()
                .equals(other._listLastMessages, _listLastMessages) &&
            (identical(other.lastMessage, lastMessage) ||
                other.lastMessage == lastMessage) &&
            (identical(other.lastTimeMessage, lastTimeMessage) ||
                other.lastTimeMessage == lastTimeMessage));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      messageId,
      lastSenderId,
      const DeepCollectionEquality().hash(_users),
      const DeepCollectionEquality().hash(_messages),
      const DeepCollectionEquality().hash(_peopleInChat),
      const DeepCollectionEquality().hash(_listLastMessages),
      lastMessage,
      lastTimeMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MessageDTOCopyWith<_$_MessageDTO> get copyWith =>
      __$$_MessageDTOCopyWithImpl<_$_MessageDTO>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MessageDTOToJson(
      this,
    );
  }
}

abstract class _MessageDTO extends MessageDTO {
  const factory _MessageDTO(
      {required final String messageId,
      required final String lastSenderId,
      required final List<dynamic> users,
      required final Map<dynamic, dynamic> messages,
      required final Map<dynamic, dynamic> peopleInChat,
      required final Map<dynamic, dynamic>? listLastMessages,
      required final String lastMessage,
      required final String lastTimeMessage}) = _$_MessageDTO;
  const _MessageDTO._() : super._();

  factory _MessageDTO.fromJson(Map<String, dynamic> json) =
      _$_MessageDTO.fromJson;

  @override
  String get messageId;
  @override
  String get lastSenderId;
  @override
  List<dynamic> get users;
  @override
  Map<dynamic, dynamic> get messages;
  @override
  Map<dynamic, dynamic> get peopleInChat;
  @override
  Map<dynamic, dynamic>? get listLastMessages;
  @override
  String get lastMessage;
  @override
  String get lastTimeMessage;
  @override
  @JsonKey(ignore: true)
  _$$_MessageDTOCopyWith<_$_MessageDTO> get copyWith =>
      throw _privateConstructorUsedError;
}

LastMessageDTO _$LastMessageDTOFromJson(Map<String, dynamic> json) {
  return _LastMessageDTO.fromJson(json);
}

/// @nodoc
mixin _$LastMessageDTO {
  String get sendById => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LastMessageDTOCopyWith<LastMessageDTO> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LastMessageDTOCopyWith<$Res> {
  factory $LastMessageDTOCopyWith(
          LastMessageDTO value, $Res Function(LastMessageDTO) then) =
      _$LastMessageDTOCopyWithImpl<$Res, LastMessageDTO>;
  @useResult
  $Res call({String sendById, String message});
}

/// @nodoc
class _$LastMessageDTOCopyWithImpl<$Res, $Val extends LastMessageDTO>
    implements $LastMessageDTOCopyWith<$Res> {
  _$LastMessageDTOCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sendById = null,
    Object? message = null,
  }) {
    return _then(_value.copyWith(
      sendById: null == sendById
          ? _value.sendById
          : sendById // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LastMessageDTOCopyWith<$Res>
    implements $LastMessageDTOCopyWith<$Res> {
  factory _$$_LastMessageDTOCopyWith(
          _$_LastMessageDTO value, $Res Function(_$_LastMessageDTO) then) =
      __$$_LastMessageDTOCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String sendById, String message});
}

/// @nodoc
class __$$_LastMessageDTOCopyWithImpl<$Res>
    extends _$LastMessageDTOCopyWithImpl<$Res, _$_LastMessageDTO>
    implements _$$_LastMessageDTOCopyWith<$Res> {
  __$$_LastMessageDTOCopyWithImpl(
      _$_LastMessageDTO _value, $Res Function(_$_LastMessageDTO) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sendById = null,
    Object? message = null,
  }) {
    return _then(_$_LastMessageDTO(
      sendById: null == sendById
          ? _value.sendById
          : sendById // ignore: cast_nullable_to_non_nullable
              as String,
      message: null == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LastMessageDTO extends _LastMessageDTO {
  const _$_LastMessageDTO({required this.sendById, required this.message})
      : super._();

  factory _$_LastMessageDTO.fromJson(Map<String, dynamic> json) =>
      _$$_LastMessageDTOFromJson(json);

  @override
  final String sendById;
  @override
  final String message;

  @override
  String toString() {
    return 'LastMessageDTO(sendById: $sendById, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LastMessageDTO &&
            (identical(other.sendById, sendById) ||
                other.sendById == sendById) &&
            (identical(other.message, message) || other.message == message));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, sendById, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LastMessageDTOCopyWith<_$_LastMessageDTO> get copyWith =>
      __$$_LastMessageDTOCopyWithImpl<_$_LastMessageDTO>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LastMessageDTOToJson(
      this,
    );
  }
}

abstract class _LastMessageDTO extends LastMessageDTO {
  const factory _LastMessageDTO(
      {required final String sendById,
      required final String message}) = _$_LastMessageDTO;
  const _LastMessageDTO._() : super._();

  factory _LastMessageDTO.fromJson(Map<String, dynamic> json) =
      _$_LastMessageDTO.fromJson;

  @override
  String get sendById;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$_LastMessageDTOCopyWith<_$_LastMessageDTO> get copyWith =>
      throw _privateConstructorUsedError;
}
