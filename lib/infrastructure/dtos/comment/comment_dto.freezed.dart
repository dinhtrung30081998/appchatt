// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'comment_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CommentDTO _$CommentDTOFromJson(Map<String, dynamic> json) {
  return _CommentDTO.fromJson(json);
}

/// @nodoc
mixin _$CommentDTO {
  String get profilePicture => throw _privateConstructorUsedError;
  String get uid => throw _privateConstructorUsedError;
  String get fullName => throw _privateConstructorUsedError;
  String get comment => throw _privateConstructorUsedError;
  String get commentId => throw _privateConstructorUsedError;
  String get datePublished => throw _privateConstructorUsedError;
  List<dynamic> get likes => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CommentDTOCopyWith<CommentDTO> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CommentDTOCopyWith<$Res> {
  factory $CommentDTOCopyWith(
          CommentDTO value, $Res Function(CommentDTO) then) =
      _$CommentDTOCopyWithImpl<$Res, CommentDTO>;
  @useResult
  $Res call(
      {String profilePicture,
      String uid,
      String fullName,
      String comment,
      String commentId,
      String datePublished,
      List<dynamic> likes});
}

/// @nodoc
class _$CommentDTOCopyWithImpl<$Res, $Val extends CommentDTO>
    implements $CommentDTOCopyWith<$Res> {
  _$CommentDTOCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? profilePicture = null,
    Object? uid = null,
    Object? fullName = null,
    Object? comment = null,
    Object? commentId = null,
    Object? datePublished = null,
    Object? likes = null,
  }) {
    return _then(_value.copyWith(
      profilePicture: null == profilePicture
          ? _value.profilePicture
          : profilePicture // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      comment: null == comment
          ? _value.comment
          : comment // ignore: cast_nullable_to_non_nullable
              as String,
      commentId: null == commentId
          ? _value.commentId
          : commentId // ignore: cast_nullable_to_non_nullable
              as String,
      datePublished: null == datePublished
          ? _value.datePublished
          : datePublished // ignore: cast_nullable_to_non_nullable
              as String,
      likes: null == likes
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CommentDTOCopyWith<$Res>
    implements $CommentDTOCopyWith<$Res> {
  factory _$$_CommentDTOCopyWith(
          _$_CommentDTO value, $Res Function(_$_CommentDTO) then) =
      __$$_CommentDTOCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String profilePicture,
      String uid,
      String fullName,
      String comment,
      String commentId,
      String datePublished,
      List<dynamic> likes});
}

/// @nodoc
class __$$_CommentDTOCopyWithImpl<$Res>
    extends _$CommentDTOCopyWithImpl<$Res, _$_CommentDTO>
    implements _$$_CommentDTOCopyWith<$Res> {
  __$$_CommentDTOCopyWithImpl(
      _$_CommentDTO _value, $Res Function(_$_CommentDTO) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? profilePicture = null,
    Object? uid = null,
    Object? fullName = null,
    Object? comment = null,
    Object? commentId = null,
    Object? datePublished = null,
    Object? likes = null,
  }) {
    return _then(_$_CommentDTO(
      profilePicture: null == profilePicture
          ? _value.profilePicture
          : profilePicture // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      fullName: null == fullName
          ? _value.fullName
          : fullName // ignore: cast_nullable_to_non_nullable
              as String,
      comment: null == comment
          ? _value.comment
          : comment // ignore: cast_nullable_to_non_nullable
              as String,
      commentId: null == commentId
          ? _value.commentId
          : commentId // ignore: cast_nullable_to_non_nullable
              as String,
      datePublished: null == datePublished
          ? _value.datePublished
          : datePublished // ignore: cast_nullable_to_non_nullable
              as String,
      likes: null == likes
          ? _value._likes
          : likes // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CommentDTO extends _CommentDTO {
  const _$_CommentDTO(
      {required this.profilePicture,
      required this.uid,
      required this.fullName,
      required this.comment,
      required this.commentId,
      required this.datePublished,
      required final List<dynamic> likes})
      : _likes = likes,
        super._();

  factory _$_CommentDTO.fromJson(Map<String, dynamic> json) =>
      _$$_CommentDTOFromJson(json);

  @override
  final String profilePicture;
  @override
  final String uid;
  @override
  final String fullName;
  @override
  final String comment;
  @override
  final String commentId;
  @override
  final String datePublished;
  final List<dynamic> _likes;
  @override
  List<dynamic> get likes {
    if (_likes is EqualUnmodifiableListView) return _likes;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_likes);
  }

  @override
  String toString() {
    return 'CommentDTO(profilePicture: $profilePicture, uid: $uid, fullName: $fullName, comment: $comment, commentId: $commentId, datePublished: $datePublished, likes: $likes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CommentDTO &&
            (identical(other.profilePicture, profilePicture) ||
                other.profilePicture == profilePicture) &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.fullName, fullName) ||
                other.fullName == fullName) &&
            (identical(other.comment, comment) || other.comment == comment) &&
            (identical(other.commentId, commentId) ||
                other.commentId == commentId) &&
            (identical(other.datePublished, datePublished) ||
                other.datePublished == datePublished) &&
            const DeepCollectionEquality().equals(other._likes, _likes));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      profilePicture,
      uid,
      fullName,
      comment,
      commentId,
      datePublished,
      const DeepCollectionEquality().hash(_likes));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CommentDTOCopyWith<_$_CommentDTO> get copyWith =>
      __$$_CommentDTOCopyWithImpl<_$_CommentDTO>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CommentDTOToJson(
      this,
    );
  }
}

abstract class _CommentDTO extends CommentDTO {
  const factory _CommentDTO(
      {required final String profilePicture,
      required final String uid,
      required final String fullName,
      required final String comment,
      required final String commentId,
      required final String datePublished,
      required final List<dynamic> likes}) = _$_CommentDTO;
  const _CommentDTO._() : super._();

  factory _CommentDTO.fromJson(Map<String, dynamic> json) =
      _$_CommentDTO.fromJson;

  @override
  String get profilePicture;
  @override
  String get uid;
  @override
  String get fullName;
  @override
  String get comment;
  @override
  String get commentId;
  @override
  String get datePublished;
  @override
  List<dynamic> get likes;
  @override
  @JsonKey(ignore: true)
  _$$_CommentDTOCopyWith<_$_CommentDTO> get copyWith =>
      throw _privateConstructorUsedError;
}
