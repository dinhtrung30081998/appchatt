// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CommentDTO _$$_CommentDTOFromJson(Map<String, dynamic> json) =>
    _$_CommentDTO(
      profilePicture: json['profilePicture'] as String,
      uid: json['uid'] as String,
      fullName: json['fullName'] as String,
      comment: json['comment'] as String,
      commentId: json['commentId'] as String,
      datePublished: json['datePublished'] as String,
      likes: json['likes'] as List<dynamic>,
    );

Map<String, dynamic> _$$_CommentDTOToJson(_$_CommentDTO instance) =>
    <String, dynamic>{
      'profilePicture': instance.profilePicture,
      'uid': instance.uid,
      'fullName': instance.fullName,
      'comment': instance.comment,
      'commentId': instance.commentId,
      'datePublished': instance.datePublished,
      'likes': instance.likes,
    };
