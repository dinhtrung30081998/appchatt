import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/comment/comment_entity.dart';

part 'comment_dto.freezed.dart';
part 'comment_dto.g.dart';

@freezed
abstract class CommentDTO implements _$CommentDTO {
  const CommentDTO._();

  const factory CommentDTO({
    required String profilePicture,
    required String uid,
    required String fullName,
    required String comment,
    required String commentId,
    required String datePublished,
    required List likes,
  }) = _CommentDTO;

  factory CommentDTO.fromDomain(CommentEntity commentEntity) {
    return CommentDTO(
      profilePicture: commentEntity.profilePicture,
      uid: commentEntity.uid,
      fullName: commentEntity.fullName,
      comment: commentEntity.comment,
      commentId: commentEntity.commentId,
      datePublished: commentEntity.datePublished,
      likes: commentEntity.likes,
    );
  }

  CommentEntity toDomain() {
    return CommentEntity(
      profilePicture: profilePicture,
      uid: uid,
      fullName: fullName,
      comment: comment,
      commentId: commentId,
      datePublished: datePublished,
      likes: likes,
    );
  }

  factory CommentDTO.fromJson(Map<String, dynamic> json) => _$CommentDTOFromJson(json);
}
