// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChatDTO _$$_ChatDTOFromJson(Map<String, dynamic> json) => _$_ChatDTO(
      sendById: json['sendById'] as String,
      type: json['type'] as String,
      sendingTime: DateTime.parse(json['sendingTime'] as String),
      text: json['text'] as String,
    );

Map<String, dynamic> _$$_ChatDTOToJson(_$_ChatDTO instance) =>
    <String, dynamic>{
      'sendById': instance.sendById,
      'type': instance.type,
      'sendingTime': instance.sendingTime.toIso8601String(),
      'text': instance.text,
    };
