// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'chat_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChatDTO _$ChatDTOFromJson(Map<String, dynamic> json) {
  return _ChatDTO.fromJson(json);
}

/// @nodoc
mixin _$ChatDTO {
  String get sendById => throw _privateConstructorUsedError;
  String get type => throw _privateConstructorUsedError;
  DateTime get sendingTime => throw _privateConstructorUsedError;
  String get text => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatDTOCopyWith<ChatDTO> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatDTOCopyWith<$Res> {
  factory $ChatDTOCopyWith(ChatDTO value, $Res Function(ChatDTO) then) =
      _$ChatDTOCopyWithImpl<$Res, ChatDTO>;
  @useResult
  $Res call({String sendById, String type, DateTime sendingTime, String text});
}

/// @nodoc
class _$ChatDTOCopyWithImpl<$Res, $Val extends ChatDTO>
    implements $ChatDTOCopyWith<$Res> {
  _$ChatDTOCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sendById = null,
    Object? type = null,
    Object? sendingTime = null,
    Object? text = null,
  }) {
    return _then(_value.copyWith(
      sendById: null == sendById
          ? _value.sendById
          : sendById // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      sendingTime: null == sendingTime
          ? _value.sendingTime
          : sendingTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChatDTOCopyWith<$Res> implements $ChatDTOCopyWith<$Res> {
  factory _$$_ChatDTOCopyWith(
          _$_ChatDTO value, $Res Function(_$_ChatDTO) then) =
      __$$_ChatDTOCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String sendById, String type, DateTime sendingTime, String text});
}

/// @nodoc
class __$$_ChatDTOCopyWithImpl<$Res>
    extends _$ChatDTOCopyWithImpl<$Res, _$_ChatDTO>
    implements _$$_ChatDTOCopyWith<$Res> {
  __$$_ChatDTOCopyWithImpl(_$_ChatDTO _value, $Res Function(_$_ChatDTO) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sendById = null,
    Object? type = null,
    Object? sendingTime = null,
    Object? text = null,
  }) {
    return _then(_$_ChatDTO(
      sendById: null == sendById
          ? _value.sendById
          : sendById // ignore: cast_nullable_to_non_nullable
              as String,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      sendingTime: null == sendingTime
          ? _value.sendingTime
          : sendingTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatDTO extends _ChatDTO {
  const _$_ChatDTO(
      {required this.sendById,
      required this.type,
      required this.sendingTime,
      required this.text})
      : super._();

  factory _$_ChatDTO.fromJson(Map<String, dynamic> json) =>
      _$$_ChatDTOFromJson(json);

  @override
  final String sendById;
  @override
  final String type;
  @override
  final DateTime sendingTime;
  @override
  final String text;

  @override
  String toString() {
    return 'ChatDTO(sendById: $sendById, type: $type, sendingTime: $sendingTime, text: $text)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatDTO &&
            (identical(other.sendById, sendById) ||
                other.sendById == sendById) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.sendingTime, sendingTime) ||
                other.sendingTime == sendingTime) &&
            (identical(other.text, text) || other.text == text));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, sendById, type, sendingTime, text);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatDTOCopyWith<_$_ChatDTO> get copyWith =>
      __$$_ChatDTOCopyWithImpl<_$_ChatDTO>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatDTOToJson(
      this,
    );
  }
}

abstract class _ChatDTO extends ChatDTO {
  const factory _ChatDTO(
      {required final String sendById,
      required final String type,
      required final DateTime sendingTime,
      required final String text}) = _$_ChatDTO;
  const _ChatDTO._() : super._();

  factory _ChatDTO.fromJson(Map<String, dynamic> json) = _$_ChatDTO.fromJson;

  @override
  String get sendById;
  @override
  String get type;
  @override
  DateTime get sendingTime;
  @override
  String get text;
  @override
  @JsonKey(ignore: true)
  _$$_ChatDTOCopyWith<_$_ChatDTO> get copyWith =>
      throw _privateConstructorUsedError;
}
