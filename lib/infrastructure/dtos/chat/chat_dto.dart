import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/chat/chat_entity.dart';

part 'chat_dto.freezed.dart';
part 'chat_dto.g.dart';

@freezed
abstract class ChatDTO implements _$ChatDTO {
  const ChatDTO._();

  const factory ChatDTO({
    required String sendById,
    required String type,
    required DateTime sendingTime,
    required String text,
  }) = _ChatDTO;

  factory ChatDTO.fromDomain(ChatEntity chatEntity) {
    return ChatDTO(
      sendById: chatEntity.sendById,
      type: chatEntity.type,
      sendingTime: chatEntity.sendingTime,
      text: chatEntity.text,
    );
  }

  factory ChatDTO.fromRTDB(Map<String, dynamic> json) {
    return ChatDTO(
      sendById: json['sendById'] as String,
      type: json['type'] as String,
      sendingTime: json['sendingTime'] as DateTime,
      text: json['text'] as String,
    );
  }

  ChatEntity toDomain() {
    return ChatEntity(
      sendById: sendById,
      type: type,
      sendingTime: sendingTime,
      text: text,
    );
  }

  factory ChatDTO.fromJson(Map<String, dynamic> json) => _$ChatDTOFromJson(json);
}
