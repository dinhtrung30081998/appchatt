import 'package:app_chat/domain/value_objects/auth/email_address.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/entities/user/user_entity.dart';
import '../../dtos/user/user_dto.dart';

abstract class UserInforRemote {
  Future<UserEntity> getUserDetails(EmailAddress emailAddress);
}

@lazySingleton
@Injectable(as: UserInforRemote)
class UserInforRemoteImpl implements UserInforRemote {
  final FirebaseFirestore firebaseFirestore;
  final FirebaseAuth firebaseAuth;

  UserInforRemoteImpl(this.firebaseAuth, {required this.firebaseFirestore});

  @override
  Future<UserEntity> getUserDetails(EmailAddress emailAddress) async {
    final emailStr = emailAddress.getValueOrCrash();
    final snapshot = await firebaseFirestore.collection('users').where('email', isEqualTo: emailStr).get();
    final users = snapshot.docs.map((user) => UserDTO.fromJson(user.data()).toDomain()).single;
    return users;
  }
}
