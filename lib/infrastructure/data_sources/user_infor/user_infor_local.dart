import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserInforLocal {
  Future<bool> cachedUserId(String uid);
  Future<bool> cachedEmail(String email);
  Future<bool> cachedFullName(String fullName);
  Future<bool> cachedBio(String bio);
  Future<bool> cachedPhoneNumber(String phoneNumber);
  Future<bool> cachedProfilePicture(String profilePicture);
  Future<bool> cachedFollowing(List<String> following);
  Future<bool> cachedToken(String token);

  String? getCacheEmail();
  String? getCacheUid();
  String? getCacheFullName();
  String? getCachePhoneNumber();
  String? getCacheProfilePicture();
  String? getCacheToken();
  List<String>? getFollowing();

  bool isCachedUserId();
  Future<bool> isCachedEmail();
  bool isCachedFullName();
  bool isCachedBio();
  Future<bool> isCachedPhoneNumber();
  Future<bool> isCachedProfilePicture();
}

const cachedUserIdKey = "CACHED_USER_ID";
const cachedUserEmailKey = "CACHED_USER_EMAIL";
const cachedFullNameKey = "CACHED_USER_FULLNAME";
const cachedBioKey = "CACHED_USER_BIO";
const cachedPhoneNumberKey = "CACHED_USER_PHONENUMBER";
const cachedProfilePictureKey = "CACHED_USER_PROFILE_PICTURE";
const cachedFollowersKey = "CACHED_USER_FOLLOWERS";
const cachedFollowingKey = "CACHED_USER_FOLLOWING";
const cachedTokenKey = "CACHED_USER_TOKEN";

@lazySingleton
@Injectable(as: UserInforLocal)
class UserInforLocalImpl implements UserInforLocal {
  final SharedPreferences sharedPreferences;

  UserInforLocalImpl({required this.sharedPreferences});

  @override
  Future<bool> cachedUserId(String uid) async {
    return await sharedPreferences.setString(cachedUserIdKey, uid);
  }

  @override
  Future<bool> cachedEmail(String email) async {
    return await sharedPreferences.setString(cachedUserEmailKey, email);
  }

  @override
  Future<bool> cachedFullName(String fullName) async {
    return await sharedPreferences.setString(cachedFullNameKey, fullName);
  }

  @override
  Future<bool> cachedBio(String bio) async {
    return await sharedPreferences.setString(cachedBioKey, bio);
  }

  @override
  Future<bool> cachedPhoneNumber(String phoneNumber) async {
    return await sharedPreferences.setString(cachedPhoneNumberKey, phoneNumber);
  }

  @override
  Future<bool> cachedProfilePicture(String profilePicture) async {
    return await sharedPreferences.setString(cachedProfilePictureKey, profilePicture);
  }

  @override
  Future<bool> cachedToken(String token) async {
    return await sharedPreferences.setString(cachedTokenKey, token);
  }

  @override
  Future<bool> cachedFollowing(List<String> following) async {
    return await sharedPreferences.setStringList(cachedFollowingKey, following);
  }

  @override
  String? getCacheUid() {
    return sharedPreferences.getString(cachedUserIdKey);
  }

  @override
  String? getCacheEmail() {
    return sharedPreferences.getString(cachedUserEmailKey);
  }

  @override
  String? getCacheFullName() {
    return sharedPreferences.getString(cachedFullNameKey);
  }

  @override
  String? getCachePhoneNumber() {
    return sharedPreferences.getString(cachedPhoneNumberKey);
  }

  @override
  String? getCacheProfilePicture() {
    return sharedPreferences.getString(cachedProfilePictureKey);
  }

  @override
  String? getCacheToken() {
    return sharedPreferences.getString(cachedTokenKey);
  }

  @override
  bool isCachedUserId() {
    final jsonToString = sharedPreferences.getString(cachedUserIdKey);
    if (jsonToString!.isNotEmpty) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> isCachedEmail() {
    final jsonToString = sharedPreferences.getString(cachedUserEmailKey);
    if (jsonToString != null) {
      return Future.value(true);
    } else {
      return Future.value(false);
    }
  }

  @override
  bool isCachedFullName() {
    final jsonToString = sharedPreferences.getString(cachedFullNameKey);
    if (jsonToString!.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  bool isCachedBio() {
    final jsonToString = sharedPreferences.getString(cachedBioKey);
    if (jsonToString!.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> isCachedPhoneNumber() {
    final jsonToString = sharedPreferences.getString(cachedPhoneNumberKey);
    if (jsonToString != null) {
      return Future.value(true);
    } else {
      return Future.value(false);
    }
  }

  @override
  Future<bool> isCachedProfilePicture() {
    final jsonToString = sharedPreferences.getString(cachedProfilePictureKey);
    if (jsonToString != null) {
      return Future.value(true);
    } else {
      return Future.value(false);
    }
  }

  @override
  List<String>? getFollowing() {
    return sharedPreferences.getStringList(cachedFollowingKey);
  }
}
