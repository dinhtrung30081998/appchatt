import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../domain/core/failures/search_friends/search_friends_failure.dart';
import '../../../domain/entities/user/user_entity.dart';
import '../../dtos/user/user_dto.dart';

abstract class SearchFriendsLocal {
  Future<List<UserEntity>> getCachedSearchFriends();
  Future<Unit> cachedSearchFriends(List<UserEntity> searchFriends);
}

const cachedSearchFriendsKey = "CACHED_SEARCH_FRIENDS";

@lazySingleton
@Injectable(as: SearchFriendsLocal)
class SearchFriendsLocalImpl implements SearchFriendsLocal {
  final SharedPreferences sharedPreferences;

  SearchFriendsLocalImpl(this.sharedPreferences);

  @override
  Future<List<UserEntity>> getCachedSearchFriends() {
    final jsonToString = sharedPreferences.getString(cachedSearchFriendsKey);
    if (jsonToString != null) {
      List decodeJsonData = json.decode(jsonToString);
      List<UserEntity> jsonToSearchFriends =
          decodeJsonData.map<UserEntity>((users) => UserDTO.fromJson(users).toDomain()).toList();
      return Future.value(jsonToSearchFriends);
    } else {
      throw const EmptyCacheFriends();
    }
  }

  @override
  Future<Unit> cachedSearchFriends(List<UserEntity> searchFriends) {
    List searchFriendsJson =
        searchFriends.map<Map<String, dynamic>>((users) => UserDTO.fromDomain(users).toJson()).toList();
    sharedPreferences.setString(cachedSearchFriendsKey, json.encode(searchFriendsJson));
    return Future.value(unit);
  }
}
