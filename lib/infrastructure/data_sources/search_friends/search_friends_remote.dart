import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/entities/user/user_entity.dart';
import '../../dtos/user/user_dto.dart';

abstract class SearchFriendsRemote {
  Future<List<UserEntity>> searchFriendsRemote(String query);
}

@lazySingleton
@Injectable(as: SearchFriendsRemote)
class SearchFriendsRemoteImpl implements SearchFriendsRemote {
  final FirebaseFirestore firebaseFirestore;

  SearchFriendsRemoteImpl(this.firebaseFirestore);

  @override
  Future<List<UserEntity>> searchFriendsRemote(String query) async {
    List<UserEntity> searchFriends = [];
    final listDoc = await firebaseFirestore.collection('users').where('fullName', isEqualTo: query).get();

    for (var item in listDoc.docs) {
      searchFriends.add(UserDTO.fromJson(item.data()).toDomain());
    }

    return searchFriends;
  }
}
