import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:injectable/injectable.dart';
import 'infrastructure/core/di/infrastructure_injection.dart' as di;
import 'root_widget.dart';

void main() async {
  WidgetsBinding binding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: binding);

  await di.configureDependencies(Environment.prod);
  if (kIsWeb) {
    await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyDDU0PA-1IPrFWp1-I8Exdz-KgLKlWHMEM",
          authDomain: "chatapp-3203e.firebaseapp.com",
          databaseURL: "https://chatapp-3203e-default-rtdb.firebaseio.com",
          projectId: "chatapp-3203e",
          storageBucket: "chatapp-3203e.appspot.com",
          messagingSenderId: "36131479916",
          appId: "1:36131479916:web:7d0e0343598598e88fd349",
          measurementId: "G-JND2H4XERV"),
    );
  } else {
    await Firebase.initializeApp();
  }

  runApp(RootWidget());
}
